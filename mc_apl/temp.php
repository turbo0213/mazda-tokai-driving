<?php

require_once(SMARTY_DIR . 'Smarty.class.php');

//templateクラス

Class smTemplate{
	
	var $smarty;

	function smTemplate(){

    	$this->smarty = new Smarty;

	    $params =  array(
	        'caching'        => SMARTY_CACHING,
	        'cache_lifetime' => SMARTY_CACHE_LIFETIME,
	        'cache_dir' => SMARTY_CACHE_DIR,
	        'force_compile'  => SMARTY_FORCE_COMPILE,
	        'compile_dir'    => SMARTY_COMPILE_DIR,
	        'debug_tpl'      => SMARTY_DEBUG_TPL,
	        'debugging_ctrl' => SMARTY_DEBUGGING_CTRL,
	        'debugging'      => SMARTY_DEBUGGING,
	        'template_dir'       => COMMON_TEMPLATE_DIR);
    	foreach ( $params AS $key => $value ) $this->smarty->$key = $value;

		$this->common_assign();
	}
	
	function common_assign(){
        if(strpos($_SERVER["REQUEST_URI"],"pass.php") === false and strpos($_SERVER["REQUEST_URI"],"index.php") === false) {
            if ($_SESSION['oneday']['pw_chg_flg'] == 1) {
                header('location: pass.php');
            }
        }
		$this->smarty->assign('no_result_flg',$_SESSION['oneday']['no_result_flg']);
        $this->smarty->assign("flyer",$_SESSION['flyer']);
        $this->smarty->assign("ybp",$_SESSION['param']);
        $this->smarty->assign("p",$_SESSION['param2']);
        $this->smarty->assign("vcp76",$_SESSION['param3']);
	}

	function error_assign($req){
		
		//エラー処理
 		if ($req->hasErrors()) {
			$errors =& $req->getErrors();
	   		$this->smarty->assign('error_flg', 1);
	   		$this->smarty->assign('errors', $errors);
		}

	}

}

?>