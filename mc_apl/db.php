<?php

//include_once(ADODB_DIR . 'adodb-exceptions.inc.php');
require_once(ADODB_DIR . 'adodb.inc.php');
include_once(ADODB_DIR . 'rsfilter.inc.php');

//
function rsPreprocessing(&$arr,$rs){
	foreach($arr as $k => $v) {
		$arr[$k] = str_replace("\\\\","\\",trim($v));
	}
}

class ASDB
{
	var $con;
	var $error_msg;

    function ASDB()
    {
        //ADOdbの設定
        $GLOBALS["ADODB_CACHE_DIR"] = ADODB_CACHE_DIR;
        $GLOBALS["ADODB_FORCE_TYPE"] = ADODB_FORCE_VALUE;

		$this->con = ADONewConnection(ADODB_DSN);

        $this->con->debug = ADODB_DEBUG;    //デバッグ設定
        $this->con->cacheSecs = ADODB_CACHE_SECS; //キャッシュ有効時間
        
        $this->error_msg = "";
    }

    //コネクションを返す
    function & getCon(){

    	return $this->con;
	
    }

    //要素をクオート
    function getQStr($data){
    	return pg_escape_string($data);
    }

    //レコードセット全ての要素をアンクオートし配列を返す
    function & ASGetAll($sql){

    	$data_array = array();

	    $rs =& $this->con->Execute($sql);

    	if(!$rs){
    		return $data_array();
    	}
    	//データベースより取得したデータの前処理を行う
    	$rs = RSFilter($rs,'rsPreprocessing');
		//取得した全データを配列に格納
    	$data_array =& $rs->GetArray();

		//配列を返す
    	return $data_array;
    }
    
    //レコードセット全ての要素をアンクオートしレコードセットを返す
    function & ASExecute($sql){

    	$data_array = array();

	    $rs =& $this->con->Execute($sql);

    	if(!$rs){
    		return $data_array;
    	}

    	//フィルタリング
    	$rs = RSFilter($rs,'rsPreprocessing');

		//レコードセットを返す
    	return $rs;
    }

    //1行取得
    function & ASGetRow($sql){

	    $rs =& $this->con->Execute($sql);

    	if(!$rs){
	    	return false;
    	}
    	if($rs->EOF){
	    	return false;
    	}

    	return $rs;
    }

    function hasErrors(){

		if($this->error_msg){
			return true;
		}
		
		return false;
    }
    
    function getMessage(){
    	
    	$ret = $this->error_msg;
    	$this->error_msg = "";

    	return $ret;	
    }

}

class ASDB_MAIN
{
	var $con;
	var $error_msg;

    function ASDB_MAIN()
    {
		//ADOdbの設定
		$GLOBALS["ADODB_CACHE_DIR"] = ADODB_CACHE_DIR;
		$GLOBALS["ADODB_FORCE_TYPE"] = ADODB_FORCE_VALUE;

		$this->con = ADONewConnection(ADODB_DSN2);

		$this->con->debug = ADODB_DEBUG;    //デバッグ設定
		$this->con->cacheSecs = ADODB_CACHE_SECS; //キャッシュ有効時間

		$this->error_msg = "";
    }

    //コネクションを返す
    function & getCon(){
    	return $this->con;
    }

    //要素をクオート
    function getQStr($data){
//    	return mysql_real_escape_string($data);
		return pg_escape_string($data);
    }

    //レコードセット全ての要素をアンクオートし配列を返す
    function & ASGetAll($sql){
    	$data_array = array();
	    $rs =& $this->con->Execute($sql);
    	if(!$rs){
    		return $data_array();
    	}
    	//データベースより取得したデータの前処理を行う
    	$rs = RSFilter($rs,'rsPreprocessing');
		//取得した全データを配列に格納
    	$data_array =& $rs->GetArray();
		//配列を返す
    	return $data_array;
    }
    
    //レコードセット全ての要素をアンクオートしレコードセットを返す
    function & ASExecute($sql){
    	$data_array = array();
	    $rs =& $this->con->Execute($sql);
    	if(!$rs){
    		return $data_array;
    	}
    	//フィルタリング
    	$rs = RSFilter($rs,'rsPreprocessing');
		//レコードセットを返す
    	return $rs;
    }

    //1行取得
    function & ASGetRow($sql){
	    $rs =& $this->con->Execute($sql);
    	if(!$rs){
	    	return false;
    	}
    	if($rs->EOF){
	    	return false;
    	}
    	return $rs;
    }

    function hasErrors(){
		if($this->error_msg){
			return true;
		}
		return false;
    }
    
    function getMessage(){
    	$ret = $this->error_msg;
    	$this->error_msg = "";
    	return $ret;
    }
}

class ASDB_SERVICE
{
	var $con;
	var $error_msg;

	function ASDB_SERVICE()
	{
		//ADOdbの設定
		$GLOBALS["ADODB_CACHE_DIR"] = ADODB_CACHE_DIR;
		$GLOBALS["ADODB_FORCE_TYPE"] = ADODB_FORCE_VALUE;

		$this->con = ADONewConnection(ADODB_DSN3);

		$this->con->debug = ADODB_DEBUG;    //デバッグ設定
		$this->con->cacheSecs = ADODB_CACHE_SECS; //キャッシュ有効時間

		$this->error_msg = "";
	}

	//コネクションを返す
	function & getCon(){
		return $this->con;
	}

	//要素をクオート
	function getQStr($data){
//    	return mysql_real_escape_string($data);
		return pg_escape_string($data);
	}

	//レコードセット全ての要素をアンクオートし配列を返す
	function & ASGetAll($sql){
		$data_array = array();
		$rs =& $this->con->Execute($sql);
		if(!$rs){
			return $data_array();
		}
		//データベースより取得したデータの前処理を行う
		$rs = RSFilter($rs,'rsPreprocessing');
		//取得した全データを配列に格納
		$data_array =& $rs->GetArray();
		//配列を返す
		return $data_array;
	}

	//レコードセット全ての要素をアンクオートしレコードセットを返す
	function & ASExecute($sql){
		$data_array = array();
		$rs =& $this->con->Execute($sql);
		if(!$rs){
			return $data_array;
		}
		//フィルタリング
		$rs = RSFilter($rs,'rsPreprocessing');
		//レコードセットを返す
		return $rs;
	}

	//1行取得
	function & ASGetRow($sql){
		$rs =& $this->con->Execute($sql);
		if(!$rs){
			return false;
		}
		if($rs->EOF){
			return false;
		}
		return $rs;
	}

	function hasErrors(){
		if($this->error_msg){
			return true;
		}
		return false;
	}

	function getMessage(){
		$ret = $this->error_msg;
		$this->error_msg = "";
		return $ret;
	}
}
?>
