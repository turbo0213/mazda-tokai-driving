<?php

Class util{

	function sec_encrypt($arg){
		$encrypted_txt=mcrypt_cbc(MCRYPT_RIJNDAEL_256, ENC_KEY, $arg, MCRYPT_ENCRYPT, base64_decode(CRYPT_IV_ENCODE));
		$encrypted_txt=base64_encode($encrypted_txt);
		return $encrypted_txt;
	}

	function sec_decrypt($arg){
		$arg=base64_decode($arg);
		$decrypted_txt=mcrypt_cbc(MCRYPT_RIJNDAEL_256, ENC_KEY, $arg, MCRYPT_DECRYPT, base64_decode(CRYPT_IV_ENCODE));
		return $decrypted_txt;
	}
	
	function searchAddr($file_name,$yuubin1, $yuubin2) {
		$pref = "";
		$addr = "";
		$yuubin = $yuubin1 . $yuubin2;
		if ($file = fopen($file_name, "r")) {
			while ($line = fgets($file)) {
				list($d1, $old, $new, $d2, $d3, $d4, $pref, $addr1, $addr2, $d5) = split(",", $line, 10);
				$pref = str_replace("\"", "", $pref);
				$new = str_replace("\"", "", $new);
				if ($yuubin2 == "0000") {
					$addr3 = str_replace("\"", "", $addr1);
				} else {
					$addr3 = str_replace("\"", "", $addr1) . str_replace("\"", "", $addr2);
				}
				if (($replace = strstr($addr3, "（"))) {
					$addr = str_replace($replace, "", $addr3);
				} else {
					$addr = $addr3;
				}
	
				if ($yuubin == $new) {
					break;
				} else {
					$pref = "";
					$addr = "";
				}
			}
			fclose($file);
		}
		$pref_id = $this->prefecture_list(2,$pref);
		return array($pref, $addr);
	}

	function searchAddr2($file_name,$yuubin) {
		$pref = "";
		$addr = "";
		if ($file = fopen($file_name, "r")) {
			while ($line = fgets($file)) {
				list($d1, $old, $new, $d2, $d3, $d4, $pref, $addr1, $addr2, $d5) = split(",", $line, 10);
				$pref = str_replace("\"", "", $pref);
				$new = str_replace("\"", "", $new);
				if ($yuubin2 == "0000") {
					$addr3 = str_replace("\"", "", $addr1);
				} else {
					$addr3 = str_replace("\"", "", $addr1) . str_replace("\"", "", $addr2);
				}
				if (($replace = strstr($addr3, "（"))) {
					$addr = str_replace($replace, "", $addr3);
				} else {
					$addr = $addr3;
				}

				if ($yuubin == $new) {
					break;
				} else {
					$pref = "";
					$addr = "";
				}
			}
			fclose($file);
		}

		$pref_id = $this->prefecture_list(2,$pref);

		return $pref_id;
	}
	
	function get_image_pattern(){
		return '/^(.*)\.(jpeg|jpg|gif)$/i';
	}

	//画像チェック
	function img_check($value){

		$ret = "";
		if($value){
			//$regex = '/^[a-zA-Z0-9\_\-]+\.[A-Za-z]+$/';
			$regex = '/^[a-zA-Z0-9\_\-]+\.(jpeg|jpg|gif)$/i';
	        if (!preg_match($regex, $value)) {
	        	//$ret = '画像の形式が正しくありません';
	        }
		}

		return $ret;
 	}

	//金額チェック
	function price_check($value){

		$ret = "";
		if($value){
			$regex = '/^[1-9][0-9]+$/i';
	        if (!preg_match($regex, $value)) {
	        	$ret = '金額の形式が正しくありません';
	        }
		}

		return $ret;
 	}

	//数字チェック
	function number_check($value){

		$ret = "";
		if($value){
			$regex = '/^[0-9]+$/i';
	        if (!preg_match($regex, $value)) {
	        	$ret = '数字が正しくありません';
	        }
		}

		return $ret;
 	}
	
	function get_pickupcatalog(&$DB,&$util){
	}

	//フラグチェック
	function flg_check($value){

		$ret = "";
		if($value){
			$regex = '/^[0-1]$/i';
	        if (!preg_match($regex, $value)) {
	        	$ret = 'フラグの形式が正しくありません';
	        }
		}

		return $ret;
 	}

	//月リスト生成
	function month_list(){

		$data[''] = "--";
		$data['01'] = "1";
		$data['02'] = "2";
		$data['03'] = "3";
		$data['04'] = "4";
		$data['05'] = "5";
		$data['06'] = "6";
		$data['07'] = "7";
		$data['08'] = "8";
		$data['09'] = "9";
		$data['10'] = "10";
		$data['11'] = "11";
		$data['12'] = "12";

    	return $data;

	}

	//月リスト生成
	function month_list2(){

		$data['01'] = "1";
		$data['02'] = "2";
		$data['03'] = "3";
		$data['04'] = "4";
		$data['05'] = "5";
		$data['06'] = "6";
		$data['07'] = "7";
		$data['08'] = "8";
		$data['09'] = "9";
		$data['10'] = "10";
		$data['11'] = "11";
		$data['12'] = "12";

    	return $data;

	}

	//日リスト生成
	function day_list(){

		$data[""] = "--";
		$data["01"] = "1";
		$data["02"] = "2";
		$data["03"] = "3";
		$data["04"] = "4";
		$data["05"] = "5";
		$data["06"] = "6";
		$data["07"] = "7";
		$data["08"] = "8";
		$data["09"] = "9";
		$data["10"] = "10";
		$data["11"] = "11";
		$data["12"] = "12";
		$data["13"] = "13";
		$data["14"] = "14";
		$data["15"] = "15";
		$data["16"] = "16";
		$data["17"] = "17";
		$data["18"] = "18";
		$data["19"] = "19";
		$data["20"] = "20";
		$data["21"] = "21";
		$data["22"] = "22";
		$data["23"] = "23";
		$data["24"] = "24";
		$data["25"] = "25";
		$data["26"] = "26";
		$data["27"] = "27";
		$data["28"] = "28";
		$data["29"] = "29";
		$data["30"] = "30";
		$data["31"] = "31";

    	return $data;

	}
	
	//日リスト生成
	function day_list2(){

		$data["01"] = "1";
		$data["02"] = "2";
		$data["03"] = "3";
		$data["04"] = "4";
		$data["05"] = "5";
		$data["06"] = "6";
		$data["07"] = "7";
		$data["08"] = "8";
		$data["09"] = "9";
		$data["10"] = "10";
		$data["11"] = "11";
		$data["12"] = "12";
		$data["13"] = "13";
		$data["14"] = "14";
		$data["15"] = "15";
		$data["16"] = "16";
		$data["17"] = "17";
		$data["18"] = "18";
		$data["19"] = "19";
		$data["20"] = "20";
		$data["21"] = "21";
		$data["22"] = "22";
		$data["23"] = "23";
		$data["24"] = "24";
		$data["25"] = "25";
		$data["26"] = "26";
		$data["27"] = "27";
		$data["28"] = "28";
		$data["29"] = "29";
		$data["30"] = "30";
		$data["31"] = "31";

    	return $data;

	}
	
	//週目リスト生成
	function weekly_list(){

		$data[""] = "--";
		$data['1'] = "1";
		$data['2'] = "2";
		$data['3'] = "3";
		$data['4'] = "4";
		$data['5'] = "5";
		$data['6'] = "6";

    	return $data;

	}

	//メールアドレスチェック
	function mail_check($name,$value){

		$ret = "";
		if($value){
			//$regex = '/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/';
			$regex = '/^[a-zA-Z0-9\_\.\-]+?@[A-Za-z0-9\_\.\-]+\.[A-Za-z0-9\_\.\-]+$/';
	        if (!preg_match($regex, $value)) {
	        	$ret = $name.'の形式が正しくありません';
	        }
		}

		return $ret;
 	}

	//TEL、FAXチェック
	function tel_check($name,$value){

		$ret = "";
		if($value){

			if(!ereg("^[0-9]{1,5}-[0-9]{1,5}-[0-9]{1,5}$", $value)) {

				$ret = $name.'の形式が正しくありません';

			}
		}
		
		return $ret;

 	}

	//郵便番号チェック
	function postcd_check($name,$value){

		$ret = "";
		if($value){

			if(!ereg("^[0-9]{3}-[0-9]{4}$", $value) and !ereg("^[0-9]{7}$", $value)) {

				$ret = $name.'の形式が正しくありません';

			}
		}
		
		return $ret;

 	}
 	
	function prefecture_list($mode=0,$selected=0){
		
		$ret = "";
		$pre[0] = "都道府県を選択";
		$pre[1] = "北海道";
		$pre[2] = "青森県";
		$pre[3] = "岩手県";
		$pre[4] = "宮城県";
		$pre[5] = "秋田県";
		$pre[6] = "山形県";
		$pre[7] = "福島県";
		$pre[8] = "茨城県";
		$pre[9] = "栃木県";
		$pre[10] = "群馬県";
		$pre[11] = "埼玉県";
		$pre[12] = "千葉県";
		$pre[13] = "東京都";
		$pre[14] = "神奈川県";
		$pre[15] = "新潟県";
		$pre[16] = "富山県";
		$pre[17] = "石川県";
		$pre[18] = "福井県";
		$pre[19] = "山梨県";
		$pre[20] = "長野県";
		$pre[21] = "岐阜県";
		$pre[22] = "静岡県";
		$pre[23] = "愛知県";
		$pre[24] = "三重県";
		$pre[25] = "滋賀県";
		$pre[26] = "京都府";
		$pre[27] = "大阪府";
		$pre[28] = "兵庫県";
		$pre[29] = "奈良県";
		$pre[30] = "和歌山県";
		$pre[31] = "島根県";
		$pre[32] = "鳥取県";
		$pre[33] = "岡山県";
		$pre[34] = "広島県";
		$pre[35] = "山口県";
		$pre[36] = "香川県";
		$pre[37] = "徳島県";
		$pre[38] = "愛媛県";
		$pre[39] = "高知県";
		$pre[40] = "福岡県";
		$pre[41] = "佐賀県";
		$pre[42] = "長崎県";
		$pre[43] = "熊本県";
		$pre[44] = "大分県";
		$pre[45] = "宮崎県";
		$pre[46] = "鹿児島県";
		$pre[47] = "沖縄県";
		switch($mode){

			case 1:
				if( $selected ){
					$ret = $pre[$selected];
				}
			break;

			case 2:
				foreach($pre as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;

			default:
				$ret = $pre;
			break;
		}

		return $ret;

	}

	function area_list($mode=0,$selected=0){
		$ret = "";
		$pre[2101] = "岐阜・西濃";
		$pre[2102] = "中濃";
		$pre[2103] = "東濃";
		$pre[2104] = "飛騨北部";
		$pre[2301] = "尾張西部";
		$pre[2302] = "尾張東部";
		$pre[2303] = "知多地域";
		$pre[2304] = "西三河南部";
		$pre[2305] = "西三河北西部";
		$pre[2306] = "東三河南部";
		$pre[2401] = "伊賀";
		$pre[2402] = "伊勢志摩";
		$pre[2403] = "紀勢・東紀州";
		$pre[2404] = "中部";
		$pre[2405] = "北部";
		switch($mode){
			case 1:
				if( $selected ){
					$ret = $pre[$selected];
				}
			break;
			case 2:
				foreach($pre as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $pre;
			break;
		}
		return $ret;
	}
/*
	function pref_area_code($pref_name="",$area_name=""){
		$pre["福岡"]["福岡地区"] = array('40','4001');
		$pre["福岡"]["久留米地区"] = array('40','4002');
		$pre["福岡"]["北九州・筑豊地区"] = array('40','4003');
		$pre["大分"]["大分地区"] = array('44','4401');
		$pre["長崎"]["長崎地区"] = array('42','4201');
		$ret = $pre[$pref_name][$area_name];
		return $ret;
	}
*/
	function car_image($type,$mode=0,$selected=0){
	
		$ret = "";
		if($type == 1){	//平日
			$pre["car00001"] = "atenza_sedan.jpg";
			$pre["car00002"] = "atenza_sedan.jpg";
			$pre["car00003"] = "demio_ge.jpg";
			$pre["car00004"] = "axela_sport.jpg";
			$pre["car00005"] = "axela_sedan.jpg";
			$pre["car00006"] = "axela_sedan.jpg";
			$pre["car00007"] = "axela_sport.jpg";
			$pre["car00008"] = "cx3.jpg";
			$pre["car00009"] = "cx-8.jpg";
			$pre["car00010"] = "cx3.jpg";
			$pre["car00011"] = "cx5.jpg";
			$pre["car00012"] = "cx5.jpg";
			$pre["car00013"] = "cx-8.jpg";
			$pre["car00014"] = "demio_ge.jpg";
			$pre["car00015"] = "demio_de.jpg";
			$pre["car00016"] = "demio_de.jpg";
			$pre["car00017"] = "roadster.jpg";
			$pre["car00018"] = "roadster.jpg";
			$pre["car00019"] = "axela_sedan.jpg";
			$pre["car00020"] = "cx5.jpg";
			$pre["car00021"] = "roadsterrf.jpg";
			$pre["car00022"] = "roadsterrf.jpg";
			$pre["car00023"] = "cx3.jpg";
			$pre["car00024"] = "cx3.jpg";
			$pre["car00025"] = "cx-8.jpg";
			$pre["car00026"] = "atenza_sedan.jpg";
			$pre["car00027"] = "mazda3_fb.jpg";
			$pre["car00028"] = "mazda3_sedan.jpg";
			$pre["car00029"] = "mazda3_fb.jpg";
			$pre["car00030"] = "mazda3_sedan.jpg";
			$pre["car00031"] = "cx3_1800.jpg";
			$pre["car00032"] = "mazda3_fb.jpg";
            $pre["car00033"] = "mazda6_sedan.jpg";
            $pre["car00034"] = "mazda6_sedan.jpg";
            $pre["car00035"] = "mazda6_wgn.jpg";
            $pre["car00036"] = "mazda6_wgn.jpg";
            $pre["car00037"] = "cx30.jpg";
            $pre["car00038"] = "cx30.jpg";
            $pre["car00039"] = "mazda2.jpg";
            $pre["car00040"] = "mazda2.jpg";
            $pre["car00041"] = "cx30.jpg";
            $pre["car00042"] = "mazda3_fb.jpg";
            $pre["car00043"] = "cx5.jpg";
            $pre["car00044"] = "cx3.jpg";
            $pre["car00045"] = "mazda3_sedan.jpg";
            $pre["car00046"] = "mx30.jpg";
		}
		else{
            $pre["car00001"] = "atenza_sedan.jpg";
            $pre["car00002"] = "atenza_sedan.jpg";
            $pre["car00003"] = "demio_ge.jpg";
            $pre["car00004"] = "axela_sport.jpg";
            $pre["car00005"] = "axela_sedan.jpg";
            $pre["car00006"] = "axela_sedan.jpg";
            $pre["car00007"] = "axela_sport.jpg";
            $pre["car00008"] = "cx3.jpg";
            $pre["car00009"] = "cx-8.jpg";
            $pre["car00010"] = "cx3.jpg";
            $pre["car00011"] = "cx5.jpg";
            $pre["car00012"] = "cx5.jpg";
            $pre["car00013"] = "cx-8.jpg";
            $pre["car00014"] = "demio_ge.jpg";
            $pre["car00015"] = "demio_de.jpg";
            $pre["car00016"] = "demio_de.jpg";
            $pre["car00017"] = "roadster.jpg";
            $pre["car00018"] = "roadster.jpg";
            $pre["car00019"] = "axela_sedan.jpg";
            $pre["car00020"] = "cx5.jpg";
            $pre["car00021"] = "roadsterrf.jpg";
            $pre["car00022"] = "roadsterrf.jpg";
            $pre["car00023"] = "cx3.jpg";
            $pre["car00024"] = "cx3.jpg";
            $pre["car00025"] = "cx-8.jpg";
            $pre["car00026"] = "atenza_sedan.jpg";
            $pre["car00027"] = "mazda3_fb.jpg";
            $pre["car00028"] = "mazda3_sedan.jpg";
            $pre["car00029"] = "mazda3_fb.jpg";
			$pre["car00030"] = "mazda3_sedan.jpg";
			$pre["car00031"] = "cx3_1800.jpg";
			$pre["car00032"] = "mazda3_fb.jpg";
            $pre["car00033"] = "mazda6_sedan.jpg";
            $pre["car00034"] = "mazda6_sedan.jpg";
            $pre["car00035"] = "mazda6_wgn.jpg";
            $pre["car00036"] = "mazda6_wgn.jpg";
            $pre["car00037"] = "cx30.jpg";
            $pre["car00038"] = "cx30.jpg";
            $pre["car00039"] = "mazda2.jpg";
            $pre["car00040"] = "mazda2.jpg";
            $pre["car00041"] = "cx30.jpg";
            $pre["car00042"] = "mazda3_fb.jpg";
            $pre["car00043"] = "cx5.jpg";
            $pre["car00044"] = "cx3.jpg";
            $pre["car00045"] = "mazda3_sedan.jpg";
            $pre["car00046"] = "mx30.jpg";
		}

		switch($mode){

			case 1:
				if( $selected ){
					$ret = $pre[$selected];
				}
			break;

			case 2:
				foreach($pre as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;

			default:
				$ret = $pre;
			break;
		}

		return $ret;
	}

	function pday_list($mode=0,$selected=0){
		$ret = "";
		$pre["2018-01-01"] = "1";
		$pre["2018-01-08"] = "1";
		$pre["2018-02-11"] = "1";
		$pre["2018-02-12"] = "1";
		$pre["2018-03-21"] = "1";
		$pre["2018-04-29"] = "1";
		$pre["2018-04-30"] = "1";
		$pre["2018-05-03"] = "1";
		$pre["2018-05-04"] = "1";
		$pre["2018-05-05"] = "1";
		$pre["2018-07-16"] = "1";
		$pre["2018-08-11"] = "1";
		$pre["2018-09-17"] = "1";
		$pre["2018-09-23"] = "1";
		$pre["2018-09-24"] = "1";
		$pre["2018-10-08"] = "1";
		$pre["2018-11-03"] = "1";
		$pre["2018-11-23"] = "1";
		$pre["2018-12-23"] = "1";
		$pre["2018-12-24"] = "1";
		$pre["2018-12-31"] = "1";
		$pre["2019-01-01"] = "1";
		$pre["2019-01-14"] = "1";
		$pre["2019-02-11"] = "1";
		$pre["2019-03-21"] = "1";
        $pre["2018-04-29"] = "1";
        $pre["2018-04-30"] = "1";
        $pre["2018-05-03"] = "1";
        $pre["2018-05-04"] = "1";
        $pre["2018-05-05"] = "1";
        $pre["2018-07-16"] = "1";
        $pre["2018-08-11"] = "1";
        $pre["2018-09-17"] = "1";
        $pre["2018-09-23"] = "1";
        $pre["2018-09-24"] = "1";
        $pre["2018-10-08"] = "1";
        $pre["2018-11-03"] = "1";
        $pre["2018-11-23"] = "1";
        $pre["2018-12-23"] = "1";
        $pre["2018-12-24"] = "1";
        $pre["2019-01-01"] = "1";
        $pre["2019-01-14"] = "1";
        $pre["2019-02-11"] = "1";
        $pre["2019-03-21"] = "1";
        $pre["2019-04-29"] = "1";
        $pre["2019-04-30"] = "1";
        $pre["2019-05-01"] = "1";
        $pre["2019-05-02"] = "1";
        $pre["2019-05-03"] = "1";
        $pre["2019-05-04"] = "1";
        $pre["2019-05-05"] = "1";
        $pre["2019-05-06"] = "1";
        $pre["2019-07-15"] = "1";
        $pre["2019-08-11"] = "1";
        $pre["2019-08-12"] = "1";
        $pre["2019-09-16"] = "1";
        $pre["2019-09-23"] = "1";
        $pre["2019-10-14"] = "1";
        $pre["2019-10-22"] = "1";
        $pre["2019-11-03"] = "1";
        $pre["2019-11-04"] = "1";
        $pre["2019-11-23"] = "1";
        $pre["2020-01-01"] = "1";
        $pre["2020-01-13"] = "1";
        $pre["2020-02-11"] = "1";
        $pre["2020-02-23"] = "1";
        $pre["2020-02-24"] = "1";
        $pre["2020-03-20"] = "1";
        $pre["2020-04-29"] = "1";
        $pre["2020-05-03"] = "1";
        $pre["2020-05-04"] = "1";
        $pre["2020-05-05"] = "1";
        $pre["2020-05-06"] = "1";
        $pre["2020-07-23"] = "1";
        $pre["2020-07-24"] = "1";
        $pre["2020-08-10"] = "1";
        $pre["2020-09-21"] = "1";
        $pre["2020-09-22"] = "1";
        $pre["2020-11-03"] = "1";
        $pre["2020-11-23"] = "1";
        $pre["2021-01-01"] = "1";
        $pre["2021-01-11"] = "1";
        $pre["2021-02-11"] = "1";
        $pre["2021-02-23"] = "1";
        $pre["2021-03-20"] = "1";
        $pre["2021-04-29"] = "1";
        $pre["2021-05-03"] = "1";
        $pre["2021-05-04"] = "1";
        $pre["2021-05-05"] = "1";
        $pre["2021-07-19"] = "1";
        $pre["2021-08-11"] = "1";
        $pre["2021-09-20"] = "1";
        $pre["2021-09-23"] = "1";
        $pre["2021-10-11"] = "1";
        $pre["2021-11-03"] = "1";
        $pre["2021-11-23"] = "1";
		switch($mode){
			case 1:
				if( $selected ){
					if($pre[$selected]){
						$ret = $pre[$selected];
					}
					else{
						$ret = "";
					}
				}
			break;
			case 2:
				foreach($pre as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $pre;
			break;
		}
		return $ret;
	}

	function holiday_list($DB,$shop_id="",$mode=0,$selected=0){
		if($shop_id){
			$sql = "select mother_flg from shop ";
			$sql .= " where shop_id='".$DB->getQStr($shop_id)."' ";
			$rs =& $DB->ASExecute($sql);
			$mother_flg = "";
			if($rs){
				if(!$rs->EOF){
					$mother_flg = $rs->fields('mother_flg');
				}
				$rs->Close();
			}
		}
		else{
			$mother_flg = "";
		}
		$ret = "";
		if($mother_flg){
			$pre['2018-01-24'] = '1';
			$pre['2018-02-21'] = '1';
			$pre['2018-03-28'] = '1';
			$pre['2018-04-18'] = '1';
			$pre['2018-05-23'] = '1';
			$pre['2018-06-13'] = '1';
			$pre['2018-07-11'] = '1';
			$pre['2018-08-29'] = '1';
			$pre['2018-09-12'] = '1';
			$pre['2018-10-24'] = '1';
			$pre['2018-11-14'] = '1';
			$pre['2018-12-12'] = '1';
			$pre['2019-01-23'] = '1';
			$pre['2019-02-13'] = '1';
			$pre['2019-03-20'] = '1';
            $pre['2019-04-24'] = '1';
            $pre['2019-05-22'] = '1';
            $pre['2019-06-12'] = '1';
            $pre['2019-07-17'] = '1';
            $pre['2019-08-21'] = '1';
            $pre['2019-09-11'] = '1';
            $pre['2019-10-23'] = '1';
            $pre['2019-11-13'] = '1';
            $pre['2019-12-11'] = '1';
            $pre['2020-01-22'] = '1';
            $pre['2020-02-26'] = '1';
            $pre['2020-03-11'] = '1';
            $pre['2020-04-15'] = '1';
            $pre['2020-05-13'] = '1';
            $pre['2020-06-24'] = '1';
            $pre['2020-07-15'] = '1';
            $pre['2020-08-19'] = '1';
            $pre['2020-09-23'] = '1';
            $pre['2020-10-14'] = '1';
            $pre['2020-11-11'] = '1';
            $pre['2020-12-23'] = '1';
            $pre['2021-01-13'] = '1';
            $pre['2021-02-24'] = '1';
            $pre['2021-03-24'] = '1';
		}
		$pre['2018-01-01'] = '1';
		$pre['2018-01-02'] = '1';
		$pre['2018-01-09'] = '1';
		$pre['2018-01-16'] = '1';
		$pre['2018-01-17'] = '1';
		$pre['2018-01-23'] = '1';
		$pre['2018-01-30'] = '1';
		$pre['2018-02-06'] = '1';
		$pre['2018-02-07'] = '1';
		$pre['2018-02-13'] = '1';
		$pre['2018-02-20'] = '1';
		$pre['2018-02-27'] = '1';
		$pre['2018-03-06'] = '1';
		$pre['2018-03-07'] = '1';
		$pre['2018-03-13'] = '1';
		$pre['2018-03-20'] = '1';
		$pre['2018-03-27'] = '1';
		$pre['2018-04-03'] = '1';
		$pre['2018-04-04'] = '1';
		$pre['2018-04-10'] = '1';
		$pre['2018-04-11'] = '1';
		$pre['2018-04-17'] = '1';
		$pre['2018-04-24'] = '1';
		$pre['2018-05-01'] = '1';
		$pre['2018-05-07'] = '1';
		$pre['2018-05-08'] = '1';
		$pre['2018-05-09'] = '1';
		$pre['2018-05-10'] = '1';
		$pre['2018-05-15'] = '1';
		$pre['2018-05-16'] = '1';
		$pre['2018-05-22'] = '1';
		$pre['2018-05-29'] = '1';
		$pre['2018-06-05'] = '1';
		$pre['2018-06-06'] = '1';
		$pre['2018-06-12'] = '1';
		$pre['2018-06-19'] = '1';
		$pre['2018-06-20'] = '1';
		$pre['2018-06-26'] = '1';
		$pre['2018-06-28'] = '1';
		$pre['2018-07-03'] = '1';
		$pre['2018-07-04'] = '1';
		$pre['2018-07-10'] = '1';
		$pre['2018-07-17'] = '1';
		$pre['2018-07-24'] = '1';
		$pre['2018-07-25'] = '1';
		$pre['2018-07-31'] = '1';
		$pre['2018-08-07'] = '1';
		$pre['2018-08-08'] = '1';
		$pre['2018-08-13'] = '1';
		$pre['2018-08-14'] = '1';
		$pre['2018-08-15'] = '1';
		$pre['2018-08-16'] = '1';
		$pre['2018-08-21'] = '1';
		$pre['2018-08-22'] = '1';
		$pre['2018-08-28'] = '1';
		$pre['2018-09-04'] = '1';
		$pre['2018-09-05'] = '1';
		$pre['2018-09-11'] = '1';
		$pre['2018-09-18'] = '1';
		$pre['2018-09-19'] = '1';
		$pre['2018-09-25'] = '1';
		$pre['2018-10-02'] = '1';
		$pre['2018-10-03'] = '1';
		$pre['2018-10-09'] = '1';
		$pre['2018-10-16'] = '1';
		$pre['2018-10-17'] = '1';
		$pre['2018-10-23'] = '1';
		$pre['2018-10-30'] = '1';
		$pre['2018-11-06'] = '1';
		$pre['2018-11-07'] = '1';
		$pre['2018-11-13'] = '1';
		$pre['2018-11-20'] = '1';
		$pre['2018-11-21'] = '1';
		$pre['2018-11-27'] = '1';
		$pre['2018-12-04'] = '1';
		$pre['2018-12-05'] = '1';
		$pre['2018-12-11'] = '1';
		$pre['2018-12-18'] = '1';
		$pre['2018-12-25'] = '1';
		$pre['2018-12-28'] = '1';
		$pre['2018-12-29'] = '1';
		$pre['2018-12-30'] = '1';
		$pre['2018-12-31'] = '1';
		$pre['2019-01-01'] = '1';
		$pre['2019-01-02'] = '1';
		$pre['2019-01-03'] = '1';
		$pre['2019-01-08'] = '1';
		$pre['2019-01-15'] = '1';
		$pre['2019-01-16'] = '1';
		$pre['2019-01-22'] = '1';
		$pre['2019-01-29'] = '1';
		$pre['2019-02-05'] = '1';
		$pre['2019-02-06'] = '1';
		$pre['2019-02-12'] = '1';
		$pre['2019-02-19'] = '1';
		$pre['2019-02-20'] = '1';
		$pre['2019-02-26'] = '1';
		$pre['2019-03-05'] = '1';
		$pre['2019-03-06'] = '1';
		$pre['2019-03-12'] = '1';
		$pre['2019-03-13'] = '1';
		$pre['2019-03-19'] = '1';
		$pre['2019-03-26'] = '1';
		$pre['2019-04-02'] = '1';
		$pre['2019-04-03'] = '1';
		$pre['2019-04-09'] = '1';
        $pre['2019-04-16'] = '1';
        $pre['2019-04-17'] = '1';
        $pre['2019-04-23'] = '1';
        $pre['2019-04-30'] = '1';
        $pre['2019-05-01'] = '1';
        $pre['2019-05-02'] = '1';
        $pre['2019-05-07'] = '1';
        $pre['2019-05-13'] = '1';
        $pre['2019-05-14'] = '1';
        $pre['2019-05-15'] = '1';
        $pre['2019-05-21'] = '1';
        $pre['2019-05-28'] = '1';
        $pre['2019-06-04'] = '1';
        $pre['2019-06-05'] = '1';
        $pre['2019-06-11'] = '1';
        $pre['2019-06-18'] = '1';
        $pre['2019-06-19'] = '1';
        $pre['2019-06-25'] = '1';
        $pre['2019-07-02'] = '1';
        $pre['2019-07-03'] = '1';
        $pre['2019-07-09'] = '1';
        $pre['2019-07-16'] = '1';
        $pre['2019-07-23'] = '1';
        $pre['2019-07-24'] = '1';
        $pre['2019-07-30'] = '1';
        $pre['2019-08-05'] = '1';
        $pre['2019-08-07'] = '1';
        $pre['2019-08-13'] = '1';
        $pre['2019-08-14'] = '1';
        $pre['2019-08-15'] = '1';
        $pre['2019-08-16'] = '1';
        $pre['2019-08-20'] = '1';
        $pre['2019-08-27'] = '1';
        $pre['2019-09-03'] = '1';
        $pre['2019-09-04'] = '1';
        $pre['2019-09-10'] = '1';
        $pre['2019-09-17'] = '1';
        $pre['2019-09-18'] = '1';
        $pre['2019-09-24'] = '1';
        $pre['2019-10-01'] = '1';
        $pre['2019-10-02'] = '1';
        $pre['2019-10-08'] = '1';
        $pre['2019-10-15'] = '1';
        $pre['2019-10-16'] = '1';
        $pre['2019-10-22'] = '1';
        $pre['2019-10-29'] = '1';
        $pre['2019-11-05'] = '1';
        $pre['2019-11-06'] = '1';
        $pre['2019-11-12'] = '1';
        $pre['2019-11-19'] = '1';
        $pre['2019-11-20'] = '1';
        $pre['2019-11-26'] = '1';
        $pre['2019-12-03'] = '1';
        $pre['2019-12-04'] = '1';
        $pre['2019-12-10'] = '1';
        $pre['2019-12-17'] = '1';
        $pre['2019-12-18'] = '1';
        $pre['2019-12-24'] = '1';
        $pre['2019-12-28'] = '1';
        $pre['2019-12-29'] = '1';
        $pre['2019-12-30'] = '1';
        $pre['2019-12-31'] = '1';
        $pre['2020-01-01'] = '1';
        $pre['2020-01-02'] = '1';
        $pre['2020-01-03'] = '1';
        $pre['2020-01-07'] = '1';
        $pre['2020-01-14'] = '1';
        $pre['2020-01-15'] = '1';
        $pre['2020-01-21'] = '1';
        $pre['2020-01-28'] = '1';
        $pre['2020-02-04'] = '1';
        $pre['2020-02-05'] = '1';
        $pre['2020-02-11'] = '1';
        $pre['2020-02-18'] = '1';
        $pre['2020-02-19'] = '1';
        $pre['2020-02-25'] = '1';
        $pre['2020-03-03'] = '1';
        $pre['2020-03-04'] = '1';
        $pre['2020-03-10'] = '1';
        $pre['2020-03-17'] = '1';
        $pre['2020-03-18'] = '1';
        $pre['2020-03-24'] = '1';
        $pre['2020-03-31'] = '1';
        $pre['2020-04-07'] = '1';
        $pre['2020-04-08'] = '1';
        $pre['2020-04-14'] = '1';
        $pre['2020-04-21'] = '1';
        $pre['2020-04-22'] = '1';
        $pre['2020-04-28'] = '1';
        $pre['2020-05-05'] = '1';
        $pre['2020-05-06'] = '1';
        $pre['2020-05-07'] = '1';
        $pre['2020-05-08'] = '1';
        $pre['2020-05-12'] = '1';
        $pre['2020-05-19'] = '1';
        $pre['2020-05-20'] = '1';
        $pre['2020-05-26'] = '1';
        $pre['2020-06-02'] = '1';
        $pre['2020-06-03'] = '1';
        $pre['2020-06-09'] = '1';
        $pre['2020-06-16'] = '1';
        $pre['2020-06-17'] = '1';
        $pre['2020-06-23'] = '1';
        $pre['2020-06-30'] = '1';
        $pre['2020-07-07'] = '1';
        $pre['2020-07-08'] = '1';
        $pre['2020-07-14'] = '1';
        $pre['2020-07-21'] = '1';
        $pre['2020-07-22'] = '1';
        $pre['2020-07-28'] = '1';
        $pre['2020-08-04'] = '1';
        $pre['2020-08-05'] = '1';
        $pre['2020-08-11'] = '1';
        $pre['2020-08-12'] = '1';
        $pre['2020-08-13'] = '1';
        $pre['2020-08-14'] = '1';
        $pre['2020-08-18'] = '1';
        $pre['2020-08-25'] = '1';
        $pre['2020-09-01'] = '1';
        $pre['2020-09-02'] = '1';
        $pre['2020-09-08'] = '1';
        $pre['2020-09-15'] = '1';
        $pre['2020-09-16'] = '1';
        $pre['2020-09-22'] = '1';
        $pre['2020-09-29'] = '1';
        $pre['2020-10-06'] = '1';
        $pre['2020-10-07'] = '1';
        $pre['2020-10-13'] = '1';
        $pre['2020-10-20'] = '1';
        $pre['2020-10-21'] = '1';
        $pre['2020-10-27'] = '1';
        $pre['2020-11-03'] = '1';
        $pre['2020-11-04'] = '1';
        $pre['2020-11-10'] = '1';
        $pre['2020-11-17'] = '1';
        $pre['2020-11-18'] = '1';
        $pre['2020-11-24'] = '1';
        $pre['2020-12-01'] = '1';
        $pre['2020-12-02'] = '1';
        $pre['2020-12-08'] = '1';
        $pre['2020-12-15'] = '1';
        $pre['2020-12-16'] = '1';
        $pre['2020-12-22'] = '1';
        $pre['2020-12-28'] = '1';
        $pre['2020-12-29'] = '1';
        $pre['2020-12-30'] = '1';
        $pre['2020-12-31'] = '1';
        $pre['2021-01-01'] = '1';
        $pre['2021-01-02'] = '1';
        $pre['2021-01-03'] = '1';
        $pre['2021-01-06'] = '1';
        $pre['2021-01-07'] = '1';
        $pre['2021-01-12'] = '1';
        $pre['2021-01-19'] = '1';
        $pre['2021-01-20'] = '1';
        $pre['2021-01-26'] = '1';
        $pre['2021-02-02'] = '1';
        $pre['2021-02-03'] = '1';
        $pre['2021-02-09'] = '1';
        $pre['2021-02-16'] = '1';
        $pre['2021-02-17'] = '1';
        $pre['2021-02-23'] = '1';
        $pre['2021-03-02'] = '1';
        $pre['2021-03-03'] = '1';
        $pre['2021-03-09'] = '1';
        $pre['2021-03-16'] = '1';
        $pre['2021-03-17'] = '1';
        $pre['2021-03-23'] = '1';
        $pre['2021-03-30'] = '1';
        $pre['2021-04-06'] = '1';
        $pre['2021-04-07'] = '1';

		switch($mode){
			case 1:
				if( $selected ){
					if($pre[$selected]){
						$ret = $pre[$selected];
					}
					else{
						$ret = "";
					}
				}
			break;
			case 2:
				foreach($pre as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $pre;
			break;
		}
		return $ret;
	}
	
	//年リスト生成
	function year_list($mode=0,$max=0,$start=0){

		$data[""] = "----";
		
		$start = 2018;
		$max = date("Y") + 1;

		for($i=$start;$i<=$max;$i++){
			$data[$i] = $i;
		}

    	return $data;

	}

	//年リスト生成(車種の登録年月日で使用)
	function year2_list($mode=0,$max=0,$start=0){

		$data[""] = "----";
		
		$start = 2014;
		$max = date("Y") + 1;

		for($i=$start;$i<=$max;$i++){
			$data[$i] = $i;
		}

    	return $data;

	}

    //年リスト生成(予約確定で使用)
    function year3_list($mode=0,$max=0,$start=0){
        $data[""] = "----";
        $start = date("Y");
        $max = date("Y") + 1;
        for($i=$start;$i<=$max;$i++){
            $data[$i] = $i;
        }
        return $data;
    }

	//時リスト生成
	function hour_list(){

//		$data[''] = "選択";
		$data['00'] = "0";
		$data['01'] = "1";
		$data['02'] = "2";
		$data['03'] = "3";
		$data['04'] = "4";
		$data['05'] = "5";
		$data['06'] = "6";
		$data['07'] = "7";
		$data['08'] = "8";
		$data['09'] = "9";
		$data['10'] = "10";
		$data['11'] = "11";
		$data['12'] = "12";
		$data['13'] = "13";
		$data['14'] = "14";
		$data['15'] = "15";
		$data['16'] = "16";
		$data['17'] = "17";
		$data['18'] = "18";
		$data['19'] = "19";
		$data['20'] = "20";
		$data['21'] = "21";
		$data['22'] = "22";
		$data['23'] = "23";

    	return $data;

	}

	function minuts_list(){

//		$data[""] = "選択";
		$data["00"] = "0";
		$data["01"] = "1";
		$data["02"] = "2";
		$data["03"] = "3";
		$data["04"] = "4";
		$data["05"] = "5";
		$data["06"] = "6";
		$data["07"] = "7";
		$data["08"] = "8";
		$data["09"] = "9";
		$data["10"] = "10";
		$data["11"] = "11";
		$data["12"] = "12";
		$data["13"] = "13";
		$data["14"] = "14";
		$data["15"] = "15";
		$data["16"] = "16";
		$data["17"] = "17";
		$data["18"] = "18";
		$data["19"] = "19";
		$data["20"] = "20";
		$data["21"] = "21";
		$data["22"] = "22";
		$data["23"] = "23";
		$data["24"] = "24";
		$data["25"] = "25";
		$data["26"] = "26";
		$data["27"] = "27";
		$data["28"] = "28";
		$data["29"] = "29";
		$data["30"] = "30";
		$data["31"] = "31";
		$data["32"] = "32";
		$data["33"] = "33";
		$data["34"] = "34";
		$data["35"] = "35";
		$data["36"] = "36";
		$data["37"] = "37";
		$data["38"] = "38";
		$data["39"] = "39";
		$data["40"] = "40";
		$data["41"] = "41";
		$data["42"] = "42";
		$data["43"] = "43";
		$data["44"] = "44";
		$data["45"] = "45";
		$data["46"] = "46";
		$data["47"] = "47";
		$data["48"] = "48";
		$data["49"] = "49";
		$data["50"] = "50";
		$data["51"] = "51";
		$data["52"] = "52";
		$data["53"] = "53";
		$data["54"] = "54";
		$data["55"] = "55";
		$data["56"] = "56";
		$data["57"] = "57";
		$data["58"] = "58";
		$data["59"] = "59";

    	return $data;

	}
	
	//時リスト生成
	function schedule_hour_list($mode=0,$selected=0){
		$data['10'] = "10";
		$data['11'] = "11";
		$data['12'] = "12";
		$data['13'] = "13";
		$data['14'] = "14";
		$data['15'] = "15";
		$data['16'] = "16";
		$data['17'] = "17";
		$data['18'] = "18";
		$data['19'] = "19";
		$data['20'] = "20";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	function schedule_min_list($mode=0,$selected=0){
		$data["00"] = "00";
		$data["30"] = "30";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	function test_drive_time_list($mode=0,$selected=0){
		$data["1"] = "午前";
		$data["2"] = "午後";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	function zip_check($name,$value){
		
		$ret = "";
		if($value){
			if(!ereg("^[0-9]{3}-[0-9]{4}$", $value) and !ereg("^[0-9]{7}$", $value)) {
				$ret = $name.'の形式が正しくありません';
			}
		}
		
		return $ret;
	}
	
	//参加人数
	function num_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "1";
		$data[2] = "2";

		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}
	
	//年齢リスト生成
	function age_list($mode=0,$selected=0){
		$ret = "";
		$j = 18;
		
		for($i = 18 ; $i <= 79 ; $i++){
			$data[$j] = $i;
			$j++;
		}
		//$data[count($data)] = "80歳以上";
		$data[$j] = "80歳以上";
	
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}
	
	//キャンペーンを知った媒体
	function campaign_list($mode=0,$selected=0,$disp_flg=false){
		$ret = "";
		$data[12] = "TVCM";
		$data[1] = "折込チラシ";
		$data[3] = "交通広告";
		$data[5] = "ダイレクトメール";
		$data[6] = "映画広告";
		$data[13] = "雑誌広告";
		$data[7] = "Web広告";
		$data[8] = "Facebook";
		$data[9] = "メールマガジン";
		$data[10] = "出張展示会";
		$data[11] = "営業スタッフの勧め";
		$data[99] = "その他";
		if(!$disp_flg){
			$data[2] = "ラジオ";
			$data[4] = "新聞広告";
		}
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}
	
	//キャンペーン申込の理由
	function reason_list($mode=0,$selected=0){
		$data["1"] = "ドライブするため";
		$data["2"] = "レジャーに出かけるため";
		$data["3"] = "最新のSKYACTIVを体験するため";
		$data["4"] = "友人に薦められたため";
		$data["5"] = "購入検討をするため";
		$data["6"] = "他社との比較のため";
		$data["99"] = "その他";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//日付生成
	function select_day_list($mode=0,$selected=0){
		$ret = "";
		$day = 0;
		if(date("H") >= 12){
    		$day = 2;
    	}
    	else{
    		$day = 1;
		}
		$start_day = date("Ymd",mktime(0,0,0,date("m"),date("d") + $day,date("Y")));
		$end_day = date("Ymd",mktime(0,0,0,date("m") + 2,0,date("Y")));
		//$end_day = date("Ymd",mktime(0,0,0,9,30,2014));
		$ymd = date("Y年m月d日",mktime(0,0,0,date("m"),date("d") + $day,date("Y")));
		$data = array();
		while($start_day <= $end_day){
			$data[$start_day] = $ymd;
			$day++;
			$start_day = date("Ymd",mktime(0,0,0,date("m"),date("d") + $day,date("Y")));
			$ymd = date("Y年m月d日",mktime(0,0,0,date("m"),date("d") + $day,date("Y")));
		}
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//店舗情報取得
	function shop_info_get($shop_id,$DB){
		$ret = array();
	
		$sql = "select * from shop ";
		$sql .= " where shop_id='".$DB->getQStr($shop_id)."' ";

		$rs =& $DB->ASExecute($sql);

		if($rs){
			if(!$rs->EOF){
				$ret['address'] = $rs->fields('address');
				$ret['name'] = $rs->fields('name');
				$ret['postcd'] = $rs->fields('postcd');
				$ret['tel'] = $rs->fields('tel');
				$ret['area_id'] = $rs->fields('area_id');
				$ret['pref_id'] = $rs->fields('pref_id');
			}
			$rs->Close();
		}
		return $ret;
	}

	//車種情報取得
	function car_info_get($car_detail_id,$DB){
		$ret = array();
		$sql = "select car_id,week_flg from car_detail ";
		$sql .= " where autono=".$DB->getQStr($car_detail_id);
		$rs2 =& $DB->ASExecute($sql);
		if($rs2){
			if(!$rs2->EOF){
				$sql = "select name,name2,name3 from car ";
				$sql .= " where car_id='".$DB->getQStr($rs2->fields('car_id'))."' ";
				$rs =& $DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$ret['name'] = $rs->fields('name');
						$ret['name2'] = $rs->fields('name2');
						$ret['name3'] = $rs->fields('name3');
					}
					$rs->Close();
				}
			}
			$rs2->Close();
		}
		return $ret;
	}

	//事業部一覧取得
	function station_list_get($mode=0,$selected=0,$DB){
		$ret = array();
	
		$sql = "select station_code,station_name from shop ";
		$sql .= " where del_flg = '0'";
		$sql .= " group by station_code,station_name ";
		$sql .= " order by station_code ";
//echo $sql;
		$rs =& $DB->ASExecute($sql);

		if($rs){
			while(!$rs->EOF){
				$ret[$rs->fields('station_code')] = $rs->fields('station_name');
				
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $ret;
	}

	//店舗一覧取得
	function shop_list_get($station_code,$mode=0,$selected=0,$DB){
		$ret = array();
	
		$sql = "select * from shop ";
		$sql .= " where del_flg = '0'";
		if($station_code){
			$sql .= " and station_code='".$DB->getQStr($station_code)."' ";
		}
		$sql .= " order by station_code,order_no ";
//echo $sql;
		$rs =& $DB->ASExecute($sql);

		if($rs){
			while(!$rs->EOF){
				$ret[$rs->fields('shop_id')] = $rs->fields('name');
				
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $ret;
	}

	//店舗
	function shop_id_list($data='',$mode=0,$selected=0){
		$ret = "";
	
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}
	
	//仮押さえ削除
	function del_reservation($DB,$at){
		$ret = "";
		$sql = "select * from reservation";
		$sql .= " where autono = ".$DB->getQStr($at);

		$rs =& $DB->ASExecute($sql);

		if($rs){
			if(!$rs->EOF){
				if(!$rs->fields('sei_kana')){
					$record = null;
					$record['ret_flg'] = "1";
					$record['disp_flg'] = "0";
					$record['del_flg'] = "1";
					$record['temporary_flg'] = '0';
					$record['update_date'] = time();
					
					$where  = " autono = ".$DB->getQStr($at);
					$ret = $DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
				}
			}
			$rs->Close();
		}
		return $ret;
	}
	
	//本部・店舗スタッフメールアドレス取得処理
	function send_mail($DB,$shop_id){
		$ret = array();
		// 本部メールアドレス取得
		$sql = "select * from send_mail";
		$sql .= " where driving_shop_id = '000'";
		$sql .= " and disp_flg = '1' and del_flg = '0'";
		$sql .= " and access_kb = '4'";
        $sql .= " and driving_user_flg = '1'";
		$sql .= " and driving_mail <> ''";
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."<br>";
}
		$rs =& $DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				//$ret[] = $rs->fields('staff_name');
				//あとで↓を有効にする
				$ret[] = $rs->fields('mail');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		// 店舗スタッフメールアドレス取得
		$station_code = "";
		$sql = "select * from send_mail";
		$sql .= " where driving_shop_id = '".$DB->getQStr($shop_id)."'";
		$sql .= " and disp_flg = '1' and del_flg = '0'";
//        $sql .= " and access_kb = '1'";
        $sql .= " and access_kb in ('1','2')";
        $sql .= " and driving_user_flg = '1'";
		$sql .= " and driving_mail <> ''";
		$rs =& $DB->ASExecute($sql);
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."<br>";
}
		if($rs){
			while(!$rs->EOF){
				//$ret[] = $rs->fields('staff_name');
				//あとで↓を有効にする
				$ret[] = $rs->fields('mail');
				$station_code = $rs->fields('station_code');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($station_code){
			$sql = "select * from send_mail";
			$sql .= " where driving_station_code = '".$DB->getQStr($station_code)."'";
			$sql .= " and disp_flg = '1' and del_flg = '0'";
			$sql .= " and access_kb = '3'";
            $sql .= " and driving_user_flg = '1'";
			$sql .= " and driving_mail <> ''";
			$rs =& $DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$ret[] = $rs->fields('mail');
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		return $ret;
	}

	//本部・店舗スタッフ一覧取得処理
	function staff_list($DB,$shop_id){
		$ret = array();
		//本部権限
		if($_SESSION['oneday']['access_kb'] == "4"){
			// 本部スタッフ一覧取得
			$sql = "select * from send_mail";
			$sql .= " where driving_shop_id = '000'";
            $sql .= " and driving_user_flg = '1'";
			$sql .= " and disp_flg = '1' and del_flg = '0'";
			$rs =& $DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$ret[$rs->fields('autono')] = $rs->fields('staff_name');
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		// 店舗スタッフ一覧取得
		$sql = "select * from send_mail";
		$sql .= " where driving_shop_id = '".$DB->getQStr($shop_id)."'";
        $sql .= " and driving_user_flg = '1'";
		$sql .= " and disp_flg = '1' and del_flg = '0'";
		$rs =& $DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$ret[$rs->fields('autono')] = $rs->fields('staff_name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $ret;
	}

	//権限リスト(スタッフ管理で使用)
	function access_kb_list($mode=0,$selected=0){
		$data[""] = "";
		$data["1"] = "スタッフ権限";
		$data["2"] = "店長権限";
		$data["3"] = "事業部権限";
		$data["4"] = "本部権限";
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}
/*
	//イベント一覧
	function event_date_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "2015-06-01";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//イベント一覧
	function event_time_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "19:00-21:00(18:30開場)";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//1日試乗キャンペーンID
	function ref_list($mode=0,$selected=0){
		$ret = "";
		$data[1]  = "0811";
		$data[2]  = "0822_1";
		$data[3]  = "0822_2";
		$data[4]  = "0822_3";
		$data[5]  = "0822_4";
		$data[6]  = "0822_5";
		$data[7]  = "A_0206";
		$data[8]  = "B_0206";
		$data[9]  = "A_1219";
		$data[10] = "B_1219";
		$data[11] = "C_1219";
		$data[12] = "D_1219";
		$data[13] = "E_1219";
		$data[14] = "F_1219";
		$data[15] = "GDN_5_RS_1_pc";
		$data[16] = "GDN_5_RS_2_pc";
		$data[17] = "GDN_5_CX3_pc";
		$data[18] = "GDN_5_6G_pc";
		$data[19] = "GDN_5_RS_1b_pc";
		$data[20] = "GDN_5_RS_2b_pc";
		$data[21] = "GDN_5_CX3b_pc";
		$data[22] = "GDN_5_6Gb_pc";
		$data[23] = "FB_5_RS_pc";
		$data[24] = "FB_5_CX3_pc";
		$data[25] = "FB_5_6G_pc";
		$data[26] = "Docomo_5_30_pc";
		$data[27] = "Docomo_5_40_pc";
		$data[28] = "TW_5_pc";
		$data[29] = "GDN_5_RS_1_sp";
		$data[30] = "GDN_5_RS_2_sp";
		$data[31] = "GDN_5_CX3_sp";
		$data[32] = "GDN_5_6G_sp";
		$data[33] = "FB_5_RS_sp";
		$data[34] = "FB_5_CX3_sp";
		$data[35] = "FB_5_6G_sp";
		$data[36] = "Docomo_5_30_sp";
		$data[37] = "Docomo_5_40_sp";
		$data[38] = "TW_5_sp";
		$data[39] = "FB_5_DEM_pc";
		$data[40] = "FB_5_DEM_sp";
		$data[41] = "GDN_7_CX3_1_pc";
		$data[42] = "GDN_7_CX3_2_pc";
		$data[43] = "GDN_7_DEM_pc";
		$data[44] = "GDN_7_CX5_pc";
		$data[45] = "GDN_7_CX3_1b_pc";
		$data[46] = "GDN_7_CX3_2b_pc";
		$data[47] = "GDN_7_DEMb_pc";
		$data[48] = "GDN_7_CX5b_pc";
		$data[49] = "FB_7_CX3_pc";
		$data[50] = "FB_7_DEM_pc";
		$data[51] = "FB_7_CX5_pc";
		$data[52] = "YH_7_CX3_1_pc";
		$data[53] = "YH_7_CX3_2_pc";
		$data[54] = "YH_7_DEM_pc";
		$data[55] = "YH_7_CX5_pc";
		$data[56] = "GDN_7_CX3_1_sp";
		$data[57] = "GDN_7_CX3_2_sp";
		$data[58] = "GDN_7_DEM_sp";
		$data[59] = "GDN_7_CX5_sp";
		$data[60] = "GDN_7_CX3_1b_sp";
		$data[61] = "GDN_7_CX3_2b_sp";
		$data[62] = "GDN_7_DEMb_sp";
		$data[63] = "GDN_7_CX5b_sp";
		$data[64] = "FB_7_CX3_sp";
		$data[65] = "FB_7_DEM_sp";
		$data[66] = "FB_7_CX5_sp";
		$data[67] = "GDN_8_CX3_pc";
		$data[68] = "GDN_8_DEM_pc";
		$data[69] = "GDN_8_CX5_pc";
		$data[70] = "GDN_8_RS_pc";
		$data[71] = "GDN_8_6G_pc";
		$data[72] = "GDN_8_CX3_sp";
		$data[73] = "GDN_8_DEM_sp";
		$data[74] = "GDN_8_CX5_sp";
		$data[75] = "GDN_8_RS_sp";
		$data[76] = "GDN_8_6G_sp";
		$data[77] = "GDN_8_CX3b_pc";
		$data[78] = "GDN_8_CX3b_sp";
		$data[79] = "GDN_8_2_CX3_pc";
		$data[80] = "GDN_8_2_CX3b_pc";
		$data[81] = "GDN_8_2_DEM_pc";
		$data[82] = "GDN_8_2_CX5_pc";
		$data[83] = "GDN_8_2_RS_pc";
		$data[84] = "GDN_8_2_6G_pc";
		$data[85] = "GDN_RE_8_2_CX3_pc";
		$data[86] = "GDN_RE_8_2_CX3b_pc";
		$data[87] = "GDN_RE_8_2_DEM_pc";
		$data[88] = "GDN_RE_8_2_CX5_pc";
		$data[89] = "GDN_RE_8_2_RS_pc";
		$data[90] = "GDN_RE_8_2_6G_pc";
		$data[91] = "GDN_8_2_CX3_sp";
		$data[92] = "GDN_8_2_CX3b_sp";
		$data[93] = "GDN_8_2_DEM_sp";
		$data[94] = "GDN_8_2_CX5_sp";
		$data[95] = "GDN_8_2_RS_sp";
		$data[96] = "GDN_8_2_6G_sp";
		$data[97] = "GDN_RE_8_2_CX3_sp";
		$data[98] = "GDN_RE_8_2_CX3b_sp";
		$data[99] = "GDN_RE_8_2_DEM_sp";
		$data[100] = "GDN_RE_8_2_CX5_sp";
		$data[101] = "GDN_RE_8_2_RS_sp";
		$data[102] = "GDN_RE_8_2_6G_sp";
		$data[103] = "GDN_9_CX3_pc";
		$data[104] = "GDN_9_DEM_pc";
		$data[105] = "GDN_9_CX5_pc";
		$data[106] = "GDN_9_RS_pc";
		$data[107] = "GDN_9_6G_pc";
		$data[108] = "GDN_RE_9_CX3_pc";
		$data[109] = "GDN_RE_9_DEM_pc";
		$data[110] = "GDN_RE_9_CX5_pc";
		$data[111] = "GDN_RE_9_RS_pc";
		$data[112] = "GDN_RE_9_6G_pc";
		$data[113] = "FB_9_CX3_pc";
		$data[114] = "FB_9_DEM_pc";
		$data[115] = "FB_9_CX5_pc";
		$data[116] = "FB_9_RS_pc";
		$data[117] = "FB_9_6G_pc";
		$data[118] = "GDN_9_CX3_sp";
		$data[119] = "GDN_9_DEM_sp";
		$data[120] = "GDN_9_CX5_sp";
		$data[121] = "GDN_9_RS_sp";
		$data[122] = "GDN_9_6G_sp";
		$data[123] = "GDN_RE_9_CX3_sp";
		$data[124] = "GDN_RE_9_DEM_sp";
		$data[125] = "GDN_RE_9_CX5_sp";
		$data[126] = "GDN_RE_9_RS_sp";
		$data[127] = "GDN_RE_9_6G_sp";
		$data[128] = "FB_9_CX3_sp";
		$data[129] = "FB_9_DEM_sp";
		$data[130] = "FB_9_CX5_sp";
		$data[131] = "FB_9_RS_sp";
		$data[132] = "FB_9_6G_sp";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//1日試乗キャンペーンID(遷移先：関東マツダTOP)
	function ref_url_list($mode=0,$selected=0){
		$ret = "";
		$data["GDN_5_RS_1b_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_5_RS_2b_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_5_CX3b_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_5_6Gb_pc"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX3_1b_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX3_2b_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_DEMb_pc"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX5b_pc"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX3_1b_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX3_2b_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_DEMb_sp"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_7_CX5b_sp"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX3_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_DEM_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX5_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_RS_pc"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_6G_pc"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX3_sp"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_DEM_sp"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX5_sp"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_RS_sp"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_6G_sp"]   = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX3b_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_8_CX3b_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_CX3_pc"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_DEM_pc"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_CX5_pc"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_RS_pc"]     = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_6G_pc"]     = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_CX3_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_DEM_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_CX5_pc"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_RS_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_6G_pc"]  = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_CX3_pc"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_DEM_pc"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_CX5_pc"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_RS_pc"]      = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_6G_pc"]      = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_CX3_sp"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_DEM_sp"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_CX5_sp"]    = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_RS_sp"]     = "http://www.kanto-mazda.com/index.html";
		$data["GDN_9_6G_sp"]     = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_CX3_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_DEM_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_CX5_sp"] = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_RS_sp"]  = "http://www.kanto-mazda.com/index.html";
		$data["GDN_RE_9_6G_sp"]  = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_CX3_sp"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_DEM_sp"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_CX5_sp"]     = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_RS_sp"]      = "http://www.kanto-mazda.com/index.html";
		$data["FB_9_6G_sp"]      = "http://www.kanto-mazda.com/index.html";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//1人1車種制限時、重複車種
	function duplication_model($mode=0,$selected=0){
		$data = NULL;
	    //$data["car00000"] = "car00017";
		//$data["car00017"] = "car00000";
		//$data["car00007"] = "car00021";
		//$data["car00021"] = "car00007";
		//$data["car00019"] = "car00005";
		//$data["car00005"] = "car00019";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}
*/
	//掲載可否(car_detail)
	function disp_flg_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "○";
		$data[0] = "×";

		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//掲載可否(car_detail)
	function disp_flg2_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "公開";
		$data[0] = "非公開";

		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//平日→デモカー・全日→モニターカー
	function week_flg_list($mode=0,$selected=0){
		$ret = "";
		$data[1] = "デモカー";
		$data[0] = "モニターカー";

		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//申込区分 add 20160122
	function offer_kind_list($mode=0,$selected=0){
		$ret = "";
		$data[0] = "Web申込み";
		$data[1] = "代車利用";
		$data[2] = "商談利用";
		$data[3] = "来電・来店対応";
		switch($mode){
			case 1:
				$ret = $data[$selected];
				break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
				break;
			default:
				$ret = $data;
				break;
		}
		return $ret;
	}

	//時リスト生成
	function camp_hour_list(){
		$data['10'] = "10";
		$data['11'] = "11";
		$data['12'] = "12";
		$data['13'] = "13";
		$data['14'] = "14";
		$data['15'] = "15";
		$data['16'] = "16";
		$data['17'] = "17";
		$data['18'] = "18";
		$data['19'] = "19";
		$data['20'] = "20";
    	return $data;
	}
/*
	//集計順リスト(福岡地区・久留米地区)
	function shop1_list($mode=0,$selected=0){
		$data["101"] = "諸岡店";//諸岡店
		$data["141"] = "久留米合川店";//久留米合川店
		$data["103"] = "東比恵店";//東比恵店
		$data["145"] = "久留米櫛原店";//久留米櫛原店
		$data["105"] = "板付店";//板付店
		$data["151"] = "甘木店";//甘木店
		$data["117"] = "桧原店";//桧原店
		$data["153"] = "八女店";//八女店
		$data["119"] = "春日店";//春日店
		$data["155"] = "大川店";//大川店
		$data["121"] = "那珂川店";//那珂川店
		$data["157"] = "大牟田店";//大牟田店
		$data["123"] = "原店";//原店
		$data["900"] = "";//
		$data["127"] = "福重店";//福重店
		$data["901"] = "";//
		$data["131"] = "糸島店";//糸島店
		$data["902"] = "";//
		$data["135"] = "福間店";//福間店
		$data["903"] = "";//
		$data["137"] = "宗像赤間店";//宗像赤間店
		$data["904"] = "";//
		$data["139"] = "新宮店";//新宮店
		$data["905"] = "";//
		$data["133"] = "松島店";//松島店
		$data["906"] = "";//
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}

	//集計順リスト(北九州・筑豊地区・大分地区・長崎地区)
	function shop2_list($mode=0,$selected=0){
		$data["321"] = "則松店";//則松店
		$data["411"] = "大分東店";//大分東店
		$data["327"] = "穴生店";//穴生店
		$data["413"] = "大分宮崎店";//大分宮崎店
		$data["333"] = "直方店";//直方店
		$data["421"] = "中津店";//中津店
		$data["347"] = "行橋店";//行橋店
		$data["423"] = "日田店";//日田店
		$data["349"] = "三萩野店";//三萩野店
		$data["425"] = "佐伯店";//佐伯店
		$data["351"] = "到津店";//到津店
		$data["427"] = "宇佐店";//宇佐店
		$data["341"] = "飯塚店";//飯塚店
		$data["429"] = "杵築店";//杵築店
		$data["353"] = "曽根店";//曽根店
		$data["415"] = "大分鶴崎店";//大分鶴崎店
		$data["907"] = "";//
		$data["417"] = "別府店";//別府店
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}
*/
	//事業部リスト
	function station_list($mode=0,$selected=0,$mystation=""){
		if($mystation){
			$data["1"] = $mystation;
			$i = 2;
		}
		else{
			$i = 1;
		}
		$data2["1"]  =  "100";	//愛知第一B
		$data2["2"]  =  "200";	//愛知第二B
		$data2["3"]  =  "300";	//愛知第三B
		$data2["4"]  =  "400";	//三重B
		$data2["5"]  =  "500";	//岐阜B
		foreach($data2 as $key => $val){
			if($data["1"] != $val){
				$data[$i] = $val;
				$i++;
			}
		}
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}
/*
	//集計店舗リスト
	function sum_shop_list($mode=0,$selected=0){
		$data["210"] = array("210");//赤迫店
		$data["220"] = array("220");//諌早店
		$data["230"] = array("230");//佐世保店
		$data["100"] = array("101","103","105","117","119","121","123","127","131","135","137","139","133");//福岡地区
		$data["150"] = array("141","145","151","153","155","157");//久留米地区
		$data["300"] = array("321","327","333","347","349","351","341","353");//北九州・筑豊地区
		$data["400"] = array("411","413","421","423","425","427","429","415","417");//大分地区
		$data["200"] = array("210","220","230");//長崎地区
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}
*/
	function visit_flg($mode=0,$selected=0){
		$data["1"]  =  "来店";
		$data["2"]  =  "未来店";
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}

	function result_status($mode=0,$selected=0){
//		$data["1"]  =  "AB-HOT";
		$data["1"]  =  "フォロー継続";
		$data["2"]  =  "フォロー中止";
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}

	function conf_flg($mode=0,$selected=0){
		$data[""]  =  "すべて";
		$data["1"]  =  "確定";
		$data["2"]  =  "未確定";
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}

	function result_close($mode=0,$selected=0){
		$data["1"]  =  "成約あり";
		$data["2"]  =  "成約なし";
		switch($mode){
			case 1:
				$ret = $data[$selected];
			break;
			case 2:
				foreach($data as $key => $val){
					if($val == $selected){
						$ret = $key;
					}
				}
			break;
			default:
				$ret = $data;
			break;
		}
		return $ret;
	}

	function headquarters_mail(){
		$data["0"]  =  "i.like.the.light.car@gmail.com";	// 江塚 様 ezuka.k@mazda-dealer.jp
		$data["1"]  =  "crm.honbu.test@gmail.com";			// 平野 様 hirano.yu@mazda-dealer.jp
		return $data;
	}

    //本部メールアドレス取得処理
    function headquarters_send_mail($DB){
        $ret = array();
        // 本部メールアドレス取得
        $sql = "select * from send_mail";
        $sql .= " where driving_shop_id = '000'";
        $sql .= " and disp_flg = '1' and del_flg = '0'";
        $sql .= " and access_kb = '4'";
        $sql .= " and driving_user_flg = '1'";
        $sql .= " and driving_mail <> ''";
        $rs =& $DB->ASExecute($sql);
        if($rs){
            while(!$rs->EOF){
                $ret[] = $rs->fields('mail');
                $rs->MoveNext();
            }
            $rs->Close();
        }
        return $ret;
    }

    function es_kind_list($mode=0,$selected=0){
        $data["E"] = "店舗権限";
        $data["S"] = "本部権限";
        switch($mode){
            case 1:
                $ret = $data[$selected];
                break;
            case 2:
                foreach($data as $key => $val){
                    if($val == $selected){
                        $ret = $key;
                    }
                }
                break;
            default:
                $ret = $data;
                break;
        }
        return $ret;
    }
}
?>