<?php

// ● 基本関数ファイル

function convert_kana_deep($value)
{
	$value = is_array($value) ?
	array_map('convert_kana_deep', $value) :
	trim(mb_convert_kana($value, "KVas","UTF-8"));

	return $value;
}

function stripslashes_deep($value)
{
	$value = is_array($value) ?
	array_map('stripslashes_deep', $value) :
	stripslashes($value);

	return $value;
}

function delete_nullbyte( $arr )
{
    if ( is_array( $arr ) ) {
        return array_map( 'delete_nullbyte', $arr );
    }
    return str_replace( "\0", "", $arr );
}

function EUCToSJIS(&$arr){
	foreach($arr as $k => $v) {
		$arr[$k] = mb_convert_encoding($v, "SJIS", "EUC-JP");
	}
}

function SJISToEUC(&$arr){
	foreach($arr as $k => $v) {
		$arr[$k] = mb_convert_encoding($v, "EUC-JP", "SJIS");
	}
}

function ErrorProc(){

}

?>