<?php

class calendar{
    public $Year=null;
    public $Month=null;
    public $Day=null;
    public $Link=null;
    public $Num=null;
    public $Next_Year=null;
    public $Next_Month=null;
    public $Back_Year=null;
    public $Back_Month=null;
	function __construct() {
        
    }

    public function Data($car_list,$shop_id,$DB) {
        $j =  date("N",mktime(0, 0, 0, $this->Month, 1, $this->Year))-1;
        // リンク、在庫数作成
//        foreach($data as $datas){
//        	$date = explode('-',$datas['date']);
//        	$this->Link[$j + $date[2]-1] = "?mode=";
//        	$this->Num[$j + $date[2]-1] = $datas['num'];
//        }
		for( $i = 1 ; $i <= 31 ; $i++ ){
			if(!checkdate($this->Month,$i,$this->Year)){
				break;
			}
			// 日付
			$this->Day[$j] = $i;
			// 予約データ取得
			$reserve_num = 0;
			$reserve_date = date("Y-m-d",mktime(0,0,0,$this->Month,$i,$this->Year));
			$sql = "select COUNT(autono) as num from reservation";
			if($car_list){
				if(is_array($car_list)){
					$first_flg = true;
					foreach($car_list as $key => $val){
						if($first_flg){
							$sql .= " where car_detail_id in (".$DB->getQStr($val);
							$first_flg = false;
						}
						else{
							$sql .= " ,".$DB->getQStr($val);
						}
					}
					if(!$first_flg){
						$sql .= " )";
					}
				}
			}
			$sql .= " and shop_id = '".$DB->getQStr($shop_id)."'";
			$sql .= " and date = '".$DB->getQStr($reserve_date)."'";
			$sql .= " and temporary_flg = '2'";
			$sql .= " and disp_flg = '1'";
			$sql .= " and del_flg = '0'";
			$rs =& $DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$reserve_num = $reserve_num + $rs->fields('num');
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$this->Num[$j] = $reserve_num;
        	$util = new util();
        	if(date("H") >= 12){
        		$day = 1;
        	}
        	else{
        		$day = 0;
    		}
    		$ret = "";
    		$holiday_cnt = 0;
    		while(1){
        		$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $day,date("Y")));
        		$ret = $util->holiday_list($DB,$shop_id,1,$start_day);
        		if(!$ret){
        			$holiday_cnt++;
        			if($holiday_cnt == 2){
	        			break;
	        		}
        		}
        		$day++;
        	}
    		$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $day + 1,date("Y")));
        	// 2件以下の予約ありなら○
        	//if($this->Num[$j] <= 2){
        		$this->Num[$j] = "○";
        	//}
        	// 3件以上の予約ありなら△
        	//else{
        	//	$this->Num[$j] = "△";
        	//}
            $now_date = date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year));
        	if( $now_date < $start_day){
        		$this->Num[$j] = "－";
        	}
			//特例テーブル 存在する場合はカレンダーに表示しない
			$sql = "select * from exception";
			if($car_list){
				if(is_array($car_list)){
					$first_flg = true;
					foreach($car_list as $key => $val){
						if($first_flg){
							$sql .= " where car_detail_id in (".$DB->getQStr($val);
							$first_flg = false;
						}
						else{
							$sql .= " ,".$DB->getQStr($val);
						}
					}
					if(!$first_flg){
						$sql .= " )";
					}
				}
			}
        	$sql .= " and shop_id='".$DB->getQStr($shop_id)."' ";
        	$sql .= " and date = '".$DB->getQStr($now_date)."' ";
        	$sql .= " and disp_flg='1' ";
        	$sql .= " and del_flg='0' ";
        	$rs =& $DB->ASExecute($sql);
        	if($rs){
        		if(!$rs->EOF){
        			$this->Num[$j] = "－";
        		}
        		$rs->Close();
        	}
        	if($util->holiday_list($DB,$shop_id,1,$now_date)){
        		$this->Num[$j] = "定休日";
//        		$weekly = date("N",mktime(0,0,0, $this->Month,$i,$this->Year));
//        		if($weekly == 6 or $weekly == 7){
//        			$this->Num[$j] = "－";
//        		}
        	}
        	else{
                $sql = "select min(start_date) as start_date,max(end_date) as end_date from car_detail";
                $first_flg = true;
                foreach($car_list as $key => $val){
                    if($first_flg){
                        $sql .= " where autono in (".$DB->getQStr($val);
                        $first_flg = false;
                    }
                    else{
                        $sql .= " ,".$DB->getQStr($val);
                    }
                }
                if(!$first_flg){
                    $sql .= " )";
                }
                $sql .= " and disp_flg = '1'";
                $sql .= " and del_flg = '0'";
                $rs =& $DB->ASExecute($sql);
                if($rs){
                    if(!$rs->EOF){
                        if($now_date < $rs->fields('start_date')){
                            $this->Num[$j] = "－";
                        }
                        if($now_date > $rs->fields('end_date')){
                            $this->Num[$j] = "－";
                        }
                    }
                    $rs->Close();
                }
            }
/*
        	// 新店9/23～予約可能に
        	if($car_detail_id == 824 or $car_detail_id == 835 or $car_detail_id == 888 or $car_detail_id == 1176 or $car_detail_id == 1175 or $car_detail_id == 1174 or $car_detail_id == 1173 or $car_detail_id == 1172){
        		if(date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) < "2016-09-23"){
        			if($this->Num[$j] != "定休日"){
        				$this->Num[$j] = "－";
        			}
        		}
        	}
        	if(date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) == "2014-08-09"){
        		$this->Num[$j] = "－";
        	}
        	//アクセラの全車種を全店舗で10月以降予約できないように（暫定）
        	$ac_flg = false;
        	//CX-5・アテンザの全車種を全店舗で1/6以降予約できないように（暫定）
        	$cxat_flg = false;
        	//CX-3を全店舗で03/06まで予約出来ないように
        	$cx3_flg = false;
			//ロードスターを全店舗で08/01まで予約出来ないように
        	$roadster_flg = false;
        	$sql = "select * from car_detail";
        	$sql .= " where autono=".$DB->getQStr($car_detail_id);
        	$rs =& $DB->ASExecute($sql);
        	if($rs){
        		if(!$rs->EOF){
//        			if($rs->fields('car_id') == "car00001" or $rs->fields('car_id') == "car00003" or $rs->fields('car_id') == "car00004" or $rs->fields('car_id') == "car00013"){
        			if($rs->fields('car_id') == "car00001"){
        				$ac_flg = true;
        			}
        			if($rs->fields('car_id') == "car00000" or $rs->fields('car_id') == "car00005" or $rs->fields('car_id') == "car00006" or $rs->fields('car_id') == "car00007" or $rs->fields('car_id') == "car00008" or $rs->fields('car_id') == "car00009" or $rs->fields('car_id') == "car00010" or $rs->fields('car_id') == "car00011" or $rs->fields('car_id') == "car00012"){
        				$cxat_flg = true;
        			}
         			if($rs->fields('car_id') == "car00022"){
        				$cx3_flg = true;
        			}
        			if($rs->fields('car_id') == "car00023" or $rs->fields('car_id') == "car00024"){
        				$roadster_flg = true;
        			}
      			}
        		$rs->Close();
        	}
        	unset($rs);
        	
        	if($ac_flg and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) >= "2014-10-01"){
        		$this->Num[$j] = "－";
        	}
        	if($cxat_flg and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) >= "2015-01-06"){
        		$this->Num[$j] = "－";
        	}
        	if($cx3_flg and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) <= "2015-02-26"){
        		$this->Num[$j] = "－";
        	}
        	if($roadster_flg and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) >= "2015-08-01"){
        		if(date("Y-m-d") < "2015-08-01"){
	        		$this->Num[$j] = "－";
	        	}
        	}
        	//CX-3・デミオを全店舗で11/01以降予約出来ないように
        	$cx3_demio_flg = false;
        	$sql = "select * from car_detail";
        	$sql .= " where autono=".$DB->getQStr($car_detail_id);
        	$sql .= " and car_id in ('car00007','car00006','car00005','car00001','car00002','car00003','car00004')";
        	$sql .= " and end_date = '2016-10-31'";
        	$rs =& $DB->ASExecute($sql);
        	if($rs){
        		if(!$rs->EOF){
        			$cx3_demio_flg = ture;
      			}
        		$rs->Close();
        	}
        	unset($rs);
        	if($cx3_demio_flg and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) >= "2016-11-01"){
        		$this->Num[$j] = "－";
        	}

        	// add 20161114 12/23～1/10まで全店舗予約不可
        	if(date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) >= "2016-12-23" and date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)) <= "2017-01-10"){
        		$this->Num[$j] = "－";
        	}
        	$all_exception_date = date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year));
        	if($all_exception_date >= "2016-04-29" and $all_exception_date <= "2016-05-08"){
        		$this->Num[$j] = "－";
        	}
        	if($all_exception_date >= "2016-08-09" and $all_exception_date <= "2016-08-12"){
        		$this->Num[$j] = "－";
        	}
*/
        	$j++;
        }
        $baseSec = mktime(0,0,0,$this->Month,1,$this->Year);
	    $this->Back_Year =  date("Y",$baseSec - 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,1,$this->Year);
	    $this->Back_Month =  date("m",$baseSec - 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,$i,$this->Year);
	    $this->Next_Year =  date("Y",$baseSec + 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,$i,$this->Year);
	    $this->Next_Month =  date("m",$baseSec + 86400);
    }
}

?>