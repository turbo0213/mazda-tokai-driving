<?php
include_once("/home/oneday_tokai/mc_apl/top.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	// 明後日試乗予約している人にメール送信する処理
	function default_proc(){
		$limit_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-3,date("Y")));
		$sql = "select * from reservation ";
		$sql .= "where disp_flg = '1' ";
		$sql .= "and del_flg = '0' ";
		$sql .= "and temporary_flg = '2' ";
		$sql .= "and conf_flg = '1' ";
		$sql .= "and conf_date <= '".$this->DB->getQStr( $limit_date )."' ";
		$sql .= "and visit_flg not in ('1','2') ";
echo $sql;
		$sql .= "order by shop_id,staff_autono,conf_date";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['shop_id'] = $rs->fields('shop_id');
				$sql2 = "select name from shop";
				$sql2 .= " where shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."' ";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					if(!$rs2->EOF){
						$dat['shop_name'] = $rs2->fields('name');
					}
					$rs2->Close();
				}
				$dat['staff_autono'] = $rs->fields('staff_autono');
				if($dat['staff_autono']){
					$sql2 = "select staff_name from send_mail";
					$sql2 .= " where autono = ".$this->DB3->getQStr($rs->fields('staff_autono'));
					$sql2 .= " and driving_user_flg = '1'";
					$rs2 =& $this->DB3->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$dat['staff_name'] = $rs2->fields('staff_name');
						}
						$rs2->Close();
					}
				}
				$dat['disp_number'] = $rs->fields('disp_number');
				$dat['car_id'] = $rs->fields('car_detail_id');
				$year = date("Y",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$month = date("m",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$day = date("d",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$dat['reserve_date'] = date("Y年m月d日",mktime(0,0,0,$month,$day,$year));
				if($rs->fields('conf_ampm') == '1'){
					$dat['reserve_date'] .= " 午前";
				}
				else if($rs->fields('conf_ampm') == '2'){
					$dat['reserve_date'] .= " 午後";
				}
                // add 20190125 car_detail_idからの車種名取得やめ
                $car1 = "";
                if($rs->fields('car1') == "atz"){
                    $car1 = "アテンザ";
                }
                else if($rs->fields('car1') == "axl"){
                    $car1 = "アクセラ";
                }
                else if($rs->fields('car1') == "cx3"){
                    $car1 = "CX-3";
                }
                else if($rs->fields('car1') == "cx5"){
                    $car1 = "CX-5";
                }
                else if($rs->fields('car1') == "cx8"){
                    $car1 = "CX-8";
                }
                else if($rs->fields('car1') == "dmo"){
                    $car1 = "デミオ";
                }
                else if($rs->fields('car1') == "rst"){
                    $car1 = "ロードスター";
                }
                else if($rs->fields('car1') == "rsrf"){
                    $car1 = "ロードスターRF";
                }
                // add 20200109 CX-30,MAZDA2,3,6追加
                else if($rs->fields('car1') == "mz2"){
                    $car1 = "MAZDA2";
                }
                else if($rs->fields('car1') == "mzf"){
                    $car1 = "MAZDA3 FASTBACK";
                }
                else if($rs->fields('car1') == "mzs"){
                    $car1 = "MAZDA3 SEDAN";
                }
                else if($rs->fields('car1') == "mz6s"){
                    $car1 = "MAZDA6 SEDAN";
                }
                else if($rs->fields('car1') == "mz6w"){
                    $car1 = "MAZDA6 WAGON";
                }
                else if($rs->fields('car1') == "cx30"){
                    $car1 = "CX-30";
                }
                $car2 = "";
                if($rs->fields('car2') == "ge"){
                    $car2 = "ガソリン";
                }
                else if($rs->fields('car2') == "de"){
                    $car2 = "ディーゼル";
                }
                else if($rs->fields('car2') == "hev"){
                    $car2 = "ハイブリッド";
                }
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == "skyx"){
                    $car2 = "SKYACTIV-X";
                }
                $car4 = "";
                if($rs->fields('car4') == "mt"){
                    $car4 = "MT";
                }
                $car5 = "";
                if($rs->fields('car5') == "turbo"){
                    $car5 = " ターボ";
                }
                if($car4){
                    if($rs->fields('car3')) {
                        $dat['car_name'] = $car1." ".$car2." ".$rs->fields('car3') . " " . $car4.$car5;
                    }
                    else{
                        $dat['car_name'] = $car1." ".$car2." ".$car4.$car5;
                    }
                }
                else{
                    $dat['car_name'] = $car1." ".$car2." ".$rs->fields('car3').$car5;
                }
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
var_dump($data_list);
		$shop_id = "";
		$staff_autono = 0;
		$f_flg = true;
        $txt = NULL;
        $mng_txt = NULL;
        $headquarters_txt = NULL;
		if($data_list){
			if(is_array($data_list)){
				foreach($data_list as $key => $val){
					if($shop_id != $data_list[$key]['shop_id']){
						if(!$f_flg){
							$this->mail_proc($data_list[$key-1],$txt);
                            $this->mng_mail_proc($data_list[$key-1],$mng_txt);
						}
						if($data_list[$key]['staff_name']){
							$txt = "【".$data_list[$key]['shop_name']."】";
							$txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $mng_txt = "【".$data_list[$key]['shop_name']."】";
                            $mng_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】";
                            $headquarters_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
						}
						else{
							$txt = "【".$data_list[$key]['shop_name']."】\r\n";
                            $mng_txt = "【".$data_list[$key]['shop_name']."】\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】\r\n";
						}
						$txt .= "結果未登録 ------------------------------------\r\n";
                        $mng_txt .= "結果未登録 ------------------------------------\r\n";
                        $headquarters_txt .= "-----------------------------------------------\r\n";
						$shop_id = $data_list[$key]['shop_id'];
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					else if($staff_autono != $data_list[$key]['staff_autono']){
						if(!$f_flg){
							$this->mail_proc($data_list[$key-1],$txt);
						}
						if($data_list[$key]['staff_name']){
							$txt = "【".$data_list[$key]['shop_name']."】";
							$txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $mng_txt .= "【".$data_list[$key]['shop_name']."】";
                            $mng_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】";
                            $headquarters_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
						}
						else{
							$txt = "【".$data_list[$key]['shop_name']."】\r\n";
                            $mng_txt .= "【".$data_list[$key]['shop_name']."】\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】\r\n";
						}
						$txt .= "結果未登録 ------------------------------------\r\n";
                        $mng_txt .= "結果未登録 ------------------------------------\r\n";
                        $headquarters_txt .= "-----------------------------------------------\r\n";
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					else if($f_flg){
						if($data_list[$key]['staff_name']){
							$txt = "【".$data_list[$key]['shop_name']."】";
							$txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $mng_txt .= "【".$data_list[$key]['shop_name']."】";
                            $mng_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】";
                            $headquarters_txt .= "担当：".$data_list[$key]['staff_name']."\r\n";
						}
						else{
							$txt = "【".$data_list[$key]['shop_name']."】\r\n";
                            $mng_txt .= "【".$data_list[$key]['shop_name']."】\r\n";
                            $headquarters_txt .= "【".$data_list[$key]['shop_name']."】\r\n";
						}
						$txt .= "結果未登録 ------------------------------------\r\n";
                        $mng_txt .= "結果未登録 ------------------------------------\r\n";
                        $headquarters_txt .= "-----------------------------------------------\r\n";
						$shop_id = $data_list[$key]['shop_id'];
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					if($f_flg){
						$f_flg = false;
					}
					$txt .= "予約番号：".$data_list[$key]['disp_number']."\r\n";
					$txt .= "試乗日時：".$data_list[$key]['reserve_date']."\r\n";
                    $mng_txt .= "予約番号：".$data_list[$key]['disp_number']."\r\n";
                    $mng_txt .= "試乗日時：".$data_list[$key]['reserve_date']."\r\n";
                    $headquarters_txt .= "予約番号：".$data_list[$key]['disp_number']."\r\n";
                    $headquarters_txt .= "試乗日時：".$data_list[$key]['reserve_date']."\r\n";
					// upd 20190125 car_detail_idからの車種名取得やめ
                    //if($data_list[$key]['car_id']){
					//	$car_data = $this->util->car_info_get($data_list[$key]['car_id'],$this->DB);
					//}
					//$txt .= "試乗車種：".$car_data['name']." ".$car_data['name2']." ".$car_data['name3']."\r\n";
                    $txt .= "試乗車種：".$data_list[$key]['car_name']."\r\n";
					$txt .= "-----------------------------------------------\r\n";
                    //$mng_txt .= "試乗車種：".$car_data['name']." ".$car_data['name2']." ".$car_data['name3']."\r\n";
                    $mng_txt .= "試乗車種：".$data_list[$key]['car_name']."\r\n";
                    $mng_txt .= "-----------------------------------------------\r\n";
                    //$headquarters_txt .= "試乗車種：".$car_data['name']." ".$car_data['name2']." ".$car_data['name3']."\r\n";
                    $headquarters_txt .= "試乗車種：".$data_list[$key]['car_name']."\r\n";
                    $headquarters_txt .= "-----------------------------------------------\r\n";
				}
			}
		}
		// スタッフ向けメール
		if($txt){
			$this->mail_proc($data_list[$key],$txt);
		}
		// 店長向けメール
        if($mng_txt){
            $this->mng_mail_proc($data_list[$key],$mng_txt);
        }
        // 本部向けメール
        if($headquarters_txt){
            $this->headquarters_mail_proc($data_list[$key],$headquarters_txt);
        }
	}

	function mail_proc($data,$txt){
var_dump($data);
		//日本語メールを送る際に必要
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		// SMTPサーバーの情報を連想配列にセット
		$params = array(
				"host" => HOST,
				"port" => PORT,
				"auth" => false,  // SMTP認証を使用する
				"username" => USERNAME,
				"password" => PASSWORD
		);
		// PEAR::Mailのオブジェクトを作成
		// ※バックエンドとしてSMTPを指定
		$mailObject = Mail::factory("sendmail", $params);
		$FromName = mb_encode_mimeheader(FROMMAILNAME);
		$from= $FromName."<".FROMMAIL.">";
		if($data['staff_autono']){
			// 担当スタッフメールアドレス取得
			$sql = "select mail from send_mail";
			$sql .= " where driving_shop_id = '".$this->DB3->getQStr($data['shop_id'])."'";
			$sql .= " and autono = ".$this->DB3->getQStr($data['staff_autono']);
			$sql .= " and access_kb = '1'";
            $sql .= " and driving_user_flg = '1'";
			$sql .= " and driving_mail is not null and driving_mail <> ''";
			$sql .= " and disp_flg = '1' and del_flg = '0'";
echo $sql;
			$rs =& $this->DB3->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$mail_address[] = $rs->fields('driving_mail');
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		// メールヘッダ情報を連想配列としてセット
		$headers = array(
				"From" => $from,
				"Subject" => mb_encode_mimeheader(TITLE12)
		);
        $recipients = NULL;
		if($mail_address){
			if(is_array($mail_address)){
				foreach($mail_address as $key => $val){
					$recipients = "i.like.the.light.car@gmail.com";
					//あとで↓を有効にする。↑は消す。
//					if($recipients){
//						$recipients .= ",".$val;
//					}
//					else{
//						$recipients = $val;
//					}
				}
			}
            $filename = LOG_DIR."no_result_mail/".date("Ymd").".log";
            $handle = fopen($filename, 'a');
            $body = str_replace("@txt@",$txt,BODY12);
            fwrite($handle, "【STAFF_SEND_DATE】".date("Y-m-d H:i:s")."\r\n");
            fwrite($handle, $body);
            // 日本語なのでエンコード
            $body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
            // sendメソッドでメールを送信
            $mailObject->send($recipients, $headers, $body);
            fclose($handle);
            chmod($filename, 0777);
		}
	}

    function mng_mail_proc($data,$txt){
var_dump($data);
        //日本語メールを送る際に必要
        mb_language("Japanese");
        mb_internal_encoding("UTF-8");
        // SMTPサーバーの情報を連想配列にセット
        $params = array(
            "host" => HOST,
            "port" => PORT,
            "auth" => false,  // SMTP認証を使用する
            "username" => USERNAME,
            "password" => PASSWORD
        );
        // PEAR::Mailのオブジェクトを作成
        // ※バックエンドとしてSMTPを指定
        $mailObject = Mail::factory("sendmail", $params);
        $FromName = mb_encode_mimeheader(FROMMAILNAME);
        $from= $FromName."<".FROMMAIL.">";
        // 店長メールアドレス取得
        $sql = "select mail from send_mail";
        $sql .= " where driving_shop_id = '".$this->DB3->getQStr($data['shop_id'])."'";
        $sql .= " and access_kb = '2'";
        $sql .= " and driving_user_flg = '1'";
        $sql .= " and driving_mail is not null and driving_mail <> ''";
        $sql .= " and disp_flg = '1' and del_flg = '0'";
        echo $sql;
        $rs =& $this->DB->ASExecute($sql);
        if($rs){
            while(!$rs->EOF){
                $mail_address[] = $rs->fields('driving_mail');
                $rs->MoveNext();
            }
            $rs->Close();
        }
var_dump($mail_address);
        // メールヘッダ情報を連想配列としてセット
        $headers = array(
            "From" => $from,
            "Subject" => mb_encode_mimeheader(TITLE12)
        );
        $recipients = NULL;
        if($mail_address){
            $filename = LOG_DIR."no_result_mail/".date("Ymd").".log";
            $handle = fopen($filename, 'a');
            if(is_array($mail_address)){
                foreach($mail_address as $key => $val){
					$recipients = "tokai.mazda.test@gmail.com";
                    //あとで↓を有効にする。↑は消す。
//                    if($recipients){
//                        $recipients .= ",".$val;
//                    }
//                    else{
//                        $recipients = $val;
//                    }
                }
            }
            $body = str_replace("@txt@",$txt,BODY12);
            fwrite($handle, "【MANAGER_SEND_DATE】".date("Y-m-d H:i:s")."\r\n");
            fwrite($handle, $body);
            // 日本語なのでエンコード
            $body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
            // sendメソッドでメールを送信
            $mailObject->send($recipients, $headers, $body);
            fclose($handle);
            chmod($filename, 0777);
        }
    }

    function headquarters_mail_proc($data,$txt){
var_dump($data);
        //日本語メールを送る際に必要
        mb_language("Japanese");
        mb_internal_encoding("UTF-8");
        // SMTPサーバーの情報を連想配列にセット
        $params = array(
            "host" => HOST,
            "port" => PORT,
            "auth" => false,  // SMTP認証を使用する
            "username" => USERNAME,
            "password" => PASSWORD
        );
        // PEAR::Mailのオブジェクトを作成
        // ※バックエンドとしてSMTPを指定
        $mailObject = Mail::factory("sendmail", $params);
        $FromName = mb_encode_mimeheader(FROMMAILNAME);
        $from= $FromName."<".FROMMAIL.">";
        // 本部メールアドレス取得
        $mail_address = $this->util->headquarters_mail();
var_dump($mail_address);
        // メールヘッダ情報を連想配列としてセット
        $headers = array(
            "From" => $from,
            "Subject" => mb_encode_mimeheader(TITLE12)
        );
        $recipients = NULL;
        if($mail_address){
            $filename = LOG_DIR."no_result_mail/".date("Ymd").".log";
            $handle = fopen($filename, 'a');
            if(is_array($mail_address)){
                foreach($mail_address as $key => $val){
					$recipients = "crm.honbu.test@gmail.com";
                    //あとで↓を有効にする。↑は消す。
//                    if($recipients){
//                        $recipients .= ",".$val;
//                    }
//                    else{
//                        $recipients = $val;
//                    }
                }
            }
            $body = str_replace("@txt@",$txt,BODY12);
            fwrite($handle, "【HEADQUARTERS_SEND_DATE】".date("Y-m-d H:i:s")."\r\n");
            fwrite($handle, $body);
            // 日本語なのでエンコード
            $body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
            // sendメソッドでメールを送信
            $mailObject->send($recipients, $headers, $body);
            fclose($handle);
            chmod($filename, 0777);
        }
    }
}
?>