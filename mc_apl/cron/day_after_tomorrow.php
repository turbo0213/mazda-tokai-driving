<?php
include_once("/home/oneday_tokai/mc_apl/top.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
	}

	function execute(){
		
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}
	
	// 明後日試乗予約している人にメール送信する処理
	function default_proc(){
		//店舗一覧取得
		$shop = array();
		$sql = "select shop_id from shop ";
		$sql .= " order by shop_id asc";
//echo $sql."\n";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$shop[] = $rs->fields('shop_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
//print_r($shop);
		if($shop and is_array($shop)){
			foreach($shop as $key => $val){
				$date = array();
				$ymd = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
				if($this->util->holiday_list($this->DB,$val,1,$ymd)){
					continue;
				}
				$day = date("d")+1;
				$ymd = "";
				$holiday_flg = 0;
				$yokujitsu_flg = 0;
				$ymd = date("Y-m-d",mktime(0,0,0,date("m"),$day,date("Y")));
				$yokujitsu_flg = $this->util->holiday_list($this->DB,$val,1,$ymd);
				$day++;
				if(!$yokujitsu_flg){
					$date[] = date("Y-m-d",mktime(0,0,0,date("m"),$day,date("Y")));
				}
				else{
					$holiday_cnt = 1;
					$ymd = date("Y-m-d",mktime(0,0,0,date("m"),$day,date("Y")));
					$holiday_flg = $this->util->holiday_list($this->DB,$val,1,$ymd);
					if($holiday_flg){
						$holiday_cnt++;
					}
					else{
						$date[] = $ymd;
					}
					$day++;
//echo $holiday_cnt."\n";
//echo $ymd."\n";
					$first_flg = true;
					$bef_holiday_flg = 0;
					while($holiday_cnt){
						$ymd = date("Y-m-d",mktime(0,0,0,date("m"),$day,date("Y")));
						$holiday_flg = $this->util->holiday_list($this->DB,$val,1,$ymd);
//echo $ymd."\n";
						if($first_flg){
							$first_flg = false;
							$bef_holiday_flg = $holiday_flg;
						}
						if($bef_holiday_flg <> $holiday_flg){
							$bef_holiday_flg = $holiday_flg;
							if($holiday_flg){
								break;
							}
						}
						if($holiday_flg){
							$day++;
							//$holiday_cnt++;
						}
						else{
							$date[] = $ymd;
							$holiday_cnt--;
							$day++;
						}
					}
				}
//echo $val."\n";
//print_r($date);
				if($date and is_array($date)){
					foreach($date as $key1 => $val1){
						// upd 20190130 turbo対応
					    $sql = "select disp_number,conf_date,conf_ampm,mail,shop_id,car_detail_id,";
						$sql .= "sei,mei,car1,car2,car3,car4,car5 from reservation ";
						$sql .= " where temporary_flg='2' ";
						$sql .= " and conf_date = '".$this->DB->getQStr($val1)."' ";
						$sql .= " and shop_id = '".$this->DB->getQStr($val)."' ";
						$sql .= " and disp_flg = '1' ";
						$sql .= " and del_flg = '0' ";
						$sql .= " and mail_send_flg = '1'";
echo $sql."\n";
						$rs =& $this->DB->ASExecute($sql);
						
						$data_list = array();
						if($rs){
							while(!$rs->EOF){
								$dat = array();
								$dat['disp_number'] = $rs->fields('disp_number');
								$dat['date'] = $rs->fields('conf_date');
								$dat['mail'] = $rs->fields('mail');
								$dat['shop_id'] = $rs->fields('shop_id');
								$dat['car_id'] = $rs->fields('car_detail_id');
								$year = date("Y",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
								$month = date("m",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
								$day = date("d",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
								$dat['reserve_date'] = date("Y年m月d日",mktime(0,0,0,$month,$day,$year));
								if($rs->fields('conf_ampm') == '1'){
									$dat['reserve_date'] .= " 午前";
								}
								else if($rs->fields('conf_ampm') == '2'){
									$dat['reserve_date'] .= " 午後";
								}
								$dat['customer_name'] = $rs->fields('sei')." ".$rs->fields('mei')." 様";
                                // add 20190125 car_detail_idからの車種名取得やめ
                                $car1 = "";
                                if($rs->fields('car1') == "atz"){
                                    $car1 = "アテンザ";
                                }
                                else if($rs->fields('car1') == "axl"){
                                    $car1 = "アクセラ";
                                }
                                else if($rs->fields('car1') == "cx3"){
                                    $car1 = "CX-3";
                                }
                                else if($rs->fields('car1') == "cx5"){
                                    $car1 = "CX-5";
                                }
                                else if($rs->fields('car1') == "cx8"){
                                    $car1 = "CX-8";
                                }
                                else if($rs->fields('car1') == "dmo"){
                                    $car1 = "デミオ";
                                }
                                else if($rs->fields('car1') == "rst"){
                                    $car1 = "ロードスター";
                                }
                                else if($rs->fields('car1') == "rsrf"){
                                    $car1 = "ロードスターRF";
                                }
                                // add 20200109 CX-30,MAZDA2,3,6追加
                                else if($rs->fields('car1') == "mz2"){
                                    $car1 = "MAZDA2";
                                }
                                else if($rs->fields('car1') == "mzf"){
                                    $car1 = "MAZDA3 FASTBACK";
                                }
                                else if($rs->fields('car1') == "mzs"){
                                    $car1 = "MAZDA3 SEDAN";
                                }
                                else if($rs->fields('car1') == "mz6s"){
                                    $car1 = "MAZDA6 SEDAN";
                                }
                                else if($rs->fields('car1') == "mz6w"){
                                    $car1 = "MAZDA6 WAGON";
                                }
                                else if($rs->fields('car1') == "cx30"){
                                    $car1 = "CX-30";
                                }
                                $car2 = "";
                                if($rs->fields('car2') == "ge"){
                                    $car2 = "ガソリン";
                                }
                                else if($rs->fields('car2') == "de"){
                                    $car2 = "ディーゼル";
                                }
                                else if($rs->fields('car2') == "hev"){
                                    $car2 = "ハイブリッド";
                                }
                                // add 20200109 SKYACTIV-X対応
                                else if($rs->fields('car2') == "skyx"){
                                    $car2 = "SKYACTIV-X";
                                }
                                $car4 = "";
                                if($rs->fields('car4') == "mt"){
                                    $car4 = "MT";
                                }
                                $car5 = "";
                                if($rs->fields('car5') == "turbo"){
                                    $car5 = " ターボ";
                                }
                                if($car4){
                                    if($rs->fields('car3')) {
                                        $dat['car_name'] = $car1." ".$car2." ".$rs->fields('car3') . " " . $car4.$car5;
                                    }
                                    else{
                                        $dat['car_name'] = $car1." ".$car2." ".$car4.$car5;
                                    }
                                }
                                else{
                                    $dat['car_name'] = $car1." ".$car2." ".$rs->fields('car3').$car5;
                                }
                                $data_list[] = $dat;
								$rs->MoveNext();
							}
							$rs->Close();
						}
print_r($data_list);
						$filename = LOG_DIR."day_after_tomorrow/".date("Ymd").".log";
						$handle = fopen($filename, 'a');
						//日本語メールを送る際に必要
						mb_language("Japanese");
						mb_internal_encoding("UTF-8");

						// SMTPサーバーの情報を連想配列にセット
						$params = array(
								"host" => HOST,
								"port" => PORT,
								"auth" => false,  // SMTP認証を使用する
								"username" => USERNAME,
								"password" => PASSWORD
						);

						// PEAR::Mailのオブジェクトを作成
						// ※バックエンドとしてSMTPを指定
						$mailObject = Mail::factory("sendmail", $params);

						$FromName = mb_encode_mimeheader(FROMMAILNAME);
						$from= $FromName."<".FROMMAIL.">";

						if($data_list and is_array($data_list)){
							foreach($data_list as $key2 => $val2){
								if($val2['shop_id']){
									$shop_data = $this->util->shop_info_get($val2['shop_id'],$this->DB);
								}
                                // del 20190125 car_detail_idからの車種名取得やめ
                                //if($val2['car_id']){
                                //	$car_data = $this->util->car_info_get($val2['car_id'],$this->DB);
                                //}
								//$reserve_date = substr($val2['date'],0,4)."年".substr($val2['date'],5,2)."月".substr($val2['date'],8,2)."日";
								// 送信先のメールアドレス
								//$recipients = "kyushu.mazda.test@gmail.com";
								$recipients = $val2['mail'];
								// メールヘッダ情報を連想配列としてセット
								$headers = array(
										"From" => $from,
										"Subject" => mb_encode_mimeheader(TITLE7)
								);
								// メール本文
								$body = str_replace("@number@",$val2['disp_number'],BODY7);
								$body = str_replace("@shop_name1@",$shop_data['name'],$body);
								$body = str_replace("@shop_name2@",$shop_data['name'],$body);
								$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
								$body = str_replace("@reserve_date@",$val2['reserve_date'],$body);
                                // upd 20190125 car_detail_idからの車種名取得やめ
                                $body = str_replace("@reserve_car@",$val2['car_name'],$body);
								//$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
								//$body .= $val2['mail'];
								fwrite($handle, "【CUSTOMER_SEND_DATE】".date("Y-m-d H:i:s")."\r\n");
								fwrite($handle, $body);
								// 日本語なのでエンコード
								$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
								// sendメソッドでメールを送信
								$mailObject->send($recipients, $headers, $body);
								
								// 本部メールアドレス取得
								$mail_address = array();
								$sql = "select * from send_mail";
								$sql .= " where driving_shop_id = '000'";
                                $sql .= " and driving_user_flg = '1'";
								$sql .= " and disp_flg = '1' and del_flg = '0'";
								$rs =& $this->DB3->ASExecute($sql);
								if($rs){
									while(!$rs->EOF){
										//$mail_address[] = $rs->fields('staff_name');
										//あとで↓を有効にする
										$mail_address[] = $rs->fields('driving_mail');
										$rs->MoveNext();
									}
									$rs->Close();
								}
								// 店舗スタッフメールアドレス取得
								$sql = "select * from send_mail";
								$sql .= " where driving_shop_id = '".$this->DB3->getQStr($val2['shop_id'])."'";
                                $sql .= " and driving_user_flg = '1'";
								$sql .= " and disp_flg = '1' and del_flg = '0'";
								$rs =& $this->DB3->ASExecute($sql);
								if($rs){
									while(!$rs->EOF){
										//$mail_address[] = $rs->fields('staff_name');
										//あとで↓を有効にする
										$mail_address[] = $rs->fields('driving_mail');
										$rs->MoveNext();
									}
									$rs->Close();
								}
								// メールヘッダ情報を連想配列としてセット
								$headers = array(
										"From" => $from,
										"Subject" => mb_encode_mimeheader(TITLE8)
								);
								if($mail_address and is_array($mail_address)){
									foreach($mail_address as $key3 => $val3){
										//$recipients = "kyushu.mazda.test@gmail.com";
										//あとで↓を有効にする。↑は消す。
										$recipients = $val3;
										//あとで消す。
										$body = str_replace("@number@",$val2['disp_number'],BODY8);
										$body = str_replace("@shop_name1@",$shop_data['name'],$body);
										$body = str_replace("@shop_name2@",$shop_data['name'],$body);
										$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
										$body = str_replace("@reserve_date@",$val2['reserve_date'],$body);
                                        // add 20190125 car_detail_idからの車種名取得やめ
                                        $body = str_replace("@reserve_car@",$val2['car_name'],$body);
										//$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
										$body = str_replace("@customer_name@",$val2['customer_name'],$body);
										//$body = str_replace("@staff_name@",$val3,$body);
										//$body .= $val3;
										fwrite($handle, "【STAFF_SEND_DATE】".date("Y-m-d H:i:s")."\r\n");
										fwrite($handle, $body);
										// 日本語なのでエンコード
										$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
										// sendメソッドでメールを送信
										$mailObject->send($recipients, $headers, $body);
									}
								}
							}
						}
						fclose($handle);
						chmod($filename, 0777);
					}
				}
			}
		}
	}
}

?>