<?php
include_once("/home/oneday_tokai/mc_apl/top.php");

require_once("Mail.php");
require_once("Mail/mime.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
	}
	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	// 24時間以内に試乗予約しているデータを本部にメール送信する
	function default_proc(){
        $filename = BASE_DIR."dat/reserve".date("Ymd").".csv";
        $handle = fopen($filename, 'w');
        $title = '"事業部","店舗","予約日時","予約番号","予約希望日1","","予約希望日2","","予約希望日3","","予約確定日","","車種","","","姓","名","セイ","メイ","連絡先","連絡可能な時間帯","メールアドレス","意見・要望"';
        $title .= "\r\n";
        fwrite($handle, mb_convert_encoding($title, "SJIS-win", "auto"));
        $start = date("Y-m-d H:i:s",mktime(8,0,0,date("m"),date("d")-1,date("Y")));
        $end = date("Y-m-d H:i:s",mktime(7,59,59,date("m"),date("d"),date("Y")));
	    $sql = "select r.*,s.station_name as station_name,s.name as shop_name from reservation as r,shop as s ";
	    $sql .= "where r.shop_id = s.shop_id ";
	    $sql .= "and r.create_date between '".$this->DB->getQStr($start)."' and '".$this->DB->getQStr($end)."' ";
	    $sql .= "and r.disp_flg = '1' and r.del_flg = '0' ";
	    $sql .= "and s.disp_flg = '1' and s.del_flg = '0' ";
        $sql .= "and r.temporary_flg = '2' ";
	    $sql .= "order by s.station_code,r.shop_id,r.create_date";
        $rs =& $this->DB->ASExecute($sql);
        if($rs){
            while(!$rs->EOF){
                $data = "";
                $data .= $rs->fields('station_name').",";
                $data .= $rs->fields('shop_name').",";
                $create_date = substr($rs->fields('create_date'),0,16);
                $data .= $create_date.",";
                $data .= $rs->fields('disp_number').",";
                $data .= $rs->fields('date').",";
                $ampm = "";
                if($rs->fields('ampm') == '1'){
                    $ampm = "午前";
                }
                else if($rs->fields('ampm') == '2'){
                    $ampm = "午後";
                }
                $data .= $ampm.",";
                $data .= $rs->fields('date2').",";
                $ampm = "";
                if($rs->fields('ampm2') == '1'){
                    $ampm = "午前";
                }
                else if($rs->fields('ampm2') == '2'){
                    $ampm = "午後";
                }
                $data .= $ampm.",";
                $data .= $rs->fields('date3').",";
                $ampm = "";
                if($rs->fields('ampm3') == '1'){
                    $ampm = "午前";
                }
                else if($rs->fields('ampm3') == '2'){
                    $ampm = "午後";
                }
                $data .= $ampm.",";
                $data .= $rs->fields('conf_date').",";
                $ampm = "";
                if($rs->fields('conf_ampm') == '1'){
                    $ampm = "午前";
                }
                else if($rs->fields('conf_ampm') == '2'){
                    $ampm = "午後";
                }
                $data .= $ampm.",";
                $car1 = "";
                if($rs->fields('car1') == "atz"){
                    $car1 = "アテンザ";
                }
                else if($rs->fields('car1') == "axl"){
                    $car1 = "アクセラ";
                }
                else if($rs->fields('car1') == "cx3"){
                    $car1 = "CX-3";
                }
                else if($rs->fields('car1') == "cx30"){
                    $car1 = "CX-30";
                }
                else if($rs->fields('car1') == "cx5"){
                    $car1 = "CX-5";
                }
                else if($rs->fields('car1') == "cx8"){
                    $car1 = "CX-8";
                }
                else if($rs->fields('car1') == "dmo"){
                    $car1 = "デミオ";
                }
                else if($rs->fields('car1') == "mz2"){
                    $car1 = "MAZDA2";
                }
                else if($rs->fields('car1') == "mz6s"){
                    $car1 = "MAZDA6 SEDAN";
                }
                else if($rs->fields('car1') == "mz6w"){
                    $car1 = "MAZDA6 WAGON";
                }
                else if($rs->fields('car1') == "mzf"){
                    $car1 = "MAZDA3 FASTBACK";
                }
                else if($rs->fields('car1') == "mzs"){
                    $car1 = "MAZDA3 SEDAN";
                }
                else if($rs->fields('car1') == "rst"){
                    $car1 = "ロードスター";
                }
                else if($rs->fields('car1') == "rsrf"){
                    $car1 = "ロードスターRF";
                }
                // add 20200109 CX-30,MAZDA2,3,6追加
                else if($rs->fields('car1') == "mz2"){
                    $car1 = "MAZDA2";
                }
                else if($rs->fields('car1') == "mzf"){
                    $car1 = "MAZDA3 FASTBACK";
                }
                else if($rs->fields('car1') == "mzs"){
                    $car1 = "MAZDA3 SEDAN";
                }
                else if($rs->fields('car1') == "mz6s"){
                    $car1 = "MAZDA6 SEDAN";
                }
                else if($rs->fields('car1') == "mz6w"){
                    $car1 = "MAZDA6 WAGON";
                }
                else if($rs->fields('car1') == "cx30"){
                    $car1 = "CX-30";
                }
                $data .= $car1.",";
                $car2 = "";
                if($rs->fields('car2') == "ge"){
                    $car2 = "ガソリン";
                }
                else if($rs->fields('car2') == "de"){
                    $car2 = "ディーゼル";
                }
                else if($rs->fields('car2') == "hev"){
                    $car2 = "ハイブリッド";
                }
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == "skyx"){
                    $car2 = "SKYACTIV-X";
                }
                $data .= $car2.",";
                $car4 = "";
                if($rs->fields('car4') == "mt"){
                    $car4 = "MT";
                }
                $car5 = "";
                if($rs->fields('car5') == "turbo"){
                    $car5 = " ターボ";
                }
                if($car4){
                    if($rs->fields('car3')) {
                        $data .= $rs->fields('car3') . " " . $car4.$car5.",";
                    }
                    else{
                        $data .= $car4.$car5.",";
                    }
                }
                else{
                    $data .= $rs->fields('car3').$car5.",";
                }
                $data .= $rs->fields('sei').",";
                $data .= $rs->fields('mei').",";
                $data .= $rs->fields('sei_kana').",";
                $data .= $rs->fields('mei_kana').",";
                $data .= $rs->fields('tel').",";
                $data .= $rs->fields('v_schedule_time').",";
                $data .= $rs->fields('mail').",";
                $data .= '"'.$rs->fields('comment').'"';
                $data .= "\r\n";
                fwrite($handle, mb_convert_encoding($data, "SJIS-win", "auto"));
                $rs->MoveNext();
            }
            $rs->Close();
        }
		//日本語メールを送る際に必要
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		// SMTPサーバーの情報を連想配列にセット
		$params = array(
				"host" => HOST,
				"port" => PORT,
				"auth" => false
		);
		// PEAR::Mailのオブジェクトを作成
		// ※バックエンドとしてSMTPを指定
		$mailObject = Mail::factory("sendmail", $params);
		$FromName = mb_encode_mimeheader(FROMMAILNAME);
		$from= $FromName."<".FROMMAIL.">";
		$title = mb_encode_mimeheader("「オンライン試乗予約」予約データ送付 ".date('Y/m/d'));
		$headquarters = $this->util->headquarters_send_mail($this->DB3);
		$headers = array();
		$headers['From'] = $from;
        $mail_address = "";
		if($headquarters){
		    if(is_array($headquarters)){
		        foreach($headquarters as $key => $val){
                    if($mail_address){
                        $mail_address .= ",".$val;
                    }
                    else{
                        $mail_address = $val;
                    }
                }
            }
        }
        //$headers['To'] = $mail_address;
        // テスト用　最終上を有効にする
        $headers['To'] = "tokai.mazda.test@gmail.com,crm.honbu.test@gmail.com,system@359lab.com";
		$headers['Subject'] = $title;
        $body  = "オンライン試乗予約サイトにおいて、\r\n";
        $body .= "24時間以内に予約されたデータを添付します。\r\n\r\n";
        $body .= "----------------------------------------------------------------------\r\n";
        $body .= "東海マツダ\r\n";
        // テスト用
        $body .= $mail_address;
		$body = mb_convert_encoding($body, "ISO-2022-JP", "auto");
		$mimeObject = new Mail_Mime("¥n");
		$mimeObject -> setTxtBody($body);
		$mimeObject -> addAttachment($filename, "text/csv");
		$bodyParam = array(
		  "head_charset" => "ISO-2022-JP",
		  "text_charset" => "ISO-2022-JP"
		);
		$mime = new Mail_mime();
		$mime->addAttachment($filename, "text/csv");
		$mime->setTXTBody($body);
		$body = $mime->get($bodyParam);
		$headers = $mime->headers($headers);
		// sendメソッドでメールを送信
		$mailObject->send($mail_address, $headers, $body);
		fclose($handle);
		chmod($filename, 0777);
	}
}
?>