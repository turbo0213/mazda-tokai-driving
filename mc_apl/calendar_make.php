<?php

class calendar{

    public $Year=null;
    public $Month=null;
    public $Day=null;
    public $Link=null;
    public $Num=null;
    public $Next_Year=null;
    public $Next_Month=null;
    public $Back_Year=null;
    public $Back_Month=null;
	
	function __construct() {
        
    }
    
    public function Data($car_list,$shop_id,$DB) {
        $j =  date("N",mktime(0, 0, 0, $this->Month, 1, $this->Year))-1;
        // リンク、在庫数作成
//        foreach($data as $datas){
//        	$date = explode('-',$datas['date']);
//        	$this->Link[$j + $date[2]-1] = "?mode=";
//        	$this->Num[$j + $date[2]-1] = $datas['num'];
//        }
        for( $i = 1 ; $i <= 31 ; $i++ ){
        	if( !checkdate ( $this->Month,$i,$this->Year ) ){
        		break;
        	}
        	// 日付
        	$this->Day[$j] = $i;
        	$this->DayFormat[$j] = sprintf('%02d', $i);
			// 予約データ取得
			$reserve_num = 0;
			$reserve_date = date("Y-m-d",mktime(0,0,0,$this->Month,$i,$this->Year));
			if($car_list){
				if(is_array($car_list)){
					$sql = "select COUNT(autono) as num from reservation";
					$first_flg = true;
					foreach($car_list as $key => $val){
						if($first_flg){
							$sql .= " where car_detail_id in (".$DB->getQStr($val);
							$first_flg = false;
						}
						else{
							$sql .= " ,".$DB->getQStr($val);
						}
					}
					if(!$first_flg){
						$sql .= " )";
					}
					$sql .= " and shop_id = '".$DB->getQStr($shop_id)."'";
                    $sql .= " and ((conf_flg = '1'";
					$sql .= " and conf_date = '".$DB->getQStr($reserve_date)."')";
                    $sql .= " or (conf_flg = '0' and date = '".$DB->getQStr($reserve_date)."')";
                    $sql .= " or (conf_flg = '0' and date2 = '".$DB->getQStr($reserve_date)."')";
                    $sql .= " or (conf_flg = '0' and date3 = '".$DB->getQStr($reserve_date)."'))";
					$sql .= " and temporary_flg = '2'";
					$sql .= " and disp_flg = '1'";
					$sql .= " and del_flg = '0'";
					$rs =& $DB->ASExecute($sql);
					if($rs){
						while(!$rs->EOF){
							$reserve_num = $reserve_num + $rs->fields('num');
							$rs->MoveNext();
						}
						$rs->Close();
					}
                    $car1 = "";
                    $car2 = "";
                    $car3 = "";
                    $car4 = "";
                    $sql = "select car1,car2,car3,car4 from car_detail";
                    $first_flg = true;
                    foreach($car_list as $key => $val){
                        if($first_flg){
                            $sql .= " where autono in (".$DB->getQStr($val);
                            $first_flg = false;
                        }
                        else{
                            $sql .= " ,".$DB->getQStr($val);
                        }
                    }
                    if(!$first_flg){
                        $sql .= " )";
                    }
                    $sql .= " and shop_id = '".$DB->getQStr($shop_id)."'";
                    $sql .= " and disp_flg = '1'";
                    $sql .= " and del_flg = '0'";
                    $sql .= " and start_date <= '".$DB->getQStr($reserve_date)."'";
                    $sql .= " and end_date >= '".$DB->getQStr($reserve_date)."'";
                    $sql .= " order by autono desc limit 1";
                    $rs =& $DB->ASExecute($sql);
                    if($rs){
                        if(!$rs->EOF){
                            $car1 = $rs->fields('car1');
                            $car2 = $rs->fields('car2');
                            $car3 = $rs->fields('car3');
                            $car4 = $rs->fields('car4');
                        }
                        $rs->Close();
                    }
                    if($car1 and $car2){
                        $sql = "select COUNT(autono) as num from reservation";
                        $sql .= " where shop_id = '".$DB->getQStr($shop_id)."'";
                        $sql .= " and ((conf_flg = '1'";
                        $sql .= " and conf_date = '".$DB->getQStr($reserve_date)."')";
                        $sql .= " or (conf_flg = '0' and date = '".$DB->getQStr($reserve_date)."')";
                        $sql .= " or (conf_flg = '0' and date2 = '".$DB->getQStr($reserve_date)."')";
                        $sql .= " or (conf_flg = '0' and date3 = '".$DB->getQStr($reserve_date)."'))";
                        $sql .= " and temporary_flg = '2'";
                        $sql .= " and disp_flg = '1'";
                        $sql .= " and del_flg = '0'";
                        $sql .= " and car1 = '".$DB->getQStr($car1)."'";
                        $sql .= " and car2 = '".$DB->getQStr($car2)."'";
                        if($car3) {
                            $sql .= " and car3 = '" . $DB->getQStr($car3) . "'";
                        }
                        if($car4) {
                            $sql .= " and car4 = '" . $DB->getQStr($car4) . "'";
                        }
                        $rs =& $DB->ASExecute($sql);
                        if($rs){
                            if(!$rs->EOF){
                                $reserve_num = $reserve_num + $rs->fields('num');
                                $rs->MoveNext();
                            }
                            $rs->Close();
                        }
                    }
				}
			}
			if($reserve_num <= 0){
				$this->Num[$j] = "掲載可";
			}
        	if(date("H") >= 12){
        		$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 2,date("Y")));
        	}
        	else{
        		$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 1,date("Y")));
        	}
        	$util = new util();
        	//平日のみ車種の場合祝祭日を取得
//			if($week_flg == "1"){
//				if($util->pday_list(1,date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)))){
//	        		$this->Num[$j] = "－";
//	        	}
//			}
        	if($util->holiday_list($DB,$shop_id,1,date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)))){
        		$this->Num[$j] = "定休日";
        	}
        	//掲載不可表示用
			$now_date = date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year));
			$now_w = date("w",mktime(0,0,0, $this->Month,$i,$this->Year));
			if($_SESSION["exception_date_list"]){
				//exceptionに存在すれば
				if(in_array($now_date, $_SESSION["exception_date_list"])){
					$this->Num[$j] = "掲載不可";
				}
				//exceptionに存在しなければ
				else{
					//試乗開始日終了日内ならば
//					if( $now_date >= $start_end_day[0] && $now_date <= $start_end_day[1]){
						//定休日でない
						if(!$util->holiday_list($DB,$shop_id,1,$now_date)){
//							$this->Num[$j] = "掲載可";
//							if($week_flg == "1"){
//								if($util->pday_list(1,date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)))){
//					        		$this->Num[$j] = "－";
//					        	}
//					        	if($now_w == 0 or $now_w == 6){
//									$this->Num[$j] = "－";
//								}
//							}
                            $sql = "select min(start_date) as start_date,max(end_date) as end_date from car_detail";
                            $first_flg = true;
                            if($car_list) {
                                if(is_array($car_list)) {
                                    foreach ($car_list as $key => $val) {
                                        if ($first_flg) {
                                            $sql .= " where autono in (" . $DB->getQStr($val);
                                            $first_flg = false;
                                        } else {
                                            $sql .= " ," . $DB->getQStr($val);
                                        }
                                    }
                                    if (!$first_flg) {
                                        $sql .= " )";
                                    }
                                    $sql .= " and disp_flg = '1'";
                                    $sql .= " and del_flg = '0'";
                                    $rs =& $DB->ASExecute($sql);
                                    if ($rs) {
                                        if (!$rs->EOF) {
                                            if ($now_date < $rs->fields('start_date')) {
                                                $this->Num[$j] = "－";
                                            }
                                            if ($now_date > $rs->fields('end_date')) {
                                                $this->Num[$j] = "－";
                                            }
                                        }
                                        $rs->Close();
                                    }
                                }
                            }
						}
//					}
				}
			}
			else{
//				if( $now_date >= $start_end_day[0] && $now_date <= $start_end_day[1]){
					//定休日でない
					if(!$util->holiday_list($DB,$shop_id,1,$now_date)){
//						$this->Num[$j] = "掲載可";
//						if($week_flg == "1"){
//							if($util->pday_list(1,date("Y-m-d",mktime(0,0,0, $this->Month,$i,$this->Year)))){
//				        		$this->Num[$j] = "－";
//				        	}
//				        	if($now_w == 0 or $now_w == 6){
//								$this->Num[$j] = "－";
//							}
//						}
                        $sql = "select min(start_date) as start_date,max(end_date) as end_date from car_detail";
                        $first_flg = true;
                        if($car_list) {
                            if (is_array($car_list)) {
                                foreach ($car_list as $key => $val) {
                                    if ($first_flg) {
                                        $sql .= " where autono in (" . $DB->getQStr($val);
                                        $first_flg = false;
                                    } else {
                                        $sql .= " ," . $DB->getQStr($val);
                                    }
                                }
                                if (!$first_flg) {
                                    $sql .= " )";
                                }
                                $sql .= " and disp_flg = '1'";
                                $sql .= " and del_flg = '0'";
                                $rs =& $DB->ASExecute($sql);
                                if ($rs) {
                                    if (!$rs->EOF) {
                                        if ($now_date < $rs->fields('start_date')) {
                                            $this->Num[$j] = "－";
                                        }
                                        if ($now_date > $rs->fields('end_date')) {
                                            $this->Num[$j] = "－";
                                        }
                                    }
                                    $rs->Close();
                                }
                            }
                        }
					}
//				}
			}
//			if($reservation_flg){//if($this->Num[$j] <= 0){
//        		$this->Num[$j] = "×";
//        	}
        	$j++;
        }
        $baseSec = mktime(0,0,0,$this->Month,1,$this->Year);
	    $this->Back_Year =  date("Y",$baseSec - 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,1,$this->Year);
	    $this->Back_Month =  date("m",$baseSec - 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,$i,$this->Year);
	    $this->Next_Year =  date("Y",$baseSec + 86400);
	    
	    $baseSec = mktime(0,0,0,$this->Month,$i,$this->Year);
	    $this->Next_Month =  date("m",$baseSec + 86400);
    }
}

?>