<?php

// ユーザーの定義したエラーハンドリング関数
function ASErrorHandler($errno, $errmsg, $filename, $linenum, $vars) 
{
   // エラーエントリのタイムスタンプ
   $dt = date("Y-m-d H:i:s (T)");

   // define an assoc array of error string
   // in reality the only entries we should
   // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
   // E_USER_WARNING and E_USER_NOTICE
   $errortype = array (
               E_WARNING        => "Warning",
               E_NOTICE          => "Notice",
               E_USER_ERROR      => "User Error",
               E_USER_WARNING    => "User Warning",
               E_USER_NOTICE    => "User Notice",
               E_STRICT          => "Runtime Notice"
               );

   // set of errors for which a var trace will be saved
   $output_errors = array(E_WARNING, E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);
   
   $err = "<errorentry>\n";
   $err .= "\t<datetime>" . $dt . "</datetime>\n";
   $err .= "\t<errornum>" . $errno . "</errornum>\n";
   $err .= "\t<errortype>" . $errortype[$errno] . "</errortype>\n";
   $err .= "\t<errormsg>" . $errmsg . "</errormsg>\n";
   $err .= "\t<scriptname>" . $filename . "</scriptname>\n";
   $err .= "\t<scriptlinenum>" . $linenum . "</scriptlinenum>\n";
   $err .= "</errorentry>\n\n";

	if(in_array($errno, $output_errors)){

		//echo $err;
		// エラーログを保存
		error_log($err, 3, LOG_DIR."user_error.log");
		exit;

		//エラー画面表示 
		//header("Location:/manager/error.php");
	}

}

?>