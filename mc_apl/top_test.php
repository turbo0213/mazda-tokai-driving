<?php

// ● ディレクトリ設定
define('BASE_DIR', '/var/www/hp/mc_apl/');
define('LOG_DIR',BASE_DIR.'logs/');

define("SITE_ENCODE", "UTF-8");

setlocale(LC_ALL,'ja_JP.UTF-8');
ini_set("date.timezone","Asia/Tokyo");

// ● セッション設定
ini_set('session.save_handler','files');
ini_set('session.save_path', BASE_DIR . 'session');
ini_set('session.use_cookies',1);
ini_set('session.gc_maxlifetime', 900);

ini_set('session.cache_limiter', 'nocache');

session_start();

// ● 言語設定
mb_language("Japanese");
mb_internal_encoding("UTF-8");

// ● エラー設定
ini_set('error_reporting','E_ALL & ~E_NOTICE');
ini_set('display_errors',1);

ini_set('error_log',BASE_DIR.'logs/php_error.log');


// ● その他の設定

// SMTPサーバー名
define('HOST', "localhost");
// ポート番号
define('PORT', "25");
// SMTPのユーザー名
define('USERNAME', "mzd-ls");
// SMTPのパスワード
define('PASSWORD', "bF8ehggjm");
// 送信者
define('FROMMAIL', "tokai-mazda@driving.tokai-mazda.co.jp");
define('FROMMAILNAME', "東海マツダ");
/*****************************/
/* お客様用 仮登録完了メール */
/*****************************/
// 件名
define('TITLE', "「1日試乗」ご予約");
// メール本文 仮登録完了メール
$body  = "この度は「東海マツダ1日試乗」にご応募ありがとうございます。\r\n";
$body .= "下記のURLにアクセスして頂き、ご予約の完了をお願い致します。\r\n";
$body .= "※ご予約のボタンクリック後、60分以内にアクセスし、予約登録を完了してください。60分以上経ちますとご予約が無効となります。予めご了承ください。\r\n\r\n";
$body .= "https://driving.tokai-mazda.co.jp/mail.php?mode=entry&number=@number@&D=@ref@";
$body .= "\r\n\r\n\r\n";
$body .= "--------------------\r\n";
$body .= "東海マツダ  @shop_name2@\r\n";
$body .= "電話番号：@shop_tel@\r\n";
define('BODY', $body);
/*****************************/
/* お客様用 本登録完了メール */
/*****************************/
// 件名
define('TITLE2', "「オンライン試乗予約」お申込受付完了");
// メール本文 本登録完了メール
$body2  = "この度は、「東海マツダオンライン試乗予約」に申込み頂き、誠にありがとうございます。\r\n\r\n";
$body2 .= "お申込受付が完了いたしました。\r\n";
$body2 .= "お客様のお申込受付内容は以下になります。\r\n\r\n";
$body2 .= "□お申込受付内容　---------------\r\n";
$body2 .= "・第1希望：@reserve_date1@\r\n";
$body2 .= "@reserve_date2@";
$body2 .= "@reserve_date3@";
//$body2 .= "・第2希望：@reserve_date2@\r\n";
//$body2 .= "・第3希望：@reserve_date3@\r\n";
$body2 .= "・店舗：@shop_name2@\r\n";
$body2 .= "・車種：@reserve_car@\r\n";
$body2 .= "・申込受付番号：@number@\r\n";
$body2 .= "---------------------------------\r\n\r\n";
//$body2 .= "\r\n\r\n";
$body2 .= "※弊社担当スタッフよりご連絡させていただき、日程の確定をさせていただきます。\r\n";
//$body2 .= "※誠に勝手ではございますが、弊社は下記のとおり休暇させていただきます。\r\n";
//$body2 .= "　休暇期間：5月5日（火）～5月8日（金）\r\n";
//$body2 .= "　期間中はご不便をおかけしますが、ご了承のほどよろしくお願いいたします。\r\n";
//$body2 .= "　尚、休暇中にいただいたお問い合わせにつきましては、5月9日(土)以降順次対応させていただきます。\r\n\r\n";
$body2 .= "※確定しました予約日の前々日（弊社定休日と重なる場合はその前日）には、 再度ご確認のメールを配信させていただきます。\r\n";
$body2 .= "※申込受付番号はお申込受付内容の変更、キャンセルの際に必要となりますので、お控え頂き、大切に保管ください。\r\n";
$body2 .= "※お申込受付内容の変更、キャンセルは、下記URLにアクセスしてください。\r\n";
if($_SESSION['sp_flg'] == '1'){
	$body2 .= "　https://driving.tokai-mazda.co.jp/sp/edit.php";
}
else{
	$body2 .= "　https://driving.tokai-mazda.co.jp/edit.php";
}
$body2 .= "\r\n\r\n";
$body2 .= "当日は、運転免許証（日本国内で有効なもの）を必ずご持参ください。\r\n\r\n";
if($_SESSION['sp_flg'] == '1' and $_SESSION['param'] == 'ybp1808'){
	$body2 .= "SUV大試乗会開催中！\r\n";
	$body2 .= "スペシャルクーポンを下記URLよりご確認ください。\r\n";
	$body2 .= "　https://driving.tokai-mazda.co.jp/sp/cpimg_201808.html";
	$body2 .= "\r\n\r\n";
}
elseif($_SESSION['sp_flg'] == '1' and $_SESSION['param3'] == 'ybp1812'){
    $body2 .= "SUV大試乗会開催中！\r\n";
    $body2 .= "スペシャルクーポンを下記URLよりご確認ください。\r\n";
    $body2 .= "　https://driving.tokai-mazda.co.jp/sp/cpimg_201812.html";
    $body2 .= "\r\n\r\n";
}
$body2 .= "それでは、ご来店を心よりお待ち申し上げております。\r\n\r\n";
$body2 .= "--------------------\r\n\r\n";
$body2 .= "東海マツダ @shop_name2@\r\n";
$body2 .= "電話番号：@shop_tel@\r\n";
define('BODY2', $body2);
/*****************************************/
/* 本部・店舗スタッフ用 本登録完了メール */
/*****************************************/
// 件名
define('TITLE4', "「オンライン試乗予約」申込受付登録");
// メール本文 本登録完了メール
$body4  = "「オンライン試乗予約」への申込みがありました。\r\n";
$body4 .= "1.　管理画面よりお客様情報、試乗にあたっての要望を確認してください。\r\n";
$body4 .= "https://driving.tokai-mazda.co.jp/oneday_admin/login.php?number=@number@";
$body4 .= "\r\n";
$body4 .= "2.　担当者を決定し、お客様に連絡して日時を確定してください。\r\n\r\n";
$body4 .= "申込受付内容------------------------------------------------------------\r\n";
$body4 .= "・申込受付番号：@number@\r\n";
$body4 .= "・第1希望：@reserve_date1@\r\n";
$body4 .= "@reserve_date2@";
$body4 .= "@reserve_date3@";
//$body4 .= "第2希望：@reserve_date2@\r\n";
//$body4 .= "第3希望：@reserve_date3@\r\n";
$body4 .= "・店舗：@shop_name2@\r\n";
$body4 .= "・車種：@reserve_car@\r\n";
$body4 .= "・お客様名：@customer_name@\r\n";
//$body4 .= "源泉：@offer_kind@\r\n";
$body4 .= "----------------------------------------------------------------------\r\n";
$body4 .= "東海マツダ\r\n";
//テスト用あとで削除すること！
//$body4 .= "@staff_name@\r\n";
define('BODY4', $body4);
/*********************************/
/* お客様用 キャンセル完了メール */
/*********************************/
// 件名
define('TITLE3', "「オンライン試乗予約」お申込受付キャンセル完了");
// メール本文 キャンセル完了メール
$body3  = "以下の申込受付番号についてキャンセルが完了いたしました。\r\n";
$body3 .= "ご利用ありがとうございました。\r\n";
$body3 .= "@number@";
$body3 .= "\r\n\r\n\r\n";
$body3 .= "--------------------\r\n";
$body3 .= "東海マツダ @shop_name2@\r\n";
$body3 .= "電話番号：@shop_tel@\r\n";
define('BODY3', $body3);
/*********************************************/
/* 本部・店舗スタッフ用 キャンセル完了メール */
/*********************************************/
// 件名
define('TITLE5', "「オンライン試乗予約」申込受付キャンセル");
// メール本文 キャンセル完了メール
$body5  = "以下の申込受付番号で「オンライン試乗予約」へのキャンセルがありました。\r\n";
//$body5 .= "デモカー管理台帳に転記してください。\r\n";
$body5 .= "\r\n";
$body5 .= "申込受付内容------------------------------------------------------------\r\n";
$body5 .= "・申込受付番号：@number@\r\n";
$body5 .= "・第1希望：@reserve_date1@\r\n";
$body5 .= "@reserve_date2@";
$body5 .= "@reserve_date3@";
//$body5 .= "ご予約日時：@reserve_date@\r\n";
$body5 .= "・店舗：@shop_name2@\r\n";
$body5 .= "・車種：@reserve_car@\r\n";
$body5 .= "・お客様名：@customer_name@\r\n";
//$body5 .= "源泉：@offer_kind@\r\n";
$body5 .= "https://driving.tokai-mazda.co.jp/oneday_admin/login.php";
$body5 .= "\r\n";
$body5 .= "----------------------------------------------------------------------\r\n";
$body5 .= "東海マツダ\r\n";
//テスト用あとで削除すること！
//$body5 .= "@staff_name@\r\n";
define('BODY5', $body5);
/***************************************/
/* 本部・店舗スタッフ用 予約変更メール */
/***************************************/
// 件名
define('TITLE6', "「オンライン試乗予約」申込受付変更");
// メール本文 予約変更メール
$body6  = "以下の申込受付番号で「オンライン試乗予約」への申込み変更がありました。\r\n";
$body6 .= "1.管理画面よりお客様情報・試乗にあたっての要望を確認してください。\r\n";
$body6 .= "https://driving.tokai-mazda.co.jp/oneday_admin/login.php?number=@number@";
$body6 .= "\r\n";
//$body6 .= "2.　デモカー管理台帳に転記してください。\r\n";
$body6 .= "\r\n";
$body6 .= "申込受付内容------------------------------------------------------------\r\n";
$body6 .= "・申込受付番号：@number@\r\n";
$body6 .= "・第1希望：@reserve_date1@\r\n";
$body6 .= "@reserve_date2@";
$body6 .= "@reserve_date3@";
$body6 .= "・店舗：@shop_name2@\r\n";
$body6 .= "・車種：@reserve_car@\r\n";
$body6 .= "・お客様名：@customer_name@\r\n";
//$body6 .= "源泉：@offer_kind@\r\n";
$body6 .= "----------------------------------------------------------------------\r\n";
$body6 .= "東海マツダ\r\n";
//テスト用あとで削除すること！
//$body6 .= "@staff_name@\r\n";
define('BODY6', $body6);
/*********************************/
/* お客様用 前々日予約確認メール */
/*********************************/
// 件名
define('TITLE7', "「オンライン試乗予約」ご予約内容確認");
// メール本文 前々日予約確認メール
$body7  = "この度は「東海マツダオンライン試乗予約」に申込み頂き、誠にありがとうございます。\r\n";
$body7 .= "お客様のご予約日が2営業日前になりましたので、改めてご予約内容の確認をお願い致します。\r\n\r\n";
$body7 .= "ご予約内容---------------\r\n";
$body7 .= "申込受付番号：@number@\r\n";
$body7 .= "日付：@reserve_date@\r\n";
$body7 .= "店舗：@shop_name2@\r\n";
$body7 .= "車種：@reserve_car@\r\n";
$body7 .= "-------------------------\r\n";
$body7 .= "\r\n";
$body7 .= "東海マツダ @shop_name2@\r\n";
$body7 .= "電話番号：@shop_tel@\r\n";
define('BODY7', $body7);
/*********************************************/
/* 本部・店舗スタッフ用 前々日予約確認メール */
/*********************************************/
// 件名
define('TITLE8', "「オンライン試乗予約」予約内容確認(2営業日前)");
// メール本文 前々日予約確認メール
$body8  = "「オンライン試乗予約」予約日まで2営業日となりました。\r\n";
$body8 .= "・デモカーの準備は大丈夫ですか？\r\n";
$body8 .= "・お客様情報の再確認をお願いします。\r\n";
$body8 .= "https://driving.tokai-mazda.co.jp/oneday_admin/login.php?number=@number@";
$body8 .= "\r\n\r\n";
$body8 .= "予約内容------------------------------------------------------------\r\n";
$body8 .= "申込受付番号：@number@\r\n";
$body8 .= "日付：@reserve_date@\r\n";
//$body8 .= "ご来店希望日時：@v_schedule_time@\r\n";
$body8 .= "店舗：@shop_name2@\r\n";
$body8 .= "車種：@reserve_car@\r\n";
$body8 .= "お客様名：@customer_name@\r\n";
//$body8 .= "源泉：@offer_kind@\r\n";
$body8 .= "----------------------------------------------------------------------\r\n";
$body8 .= "東海マツダ\r\n";
//テスト用あとで削除すること！
//$body8 .= "@staff_name@\r\n";
define('BODY8', $body8);
/*******************************/
/* お客様用 前日予約確認メール */
/*******************************/
// 件名
define('TITLE9', "「1日試乗」予約最終確認");
// メール本文 前日予約確認メール
$body9  = "この度は「東海マツダ1日試乗」にご応募ありがとうございます。\r\n";
$body9 .= "お客様の試乗ご予約日が明日となりました。\r\n";
$body9 .= "ご予約内容を確認の上、ご不明な点がありましたらご予約店舗へお問い合わせください。\r\n\r\n";
$body9 .= "@number@\r\n\r\n";
$body9 .= "ご予約内容---------------\r\n";
$body9 .= "日付：@reserve_date@\r\n";
$body9 .= "店舗：@shop_name2@\r\n";
$body9 .= "車種：@reserve_car@\r\n";
$body9 .= "\r\n\r\n";
$body9 .= "未成年の方は親権者の同意が必要となります。\r\n";
$body9 .= "下記URLより同意書をダウンロードし、必要事項をご記入の上当日ご持参ください。\r\n";
$body9 .= "https://driving.tokai-mazda.co.jp/shared/document/doisho.pdf";
$body9 .= "\r\n\r\n\r\n";
$body9 .= "--------------------\r\n";
$body9 .= "東海マツダ @shop_name2@\r\n";
$body9 .= "電話番号：@shop_tel@\r\n";
define('BODY9', $body9);
/*******************************************/
/* 本部・店舗スタッフ用 前日予約確認メール */
/*******************************************/
// 件名
define('TITLE10', "「1日試乗」予約最終確認");
// メール本文 前日予約確認メール
$body10  = "以下の「1日試乗」予約日が明日となりました。\r\n";
$body10 .= "確認をお願いします。\r\n";
$body10 .= "\r\n";
$body10 .= "@number@\r\n\r\n";
$body10 .= "予約内容---------------\r\n";
$body10 .= "日付：@reserve_date@\r\n";
$body10 .= "店舗：@shop_name2@\r\n";
$body10 .= "車種：@reserve_car@\r\n";
$body10 .= "\r\n\r\n";
$body10 .= "--------------------\r\n";
$body10 .= "東海マツダ\r\n";
//テスト用あとで削除すること！
//$body10 .= "@staff_name@\r\n";
define('BODY10', $body10);
/*********************************/
/* SP版お客様用 仮登録完了メール */
/*********************************/
// 件名
define('TITLE11', "「1日試乗」ご予約");
// メール本文 仮登録完了メール
$body11  = "この度は「東海マツダ1日試乗」にご応募ありがとうございます。\r\n";
$body11 .= "下記のURLにアクセスして頂き、ご予約の完了をお願い致します。\r\n";
$body11 .= "※ご予約のボタンクリック後、60分以内にアクセスし、予約登録を完了してください。60分以上経ちますとご予約が無効となります。予めご了承ください。\r\n\r\n";
$body11 .= "https://driving.tokai-mazda.co.jp/sp/mail.php?mode=entry&number=@number@&D=@ref@";
$body11 .= "\r\n\r\n\r\n";
$body11 .= "--------------------\r\n";
$body11 .= "東海マツダ  @shop_name2@\r\n";
$body11 .= "電話番号：@shop_tel@\r\n";
define('BODY11', $body11);
/*************************************/
/* 本部・店舗用 結果未入力通知メール */
/*************************************/
// 件名
define('TITLE12', "「オンライン試乗予約」試乗結果未登録通知");
// メール本文
$body12  = "「オンライン試乗予約」で3日以前に試乗された方の「結果登録」がされていません。\r\n";
$body12 .= "管理画面より「結果登録」を行ってください。\r\n";
$body12 .= "https://driving.tokai-mazda.co.jp/oneday_admin/login.php";
$body12 .= "\r\n\r\n";
$body12 .= "@txt@";
$body12 .= "\r\n";
$body12 .= "--------------------\r\n";
$body12 .= "東海マツダ\r\n";
define('BODY12', $body12);

// ● Smarty設定
define('SMARTY_DIR',BASE_DIR.'smarty/');
define('SMARTY_CACHING', false);
define('SMARTY_CACHE_DIR', BASE_DIR.'cache/template');
define('SMARTY_CACHE_LIFETIME', 60);
define('SMARTY_COMPILE_DIR', BASE_DIR.'compile/template/test');
define('SMARTY_FORCE_COMPILE', false);
define('SMARTY_DEBUGGING_CTRL', 'NONE');
define('SMARTY_DEBUGGING', false);
define('SMARTY_SECURITY',true);
define('SMARTY_DEBUG_TPL', '');
define('COMMON_TEMPLATE_DIR', BASE_DIR.'template/test');

// ● ADOdb設定
define('ADODB_DIR', BASE_DIR.'adodb/');
define('ADODB_CACHE_DIR' , BASE_DIR.'cache/adodb');
define('ADODB_CACHE_SECS' ,  '60');
define('ADODB_FORCE_TYPE',0);
define('ADODB_DEBUG' , false);

define('ADODB_DSN','postgres://tokai_db_user:2TCcYt8Yj4wA@192.168.10.33/oneday_tokai');
define('ADODB_DSN2','postgres://u_web_tokai:V2dlkGRGmI91@192.168.10.33/web_tokai');

define(ENC_KEY,"Tokai Mazda Campaign");
define(CRYPT_IV_ENCODE,"/fecauONx31KtswotRUT6yS5Mmy22TmNvmzH4ierGU4=");

define('POSTCD_FILE', BASE_DIR.'dat/KEN_ALL.CSV');

include_once(BASE_DIR.'request.php');
include_once(BASE_DIR.'base.php');
include_once(BASE_DIR.'temp.php');
include_once(BASE_DIR.'util.php');
include_once(BASE_DIR.'error.php');
include_once(BASE_DIR.'db.php');


// 自分のエラーハンドリングを行います
$old_error_handler = set_error_handler("ASErrorHandler");


?>