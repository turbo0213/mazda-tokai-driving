<?php
include_once("/home/oneday_tokai/mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $DB;
	var $tables;
	var $upd_tables;
	function form_class(){
		$this->DB = new ASDB();
		$this->tables = array(
			"car_detail"
		);
		$this->upd_tables = array(
			"car_detail",
			"zaiko"
		);
	}

	function execute(){
		$this->default_proc();
//		$this->update_proc();
	}

	function default_proc(){
		if($this->array_check($this->tables)){
			foreach($this->tables as $key => $val){
				$fp =& $this->fp_convert_encoding(LOG_DIR.$val.".csv");
				$i = 0;
				while(!feof($fp)){
					$data = $this->fgetcsv_reg($fp, 1000000, ",");
					//タイトル行取得
					if($i == 0){
						$title_line = array();
						$title_line = $data;
						$i++;
					}
					//データ行取得
					else{
						$record = null;
						if($this->array_check($data)){
							foreach($data as $key2 => $val2){
								$record[$title_line[$key2]] = $val2;
							}
							$record['create_date'] = time();
							$record['update_date'] = time();
echo $val."\n";
print_r($record);
							$ret = $this->DB->con->AutoExecute($val, $record, 'INSERT');
						}
						else{
							break;
						}
						$i++;
					}
				}
			}
		}
	}

	function update_proc(){
		if($this->array_check($this->tables)){
			foreach($this->upd_tables as $key => $val){
				$i = 1;
				if($val == "m_station"){
					$demo_val = "事業部";
					$count_sql = "select count(*) as cnt from m_station";
					$select_sql  = "select * from m_station order by autono";
				}
				else if($val == "m_base"){
					$demo_val = "拠点";
					$count_sql = "select count(*) as cnt from m_base";
					$select_sql  = "select * from m_base order by autono";
				}
				else if($val == "m_shop"){
					$demo_val = "店舗";
					$count_sql = "select count(*) as cnt from m_shop";
					$select_sql  = "select * from m_shop order by autono";
				}
				else if($val == "m_staff"){
					$demo_val = "担当者";
					$demo_val2 = "タントウシャ";
					$count_sql = "select count(*) as cnt from m_staff";
					$select_sql  = "select * from m_staff order by staff_autono";
				}
/*
				else if($val == "m_customer"){
					$demo_val = "テスト太郎";
					$demo_val2 = "テストタロウ";
					$count_sql = "select count(*) as cnt from m_customer";
					$select_sql = "select * from m_customer order by autono";
				}
*/
				$rs =& $this->DB->ASExecute($count_sql);
				$cnt = 0;
				if($rs){
					if(!$rs->EOF){
						$cnt = $rs->fields('cnt');
					}
					$rs->Close();
				}
				$loop = $cnt / 50;

				if($cnt % 50 <> 0){
					$loop++;
				}

				for($a = 0;$a <= $loop;$a++){
					$offset = 50 * $a;
					$sql  = $select_sql;
					$sql .= " limit 50 offset ".$offset;
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						while(!$rs->EOF){
							$record = null;
							$record['update_date'] = date('Y-m-d H:i:s');
							if($val == "m_station"){
								$record['station_name'] = $demo_val.$i;
								$where = "autono=".$this->DB->getQStr($rs->fields('autono'));
print_r($record);
echo $where."\n";
								$ret = $this->DB->con->AutoExecute("m_station", $record, 'UPDATE',$where);
							}
							else if($val == "m_base"){
								$record['base_name'] = $demo_val.$i;
								$where = "autono=".$this->DB->getQStr($rs->fields('autono'));
print_r($record);
echo $where."\n";
								$ret = $this->DB->con->AutoExecute("m_base", $record, 'UPDATE',$where);
							}
							else if($val == "m_shop"){
								$record['shop_name'] = $demo_val.$i;
								$where = "autono=".$this->DB->getQStr($rs->fields('autono'));
print_r($record);
echo $where."\n";
								$ret = $this->DB->con->AutoExecute("m_shop", $record, 'UPDATE',$where);
							}
							else if($val == "m_staff"){
								$record['staff_name'] = $demo_val.$i;
								$record['staff_kana'] = $demo_val2.$i;
								$record['login_id'] = $rs->fields('staff_autono');
								$record['login_pass'] = $rs->fields('staff_autono');
								$where = "staff_autono=".$this->DB->getQStr($rs->fields('staff_autono'));
print_r($record);
echo $where."\n";
								$ret = $this->DB->con->AutoExecute("m_staff", $record, 'UPDATE',$where);
							}
/*
							else if($val == "m_customer"){
								$record['customer_name'] = $demo_val.$i;
								$record['customer_kana'] = $demo_val2.$i;
								$where = "autono=".$this->DB->getQStr($rs->fields('autono'));
print_r($record);
echo $where."\n";
								$ret = $this->DB->con->AutoExecute("m_customer", $record, 'UPDATE',$where);
							}
*/
							$i++;
							$rs->MoveNext();
						}
						$rs->Close();
					}
				}
$i = $i - 1;
echo $i."\n";
			}
		}
	}

	function array_check($check_array){
		if(!$check_array){
			return false;
		}
		if(!is_array($check_array)){
			return false;
		}
		return true;
	}

	function &fp_convert_encoding($csv_pathfile){
//		$buf = mb_convert_encoding( file_get_contents($csv_pathfile), "UTF-8", "SJIS-win" ) ;
		$buf = file_get_contents($csv_pathfile);
		$buf = str_replace( "\r\n", "@< br >@", $buf );
		$buf = str_replace( "\n", "@< br >@", $buf );
		$buf = str_replace( "\r", "@< br >@", $buf );
		$buf = str_replace( "@< br >@", "\r\n", $buf );
		$fp = tmpfile();
		fwrite($fp, $buf);
		rewind($fp);
		return $fp;
	}

	function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
		$d = preg_quote($d);
		$e = preg_quote($e);
		$_line = "";
		while (($eof != true)and(!feof($handle))) {
			$_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
			$itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
			if ($itemcnt % 2 == 0) $eof = true;
		}
		$_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
		$_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
		preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
		$_csv_data = $_csv_matches[1];
		for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
			$_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
			$_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
		}
		return empty($_line) ? false : $_csv_data;
	}
}

?>