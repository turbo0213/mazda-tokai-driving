<?php
include_once("/home/oneday_tokai/mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;
/***
 * CSVのヘッダーは
 * shop_id,station_code,shop_name,staff_name,staff_id,password,mail,access_kb
 * access_kbは、1：店舗権限 3：事業部権限 4：本部権限
 * CSVの文字コードはUTF-8で。
 ***/
class form_class{
	var $DB;
	function form_class(){
		$this->DB = new ASDB();
	}

	function execute(){
		$this->default_proc();
	}

	function default_proc(){
		$fp =& $this->fp_convert_encoding(LOG_DIR."staff".date("Ymd").".csv");
		$i = 0;
		$list = array();
		while(!feof($fp)){
			$data = $this->fgetcsv_reg($fp, 1000000, ",");
			//タイトル行取得
			if($i == 0){
				$title_line = array();
				$title_line = $data;
				$i++;
			}
			//データ行取得
			else{
				$col = null;
				if($this->array_check($data)){
					foreach($data as $key => $val){
						$col[$title_line[$key]] = $val;
					}
					$list[$col['staff_id']] = $col;
				}
				else{
					break;
				}
				$i++;
			}
		}
var_dump($list);
		// csvにないスタッフは削除
		$sql = "select * from send_mail ";
		$sql .= "where del_flg = '0' ";
		$sql .= "and disp_flg = '1'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				if(!$list[$rs->fields('staff_id')]){
					$record = null;
					$record['del_flg'] = "1";
					$record['disp_flg'] = "0";
					$record['update_date'] = time();
					$where = "autono = ".$rs->fields('autono');
echo "delete:";
echo $rs->fields('staff_id')."\r\n";
					$ret = $this->DB->con->AutoExecute('send_mail', $record, 'UPDATE',$where);
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		// csvにあるスタッフは登録or更新
		if($list){
			if(is_array($list)){
				foreach($list as $key => $val){
					$find_flg = false;
					$sql = "select * from send_mail ";
					$sql .= "where staff_id = '".$this->DB->getQStr($list[$key]['staff_id'])."' ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$find_flg = true;
							$record = null;
							$record['del_flg'] = '0';
							$record['disp_flg'] = '1';
							$record['shop_name'] = $list[$key]['shop_name'];
							$record['staff_name'] = $list[$key]['staff_name'];
							// テスト環境用対応(本番はmail項目に移送する)
							$record['mail2'] = $list[$key]['mail'];
							$record['password'] = $list[$key]['password'];
							$record['access_kb'] = $list[$key]['access_kb'];
							$record['station_code'] = $list[$key]['station_code'];
							$record['shop_id'] = $list[$key]['shop_id'];
							$record['update_date'] = time();
							$where = "staff_id = '".$list[$key]['staff_id']."'";
echo "update:";
echo $list[$key]['staff_id']."\r\n";
							$ret = $this->DB->con->AutoExecute('send_mail', $record, 'UPDATE',$where);
						}
						$rs->Close();
					}
					if(!$find_flg){
						$record = null;
						$record['shop_name'] = $list[$key]['shop_name'];
						$record['staff_id'] = $list[$key]['staff_id'];
						$record['staff_name'] = $list[$key]['staff_name'];
						// テスト環境用対応(本番はmail項目に移送する)
						$record['mail'] = "tokai.mazda.test@gmail.com";
						$record['mail2'] = $list[$key]['mail'];
						$record['password'] = $list[$key]['password'];
						$record['access_kb'] = $list[$key]['access_kb'];
						$record['station_code'] = $list[$key]['station_code'];
						$record['shop_id'] = $list[$key]['shop_id'];
						$record['create_date'] = time();
						$record['update_date'] = time();
echo "insert:";
echo $list[$key]['staff_id']."\r\n";
						$ret = $this->DB->con->AutoExecute('send_mail', $record, 'INSERT');
					}
				}
			}
		}
	}

	function array_check($check_array){
		if(!$check_array){
			return false;
		}
		if(!is_array($check_array)){
			return false;
		}
		return true;
	}

	function &fp_convert_encoding($csv_pathfile){
//		$buf = mb_convert_encoding( file_get_contents($csv_pathfile), "UTF-8", "SJIS-win" ) ;
		$buf = file_get_contents($csv_pathfile);
		$buf = str_replace( "\r\n", "@< br >@", $buf );
		$buf = str_replace( "\n", "@< br >@", $buf );
		$buf = str_replace( "\r", "@< br >@", $buf );
		$buf = str_replace( "@< br >@", "\r\n", $buf );
		$fp = tmpfile();
		fwrite($fp, $buf);
		rewind($fp);
		return $fp;
	}

	function fgetcsv_reg (&$handle, $length = null, $d = ',', $e = '"') {
		$d = preg_quote($d);
		$e = preg_quote($e);
		$_line = "";
		while (($eof != true)and(!feof($handle))) {
			$_line .= (empty($length) ? fgets($handle) : fgets($handle, $length));
			$itemcnt = preg_match_all('/'.$e.'/', $_line, $dummy);
			if ($itemcnt % 2 == 0) $eof = true;
		}
		$_csv_line = preg_replace('/(?:\\r\\n|[\\r\\n])?$/', $d, trim($_line));
		$_csv_pattern = '/('.$e.'[^'.$e.']*(?:'.$e.$e.'[^'.$e.']*)*'.$e.'|[^'.$d.']*)'.$d.'/';
		preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
		$_csv_data = $_csv_matches[1];
		for($_csv_i=0;$_csv_i<count($_csv_data);$_csv_i++){
			$_csv_data[$_csv_i]=preg_replace('/^'.$e.'(.*)'.$e.'$/s','$1',$_csv_data[$_csv_i]);
			$_csv_data[$_csv_i]=str_replace($e.$e, $e, $_csv_data[$_csv_i]);
		}
		return empty($_line) ? false : $_csv_data;
	}
}

?>