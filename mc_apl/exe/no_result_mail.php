<?php
include_once("/home/oneday_tokai/mc_apl/top.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	// 明後日試乗予約している人にメール送信する処理
	function default_proc(){
		$limit_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-3,date("Y")));
		$sql = "select * from reservation ";
		$sql .= "where disp_flg = '1' ";
		$sql .= "and del_flg = '0' ";
		$sql .= "and temporary_flg = '2' ";
		$sql .= "and conf_flg = '1' ";
		$sql .= "and conf_date <= '".$this->DB->getQStr( $limit_date )."' ";
		$sql .= "and visit_flg not in ('1','2') ";
echo $sql;
		$sql .= "order by shop_id,staff_autono,conf_date";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['shop_id'] = $rs->fields('shop_id');
				$dat['staff_autono'] = $rs->fields('staff_autono');
				$dat['disp_number'] = $rs->fields('disp_number');
				$dat['car_id'] = $rs->fields('car_detail_id');
				$year = date("Y",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$month = date("m",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$day = date("d",mktime(0,0,0,substr($rs->fields('conf_date'),5,2),substr($rs->fields('conf_date'),8,2),substr($rs->fields('conf_date'),0,4)));
				$dat['reserve_date'] = date("Y年m月d日",mktime(0,0,0,$month,$day,$year));
				if($rs->fields('conf_ampm') == '1'){
					$dat['reserve_date'] .= " 午前";
				}
				else if($rs->fields('conf_ampm') == '2'){
					$dat['reserve_date'] .= " 午後";
				}
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
var_dump($data_list);
		$shop_id = "";
		$staff_autono = 0;
		$f_flg = true;
		if($data_list){
			if(is_array($data_list)){
				foreach($data_list as $key => $val){
					if($shop_id != $data_list[$key]['shop_id']){
						if(!$f_flg){
							$this->mail_proc($data_list[$key],$txt);
						}
						$txt = "結果未登録 ------------------------------------\r\n";
						$shop_id = $data_list[$key]['shop_id'];
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					else if($staff_autono != $data_list[$key]['staff_autono']){
						if(!$f_flg){
							$this->mail_proc($data_list[$key],$txt);
						}
						$txt = "結果未登録 ------------------------------------\r\n";
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					else if($f_flg){
						$txt = "結果未登録 ------------------------------------\r\n";
						$shop_id = $data_list[$key]['shop_id'];
						$staff_autono = $data_list[$key]['staff_autono'];
					}
					if($f_flg){
						$f_flg = false;
					}
					$txt .= "予約番号：".$data_list[$key]['disp_number']."\r\n";
					$txt .= "試乗日時：".$data_list[$key]['reserve_date']."\r\n";
					if($data_list[$key]['car_id']){
						$car_data = $this->util->car_info_get($data_list[$key]['car_id'],$this->DB);
					}
					$txt .= "試乗車種：".$car_data['name']." ".$car_data['name2']." ".$car_data['name3']."\r\n";
					$txt .= "-----------------------------------------------\r\n";
				}
			}
		}
		if($txt){
			$this->mail_proc($data_list[$key],$txt);
		}
	}

	function mail_proc($data,$txt){
		//日本語メールを送る際に必要
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		// SMTPサーバーの情報を連想配列にセット
		$params = array(
				"host" => HOST,
				"port" => PORT,
				"auth" => false,  // SMTP認証を使用する
				"username" => USERNAME,
				"password" => PASSWORD
		);
		// PEAR::Mailのオブジェクトを作成
		// ※バックエンドとしてSMTPを指定
		$mailObject = Mail::factory("sendmail", $params);
		$FromName = mb_encode_mimeheader(FROMMAILNAME);
		$from= $FromName."<".FROMMAIL.">";
		// 本部メールアドレス取得
		$mail_address = array();
		$mail_address = $this->util->headquarters_mail();
		// 店舗スタッフメールアドレス取得
		$sql = "select mail from send_mail";
		$sql .= " where shop_id = '".$this->DB->getQStr($data['shop_id'])."'";
//		if($data['staff_autono']){
//			$sql .= " and autono = ".$this->DB->getQStr($data['staff_autono']);
//		}
		$sql .= " and mail is not null and mail <> ''";
		$sql .= " and disp_flg = '1' and del_flg = '0'";
echo $sql;
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$mail_address[] = $rs->fields('mail');
				$rs->MoveNext();
			}
			$rs->Close();
		}
var_dump($mail_address);
		// メールヘッダ情報を連想配列としてセット
		$headers = array(
				"From" => $from,
				"Subject" => mb_encode_mimeheader(TITLE12)
		);
		if($mail_address){
			if(is_array($mail_address)){
				foreach($mail_address as $key => $val){
					$recipients = "tokai.mazda.test@gmail.com";
					//あとで↓を有効にする。↑は消す。
//					if($recipients){
//						$recipients .= ",".$val;
//					}
//					else{
//						$recipients = $val;
//					}
				}
			}
		}
		$body = str_replace("@txt@",$txt,BODY12);
		// 日本語なのでエンコード
		$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
		// sendメソッドでメールを送信
		$mailObject->send($recipients, $headers, $body);
	}
}

?>