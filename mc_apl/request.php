<?php

class reqData {

	var $params = Array();
	var $get_params = Array();
	
	function reqData() {

		$this->first();
		if( is_array($_POST) ) {
			$_POST = delete_nullbyte($_POST);
			$_POST = array_map('convert_kana_deep', $_POST);
			foreach( $_POST as $name => $value) {
				$this->set($name, $value);
			}
			$_GET = delete_nullbyte($_GET);
			$_GET = array_map('convert_kana_deep', $_GET);
			foreach( $_GET as $name => $value) {
				$this->get_set($name, $value);
			}
		}

	}

	function first(){
		set_magic_quotes_runtime(0);
		if (get_magic_quotes_gpc()) {
			if( is_array($_REQUEST) ) {
				$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
			}
		}
	}

	function set($name, $data) {
		$this->params[$name] = $data;
	}

	function & get ($name, $value = 'NULL'){
        if (isset($this->params[$name]))
        {
            return $this->params[$name];
        }
        else if ($value != 'NULL')
        {
            return $value;
        }
        return NULL;
    }

	function get_set($name, $data) {
		$this->get_params[$name] = $data;
	}

	function & get_get ($name, $value = 'NULL'){
        if (isset($this->get_params[$name]))
        {
            return $this->get_params[$name];
        }
        else if ($value != 'NULL')
        {
            return $value;
        }
        return NULL;
    }
    
    function getError ($name){
        $retval = null;
        if (isset($this->errors[$name]))
        {
            $retval = $this->errors[$name];
        }
        return $retval;
    }

    function getErrors (){
        return $this->errors;
    }

    function hasError ($name){
        return isset($this->errors[$name]);
    }

    function hasErrors (){
        return (count($this->errors) > 0);
    }

    function & removeError ($name){
        $retval = null;
        if (isset($this->errors[$name]))
        {
            $retval =& $this->errors[$name];
            unset($this->errors[$name]);
        }
        return $retval;
    }

	function setError($name,$message){
		$this->errors[$name] = $message;
	}

    function setErrors ($errors){
        $this->errors = array_merge($this->errors, $errors);
    }

}


?>
