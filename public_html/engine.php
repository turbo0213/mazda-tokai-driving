<?php
include_once("../mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

/************************
 * エンジン・排気量選択 *
 ************************/
class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		$this->data_get();
		$this->templ->smarty->assign("shop_id",$this->req->get_get('shop_id'));
		$this->templ->smarty->assign("select_flg",$this->req->get_get('select_flg'));
		$this->templ->smarty->assign("car1",$this->req->get_get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get_get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get_get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get_get('car4'));
		$this->templ->smarty->assign("car5",$this->req->get_get('car5'));
//		$this->templ->smarty->assign("s",$this->req->get_get('s'));
//		$this->templ->smarty->assign("c",$this->req->get_get('c'));
		$this->templ->smarty->display("engine.html");
		exit;
	}

	function data_get(){
		if($this->req->get_get('shop_id')){
			$sql = "select * from shop ";
			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("name",$rs->fields('name'));
				}
				$rs->Close();
			}
		}
		$data_list = array();
		$car3_list = array();
		$i = 0;
		$car3 = "";
		$car12 = "";
		$sql = "select * from car";
		$sql .= " where disp_flg='1'";
		$sql .= " and del_flg='0'";
		$sql .= " and car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		$sql .= " order by car1,car2,car4,car3";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql2 = "select * from car_detail";
				$sql2 .= " where end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
				$sql2 .= " and car_id = '".$this->DB->getQStr($rs->fields('car_id'))."'";
				if($this->req->get_get('shop_id')) {
					$sql2 .= " and shop_id = '" . $this->DB->getQStr($this->req->get_get('shop_id')) . "'";
				}
				$sql2 .= " and disp_flg='1'";
				$sql2 .= " and del_flg='0'";
				$sql2 .= " order by car1,car2,car3,car4";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						$dat = array();
						$comp_car12 = $rs2->fields('car1').$rs2->fields('car2');
						// upd 20190130 turbo対応
						$comp_car = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
						if($car12 != $comp_car12){
							$car12 = $rs2->fields('car1').$rs2->fields('car2');
							// upd 20190130 turbo対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						else if($car3 != $comp_car){
							// upd 20190130 turbo対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						if($data_list[$rs->fields('car1')]){
							$data_list[$rs->fields('car1')][$rs->fields('car2')] = $rs->fields('car2');
						}
						else{
							$dat['car1'] = $rs->fields('car1');
							$dat[$rs->fields('car2')] = $rs->fields('car2');
							$dat['name'] = $rs->fields('name');
							$dat['car_image'] = $this->util->car_image(0,1,$rs->fields('car_id'));
							$data_list[$rs->fields('car1')] = $dat;
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("data_list",$data_list);
		$this->templ->smarty->assign("car3_list",$car3_list);
	}
}
?>
