<?php
include_once("../mc_apl/top.php");
//require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			// 予約情報(認証)
			case 'login':
				$this->login_proc();
			break;
			// 予約情報取消(確認)
			case 'cancel_confirm':
				$this->cancel_confirm_proc();
			break;
			// 予約キャンセル画面(登録)
			case 'cancell_entry':
				$this->cancell_entry_proc();
			break;
			// 予約変更画面(入力チェック転送処理)
			case 'change_entry':
				$this->change_entry_proc();
			break;
			// カレンダー画面からの戻り
			case 'disp':
				$this->disp_proc();
			break;
			// 予約情報(入力)
			default:
				$this->default_proc();
			break;
		}
	}

	// 予約情報入力(入力)
	function default_proc(){
		$_SESSION['disp_number'] = "";
		if( $this->req->get('number') )$this->templ->smarty->assign("number",$this->req->get('number'));
		if( $this->req->get('sei') )$this->templ->smarty->assign("sei",$this->req->get('sei'));
		if( $this->req->get('mei') )$this->templ->smarty->assign("mei",$this->req->get('mei'));
		if( $this->req->get('mail') )$this->templ->smarty->assign("mail",$this->req->get('mail'));
		$this->templ->smarty->display("login.html");
		exit;
	}

	function login_proc(){
		// 入力データチェック
		$error = $this->datacheck_proc();
		if(!empty($error)){
			$this->templ->smarty->assign("error",$error);
			$this->default_proc();
			return;
		}
		$login_ok = false;
		$dat = array();
		$number = $this->req->get('number');
		$sei = $this->req->get('sei');
		$mei = $this->req->get('mei');
//		$tel = $this->req->get('tel1')."-".$this->req->get('tel2')."-".$this->req->get('tel3');
		$mail = $this->req->get('mail');

		$_SESSION['disp_number'] = $number;
		//認証
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($number)."' ";
		$sql .= " and sei='".$this->DB->getQStr($sei)."' ";
		$sql .= " and mei='".$this->DB->getQStr($mei)."' ";
//		$sql .= " and tel='".$this->DB->getQStr($tel)."' ";
		$sql .= " and mail='".$this->DB->getQStr($mail)."' ";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$sql .= " order by date asc";
		$rs =& $this->DB->ASExecute($sql);
		$login_ok = false;
		if($rs){
			if(!$rs->EOF){
				$dat['number'] = $rs->fields('disp_number');
				$dat['shop_id'] = $rs->fields('shop_id');
				if($rs->fields('car_detail_id')){
					$dat['car_detail_id'] = $rs->fields('car_detail_id');
				}
				$dat['car1'] = $rs->fields('car1');
				$dat['car2'] = $rs->fields('car2');
				$dat['car3'] = $rs->fields('car3');
				$dat['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $dat['car5'] = $rs->fields('car5');
				$dat["date_first"] = $rs->fields('date');
				$dat["ampm_first"] = $rs->fields('ampm');
				$dat["ampm_first_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm'));
				$dat["date_second"] = $rs->fields('date2');
				$dat["ampm_second"] = $rs->fields('ampm2');
				$dat["ampm_second_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm2'));
				$dat["date_third"] = $rs->fields('date3');
				$dat["ampm_third"] = $rs->fields('ampm3');
				$dat["ampm_third_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm3'));
				$dat['sei'] = $rs->fields('sei');
				$dat['mei'] = $rs->fields('mei');
				$dat['sei_kana'] = $rs->fields('sei_kana');
				$dat['mei_kana'] = $rs->fields('mei_kana');
//				$dat['date'] = $rs->fields('date');
//				$dat['age'] = $rs->fields('age');
				$dat['postcd'] = $rs->fields('postcd');
				$dat['address'] = $rs->fields('address');
				$dat['contact_kind'] = $rs->fields('contact_kind');
//				$dat['tel'] = $rs->fields('tel');
//				$dat['k_tel'] = $rs->fields('k_tel');
				$dat['mail'] = $rs->fields('mail');
//				$dat['campaign'] = $rs->fields('campaign');
				// add 20151030 キャンペーン申込の理由 項目追加
//				$dat['reason'] = $rs->fields('reason');
//				$dat['reason99_text'] = $rs->fields('reason99_text');
//				$dat['reason_value'] = $this->util->reason_list(1,$rs->fields('reason'));
				$dat['comment'] = $rs->fields('comment');
				$login_ok = true;
				// 予約変更期限を2日前、以降は確認のみ可能
				if($rs->fields('conf_flg') == '0'){
					$edit_ok = true;
					$holiday_cnt = 0;
					if(date("H") >= 12){
						$edit_day = 1;
					}
					else{
						$edit_day = 0;
					}
					$ret = "";
					while(1){
						$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day,date("Y")));
						$ret = $this->util->holiday_list($this->DB,$dat['shop_id'],1,$start_day);
						if(!$ret){
							$holiday_cnt++;
							if($holiday_cnt == 2){
								break;
							}
						}
						$edit_day++;
					}
					$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day + 1,date("Y")));
					if($rs->fields('date') < $date){
						$edit_ok = false;
					}
					else if($rs->fields('date2')){
						if($rs->fields('date2') < $date){
							$edit_ok = false;
						}
					}
					else if($rs->fields('date3')){
						if($rs->fields('date3') < $date){
							$edit_ok = false;
						}
					}
				}
			}
			$rs->Close();
		}

		if(!$login_ok){
			$error = "入力された内容が正しくないか、本登録が完了していない可能性があります。ご確認の上、再度ログインを行ってください。\r\n";
//			$error .= "試乗実施日の2日前の正午以降の変更・取消の場合は、ご予約店舗へ直接お願いいたします。";
			$this->templ->smarty->assign("error",$error);
			$this->default_proc();
			return;
		}
		$this->templ->smarty->assign("edit_flg",$edit_ok);
		$this->templ->smarty->assign("number",$dat['number']);
		$this->templ->smarty->assign("shop_id",$dat['shop_id']);
		$this->templ->smarty->assign("date_first",$dat['date_first']);
		list($year,$month,$day) = explode("-",$dat['date_first']);
		$this->templ->smarty->assign("year1",$year);
		$this->templ->smarty->assign("month1",$month);
		$this->templ->smarty->assign("day1",$day);
		$this->templ->smarty->assign("ampm_first",$dat['ampm_first']);
		$this->templ->smarty->assign("ampm_first_value",$this->util->test_drive_time_list(1,$dat['ampm_first']));
		$this->templ->smarty->assign("date_second",$dat['date_second']);
		list($year,$month,$day) = explode("-",$dat['date_second']);
		$this->templ->smarty->assign("year2",$year);
		$this->templ->smarty->assign("month2",$month);
		$this->templ->smarty->assign("day2",$day);
		$this->templ->smarty->assign("ampm_second",$dat['ampm_second']);
		$this->templ->smarty->assign("ampm_second_value",$this->util->test_drive_time_list(1,$dat['ampm_second']));
		$this->templ->smarty->assign("date_third",$dat['date_third']);
		list($year,$month,$day) = explode("-",$dat['date_third']);
		$this->templ->smarty->assign("year3",$year);
		$this->templ->smarty->assign("month3",$month);
		$this->templ->smarty->assign("day3",$day);
		$this->templ->smarty->assign("ampm_third",$dat['ampm_third']);
		$this->templ->smarty->assign("ampm_third_value",$this->util->test_drive_time_list(1,$dat['ampm_third']));
		$this->templ->smarty->assign("car1",$dat['car1']);
		$this->templ->smarty->assign("car2",$dat['car2']);
		$this->templ->smarty->assign("car3",$dat['car3']);
		$this->templ->smarty->assign("car4",$dat['car4']);
		// add 20190130 turbo対応
        $this->templ->smarty->assign("car5",$dat['car5']);
		$this->templ->smarty->assign("sei",$dat['sei']);
		$this->templ->smarty->assign("mei",$dat['mei']);
		$this->templ->smarty->assign("sei_kana",$dat['sei_kana']);
		$this->templ->smarty->assign("mei_kana",$dat['mei_kana']);
		$this->templ->smarty->assign("postcd",$dat['postcd']);
		$this->templ->smarty->assign("address",$dat['address']);
		$this->templ->smarty->assign("contact_kind",$dat['contact_kind']);
		$this->templ->smarty->assign("mail",$dat['mail']);
		$this->templ->smarty->assign("comment",$dat['comment']);
		// 車種
		if($dat['car_detail_id']){
			$sql2 = "select * from car_detail";
			$sql2 .= " where autono = ".$this->DB->getQStr($dat['car_detail_id']);
			$sql2 .= " and shop_id = '".$this->DB->getQStr($dat['shop_id'])."' ";
			$sql2 .= " and disp_flg='1' ";
			$sql2 .= " and del_flg='0' ";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				if(!$rs2->EOF){
					$sql = "select name,name2,name3 from car ";
					$sql .= " where car_id='".$this->DB->getQStr($rs2->fields('car_id'))."' ";
					$sql .= " and disp_flg='1' ";
					$sql .= " and del_flg='0' ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$dat['car_name'] = $rs->fields('name');
							if($dat['car2'] == "ge"){
								$dat['car_name'] .= " ガソリン";
							}
							else if($dat['car2'] == "de"){
								$dat['car_name'] .= " ディーゼル";
							}
							else if($dat['car2'] == "hev"){
								$dat['car_name'] .= " ハイブリッド";
							}
                            // add 20200109 SKYACTIV-X対応
                            else if($dat['car2'] == 'skyx'){
                                $dat['car_name'] .= " SKYACTIV-X";
                            }
							if($dat['car4'] == "mt"){
								$dat['car_name'] .= " MT";
							}
							$dat['car_name'] .= " ".$dat['car3'];
                            // add 20190130 turbo対応
							if($dat['car5'] == "turbo"){
                                $dat['car_name'] .= " ターボ";
                            }
						}
						$rs->Close();
					}
				}
				$rs2->Close();
			}
		}
		else if($dat['car1'] and $dat['car2']){
			$sql = "select name,name2 from car ";
			$sql .= " where car1 = '".$this->DB->getQStr($dat['car1'])."' ";
			$sql .= " and car2 = '".$this->DB->getQStr($dat['car2'])."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$dat['car_name'] = $rs->fields('name');
					if($dat['car2'] == "ge"){
						$dat['car_name'] .= " ガソリン";
					}
					else if($dat['car2'] == "de"){
						$dat['car_name'] .= " ディーゼル";
					}
					else if($dat['car2'] == "hev"){
						$dat['car_name'] .= " ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($dat['car2'] == 'skyx'){
                        $dat['car_name'] .= " SKYACTIV-X";
                    }
					if($dat['car4'] == "mt"){
						$dat['car_name'] .= " MT";
					}
					$dat['car_name'] .= " ".$dat['car3'];
					// add 20190130 turbo対応
                    if($dat['car5'] == "turbo"){
                        $dat['car_name'] .= " ターボ";
                    }
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign("car_name",$dat['car_name']);
		// 店舗
		$sql = "select name,tel from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($dat['shop_id'])."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$dat['shop_name'] = $rs->fields('name');
				$dat['shop_tel'] = $rs->fields('tel');
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("shop_name",$dat['shop_name']);
		$this->templ->smarty->assign("shop_tel",$dat['shop_tel']);
		$this->templ->smarty->display("edit.html");
		exit;
	}

	// カレンダー画面からの戻り処理
	function disp_proc(){
		if(!$_SESSION['disp_number']){
			header("Location:edit.php");
			exit;
		}
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($_SESSION['disp_number'])."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$dat['number'] = $rs->fields('disp_number');
				$dat['shop_id'] = $rs->fields('shop_id');
				if($rs->fields('car_detail_id')){
					$dat['car_detail_id'] = $rs->fields('car_detail_id');
				}
				$dat['car1'] = $rs->fields('car1');
				$dat['car2'] = $rs->fields('car2');
				$dat['car3'] = $rs->fields('car3');
				$dat['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $dat['car5'] = $rs->fields('car5');
				$dat["date_first"] = $rs->fields('date');
				$dat["ampm_first"] = $rs->fields('ampm');
				$dat["ampm_first_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm'));
				$dat["date_second"] = $rs->fields('date2');
				$dat["ampm_second"] = $rs->fields('ampm2');
				$dat["ampm_second_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm2'));
				$dat["date_third"] = $rs->fields('date3');
				$dat["ampm_third"] = $rs->fields('ampm3');
				$dat["ampm_third_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm3'));
				$dat['sei'] = $rs->fields('sei');
				$dat['mei'] = $rs->fields('mei');
				$dat['sei_kana'] = $rs->fields('sei_kana');
				$dat['mei_kana'] = $rs->fields('mei_kana');
				$dat['postcd'] = $rs->fields('postcd');
				$dat['address'] = $rs->fields('address');
				$dat['contact_kind'] = $rs->fields('contact_kind');
				$dat['mail'] = $rs->fields('mail');
				$dat['comment'] = $rs->fields('comment');
				$login_ok = true;
				// 予約変更期限を2日前、以降は確認のみ可能
				if($rs->fields('conf_flg') == '0'){
					$edit_ok = true;
					$holiday_cnt = 0;
					if(date("H") >= 12){
						$edit_day = 1;
					}
					else{
						$edit_day = 0;
					}
					$ret = "";
					while(1){
						$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day,date("Y")));
						$ret = $this->util->holiday_list($this->DB,$dat['shop_id'],1,$start_day);
						if(!$ret){
							$holiday_cnt++;
							if($holiday_cnt == 2){
								break;
							}
						}
						$edit_day++;
					}
					$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day + 1,date("Y")));
					if($rs->fields('date') < $date){
						$edit_ok = false;
					}
					else if($rs->fields('date2')){
						if($rs->fields('date2') < $date){
							$edit_ok = false;
						}
					}
					else if($rs->fields('date3')){
						if($rs->fields('date3') < $date){
							$edit_ok = false;
						}
					}
				}
			}
			$rs->Close();
		}
		if(!$login_ok){
			$error = "入力された内容が正しくないか、本登録が完了していない可能性があります。ご確認の上、再度ログインを行ってください。\r\n";
			$this->templ->smarty->assign("error",$error);
			$this->default_proc();
			return;
		}
		$this->templ->smarty->assign("edit_flg",$edit_ok);
		$this->templ->smarty->assign("number",$dat['number']);
		$this->templ->smarty->assign("shop_id",$dat['shop_id']);
		$this->templ->smarty->assign("date_first",$dat['date_first']);
		list($year,$month,$day) = explode("-",$dat['date_first']);
		$this->templ->smarty->assign("year1",$year);
		$this->templ->smarty->assign("month1",$month);
		$this->templ->smarty->assign("day1",$day);
		$this->templ->smarty->assign("ampm_first",$dat['ampm_first']);
		$this->templ->smarty->assign("ampm_first_value",$this->util->test_drive_time_list(1,$dat['ampm_first']));
		$this->templ->smarty->assign("date_second",$dat['date_second']);
		list($year,$month,$day) = explode("-",$dat['date_second']);
		$this->templ->smarty->assign("year2",$year);
		$this->templ->smarty->assign("month2",$month);
		$this->templ->smarty->assign("day2",$day);
		$this->templ->smarty->assign("ampm_second",$dat['ampm_second']);
		$this->templ->smarty->assign("ampm_second_value",$this->util->test_drive_time_list(1,$dat['ampm_second']));
		$this->templ->smarty->assign("date_third",$dat['date_third']);
		list($year,$month,$day) = explode("-",$dat['date_third']);
		$this->templ->smarty->assign("year3",$year);
		$this->templ->smarty->assign("month3",$month);
		$this->templ->smarty->assign("day3",$day);
		$this->templ->smarty->assign("ampm_third",$dat['ampm_third']);
		$this->templ->smarty->assign("ampm_third_value",$this->util->test_drive_time_list(1,$dat['ampm_third']));
		$this->templ->smarty->assign("car1",$dat['car1']);
		$this->templ->smarty->assign("car2",$dat['car2']);
		$this->templ->smarty->assign("car3",$dat['car3']);
		$this->templ->smarty->assign("car4",$dat['car4']);
		// add 20190130 turbo対応
        $this->templ->smarty->assign("car5",$dat['car5']);
		$this->templ->smarty->assign("sei",$dat['sei']);
		$this->templ->smarty->assign("mei",$dat['mei']);
		$this->templ->smarty->assign("sei_kana",$dat['sei_kana']);
		$this->templ->smarty->assign("mei_kana",$dat['mei_kana']);
		$this->templ->smarty->assign("postcd",$dat['postcd']);
		$this->templ->smarty->assign("address",$dat['address']);
		$this->templ->smarty->assign("contact_kind",$dat['contact_kind']);
		$this->templ->smarty->assign("mail",$dat['mail']);
		$this->templ->smarty->assign("comment",$dat['comment']);
		// 車種
		if($dat['car_detail_id']){
			$sql2 = "select * from car_detail";
			$sql2 .= " where autono = ".$this->DB->getQStr($dat['car_detail_id']);
			$sql2 .= " and shop_id = '".$this->DB->getQStr($dat['shop_id'])."' ";
			$sql2 .= " and disp_flg='1' ";
			$sql2 .= " and del_flg='0' ";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				if(!$rs2->EOF){
					$sql = "select name,name2,name3 from car ";
					$sql .= " where car_id='".$this->DB->getQStr($rs2->fields('car_id'))."' ";
					$sql .= " and disp_flg='1' ";
					$sql .= " and del_flg='0' ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$dat['car_name'] = $rs->fields('name');
							if($dat['car2'] == "ge"){
								$dat['car_name'] .= " ガソリン";
							}
							else if($dat['car2'] == "de"){
								$dat['car_name'] .= " ディーゼル";
							}
							else if($dat['car2'] == "hev"){
								$dat['car_name'] .= " ハイブリッド";
							}
							if($dat['car4'] == "mt"){
								$dat['car_name'] .= " MT";
							}
							$dat['car_name'] .= " ".$dat['car3'];
							// add 20190130 turbo対応
                            if($dat['car5'] == "turbo"){
                                $dat['car_name'] .= " ターボ";
                            }
						}
						$rs->Close();
					}
				}
				$rs2->Close();
			}
		}
		else if($dat['car1'] and $dat['car2']){
			$sql = "select name,name2 from car ";
			$sql .= " where car1 = '".$this->DB->getQStr($dat['car1'])."' ";
			$sql .= " and car2 = '".$this->DB->getQStr($dat['car2'])."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$dat['car_name'] = $rs->fields('name');
					if($dat['car2'] == "ge"){
						$dat['car_name'] .= " ガソリン";
					}
					else if($dat['car2'] == "de"){
						$dat['car_name'] .= " ディーゼル";
					}
					else if($dat['car2'] == "hev"){
						$dat['car_name'] .= " ハイブリッド";
					}
					if($dat['car4'] == "mt"){
						$dat['car_name'] .= " MT";
					}
					$dat['car_name'] .= " ".$dat['car3'];
					// add 20190130 turbo対応
                    if($dat['car5'] == "turbo"){
                        $dat['car_name'] .= " ターボ";
                    }
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign("car_name",$dat['car_name']);
		// 店舗
		$sql = "select name,tel from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($dat['shop_id'])."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$dat['shop_name'] = $rs->fields('name');
				$dat['shop_tel'] = $rs->fields('tel');
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("shop_name",$dat['shop_name']);
		$this->templ->smarty->assign("shop_tel",$dat['shop_tel']);
		$this->templ->smarty->display("edit.html");
		exit;
	}

	// 入力データチェック
	function datacheck_proc(){
		$error = "";
		// 予約番号
		if($this->req->get('number') == null){
			$error .= "予約番号を入力してください\r\n";
		}
		// 姓
		if($this->req->get('sei') == null){
			$error .= "姓を入力してください\r\n";
		}
		// 名
		if($this->req->get('mei') == null){
			$error .= "名を入力してください\r\n";
		}
		// 自宅tel
//		if($this->req->get('tel1') == null or $this->req->get('tel2') == null or $this->req->get('tel3') == null){
//			$error .= "自宅telを入力してください\r\n";
//		}else{
//			$check1 = null;
//			$check2 = null;
//			$check3 = null;
//			$check1 = $this->util->number_check($this->req->get('tel1'));
//			$check2 = $this->util->number_check($this->req->get('tel2'));
//			$check3 = $this->util->number_check($this->req->get('tel3'));
//			if(!empty($check1) or !empty($check2) or !empty($check3)) $error .= "自宅telを数字で入力してください\r\n";
//		}
		// メールアドレス
		if($this->req->get('mail') == null){
			$error .= "メールアドレスを入力してください\r\n";
		}else{
			$check1 = null;
			$check1 = $this->util->mail_check('Mail',$this->req->get('mail'));
			if(!empty($check1)) $error .= "メールアドレスの形式が正しくありません\r\n";
		}
		return $error;
	}
	
	// 予約情報入力(確認)
	function check_proc(){
/*
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
echo $_SESSION['disp_number'];
}
*/
		$this->templ->smarty->display("14.html");
		exit;
	}
	// 予約情報取消(確認)
	function cancel_confirm_proc(){
		if(!$_SESSION['disp_number']){
			header("Location:edit.php");
			exit;
		}
		//認証
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($_SESSION['disp_number'])."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
//		$sql .= " order by date asc";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$dat['number'] = $rs->fields('disp_number');
				$dat['shop_id'] = $rs->fields('shop_id');
				$dat['car1'] = $rs->fields('car1');
				$dat['car2'] = $rs->fields('car2');
				$dat['car3'] = $rs->fields('car3');
				$dat['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $dat['car5'] = $rs->fields('car5');
				$dat["date_first"] = $rs->fields('date');
				$dat["ampm_first"] = $rs->fields('ampm');
				$dat["ampm_first_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm'));
				$dat["date_second"] = $rs->fields('date2');
				$dat["ampm_second"] = $rs->fields('ampm2');
				$dat["ampm_second_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm2'));
				$dat["date_third"] = $rs->fields('date3');
				$dat["ampm_third"] = $rs->fields('ampm3');
				$dat["ampm_third_value"] = $this->util->test_drive_time_list(1,$rs->fields('ampm3'));
				$dat['sei'] = $rs->fields('sei');
				$dat['mei'] = $rs->fields('mei');
				$dat['sei_kana'] = $rs->fields('sei_kana');
				$dat['mei_kana'] = $rs->fields('mei_kana');
				$dat['postcd'] = $rs->fields('postcd');
				$dat['address'] = $rs->fields('address');
				$dat['contact_kind'] = $rs->fields('contact_kind');
				$dat['mail'] = $rs->fields('mail');
				$dat['comment'] = $rs->fields('comment');
				$login_ok = true;
				// 予約変更期限を2日前、以降は確認のみ可能
				if($rs->fields('conf_flg') == '0'){
					$edit_ok = true;
					$holiday_cnt = 0;
					if(date("H") >= 12){
						$edit_day = 1;
					}
					else{
						$edit_day = 0;
					}
					$ret = "";
					while(1){
						$start_day = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day,date("Y")));
						$ret = $this->util->holiday_list($this->DB,$dat['shop_id'],1,$start_day);
						if(!$ret){
							$holiday_cnt++;
							if($holiday_cnt == 2){
								break;
							}
						}
						$edit_day++;
					}
					$date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $edit_day + 1,date("Y")));
					if($rs->fields('date') < $date){
						$edit_ok = false;
					}
					else if($rs->fields('date2')){
						if($rs->fields('date2') < $date){
							$edit_ok = false;
						}
					}
					else if($rs->fields('date3')){
						if($rs->fields('date3') < $date){
							$edit_ok = false;
						}
					}
				}
			}
			else{
				//存在しない
				$login_ok = false;
			}
			$rs->Close();
		}
		
		if(!$login_ok){
			$this->templ->smarty->assign("error",'入力された予約番号がないか、有効期限が過ぎています。');
			$this->templ->smarty->assign("error",$error);
			$this->default_proc();
			return;
		}
		
		$this->templ->smarty->assign("edit_flg",$edit_ok);
		$this->templ->smarty->assign("number",$dat['number']);
		$this->templ->smarty->assign("shop_id",$dat['shop_id']);
		$this->templ->smarty->assign("date_first",$dat['date_first']);
		list($year,$month,$day) = explode("-",$dat['date_first']);
		$this->templ->smarty->assign("year1",$year);
		$this->templ->smarty->assign("month1",$month);
		$this->templ->smarty->assign("day1",$day);
		$this->templ->smarty->assign("ampm_first",$dat['ampm_first']);
		$this->templ->smarty->assign("ampm_first_value",$this->util->test_drive_time_list(1,$dat['ampm_first']));
		$this->templ->smarty->assign("date_second",$dat['date_second']);
		list($year,$month,$day) = explode("-",$dat['date_second']);
		$this->templ->smarty->assign("year2",$year);
		$this->templ->smarty->assign("month2",$month);
		$this->templ->smarty->assign("day2",$day);
		$this->templ->smarty->assign("ampm_second",$dat['ampm_second']);
		$this->templ->smarty->assign("ampm_second_value",$this->util->test_drive_time_list(1,$dat['ampm_second']));
		$this->templ->smarty->assign("date_third",$dat['date_third']);
		list($year,$month,$day) = explode("-",$dat['date_third']);
		$this->templ->smarty->assign("year3",$year);
		$this->templ->smarty->assign("month3",$month);
		$this->templ->smarty->assign("day3",$day);
		$this->templ->smarty->assign("ampm_third",$dat['ampm_third']);
		$this->templ->smarty->assign("ampm_third_value",$this->util->test_drive_time_list(1,$dat['ampm_third']));
		$this->templ->smarty->assign("car1",$dat['car1']);
		$this->templ->smarty->assign("car2",$dat['car2']);
		$this->templ->smarty->assign("car3",$dat['car3']);
		$this->templ->smarty->assign("car4",$dat['car4']);
		// add 20190130 turbo対応
        $this->templ->smarty->assign("car5",$dat['car5']);
		$this->templ->smarty->assign("sei",$dat['sei']);
		$this->templ->smarty->assign("mei",$dat['mei']);
		$this->templ->smarty->assign("sei_kana",$dat['sei_kana']);
		$this->templ->smarty->assign("mei_kana",$dat['mei_kana']);
		$this->templ->smarty->assign("postcd",$dat['postcd']);
		$this->templ->smarty->assign("address",$dat['address']);
		$this->templ->smarty->assign("contact_kind",$dat['contact_kind']);
		$this->templ->smarty->assign("mail",$dat['mail']);
		$this->templ->smarty->assign("comment",$dat['comment']);
		// 車種
		if($dat['car1'] and $dat['car2']){
			$sql = "select name,name2 from car ";
			$sql .= " where car1 = '".$this->DB->getQStr($dat['car1'])."' ";
			$sql .= " and car2 = '".$this->DB->getQStr($dat['car2'])."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$dat['car_name'] = $rs->fields('name');
					if($dat['car2'] == "ge"){
						$dat['car_name'] .= " ガソリン";
					}
					else if($dat['car2'] == "de"){
						$dat['car_name'] .= " ディーゼル";
					}
					else if($dat['car2'] == "hev"){
						$dat['car_name'] .= " ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($dat['car2'] == 'skyx'){
                        $dat['car_name'] .= " SKYACTIV-X";
                    }
					if($dat['car4'] == "mt"){
						$dat['car_name'] .= " MT";
					}
					$dat['car_name'] .= " ".$dat['car3'];
					// add 20190130 turbo対応
                    if($dat['car5'] == "turbo"){
                        $dat['car_name'] .= " ターボ";
                    }
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign("car_name",$dat['car_name']);
		// 店舗
		$sql = "select name,tel from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($dat['shop_id'])."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		
		$rs =& $this->DB->ASExecute($sql);
		
		if($rs){
			if(!$rs->EOF){
				$dat['shop_name'] = $rs->fields('name');
				$dat['shop_tel'] = $rs->fields('tel');
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("shop_name",$dat['shop_name']);
		$this->templ->smarty->assign("shop_tel",$dat['shop_tel']);
		$this->templ->smarty->assign("edit_flg",$this->req->get('edit_flg'));
		$this->templ->smarty->display("cancel_conf.html");
		exit;
	}
	
	// 予約キャンセル画面(登録)
	function cancell_entry_proc(){
		if(!$_SESSION['disp_number']){
			header("Location:edit.php");
			exit;
		}
		if($this->req->get('ret')){
			$this->templ->smarty->assign("number",$this->req->get('number'));
			$this->templ->smarty->assign("car_name",$this->req->get('car_name'));
			$this->templ->smarty->assign("shop_name",$this->req->get('shop_name'));
			$this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
			$this->templ->smarty->assign("date_first",$this->req->get('date_first'));
			$this->templ->smarty->assign("year1",$this->req->get('year1'));
			$this->templ->smarty->assign("month1",$this->req->get('month1'));
			$this->templ->smarty->assign("day1",$this->req->get('day1'));
			$this->templ->smarty->assign("ampm_first",$this->req->get('ampm_first'));
			$this->templ->smarty->assign("ampm_first_value",$this->req->get('ampm_first_value'));
			$this->templ->smarty->assign("date_second",$this->req->get('date_second'));
			$this->templ->smarty->assign("year2",$this->req->get('year2'));
			$this->templ->smarty->assign("month2",$this->req->get('month2'));
			$this->templ->smarty->assign("day2",$this->req->get('day2'));
			$this->templ->smarty->assign("ampm_second",$this->req->get('ampm_second'));
			$this->templ->smarty->assign("ampm_second_value",$this->req->get('ampm_second_value'));
			$this->templ->smarty->assign("date_third",$this->req->get('date_third'));
			$this->templ->smarty->assign("year3",$this->req->get('year3'));
			$this->templ->smarty->assign("month3",$this->req->get('month3'));
			$this->templ->smarty->assign("day3",$this->req->get('day3'));
			$this->templ->smarty->assign("ampm_third",$this->req->get('ampm_third'));
			$this->templ->smarty->assign("ampm_third_value",$this->req->get('ampm_third_value'));
			$this->templ->smarty->assign("car1",$this->req->get('car1'));
			$this->templ->smarty->assign("car2",$this->req->get('car2'));
			$this->templ->smarty->assign("car3",$this->req->get('car3'));
			$this->templ->smarty->assign("car4",$this->req->get('car4'));
			// add 20190130 turbo対応
            $this->templ->smarty->assign("car5",$this->req->get('car5'));
			$this->templ->smarty->assign("sei",$this->req->get('sei'));
			$this->templ->smarty->assign("mei",$this->req->get('mei'));
			$this->templ->smarty->assign("sei_kana",$this->req->get('sei_kana'));
			$this->templ->smarty->assign("mei_kana",$this->req->get('mei_kana'));
			$this->templ->smarty->assign("postcd",$this->req->get('postcd'));
			$this->templ->smarty->assign("address",$this->req->get('address'));
			$this->templ->smarty->assign("contact_kind",$this->req->get('contact_kind'));
			$this->templ->smarty->assign("mail",$this->req->get('mail'));
			$this->templ->smarty->assign("campaign",$this->req->get('campaign'));
			$this->templ->smarty->assign("comment",$this->req->get('comment'));
			$this->templ->smarty->assign("edit_flg",$this->req->get('edit_flg'));
			$this->templ->smarty->display("edit.html");
			exit;
		}
		// チェック
		$sql = "select autono from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($_SESSION['disp_number'])."' ";
		$sql .= " and temporary_flg='2' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$autono = $rs->fields('autono');
			}
			$rs->Close();
		}
		
		if(empty($autono)){
			$this->templ->smarty->assign("error",'入力された予約番号がないか、有効期限が過ぎています。');
			$this->default_proc();
			return;
		}
		
		// 削除登録
		$record = null;
		$record['temporary_flg'] = '0';
		$record['disp_flg'] = '0';
		$record['del_flg'] = '1';
		$record['update_date'] = time();
		$where  = " disp_number='".$this->DB->getQStr($this->req->get('number'))."' ";
		$where .= " and temporary_flg='2' ";
		$where .= " and disp_flg='1' ";
		$where .= " and del_flg='0' ";
		$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
		header("Location:./mail.php?mode=cancell_end&number=".$this->req->get('number'));
	}
	
	// 予約変更画面(送信)
	function change_entry_proc(){
		if(!$_SESSION['disp_number']){
			header("Location:edit.php");
			exit;
		}
		// チェック
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($_SESSION['disp_number'])."' ";
		$sql .= " and temporary_flg='2' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		$data_list = array();
		if($rs){
			if(!$rs->EOF){
				$autono = $rs->fields('autono');
				$car1 = $rs->fields('car1');
				$car2 = $rs->fields('car2');
				$car3 = $rs->fields('car3');
				$car4 = $rs->fields('car4');
				// add 20190130 turbo対応
                $car5 = $rs->fields('car5');
				$shop_id = $rs->fields('shop_id');
			}
			$rs->Close();
		}
		if(empty($autono)){
			$this->templ->smarty->assign("error",'お客様のご予約番号に誤りがあります。');
			$this->default_proc();
			return;
		}
		// upd 20190130 turbo対応
		header("Location:./entry.php?car1=".$car1."&car2=".$car2."&car3=".$car3."&car4=".$car4."&car5=".$car5."&shop_id=".$shop_id);
		exit;
	}
}
?>