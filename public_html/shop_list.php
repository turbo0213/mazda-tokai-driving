<?php
include_once("../mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();

	}

	function execute(){
		switch($this->mode){
			case 'area':
				$this->area_proc();
			break;
			case 'shop':
				$this->shop_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
        if($this->req->get_get('flyer')){
            if(!$_SESSION['flyer']) {
                $_SESSION['flyer'] = $this->req->get_get('flyer');
                $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
            }
        }
        if($this->req->get_get('p')){
            if(!$_SESSION['param2']) {
                $_SESSION['param2'] = $this->req->get_get('p');
                $this->templ->smarty->assign("p",$this->req->get_get('p'));
            }
        }
        if($this->req->get_get('vcp76')){
            if(!$_SESSION['param3']) {
                $_SESSION['param3'] = $this->req->get_get('vcp76');
                $this->templ->smarty->assign("vcp76",$this->req->get_get('vcp76'));
            }
        }
        if($this->req->get_get('car1')) {
            $this->templ->smarty->assign("car1", $this->req->get_get('car1'));
        }
        if($this->req->get_get('car2')) {
            $this->templ->smarty->assign("car2", $this->req->get_get('car2'));
        }
        if($this->req->get_get('car3')) {
            $this->templ->smarty->assign("car3", $this->req->get_get('car3'));
        }
        if($this->req->get_get('car4')) {
            $this->templ->smarty->assign("car4", $this->req->get_get('car4'));
        }
        if($this->req->get_get('car5')) {
            $this->templ->smarty->assign("car5", $this->req->get_get('car5'));
        }
        $this->form_make();
        $this->templ->smarty->display("area.html");
        exit;
        if(!$_SESSION['step']){
            $_SESSION['step'] = "shop_list";
        }
		if($this->req->get_get('car1') and $this->req->get_get('car2')){
			$this->car_pref();
			$this->templ->smarty->assign("car_flg",1);
		}
		else{
			$this->pref_get();
			$this->templ->smarty->assign("shop_flg",1);
		}
		$this->templ->smarty->assign("c",$this->req->get_get('c'));
		$this->templ->smarty->assign("s",$this->req->get_get('s'));
		$this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));
		$this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));
		$this->templ->smarty->assign("car1",$this->req->get_get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get_get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get_get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get_get('car4'));
		// add 20190130 turbo追加対応
		$this->templ->smarty->assign("car5",$this->req->get_get('car5'));
/*
		if($this->req->get_get('vcp76')){
			$_SESSION['param'] = $this->req->get_get('vcp76');
		}
*/
		$this->templ->smarty->display("pref_list.html");
		exit;
	}

	function form_make(){
        if($this->req->get_get('car1') and $this->req->get_get('car2')) {
            $areaList = array('2301', '2302', '2303', '2401', '2101');
            foreach ($areaList as $key => $val) {
                $shop_list = array();
                $sql = "select s.shop_id as shop_id,s.name as shop_name,s.postcd as postcd,s.address as address,s.tel as tel from car_detail as c,shop as s";
                $sql .= " where s.shop_id = c.shop_id";
                $sql .= " and s.area_id = '".$this->DB->getQStr($val)."'";
                $sql .= " and c.end_date >= '" . $this->DB->getQStr(date("Y-m-d")) . "'";
                $sql .= " and c.car1 = '" . $this->DB->getQStr($this->req->get_get('car1')) . "'";
                $sql .= " and c.car2 = '" . $this->DB->getQStr($this->req->get_get('car2')) . "'";
                if ($this->req->get_get('car3')) {
                    $sql .= " and c.car3 = '" . $this->DB->getQStr($this->req->get_get('car3')) . "'";
                } else {
                    $sql .= " and (c.car3 is NULL or c.car3 = '')";
                }
                if ($this->req->get_get('car4')) {
                    $sql .= " and c.car4 = '" . $this->DB->getQStr($this->req->get_get('car4')) . "'";
                } else {
                    $sql .= " and (c.car4 is NULL or c.car4 = '')";
                }
                if ($this->req->get_get('car5')) {
                    $sql .= " and c.car5 = '" . $this->DB->getQStr($this->req->get_get('car5')) . "'";
                } else {
                    $sql .= " and (c.car5 is NULL or c.car5 = '')";
                }
                $sql .= " and c.disp_flg='1'";
                $sql .= " and c.del_flg='0'";
                $sql .= " and s.disp_flg='1'";
                $sql .= " and s.del_flg='0'";
                $sql .= " order by s.area_id,s.order_no";
                $rs =& $this->DB->ASExecute($sql);
                if ($rs) {
                    while (!$rs->EOF) {
                        $data = array();
                        $data['shop_id'] = $rs->fields('shop_id');
                        $data['shop_name'] = $rs->fields('shop_name');
                        $data['postcd'] = $rs->fields('postcd');
                        $data['address'] = $rs->fields('address');
                        $data['tel'] = $rs->fields('tel');
                        $shop_list[] = $data;
                        $rs->MoveNext();
                    }
                    $rs->Close();
                }
                $this->templ->smarty->assign("result".$val, $shop_list);
            }
        }
    }

	function area_proc(){
		if(!$this->req->get_get('pref_id')){
			header("Location:shop_list.php");
		}
        if(!$_SESSION['step']){
            $_SESSION['step'] = "shop_list";
        }
        if($this->req->get_get('flyer')){
            if(!$_SESSION['flyer']) {
                $_SESSION['flyer'] = $this->req->get_get('flyer');
                $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
            }
        }
        if($this->req->get_get('p')){
            if(!$_SESSION['param2']) {
                $_SESSION['param2'] = $this->req->get_get('p');
                $this->templ->smarty->assign("p",$this->req->get_get('p'));
            }
        }
        if($this->req->get_get('vcp76')){
            if(!$_SESSION['param3']) {
                $_SESSION['param3'] = $this->req->get_get('vcp76');
                $this->templ->smarty->assign("vcp76",$this->req->get_get('vcp76'));
            }
        }
		if($this->req->get_get('car1') and $this->req->get_get('car2')){
			$this->car_area();
			$this->templ->smarty->assign("car_flg",1);
		}
		else{
			$this->area_get();
			$this->templ->smarty->assign("shop_flg",1);
		}
		$this->templ->smarty->assign("car1",$this->req->get_get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get_get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get_get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get_get('car4'));
		// add 20190130 turbo追加対応
		$this->templ->smarty->assign("car5",$this->req->get_get('car5'));
		$this->templ->smarty->assign("c",$this->req->get_get('c'));
		$this->templ->smarty->assign("s",$this->req->get_get('s'));
		$this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));
		$this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));
		switch($this->req->get_get('pref_id')){
			// 岐阜県
			case '21':
				$this->templ->smarty->display("gifu_list.html");
			exit;
			// 愛知県
			case '23':
				$this->templ->smarty->display("aichi_list.html");
			exit;
			// 三重県
			case '24':
				$this->templ->smarty->display("mie_list.html");
			exit;
		}
	}

	function shop_proc(){
        if(!$_SESSION['step']){
            $_SESSION['step'] = "shop_list";
        }
        if($this->req->get_get('flyer')){
            if(!$_SESSION['flyer']) {
                $_SESSION['flyer'] = $this->req->get_get('flyer');
                $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
            }
        }
        if($this->req->get_get('p')){
            if(!$_SESSION['param2']) {
                $_SESSION['param2'] = $this->req->get_get('p');
                $this->templ->smarty->assign("p",$this->req->get_get('p'));
            }
        }
        if($this->req->get_get('vcp76')){
            if(!$_SESSION['param3']) {
                $_SESSION['param3'] = $this->req->get_get('vcp76');
                $this->templ->smarty->assign("vcp76",$this->req->get_get('vcp76'));
            }
        }
		if($this->req->get_get('car1') and $this->req->get_get('car2')){
			$this->car_shop();
			$this->templ->smarty->assign("car_flg",1);
		}
		else{
			$this->shop_get();
			$this->templ->smarty->assign("shop_flg",1);
		}
		$this->templ->smarty->assign("car1",$this->req->get_get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get_get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get_get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get_get('car4'));
		// add 20190130 turbo追加対応
		$this->templ->smarty->assign("car5",$this->req->get_get('car5'));
		$this->templ->smarty->assign("c",$this->req->get_get('c'));
		$this->templ->smarty->assign("s",$this->req->get_get('s'));
		$this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));
		$this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));
		$this->templ->smarty->display("shop_list.html");
		exit;
	}

	function pref_get(){
		$pref_list = array();
		$sql = "select distinct(pref_id) as pref_id from shop ";
		$sql .= " where del_flg = '0'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$pref_list[] = $rs->fields('pref_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($pref_list){
			if(is_array($pref_list)){
				foreach($pref_list as $key => $val){
					$sql = "select count(shop_id) as shop_num from shop";
					$sql .= " where pref_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and del_flg = '0'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$this->templ->smarty->assign("shop_num".$val,$rs->fields('shop_num'));
						}
						$rs->Close();
					}
					$sql = "select count(ca.autono) as car_num from car_detail as ca,shop as s";
					$sql .= " where ca.shop_id = s.shop_id";
					$sql .= " and s.pref_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
					$sql .= " and s.del_flg = '0'";
					$sql .= " and ca.del_flg = '0'";
					$sql .= " and ca.disp_flg = '1'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$this->templ->smarty->assign("car_num".$val,$rs->fields('car_num'));
						}
						$rs->Close();
					}
				}
			}
		}
	}

	function area_get(){
		$area_list = array();
		$sql = "select distinct(area_id) as area_id from shop ";
		$sql .= " where del_flg = '0'";
		$sql .= " and pref_id = '".$this->DB->getQStr($this->req->get_get('pref_id'))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$area_list[] = $rs->fields('area_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($area_list){
			if(is_array($area_list)){
				foreach($area_list as $key => $val){
					$sql = "select count(shop_id) as shop_num from shop";
					$sql .= " where area_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and del_flg = '0'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$this->templ->smarty->assign("shop_num".$val,$rs->fields('shop_num'));
						}
						$rs->Close();
					}
					$sql = "select count(ca.autono) as car_num from car_detail as ca,shop as s";
					$sql .= " where ca.shop_id = s.shop_id";
					$sql .= " and s.area_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
					$sql .= " and s.del_flg = '0'";
					$sql .= " and ca.del_flg = '0'";
					$sql .= " and ca.disp_flg = '1'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$this->templ->smarty->assign("car_num".$val,$rs->fields('car_num'));
						}
						$rs->Close();
					}
				}
			}
		}
	}

	function shop_get(){
		$sql = "select * from shop ";
		$sql .= " where pref_id='".$this->DB->getQStr($this->req->get_get('pref_id'))."' ";
		$sql .= " and area_id='".$this->DB->getQStr($this->req->get_get('area_id'))."' ";
		$sql .= " and del_flg = '0'";
		$sql .= " order by station_code,order_no ";
		$rs =& $this->DB->ASExecute($sql);
		$data_list = array();
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['shop_id'] = $rs->fields('shop_id');
				$dat['pref_id'] = $rs->fields('pref_id');
				$dat['area_id'] = $rs->fields('area_id');
				$dat['name'] = $rs->fields('name');
				$dat['postcd'] = $rs->fields('postcd');
				$dat['address'] = $rs->fields('address');
				$dat['tel'] = $rs->fields('tel');
				$car_data = $this->car_data($rs->fields('shop_id'));
				if($car_data){
					if(is_array($car_data)){
						foreach($car_data as $key2 => $val2){
							$dat[$val2] = 1;
						}
					}
				}
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$area_name = "";
		if($this->req->get_get('pref_id')){
			$area_name = $this->util->prefecture_list(1,$this->req->get_get('pref_id'));
		}
		if($this->req->get_get('area_id')){
			$area_name .= " ".$this->util->area_list(1,$this->req->get_get('area_id'));
		}
		$this->templ->smarty->assign("area_name",$area_name);
		$this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));
		$this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));
		$this->templ->smarty->assign("data_list",$data_list);
	}

	function car_pref(){
		$pref_list = array();
		$sql = "select distinct(pref_id) as pref_id from shop ";
		$sql .= " where del_flg = '0'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$pref_list[] = $rs->fields('pref_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($pref_list){
			if(is_array($pref_list)){
				foreach($pref_list as $key => $val){
					$shop_num = 0;
					$car_num = 0;
					$sql = "select shop_id from shop";
					$sql .= " where pref_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and del_flg = '0'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						while(!$rs->EOF){
							$sql2 = "select count(ca.autono) as car_num from car_detail as ca,car as c";
							$sql2 .= " where ca.car_id = c.car_id";
							$sql2 .= " and ca.shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
							$sql2 .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
							$sql2 .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
							$sql2 .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
							if($this->req->get_get('car3')){
								$sql2 .= " and ca.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
							}
							if($this->req->get_get('car4')){
								$sql2 .= " and ca.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
							}
							// add 20190130 turbo追加対応
							if($this->req->get_get('car5')){
								$sql2 .= " and ca.car5 = '".$this->DB->getQStr($this->req->get_get('car5'))."'";
							}
							else{
								$sql2 .= " and (ca.car5 is NULL or ca.car5 = '')";
							}
							$sql2 .= " and c.del_flg = '0'";
							$sql2 .= " and ca.del_flg = '0'";
							$sql2 .= " and ca.disp_flg = '1'";
							$rs2 =& $this->DB->ASExecute($sql2);
							if($rs2){
								if(!$rs2->EOF){
									if((int)$rs2->fields('car_num') > 0){
										$shop_num++;
									}
									$car_num += (int)$rs2->fields('car_num');
								}
								$rs2->Close();
							}
							$rs->MoveNext();
						}
						$rs->Close();
					}
					$this->templ->smarty->assign("shop_num".$val,$shop_num);
					$this->templ->smarty->assign("car_num".$val,$car_num);
				}
			}
		}
		$area_name = "";
		$sql = "select * from car";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		//$sql .= " and car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
		$sql .= " limit 1";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$area_name = $rs->fields('name');
			}
			$rs->Close();
		}
		if($this->req->get_get('car2') == "ge"){
			$area_name .= " ガソリン";
		}
		else if($this->req->get_get('car2') == "de"){
			$area_name .= " ディーゼル";
		}
		else if($this->req->get_get('car2') == "hev"){
			$area_name .= " ハイブリッド";
		}
        // add 20200109 SKYACTIV-X対応
        else if($this->req->get_get('car2') == 'skyx'){
            $area_name .= " SKYACTIV-X";
        }
		if($this->req->get_get('car4') == "mt"){
			$area_name .= " MT";
		}
		$area_name .= " ".$this->req->get_get('car3');
		// add 20190130 turbo追加対応
		if($this->req->get_get('car5') == "turbo"){
			$area_name .= " ターボ";
		}
		$this->templ->smarty->assign("area_name",$area_name);
	}

	function car_area(){
		$area_list = array();
		$sql = "select distinct(area_id) as area_id from shop ";
		$sql .= " where del_flg = '0'";
		$sql .= " and pref_id = '".$this->DB->getQStr($this->req->get_get('pref_id'))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$area_list[] = $rs->fields('area_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($area_list){
			if(is_array($area_list)){
				foreach($area_list as $key => $val){
					$shop_num = 0;
					$car_num = 0;
					$sql = "select shop_id from shop";
					$sql .= " where area_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and del_flg = '0'";
					$sql .= " order by station_code,order_no ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						while(!$rs->EOF){
							$sql2 = "select count(ca.autono) as car_num from car_detail as ca,car as c";
							$sql2 .= " where ca.car_id = c.car_id";
							$sql2 .= " and ca.shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
							$sql2 .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
							$sql2 .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
							$sql2 .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
							if($this->req->get_get('car3')){
								$sql2 .= " and ca.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
							}
							if($this->req->get_get('car4')){
								$sql2 .= " and ca.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
							}
							// add 20190130 turbo追加対応
							if($this->req->get_get('car5')){
								$sql2 .= " and ca.car5 = '".$this->DB->getQStr($this->req->get_get('car5'))."'";
							}
							else{
								$sql2 .= " and (ca.car5 is NULL or ca.car5 = '')";
							}
							$sql2 .= " and c.del_flg = '0'";
							$sql2 .= " and ca.del_flg = '0'";
							$sql2 .= " and ca.disp_flg = '1'";
							$rs2 =& $this->DB->ASExecute($sql2);
							if($rs2){
								if(!$rs2->EOF){
									if((int)$rs2->fields('car_num') > 0){
										$shop_num++;
									}
									$car_num += (int)$rs2->fields('car_num');
								}
								$rs2->Close();
							}
							$rs->MoveNext();
						}
						$rs->Close();
					}
					$this->templ->smarty->assign("shop_num".$val,$shop_num);
					$this->templ->smarty->assign("car_num".$val,$car_num);
				}
			}
		}
		$area_name = "";
		$sql = "select * from car";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		//$sql .= " and car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
		$sql .= " limit 1";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$area_name = $rs->fields('name');
			}
			$rs->Close();
		}
		if($this->req->get_get('car2') == "ge"){
			$area_name .= " ガソリン";
		}
		else if($this->req->get_get('car2') == "de"){
			$area_name .= " ディーゼル";
		}
		else if($this->req->get_get('car2') == "hev"){
			$area_name .= " ハイブリッド";
		}
        // add 20200109 SKYACTIV-X対応
        else if($this->req->get_get('car2') == 'skyx'){
            $area_name .= " SKYACTIV-X";
        }
		if($this->req->get_get('car4') == "mt"){
			$area_name .= " MT";
		}
		$area_name .= " ".$this->req->get_get('car3');
		// add 20190130 turbo追加対応
		if($this->req->get_get('car5') == "turbo"){
			$area_name .= " ターボ";
		}
		$this->templ->smarty->assign("area_name",$area_name);
	}

	function car_shop(){
		$data_list = array();
		$shop_list = array();
		$sql = "select shop_id from shop ";
		$sql .= " where del_flg = '0'";
		$sql .= " and pref_id = '".$this->DB->getQStr($this->req->get_get('pref_id'))."'";
		$sql .= " and area_id = '".$this->DB->getQStr($this->req->get_get('area_id'))."'";
		$sql .= " order by station_code,order_no ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$shop_list[] = $rs->fields('shop_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($shop_list){
			if(is_array($shop_list)){
				foreach($shop_list as $key => $val){
					$sql = "select * from shop";
					$sql .= " where shop_id = '".$this->DB->getQStr($val)."'";
					$sql .= " and del_flg = '0'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						while(!$rs->EOF){
							$find_flg = false;
							$sql2 = "select count(ca.autono) as car_num from car_detail as ca,car as c";
							$sql2 .= " where ca.car_id = c.car_id";
							$sql2 .= " and ca.shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
							$sql2 .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
							$sql2 .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
							$sql2 .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
							if($this->req->get_get('car3')){
								$sql2 .= " and ca.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
							}
							if($this->req->get_get('car4')){
								$sql2 .= " and ca.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
							}
							// add 20190130 turbo追加対応
							if($this->req->get_get('car5')){
								$sql2 .= " and ca.car5 = '".$this->DB->getQStr($this->req->get_get('car5'))."'";
							}
							else{
								$sql2 .= " and (ca.car5 is NULL or ca.car5 = '')";
							}
							$sql2 .= " and c.del_flg = '0'";
							$sql2 .= " and ca.del_flg = '0'";
							$sql2 .= " and ca.disp_flg = '1'";
							$rs2 =& $this->DB->ASExecute($sql2);
							if($rs2){
								if(!$rs2->EOF){
									if((int)$rs2->fields('car_num') > 0){
										$find_flg = true;
									}
								}
								$rs2->Close();
							}
							if($find_flg){
								$dat = array();
								$dat['shop_id'] = $rs->fields('shop_id');
								$dat['pref_id'] = $rs->fields('pref_id');
								$dat['area_id'] = $rs->fields('area_id');
								$dat['name'] = $rs->fields('name');
								$dat['postcd'] = $rs->fields('postcd');
								$dat['address'] = $rs->fields('address');
								$dat['tel'] = $rs->fields('tel');
								$car_data = $this->car_data($rs->fields('shop_id'));
								if($car_data){
									if(is_array($car_data)){
										foreach($car_data as $key2 => $val2){
											$dat[$val2] = 1;
										}
									}
								}
								$data_list[$rs->fields('shop_id')] = $dat;
							}
							$rs->MoveNext();
						}
						$rs->Close();
					}
					$this->templ->smarty->assign("shop_num".$val,$shop_num);
					$this->templ->smarty->assign("car_num".$val,$car_num);
				}
			}
		}
		$area_name = "";
		$sql = "select * from car";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		//$sql .= " and car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
		$sql .= " limit 1";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$area_name = $rs->fields('name');
			}
			$rs->Close();
		}
		if($this->req->get_get('car2') == "ge"){
			$area_name .= " ガソリン";
		}
		else if($this->req->get_get('car2') == "de"){
			$area_name .= " ディーゼル";
		}
		else if($this->req->get_get('car2') == "hev"){
			$area_name .= " ハイブリッド";
		}
        // add 20200109 SKYACTIV-X対応
        else if($this->req->get_get('car2') == 'skyx'){
            $area_name .= " SKYACTIV-X";
        }
		if($this->req->get_get('car4') == "mt"){
			$area_name .= " MT";
		}
		$area_name .= " ".$this->req->get_get('car3');
		// add 20190130 turbo追加対応
		if($this->req->get_get('car5') == "turbo"){
			$area_name .= " ターボ";
		}
		$this->templ->smarty->assign("area_name",$area_name);
		$this->templ->smarty->assign("data_list",$data_list);
	}

	function car_data($shop_id){
		$car_data = array();
		$sql = "select c.car1 as car1 from car_detail as ca,car as c";
		$sql .= " where c.car_id = ca.car_id";
		$sql .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
		$sql .= " and ca.shop_id = '".$this->DB->getQStr($shop_id)."'";
		$sql .= " and ca.disp_flg='1'";
		$sql .= " and ca.del_flg='0'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$car_data[$rs->fields('car1')] = $rs->fields('car1');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $car_data;
	}
}
?>