<?php
include_once("../../mc_apl/top.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB2;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB2 = new ASDB_MAIN();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			// 予約情報(確認)
			case 'check':
				$this->check_proc();
			break;
			// 予約情報(登録)
			case 'entry':
				$this->entry_proc();
			break;
			//郵便番号検索
//			case 'postcd_search':
//				$this->postcd_search_proc();
//			break;
			// 予約情報(入力)
			default:
				$this->default_proc();
			break;
		}
	}

	// データセット
	function post_proc(){
		$this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
		$this->templ->smarty->assign("date_first",$this->req->get('date_first'));
		list($year,$month,$day) = explode("/",$this->req->get('date_first'));
		$this->templ->smarty->assign("year1",$year);
		$this->templ->smarty->assign("month1",$month);
		$this->templ->smarty->assign("day1",$day);
        $this->templ->smarty->assign("first_time",$this->req->get('first_time'));
		$this->templ->smarty->assign("ampm_first",$this->req->get('ampm_first'));
		$this->templ->smarty->assign("ampm_first_value",$this->util->test_drive_time_list(1,$this->req->get('ampm_first')));
		$this->templ->smarty->assign("date_second",$this->req->get('date_second'));
		list($year,$month,$day) = explode("/",$this->req->get('date_second'));
		$this->templ->smarty->assign("year2",$year);
		$this->templ->smarty->assign("month2",$month);
		$this->templ->smarty->assign("day2",$day);
		$this->templ->smarty->assign("ampm_second",$this->req->get('ampm_second'));
		$this->templ->smarty->assign("ampm_second_value",$this->util->test_drive_time_list(1,$this->req->get('ampm_second')));
		$this->templ->smarty->assign("date_third",$this->req->get('date_third'));
		list($year,$month,$day) = explode("/",$this->req->get('date_third'));
		$this->templ->smarty->assign("year3",$year);
		$this->templ->smarty->assign("month3",$month);
		$this->templ->smarty->assign("day3",$day);
		$this->templ->smarty->assign("ampm_third",$this->req->get('ampm_third'));
		$this->templ->smarty->assign("ampm_third_value",$this->util->test_drive_time_list(1,$this->req->get('ampm_third')));
		$this->templ->smarty->assign("car1",$this->req->get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get('car4'));
		// add 20190130 turbo対応
        $this->templ->smarty->assign("car5",$this->req->get('car5'));
		$this->templ->smarty->assign("area_id",$this->req->get('area_id'));
		$this->templ->smarty->assign("pref_id",$this->req->get('pref_id'));
		$this->templ->smarty->assign("sei",$this->req->get('sei'));
		$this->templ->smarty->assign("mei",$this->req->get('mei'));
		$this->templ->smarty->assign("sei_kana",$this->req->get('sei_kana'));
		$this->templ->smarty->assign("mei_kana",$this->req->get('mei_kana'));
		$this->templ->smarty->assign("postcd",$this->req->get('postcd'));
//		$this->templ->smarty->assign("postcd1",$this->req->get('postcd1'));
//		$this->templ->smarty->assign("postcd2",$this->req->get('postcd2'));
		$this->templ->smarty->assign("address",$this->req->get('address'));
		$this->templ->smarty->assign("contact_kind",$this->req->get('contact_kind'));
		$this->templ->smarty->assign("tel1",$this->req->get('tel1'));
//		$this->templ->smarty->assign("tel2",$this->req->get('tel2'));
//		$this->templ->smarty->assign("tel3",$this->req->get('tel3'));
		$this->templ->smarty->assign("mail",$this->req->get('mail'));
		$this->templ->smarty->assign("kiyaku_check",$this->req->get('kiyaku_check'));
		$this->templ->smarty->assign("comment",$this->req->get('comment'));
		$this->templ->smarty->assign("v_schedule_hour",$this->req->get('v_schedule_hour'));
		$this->templ->smarty->assign("v_schedule_min",$this->req->get('v_schedule_min'));
		$this->templ->smarty->assign("mail",$this->req->get('mail'));
		$this->templ->smarty->assign("c",$this->req->get('c'));
		$this->templ->smarty->assign("s",$this->req->get('s'));
        // add 20190329 WEB会員フラグ追加
        $this->templ->smarty->assign("member_flg",$this->req->get('member_flg'));
		$car1 = "";
		$car2 = "";
		$car3 = "";
		$car4 = "";
		// add 20190130 turbo対応
        $car5 = "";
		$shop_id = "";
		$pref_id = "";
		$area_id = "";
		$car1 = $this->req->get('car1');
		$car2 = $this->req->get('car2');
		$car3 = $this->req->get('car3');
		$car4 = $this->req->get('car4');
		// add 20190130 turbo対応
        $car5 = $this->req->get('car5');
		$shop_id = $this->req->get('shop_id');
		// 車種
		$car_name = "";
		$sql = "select name,name2 from car ";
		$sql .= " where car1 = '".$this->DB->getQStr($car1)."' ";
		$sql .= " and car2 = '".$this->DB->getQStr($car2)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$engine = "";
				if($car2 == "ge"){
					$engine = "ガソリン";
				}
				else if($car2 == "de"){
					$engine = "ディーゼル";
				}
				else if($car2 == "hev"){
					$engine = "ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($car2 == 'skyx'){
                    $engine = "SKYACTIV-X";
                }
                // add 20201009 e-SKYACTIV G対応
                else if($car2 == 'eskyg'){
                    $engine = "e-SKYACTIV G";
                }
				$mt = "";
				if($car4 == "mt"){
					$mt = " MT";
				}
				// add 20190130 turbo対応
                $turbo = "";
                if($car5 == "turbo"){
                    $turbo = " ターボ";
                }
				if($engine){
					if($car4){
						$car_name = $rs->fields('name')." ".$engine." ".$mt." ".$car3.$turbo;
					}
					else{
						$car_name = $rs->fields('name')." ".$engine." ".$car3.$turbo;
					}
				}
				else{
					if($mt){
						$car_name = $rs->fields('name')." ".$mt." ".$car3.$turbo;
					}
					else{
						$car_name = $rs->fields('name')." ".$car3.$turbo;
					}
				}
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("car_name",$car_name);
		// 店舗
		$sql = "select name from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($shop_id)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		$shop_name = "";
		if($rs){
			if(!$rs->EOF){
				$shop_name = $rs->fields('name');
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("shop_name",$shop_name);
	}

	// 入力データチェック
	function datacheck_proc(){
		mb_regex_encoding('UTF-8');
		mb_internal_encoding('UTF-8');
		$error = "";
        $first_error = false;
        $second_error = false;
        $third_error = false;
        $second_input = false;
        $third_input = false;
        if(!$this->req->get('date_first')){
            $error .= "第1希望の日付を選択してください\r\n";
            $first_error = true;
        }
        else{
            list($year1,$month1,$day1) = explode("/",$this->req->get('date_first'));
            if(!checkdate($month1,$day1,$year1)){
                $error .= "第1希望の日付を正しく選択してください\r\n";
                $first_error = true;
            }
        }
//        if(!$this->req->get('ampm_first')){
//            $error .= "第1希望の時間帯を選択してください\r\n";
//            $first_error = true;
//        }
//        else if($this->req->get('ampm_first') != '1' and $this->req->get('ampm_first') != '2'){
//            $error .= "第1希望の時間帯を正しく選択してください\r\n";
//            $first_error = true;
//        }
//        if($this->req->get('date_second')){
//            $second_input = true;
//            list($year2,$month2,$day2) = explode("/",$this->req->get('date_second'));
//            if(!checkdate($month2,$day2,$year2)){
//                $error .= "第2希望の日付を正しく選択してください\r\n";
//                $second_error = true;
//            }
//        }
//        if($this->req->get('ampm_second')){
//            if($this->req->get('ampm_second') != '1' and $this->req->get('ampm_second') != '2'){
//                $error .= "第2希望の時間帯を正しく選択してください\r\n";
//                $second_error = true;
//            }
//        }
//        else if($second_input){
//            $error .= "第2希望の時間帯を選択してください\r\n";
//        }
//        if($this->req->get('date_third')){
//            $third_input = true;
//            list($year3,$month3,$day3) = explode("/",$this->req->get('date_third'));
//            if(!checkdate($month3,$day3,$year3)){
//                $error .= "第3希望の日付を正しく選択してください\r\n";
//                $third_error = true;
//            }
//        }
//        if($this->req->get('ampm_third')){
//            if($this->req->get('ampm_third') != '1' and $this->req->get('ampm_third') != '2'){
//                $error .= "第3希望の時間帯を正しく選択してください\r\n";
//                $third_error = true;
//            }
//        }
//        else if($third_input){
//            $error .= "第3希望の時間帯を選択してください\r\n";
//        }
//        if(!$first_error and !$second_error and !$third_error){
//            if($second_input){
//                if(mktime(0,0,0,$month1,$day1,$year1) == mktime(0,0,0,$month2,$day2,$year2)){
//                    if($this->req->get('ampm_first') == $this->req->get('ampm_second')){
//                        $error .= "第1希望と第2希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
//                    }
//                    else if($third_input){
//                        if(mktime(0,0,0,$month2,$day2,$year2) == mktime(0,0,0,$month3,$day3,$year3)){
//                            if($this->req->get('ampm_second') == $this->req->get('ampm_third')){
//                                $error .= "第2希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
//                            }
//                        }
//                    }
//                }
//                else if($third_input){
//                    if(mktime(0,0,0,$month2,$day2,$year2) == mktime(0,0,0,$month3,$day3,$year3)){
//                        if($this->req->get('ampm_second') == $this->req->get('ampm_third')){
//                            $error .= "第2希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
//                        }
//                    }
//                }
//            }
//            if($third_input){
//                if(mktime(0,0,0,$month1,$day1,$year1) == mktime(0,0,0,$month3,$day3,$year3)){
//                    if($this->req->get('ampm_first') == $this->req->get('ampm_third')){
//                        $error .= "第1希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
//                    }
//                }
//            }
//        }
		// 名前
		if($this->req->get('sei') == null){
			$error .= "名前・姓を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('sei'), "UTF-8" ) > 100 ){
			$error .= "名前・姓を100文字以内で入力してください\r\n";
		}
        if(!$this->req->get('member_flg')) {
            if ($this->req->get('mei') == null) {
                $error .= "名前・名を入力してください\r\n";
            } else if (mb_strlen($this->req->get('mei'), "UTF-8") > 100) {
                $error .= "名前・名を100文字以内で入力してください\r\n";
            }
        }
		if($this->req->get('sei_kana') == null){
			//$error .= "フリガナ・姓を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('sei_kana'), "UTF-8" ) > 100 ){
			$error .= "フリガナ・姓を100文字以内で入力してください\r\n";
		}
		else{
			$str = $this->req->get('sei_kana');
			if(!mb_ereg('^[ｦ-ﾟァ-ヶa-zA-Zー\-]+$', $str)) {
			    $error .= "フリガナ・姓をカタカナまたは英字で入力してください\r\n";
			}
		}
        if(!$this->req->get('member_flg')) {
            if ($this->req->get('mei_kana') == null) {
                //$error .= "フリガナ・名を入力してください\r\n";
            } else if (mb_strlen($this->req->get('mei_kana'), "UTF-8") > 100) {
                $error .= "フリガナ・名を100文字以内で入力してください\r\n";
            } else {
                $str = $this->req->get('mei_kana');
                if (!mb_ereg('^[ｦ-ﾟァ-ヶa-zA-Zー\-]+$', $str)) {
                    $error .= "フリガナ・名をカタカナまたは英字で入力してください\r\n";
                }
            }
        }
		// 郵便番号
//		if($this->req->get('postcd1') != null or $this->req->get('postcd2') != null){
		if($this->req->get('postcd') != null){
			if(!preg_match("/^[0-9]{3}-[0-9]{4}$|^[0-9]{7}$/",$this->req->get('postcd'))){
				$error .= "郵便番号を半角数字か、半角数字・ハイフンで入力してください\r\n";
			}
//			$check1 = null;
//			$check2 = null;
//			$check1 = $this->util->number_check($this->req->get('postcd1'));
//			$check2 = $this->util->number_check($this->req->get('postcd2'));
//			if(!empty($check1) or !empty($check2)) $error .= "郵便番号を数字で入力してください\r\n";
		}
		// 住所
		if($this->req->get('address') == null){
			$error .= "お住まいの地域を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('address'), "UTF-8" ) > 500 ){
			$error .= "お住まいの地域を500文字以内で入力してください\r\n";
		}
		if($this->req->get('contact_kind') == "1"){
			// 電話番号
			if($this->req->get('tel1') == null){
				$error .= "電話番号を入力してください\r\n";
			}
			else{
				$check1 = null;
				$check1 = $this->util->number_check($this->req->get('tel1'));
				if(!empty($check1)){
					$error .= "電話番号を数字で入力してください\r\n";
				}
				else{
					if(!mb_ereg("^[0-9]{1,15}$", $this->req->get('tel1'))){
						$error .= "電話番号を正しく入力してください\r\n";
					}
				}
			}
/*
			if($this->req->get('tel1') == null or $this->req->get('tel2') == null or $this->req->get('tel3') == null){
				$error .= "電話番号を入力してください\r\n";
			}
			else{
				$check1 = null;
				$check2 = null;
				$check3 = null;
				$check1 = $this->util->number_check($this->req->get('tel1'));
				$check2 = $this->util->number_check($this->req->get('tel2'));
				$check3 = $this->util->number_check($this->req->get('tel3'));
				if(!empty($check1) or !empty($check2) or !empty($check3)){
					$error .= "電話番号を数字で入力してください\r\n";
				}
				else{
					if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel1'))){
						$error .= "電話番号1を正しく入力してください\r\n";
					}
					if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel2'))){
						$error .= "電話番号2を正しく入力してください\r\n";
					}
					if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel3'))){
						$error .= "電話番号3を正しく入力してください\r\n";
					}
				}
			}
*/
			// 連絡可能な時間帯
			if(!$this->req->get('v_schedule_hour') and !$this->req->get('v_schedule_min')){
				$error .= "ご連絡可能な時間帯を選択してください\r\n";
			}
			else{
				if($this->req->get('v_schedule_hour') != null){
					$check1 = $this->util->number_check($this->req->get('v_schedule_hour'));
					if(!empty($check1)){
						$error .= "ご連絡可能な時間帯(時)を正しく選択してください\r\n";
					}
					else if(!$this->util->schedule_hour_list(1,$this->req->get('v_schedule_hour'))){
						$error .= "ご連絡可能な時間帯(時)を正しく選択してください\r\n";
					}
					else if($this->req->get('v_schedule_min') == null){
						$error .= "ご連絡可能な時間帯(分)を選択してください\r\n";
					}
				}
				if($this->req->get('v_schedule_min') != null){
					$check1 = $this->util->number_check($this->req->get('v_schedule_min'));
					if(!empty($check1)){
						$error .= "ご連絡可能な時間帯(分)を正しく選択してください\r\n";
					}
					else if(!$this->util->schedule_min_list(1,$this->req->get('v_schedule_min'))){
						$error .= "ご連絡可能な時間帯(分)を正しく選択してください\r\n";
					}
					else if($this->req->get('v_schedule_hour') == null){
						$error .= "ご連絡可能な時間帯(時)を選択してください\r\n";
					}
				}
			}
		}
		else if($this->req->get('contact_kind') == "2"){
			// Mail
			if($this->req->get('mail') == null){
				$error .= "Mailを入力してください\r\n";
			}
			else{
				$check1 = null;
				$check1 = $this->util->mail_check('Mail',$this->req->get('mail'));
				if(!empty($check1)){
					$error .= "Mailの形式が正しくありません\r\n";
					$mail1_flg = true;
				}
				else if(!preg_match("/^.{1,200}$/",$this->req->get('mail'))){
					$error .= "Mailを200文字以内で入力してください\r\n";
					$mail1_flg = true;
				}
			}
		}
		else{
			$error .= "ご連絡先を選択してください\r\n";
		}
		// その他ご意見・ご要望
		if($this->req->get('comment') != null){
			if( mb_strlen($this->req->get('comment'), "UTF-8" ) > 1000 ){
				$error .= "その他ご意見・ご要望を1000文字以内で入力してください\r\n";
			}
		}
		if($this->req->get('kiyaku_check') == null){
			$error .= "規約に同意されていません\r\n";
		}
		return $error;
	}
/*
	//郵便番号検索
	function postcd_search_proc(){
		list($prefecture,$address) = $this->util->searchAddr(POSTCD_FILE,$_REQUEST['p1'],$_REQUEST['p2']);
		echo $prefecture.",".$address;
	}
*/
	//  仮押さえ削除
	function delete_proc(){
		// 仮押さえ登録
//		$ret = $this->util->del_reservation($this->DB,$this->req->get_get('number'));
	}
	
	// 予約情報入力(入力)
	function default_proc(){
        $non_flg = false;
        if($this->req->get_get("shop_id")){
            $_SESSION['reservation']['shop_id'] = $this->req->get_get("shop_id");
        }
        elseif($this->req->get("shop_id")){
            $_SESSION['reservation']['shop_id'] = $this->req->get("shop_id");
        }
        else{
            $non_flg = true;
        }
        if($this->req->get_get("car1")){
            $_SESSION['reservation']['car1'] = $this->req->get_get("car1");
        }
        elseif($this->req->get("car1")){
            $_SESSION['reservation']['car1'] = $this->req->get("car1");
        }
        else{
            $non_flg = true;
        }
        if($this->req->get_get("car2")){
            $_SESSION['reservation']['car2'] = $this->req->get_get("car2");
        }
        elseif($this->req->get("car2")){
            $_SESSION['reservation']['car2'] = $this->req->get("car2");
        }
        else{
            $non_flg = true;
        }
        if($this->req->get_get("car3")){
            $_SESSION['reservation']['car3'] = $this->req->get_get("car3");
        }
        elseif($this->req->get("car3")){
            $_SESSION['reservation']['car3'] = $this->req->get("car3");
        }
        else{
            $_SESSION['reservation']['car3'] = NULL;
        }
        if($this->req->get_get("car4")){
            $_SESSION['reservation']['car4'] = $this->req->get_get("car4");
        }
        elseif($this->req->get("car4")){
            $_SESSION['reservation']['car4'] = $this->req->get("car4");
        }
        else{
            $_SESSION['reservation']['car4'] = NULL;
        }
        // add 20190130 turbo対応
        if($this->req->get_get("car5")){
            $_SESSION['reservation']['car5'] = $this->req->get_get("car5");
        }
        elseif($this->req->get("car5")){
            $_SESSION['reservation']['car5'] = $this->req->get("car5");
        }
        else{
            $_SESSION['reservation']['car5'] = NULL;
        }
        if($non_flg){
            header("Location:./index.php");
        }
        //		$_SESSION['tmp_number'] = $this->req->get_get('number');
		if($_SESSION['disp_number']){
			$sql = "select * from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($_SESSION['disp_number'])."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
                    $this->req->set("date_first",str_replace("-","/",$rs->fields('date')));
//                    $this->req->set("ampm_first",$rs->fields('ampm'));
//                    $this->req->set("date_second",str_replace("-","/",$rs->fields('date2')));
//                    $this->req->set("ampm_second",$rs->fields('ampm2'));
//                    $this->req->set("date_third",str_replace("-","/",$rs->fields('date3')));
//                    $this->req->set("ampm_third",$rs->fields('ampm3'));
					$this->req->set('sei',$rs->fields('sei'));
					$this->req->set('mei',$rs->fields('mei'));
					$this->req->set('sei_kana',$rs->fields('sei_kana'));
					$this->req->set('mei_kana',$rs->fields('mei_kana'));
					$this->req->set('postcd',$rs->fields('postcd'));
//					$postcd = explode("-",$rs->fields('postcd'));
//					$this->req->set('postcd1',$postcd[0]);
//					$this->req->set('postcd2',$postcd[1]);
					$this->req->set('address',$rs->fields('address'));
					$this->req->set('contact_kind',$rs->fields('contact_kind'));
//					$tel = explode("-",$rs->fields('tel'));
//					$this->req->set('tel1',$tel[0]);
//					$this->req->set('tel2',$tel[1]);
//					$this->req->set('tel3',$tel[2]);
					$tel = $rs->fields('tel');
					if(strpos($tel,'-') !== false){
						$tel = str_replace("-","",$tel);
					}
					$this->req->set('tel1',$tel);
					$this->req->set('mail',$rs->fields('mail'));
/*
					if($rs->fields('v_schedule_time')){
						$this->req->set('v_schedule_hour',date("H",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y"))));
						$this->req->set('v_schedule_min',date("i",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y"))));
					}
*/
					$this->req->set('comment',$rs->fields('comment'));
				}
				$rs->Close();
			}
		}
        elseif ($_SESSION['member']){
            $sql = "select h.mail as mail,h.sub_mail as sub_mail,";
            $sql .= "h.name_kana as name_kana,h.name_kanji as name_kanji";
            $sql .= " from car_model_info as c,hit_customer_info as h";
            $sql .= " where c.valid_flg = '1' and c.del_flg = '0'";
            $sql .= " and h.valid_flg = '1' and h.del_flg = '0'";
            $sql .= " and c.hit_csr_cd = h.hit_csr_cd";
            $sql .= " and driving_uid = '" . $this->DB2->getQStr($_SESSION['member']['uid']) . "'";
            $rs =& $this->DB2->ASExecute($sql);
            if ($rs) {
                if (!$rs->EOF) {
                    $this->req->set('member_flg', 1);
                    $this->req->set('sei', $rs->fields('name_kanji'));
                    $this->req->set('sei_kana', $rs->fields('name_kana'));
                    $this->req->set('contact_kind',2);
                    if($rs->fields('mail')){
                        $this->req->set('mail',$rs->fields('mail'));
                    }
                    elseif($rs->fields('sub_mail')){
                        $this->req->set('mail',$rs->fields('sub_mail'));
                    }
                }
                $rs->Close();
            }
        }
		$this->req->set('shop_id',$_SESSION['reservation']['shop_id']);
		$this->req->set('car1',$_SESSION['reservation']['car1']);
		$this->req->set('car2',$_SESSION['reservation']['car2']);
		$this->req->set('car3',$_SESSION['reservation']['car3']);
		$this->req->set('car4',$_SESSION['reservation']['car4']);
		// add 20190130 turbo対応
        $this->req->set('car5',$_SESSION['reservation']['car5']);
		$this->req->set('c',$_SESSION['reservation']['c']);
		$this->req->set('s',$_SESSION['reservation']['s']);
/*
		$this->req->set('date_first',$_SESSION['reservation']['date_first']);
		$this->req->set('ampm_first',$_SESSION['reservation']['ampm_first']);
		$this->req->set('date_second',$_SESSION['reservation']['date_second']);
		$this->req->set('ampm_second',$_SESSION['reservation']['ampm_second']);
		$this->req->set('date_third',$_SESSION['reservation']['date_third']);
		$this->req->set('ampm_third',$_SESSION['reservation']['ampm_third']);
*/
		// データセット
		$this->post_proc();
		// フォーム作成
        $shop_id = $_SESSION['reservation']['shop_id'];
        $this->assign_holiday_list($shop_id);
		$v_schedule_hour_list = $this->util->schedule_hour_list();
		$v_schedule_min_list = $this->util->schedule_min_list();
		$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
		$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);
		$this->templ->smarty->display("sp/entry.html");
		exit;
	}
	
	// 予約情報入力(確認)
	function check_proc(){
		// 入力データチェック
		$error = $this->datacheck_proc();
		if(!empty($error)){
			$this->templ->smarty->assign("error",$error);
			$this->default_proc();
			return;
		}
		// データセット
		$this->post_proc();
		$this->templ->smarty->display("sp/entry_conf.html");
		exit;
	}

	// 予約情報入力(登録)
	function entry_proc(){
		// 入力データチェック
		$error = $this->datacheck_proc();
		//予約情報チェック
//		$error = $this->rescheck_proc();
		if(!empty($error)){
			$this->post_proc();
			$this->templ->smarty->assign("error",$error);
			$this->templ->smarty->display("sp/entry.html");
			exit;
		}
		// 変更入力時、元のデータを削除
		if($_SESSION['disp_number']){
			$sql = "select autono from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($_SESSION['disp_number'])."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$record = null;
					$record['disp_flg'] = "0";
					$record['del_flg'] = "1";
					$record['temporary_flg'] = '0';
					$record['update_date'] = time();
					$where  = " disp_number = '".$this->DB->getQStr($_SESSION['disp_number'])."' ";
					$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
				}
				$rs->Close();
			}
		}
		$disp_number = "";
		// 予約登録
		$record = null;
		$record['shop_id'] = $this->req->get('shop_id');
		$record['car1'] = $this->req->get('car1');
		$record['car2'] = $this->req->get('car2');
		$record['car3'] = $this->req->get('car3');
		$record['car4'] = $this->req->get('car4');
		// add 20190130 turbo対応
        $record['car5'] = $this->req->get('car5');
		$cnt = 0;
		$sql = "select count(autono) as cnt from car_detail";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get('car1'))."'";
		$sql .= " and car2 = '".$this->DB->getQStr($this->req->get('car2'))."'";
		if($this->req->get('car3')){
			$sql .= " and car3 = '".$this->DB->getQStr($this->req->get('car3'))."'";
		}
		if($this->req->get('car4')){
			$sql .= " and car4 = '".$this->DB->getQStr($this->req->get('car4'))."'";
		}
		// add 20190130 turbo対応
        if($this->req->get('car5')){
            $sql .= " and car5 = '".$this->DB->getQStr($this->req->get('car5'))."'";
        }
        else{
            $sql .= " and (car5 is NULL or car5 = '')";
        }
		$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
		$sql .= " and disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$cnt = $rs->fields('cnt');
			}
			$rs->Close();
		}
		if($cnt == 1){
			$sql = "select autono from car_detail";
			$sql .= " where car1 = '".$this->DB->getQStr($this->req->get('car1'))."'";
			$sql .= " and car2 = '".$this->DB->getQStr($this->req->get('car2'))."'";
			if($this->req->get('car3')){
				$sql .= " and car3 = '".$this->DB->getQStr($this->req->get('car3'))."'";
			}
			if($this->req->get('car4')){
				$sql .= " and car4 = '".$this->DB->getQStr($this->req->get('car4'))."'";
			}
			// add 20190130 turbo対応
            if($this->req->get('car5')){
                $sql .= " and car5 = '".$this->DB->getQStr($this->req->get('car5'))."'";
            }
            else{
                $sql .= " and (car5 is NULL or car5 = '')";
            }
			$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
			$sql .= " and disp_flg = '1'";
			$sql .= " and del_flg = '0'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$record['car_detail_id'] = $rs->fields('autono');
				}
				$rs->Close();
			}
		}
		$record['date'] = $this->req->get('date_first');
		$record['ampm'] = $this->req->get('ampm_first');
		if($this->req->get('date_second')){
			$record['date2'] = $this->req->get('date_second');
			$record['ampm2'] = $this->req->get('ampm_second');
		}
		if($this->req->get('date_third')){
			$record['date3'] = $this->req->get('date_third');
			$record['ampm3'] = $this->req->get('ampm_third');
		}
		$record['sei'] = $this->req->get('sei');
		$record['mei'] = $this->req->get('mei');
		$record['sei_kana'] = $this->req->get('sei_kana');
		$record['mei_kana'] = $this->req->get('mei_kana');
		$record['postcd'] = $this->req->get('postcd');
//		$postcd = "";
//		if($this->req->get('postcd1') or $this->req->get('postcd2')){
//			$postcd = $this->req->get('postcd1')."-".$this->req->get('postcd2');
//		}
//		$record['postcd'] = $postcd;
		$record['address'] = $this->req->get('address');
		$record['contact_kind'] = $this->req->get('contact_kind');
/*
		$tel = "";
		if($this->req->get('tel1') or $this->req->get('tel2') or $this->req->get('tel3')){
			$tel = $this->req->get('tel1')."-".$this->req->get('tel2')."-".$this->req->get('tel3');
		}
		$record['tel'] = $tel;
*/
		$record['tel'] = $this->req->get('tel1');
		$record['mail'] = $this->req->get('mail');
		if($this->req->get('v_schedule_hour') and $this->req->get('v_schedule_min')){
			$record['v_schedule_time'] = date("H:i:s",mktime($this->req->get('v_schedule_hour'),$this->req->get('v_schedule_min'),0,1,1,date("Y")));
		}
		$record['comment'] = $this->req->get('comment');
		$record['temporary_flg'] = "2";
		if($_SESSION['param']){
			$record['param'] = $_SESSION['param'];
		}
        elseif($_SESSION['param2']){
            $record['param'] = $_SESSION['param2'];
        }
        elseif($_SESSION['param3']){
            $record['param'] = $_SESSION['param3'];
        }
        if($_SESSION['step']){
            $record['step'] = $_SESSION['step'];
        }
        $record['flyer'] = $_SESSION['flyer'];
		$record['del_flg'] = "0";
		$record['disp_flg'] = "1";
		$record['sp_flg'] = "1";
		$record['create_date'] = time();
		$record['update_date'] = time();
		$con = $this->DB->getCon();
		$ret = $con->AutoExecute("reservation", $record, 'INSERT');
		$sql = "SELECT currval('reservation_autono_seq') as inid";
		$rs =& $this->DB->ASExecute($sql);
		$at = "";
		if($rs){
			if(!$rs->EOF){
				$at = $rs->fields('inid');
				$rs->Close();
			}
		}

		$record = null;
		$record['disp_number'] = $this->number_proc($this->req->get('tel3'),$at);
		$disp_number = $this->number_proc($this->req->get('tel3'),$at);
		$where  = " autono=".$this->DB->getQStr($at);
		$ret = $con->AutoExecute("reservation", $record,'UPDATE',$where);
		$_SESSION['sp_flg'] = '1';
//        unset($_SESSION['step']);
        if($_SESSION['flyer']) {
            header("Location:./mail.php?mode=entry&number=" . $disp_number . "&c=" . $this->req->get('c')."&step=".$_SESSION['step']."&flyer=".$_SESSION['flyer']);
        }
        elseif($_SESSION['param']){
            header("Location:./mail.php?mode=entry&number=" . $disp_number . "&c=" . $this->req->get('c')."&step=".$_SESSION['step']."&ybp=".$_SESSION['param']);
        }
        elseif($_SESSION['param2']){
            header("Location:./mail.php?mode=entry&number=" . $disp_number . "&c=" . $this->req->get('c')."&step=".$_SESSION['step']."&p=".$_SESSION['param2']);
        }
        elseif($_SESSION['param3']){
            header("Location:./mail.php?mode=entry&number=" . $disp_number . "&c=" . $this->req->get('c')."&step=".$_SESSION['step']."&vcp76=".$_SESSION['param3']);
        }
        else{
            header("Location:./mail.php?mode=entry&number=" . $disp_number . "&c=" . $this->req->get('c')."&step=".$_SESSION['step']);
        }
	}

	//予約番号自動生成
	function number_proc($tel3,$number){
		$number = "od".$tel3.$number;
		return $number;
	}

    function assign_holiday_list($shop_id){

        // datapickerで選択できる日付範囲指定
        $min_date = 0;
        $max_date = 0;
        if(date("H") < 12){
            $min_date = 2;
        }
        else{
            $min_date = 3;
        }
        $min_y = date("Y");
        $min_m = date("m");
        $min_d = date("d");
        $max_y = date("Y",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_m = date("m",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_d = date("d",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_date = (strtotime(date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y))) - strtotime(date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y))))/(3600*24);
        $this->templ->smarty->assign('min_date',$min_date."d");
        $this->templ->smarty->assign('max_date',$max_date."d");
        $list = $this->util->holiday_list($this->DB,$shop_id);
        $from_date = date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y));
        $to_date = date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y));
        $holiday_list = array();
        if($list){
            if(is_array($list)){
                foreach($list as $key => $val){
                    if(strtotime($key) >= strtotime($from_date) and strtotime($key) <= strtotime($to_date)){
                        $holiday_list[] = $key;
                    }
//					else if(strtotime($key) > strtotime($to_date)){
//						break;
//					}
                }
            }
        }
        // add 20191018 車種の貸出不可日設定取得
        if($this->req->get('car1') and $this->req->get('car2')){
            // car_detail_id取得
            $idList = array();
            $end_date = NULL;
            $start_date = NULL;
            $sql = "select autono,end_date,start_date from car_detail";
            $sql .= " where car1 = '".$this->DB->getQStr($this->req->get('car1'))."'";
            $sql .= " and car2 = '".$this->DB->getQStr($this->req->get('car2'))."'";
            if($this->req->get('car3')){
                $sql .= " and car3 = '".$this->DB->getQStr($this->req->get('car3'))."'";
            }
            if($this->req->get('car4')){
                $sql .= " and car4 = '".$this->DB->getQStr($this->req->get('car4'))."'";
            }
            // add 20190130 turbo対応
            if($this->req->get('car5')){
                $sql .= " and car5 = '".$this->DB->getQStr($this->req->get('car5'))."'";
            }
            else{
                $sql .= " and (car5 is NULL or car5 = '')";
            }
            $sql .= " and shop_id = '".$this->DB->getQStr($shop_id)."'";
            $sql .= " and disp_flg = '1'";
            $sql .= " and del_flg = '0'";
            $rs =& $this->DB->ASExecute($sql);
            if($rs){
                while(!$rs->EOF){
                    $idList[] = $rs->fields('autono');
                    if(!$end_date){
                        if(strtotime($rs->fields('end_date')) >= strtotime("today")){
                            $end_date = $rs->fields('end_date');
                        }
                    }
                    elseif(strtotime($end_date) > strtotime($rs->fields('end_date'))){
                        if(strtotime($rs->fields('end_date')) >= strtotime("today")){
                            $end_date = $rs->fields('end_date');
                        }
                    }
                    if(strtotime($rs->fields('start_date')) > strtotime("today")){
                        $start_date = date("Y-m-d");
                        while(strtotime($rs->fields('start_date')) > strtotime($start_date)){
                            $holiday_list[] = $start_date;
                            $start_date = date("Y-m-d",strtotime($start_date."+ 1 day"));
                        }
                    }
                    $rs->MoveNext();
                }
                $rs->Close();
            }
            if(strtotime(date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y))) > strtotime($end_date)){
                $end_date = (strtotime($end_date) - strtotime(date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y))))/(3600*24);
                $this->templ->smarty->assign('max_date',$end_date."d");
            }
            if($idList){
                if(is_array($idList)){
                    foreach($idList as $key => $val){
                        $sql = "select * from exception ";
                        $sql .= "where shop_id = '".$this->DB->getQStr($shop_id)."' ";
                        $sql .= "and car_detail_id = ".$this->DB->getQStr($val);
                        $sql .= " and disp_flg = '1'";
                        $sql .= " and del_flg = '0'";
                        $rs =& $this->DB->ASExecute($sql);
                        if($rs){
                            while(!$rs->EOF){
                                if(strtotime($rs->fields('date')) >= strtotime($from_date) and strtotime($rs->fields('date')) <= strtotime($to_date)){
                                    $holiday_list[] = $rs->fields('date');
                                    $rs->MoveNext();
                                }
                            }
                            $rs->Close();
                        }
                    }
                }
            }
        }
        $this->templ->smarty->assign('holiday_list',$holiday_list);
    }
}
?>