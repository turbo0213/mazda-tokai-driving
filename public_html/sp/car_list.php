<?php
include_once("../../mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $DB2;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB2 = new ASDB_MAIN();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
        if(!$_SESSION['step']){
            $_SESSION['step'] = "car_list";
        }
        if($this->req->get_get('flyer')){
            if(!$_SESSION['flyer']) {
                $_SESSION['flyer'] = $this->req->get_get('flyer');
                $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
            }
        }
        if($this->req->get_get('p')){
            if(!$_SESSION['param2']) {
                $_SESSION['param2'] = $this->req->get_get('p');
                $this->templ->smarty->assign("p",$this->req->get_get('p'));
            }
        }
		$this->data_get();
		$this->templ->smarty->assign("shop_id",$this->req->get_get('shop_id'));
		$this->templ->smarty->display("sp/car_list.html");
		exit;
	}

	function data_get(){
		$data_list = array();
		// add 20190304 HPのMY店舗からの予約
		if($this->req->get_get('uid')){
			$_SESSION['member']['uid'] = $this->req->get_get('uid');
			$sql = "select fav_shop_id from car_model_info ";
			$sql .= "where driving_uid = '".$this->DB2->getQStr($this->req->get_get('uid'))."' ";
			$rs =& $this->DB2->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$driving_shop_id = NULL;
					if($rs->fields('fav_shop_id')){
						$sql2 = "select driving_shop_id from shop ";
						$sql2 .= "where shop_id = '".$this->DB2->getQStr($rs->fields('fav_shop_id'))."' ";
						$rs2 =& $this->DB2->ASExecute($sql2);
						if($rs2){
							if(!$rs2->EOF){
								$driving_shop_id = $rs2->fields('driving_shop_id');
							}
							$rs2->Close();
						}
					}
					$this->req->get_set('shop_id',$driving_shop_id);
				}
				$rs->Close();
			}
        }
		// 店舗から選択の場合
		if($this->req->get_get('shop_id')){
			$this->templ->smarty->assign("shop_flg",1);
			$sql = "select * from shop ";
			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("name",$rs->fields('name'));
				}
				$rs->Close();
			}
		}
		else{
			$this->templ->smarty->assign("car_flg",1);
		}
		$car3_list = array();
		$i = 0;
		$car3 = "";
		$car12 = "";
		$sql = "select * from car";
		$sql .= " where disp_flg='1'";
		$sql .= " and del_flg='0'";
		$sql .= " order by order_no";
//		$sql .= " order by car1,car2,car4,car3";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql2 = "select * from car_detail";
				$sql2 .= " where end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
				$sql2 .= " and car_id = '".$this->DB->getQStr($rs->fields('car_id'))."'";
				if($this->req->get_get('shop_id')){
					$sql2 .= " and shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."'";
				}
				$sql2 .= " and disp_flg='1'";
				$sql2 .= " and del_flg='0'";
				$sql2 .= " order by car1,car2,car3,car4";
//				$sql2 .= " limit 1";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						$dat = array();
						$comp_car12 = $rs2->fields('car1').$rs2->fields('car2');
						// upd 20190130 turbo対応
						$comp_car = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
						if($car12 != $comp_car12){
							$car12 = $rs2->fields('car1').$rs2->fields('car2');
							// upd 20190130 turbo対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						else if($car3 != $comp_car){
							// upd 20190130 turbo対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						if($data_list[$rs->fields('car1')]){
							$data_list[$rs->fields('car1')][$rs->fields('car2')] = $rs->fields('car2');
						}
						else{
							$dat['car1'] = $rs->fields('car1');
							$dat[$rs->fields('car2')] = $rs->fields('car2');
							$dat['name'] = $rs->fields('name');
							$dat['car_image'] = $this->util->car_image(0,1,$rs->fields('car_id'));
							$data_list[$rs->fields('car1')] = $dat;
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("data_list",$data_list);
		$this->templ->smarty->assign("car3_list",$car3_list);
	}
}
?>
