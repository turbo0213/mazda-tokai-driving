<?php
include_once("../../mc_apl/top.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			// 予約完了画面
			case 'entry':
				$this->entry_proc();
			break;
			// 予約キャンセル画面(完了)
			case 'cancell_end':
				$this->cancell_end_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}
	
	function default_proc(){
		$this->templ->smarty->assign("number",$this->req->get('number'));
		$this->templ->smarty->display("sp/error.html");
		exit;
	}

	// 予約キャンセル画面(完了)
	function cancell_end_proc(){
		$_SESSION['disp_number'] = "";
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($this->req->get_get('number'))."' ";
		$sql .= " and del_flg='1'";
		$sql .= " and disp_flg='0'";
		$rs =& $this->DB->ASExecute($sql);
		$mail = "";
		if($rs){
			if(!$rs->EOF){
				$dat = array();
				$dat['autono'] = $rs->fields('autono');
				$dat['date'] = $rs->fields('date');
				$dat['mail'] = $rs->fields('mail');
				$reserve_date1 = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				if($rs->fields('ampm') == "1"){
					$reserve_date1 .= " 午前";
				}
				else{
					$reserve_date1 .= " 午後";
				}
				if($rs->fields('date2')){
					$reserve_date2 = "・第2希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date2'),5,2),substr($rs->fields('date2'),8,2),substr($rs->fields('date2'),0,4)));
					if($rs->fields('ampm2') == "1"){
						$reserve_date2 .= " 午前\r\n";
					}
					else{
						$reserve_date2 .= " 午後\r\n";
					}
				}
				if($rs->fields('date3')){
					$reserve_date3 = "・第3希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date3'),5,2),substr($rs->fields('date3'),8,2),substr($rs->fields('date3'),0,4)));
					if($rs->fields('ampm3') == "1"){
						$reserve_date3 .= " 午前\r\n";
					}
					else{
						$reserve_date3 .= " 午後\r\n";
					}
				}
				$shop_id = $rs->fields('shop_id');
				$car1 = $rs->fields('car1');
				$car2 = $rs->fields('car2');
				$car3 = $rs->fields('car3');
				$car4 = $rs->fields('car4');
				// add 20190130 turbo対応
                $car5 = $rs->fields('car5');
				$contact_kind = $rs->fields('contact_kind');
				$mail_send_flg = $rs->fields('mail_send_flg');
				$customer_name = $rs->fields('sei')." ".$rs->fields('mei')." 様";
				$year = date("Y",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				$month = date("m",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				$day = date("d",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				if($rs->fields('v_schedule_time')){
					$hour = date("H",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,0,0,0));
					$minute = date("i",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,0,0,0));
					$v_schedule_time = date("H時i分頃",mktime($hour,$minute,0,$month,$day,$year));
				}
			}
			$rs->Close();
		}
		if($shop_id){
			$shop_data = $this->util->shop_info_get($shop_id,$this->DB);
		}
		if($car1 and $car2){
			$sql = "select * from car";
			$sql .= " where car1 = '".$this->DB->getQStr($car1)."'";
			$sql .= " and car2 = '".$this->DB->getQStr($car2)."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$car_data['name'] = $rs->fields('name');
					if($car2 == "ge"){
						$car_data['name2'] = "ガソリン";
					}
					elseif($car2 == "de"){
						$car_data['name2'] = "ディーゼル";
					}
					elseif($car2 == "hev"){
						$car_data['name2'] = "ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($car2 == 'skyx'){
                        $car_data['name2'] = "SKYACTIV-X";
                    }
					if($car4 == "mt"){
						$car_data['name2'] .= " MT";
					}
					$car_data['name2'] .= " ".$car3;
					// add 20190130 turbo対応
                    if($car5 == "turbo"){
                        $car_data['name2'] .= " ターボ";
                    }
				}
				$rs->Close();
			}
		}
		//日本語メールを送る際に必要
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		// SMTPサーバーの情報を連想配列にセット
		$params = array(
			"host" => HOST,
			"port" => PORT,
			"auth" => false,  // SMTP認証を使用する
//			"username" => USERNAME,
//			"password" => PASSWORD
		);
		// PEAR::Mailのオブジェクトを作成
		// ※バックエンドとしてSMTPを指定
		$mailObject = Mail::factory("sendmail", $params);
		// 送信先のメールアドレス
		$recipients = $dat['mail'];
		$FromName = mb_encode_mimeheader(FROMMAILNAME);
		$from= $FromName."<".FROMMAIL.">";
		// メールヘッダ情報を連想配列としてセット
		$headers = array(
			"From" => $from,
			"Subject" => mb_encode_mimeheader(TITLE3)
		);
		// メール本文
		$body = str_replace("@number@",$this->req->get_get('number'),BODY3);
		$body = str_replace("@shop_name1@",$shop_data['name'],$body);
		$body = str_replace("@shop_name2@",$shop_data['name'],$body);
		$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
		// 日本語なのでエンコード
		$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
		// sendメソッドでメールを送信
		$mailObject->send($recipients, $headers, $body);
		// 本部・店舗スタッフメールアドレス取得
		$mail_address = $this->util->send_mail($this->DB3,$shop_id);
		// メールヘッダ情報を連想配列としてセット
		$headers = array(
			"From" => $from,
			"Subject" => mb_encode_mimeheader(TITLE5)
		);
		//$reserve_date = substr($reserve_date,0,4)."年".substr($reserve_date,5,2)."月".substr($reserve_date,8,2)."日";
		if($mail_address){
			if(is_array($mail_address)){
				$recipients = "";
				foreach($mail_address as $key => $val){
					//$recipients = "tokai.mazda.test@gmail.com";
					//あとで↓を有効にする。↑は消す。
					if($recipients){
						$recipients .= ",".$val;
					}
					else{
						$recipients = $val;
					}
				}
			}
			$body = str_replace("@number@",$this->req->get_get('number'),BODY5);
			$body = str_replace("@shop_name1@",$shop_data['name'],$body);
			$body = str_replace("@shop_name2@",$shop_data['name'],$body);
			$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
			$body = str_replace("@reserve_date1@",$reserve_date1,$body);
			$body = str_replace("@reserve_date2@",$reserve_date2,$body);
			$body = str_replace("@reserve_date3@",$reserve_date3,$body);
			$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2'],$body);
			$body = str_replace("@customer_name@",$customer_name,$body);
			//$body = str_replace("@offer_kind@",$offer_kind,$body);
			//$body = str_replace("@staff_name@",$val,$body);
			// 日本語なのでエンコード
			$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
			// sendメソッドでメールを送信
			$mailObject->send($recipients, $headers, $body);
		}
		$this->templ->smarty->assign("number",$this->req->get_get('number'));
		$this->templ->smarty->display("sp/cancel_end.html");
		exit;
	}
	
	// 予約完了画面
	function entry_proc(){
		// チェック
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($this->req->get_get('number'))."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		$data_list = array();
		if($rs){
			if(!$rs->EOF){
				$dat = array();
				$dat['autono'] = $rs->fields('autono');
				$dat['date'] = $rs->fields('date');
				$dat['mail'] = $rs->fields('mail');
				$reserve_date1 = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				if($rs->fields('ampm') == "1"){
					$reserve_date1 .= " 午前";
				}
				else{
					$reserve_date1 .= " 午後";
				}
				if($rs->fields('date2')){
					$reserve_date2 = "・第2希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date2'),5,2),substr($rs->fields('date2'),8,2),substr($rs->fields('date2'),0,4)));
					if($rs->fields('ampm2') == "1"){
						$reserve_date2 .= " 午前\r\n";
					}
					else{
						$reserve_date2 .= " 午後\r\n";
					}
				}
				if($rs->fields('date3')){
					$reserve_date3 = "・第3希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date3'),5,2),substr($rs->fields('date3'),8,2),substr($rs->fields('date3'),0,4)));
					if($rs->fields('ampm3') == "1"){
						$reserve_date3 .= " 午前\r\n";
					}
					else{
						$reserve_date3 .= " 午後\r\n";
					}
				}
				$shop_id = $rs->fields('shop_id');
				$car1 = $rs->fields('car1');
				$car2 = $rs->fields('car2');
				$car3 = $rs->fields('car3');
				$car4 = $rs->fields('car4');
				// add 20190130 turbo対応
                $car5 = $rs->fields('car5');
				$contact_kind = $rs->fields('contact_kind');
				$mail_send_flg = $rs->fields('mail_send_flg');
				$customer_name = $rs->fields('sei')." ".$rs->fields('mei')." 様";
				$year = date("Y",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				$month = date("m",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				$day = date("d",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
				if($rs->fields('v_schedule_time')){
					$hour = date("H",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,0,0,0));
					$minute = date("i",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,0,0,0));
					$v_schedule_time = date("H時i分頃",mktime($hour,$minute,0,$month,$day,$year));
				}
				$h_flg = "0";
				$data_list[] = $dat;
			}
			$rs->Close();
		}
		if($shop_id){
			$shop_data = $this->util->shop_info_get($shop_id,$this->DB);
		}
		if($car1 and $car2){
			$sql = "select * from car";
			$sql .= " where car1 = '".$this->DB->getQStr($car1)."'";
			$sql .= " and car2 = '".$this->DB->getQStr($car2)."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$car_data['name'] = $rs->fields('name');
					if($car2 == "ge"){
						$car_data['name2'] = "ガソリン";
					}
					elseif($car2 == "de"){
						$car_data['name2'] = "ディーゼル";
					}
					elseif($car2 == "hev"){
						$car_data['name2'] = "ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($car2 == 'skyx'){
                        $car_data['name2'] = "SKYACTIV-X";
                    }
					if($car4 == "mt"){
						$car_data['name2'] .= " MT";
					}
					$car_data['name2'] .= " ".$car3;
					// add 20190130 turbo対応
                    if($car5 == "turbo"){
                        $car_data['name2'] .= " ターボ";
                    }
				}
				$rs->Close();
			}
		}
		if($_SESSION['disp_number']){
			$bef_shop = "";
			$bef_reserve_date1 = "";
			$bef_reserve_date2 = "";
			$bef_reserve_date3 = "";
			$bef_car_data = "";
			$bef_customer_name = "";
			$sql = "select * from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($_SESSION['disp_number'])."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$bef_shop = $rs->fields('shop_id');
					$bef_customer_name = $rs->fields('sei')." ".$rs->fields('mei')." 様";
					$bef_reserve_date1 = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date'),5,2),substr($rs->fields('date'),8,2),substr($rs->fields('date'),0,4)));
					if($rs->fields('ampm') == "1"){
						$bef_reserve_date1 .= " 午前";
					}
					else{
						$bef_reserve_date1 .= " 午後";
					}
					if($rs->fields('date2')){
						$bef_reserve_date2 = "・第2希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date2'),5,2),substr($rs->fields('date2'),8,2),substr($rs->fields('date2'),0,4)));
						if($rs->fields('ampm2') == "1"){
							$bef_reserve_date2 .= " 午前\r\n";
						}
						else{
							$bef_reserve_date2 .= " 午後\r\n";
						}
					}
					if($rs->fields('date3')){
						$bef_reserve_date3 = "・第3希望：".date("Y年m月d日",mktime(0,0,0,substr($rs->fields('date3'),5,2),substr($rs->fields('date3'),8,2),substr($rs->fields('date3'),0,4)));
						if($rs->fields('ampm3') == "1"){
							$bef_reserve_date3 .= " 午前\r\n";
						}
						else{
							$bef_reserve_date3 .= " 午後\r\n";
						}
					}
					$sql2 = "select * from car";
					$sql2 .= " where car1 = '".$this->DB->getQStr($rs->fields('car1'))."'";
					$sql2 .= " and car2 = '".$this->DB->getQStr($rs->fields('car2'))."'";
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$bef_car_data['name'] = $rs2->fields('name');
							if($rs2->fields('car2') == "ge"){
								$bef_car_data['name2'] = "ガソリン";
							}
							elseif($rs2->fields('car2') == "de"){
								$bef_car_data['name2'] = "ディーゼル";
							}
							elseif($rs2->fields('car2') == "hev"){
								$bef_car_data['name2'] = "ハイブリッド";
							}
                            // add 20200109 SKYACTIV-X対応
                            else if($rs2->fields('car2') == 'skyx'){
                                $bef_car_data['name2'] = "SKYACTIV-X";
                            }
							if($rs->fields('car4') == "mt"){
								$bef_car_data['name2'] .= " MT";
							}
							$bef_car_data['name2'] .= " ".$rs->fields('car3');
							// add 20190130 turbo対応
                            if($rs->fields('car5') == "turbo"){
                                $bef_car_data['name2'] .= " ターボ";
                            }
						}
						$rs2->Close();
					}
				}
				$rs->Close();
			}
		}
		
		// ===================あとで修正 START===================
		if($mail_send_flg != "1"){
			//日本語メールを送る際に必要
			mb_language("Japanese");
			mb_internal_encoding("UTF-8");
			// SMTPサーバーの情報を連想配列にセット
			$params = array(
					"host" => HOST,
					"port" => PORT,
					"auth" => false,  // SMTP認証を使用する
//					"username" => USERNAME,
//					"password" => PASSWORD
			);
			// PEAR::Mailのオブジェクトを作成
			// ※バックエンドとしてSMTPを指定
			$mailObject = Mail::factory("sendmail", $params);
			// 送信先のメールアドレス
			$recipients = $data_list[0]['mail'];
			//$FromName = mb_encode_mimeheader(mb_convert_encoding(FROMMAILNAME,"JIS","UTF-8"));
			$FromName = mb_encode_mimeheader(FROMMAILNAME);
			$from= $FromName."<".FROMMAIL.">";
			if($data_list[0]['mail']){
				// メールヘッダ情報を連想配列としてセット
				$headers = array(
						"From" => $from,
						"Subject" => mb_encode_mimeheader(TITLE2)
				);
				//$reserve_date = substr($reserve_date,0,4)."年".substr($reserve_date,5,2)."月".substr($reserve_date,8,2)."日";
				// メール本文
				$body = str_replace("@number@",$this->req->get_get('number'),BODY2);
				$body = str_replace("@shop_name1@",$shop_data['name'],$body);
				$body = str_replace("@shop_name2@",$shop_data['name'],$body);
				$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
				$body = str_replace("@reserve_date1@",$reserve_date1,$body);
				$body = str_replace("@reserve_date2@",$reserve_date2,$body);
				$body = str_replace("@reserve_date3@",$reserve_date3,$body);
				$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
				// 日本語なのでエンコード
				$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
				// sendメソッドでメールを送信
				$mailObject->send($recipients, $headers, $body);
			}
			// 本部・店舗スタッフメールアドレス取得
			$mail_address = $this->util->send_mail($this->DB3,$shop_id);
			if(!$_SESSION['disp_number']){
				// メールヘッダ情報を連想配列としてセット
				$headers = array(
					"From" => $from,
					"Subject" => mb_encode_mimeheader(TITLE4)
				);
				if($mail_address){
					if(is_array($mail_address)){
						$recipients = "";
						foreach($mail_address as $key => $val){
							//$recipients = "tokai.mazda.test@gmail.com";
							//あとで↓を有効にする。↑は消す。
							if($recipients){
								$recipients .= ",".$val;
							}
							else{
								$recipients = $val;
							}
						}
					}
					$body = str_replace("@number@",$this->req->get_get('number'),BODY4);
					$body = str_replace("@shop_name1@",$shop_data['name'],$body);
					$body = str_replace("@shop_name2@",$shop_data['name'],$body);
					$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
					$body = str_replace("@reserve_date1@",$reserve_date1,$body);
					$body = str_replace("@reserve_date2@",$reserve_date2,$body);
					$body = str_replace("@reserve_date3@",$reserve_date3,$body);
					$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
					$body = str_replace("@customer_name@",$customer_name,$body);
					//$body = str_replace("@staff_name@",$val,$body);
					// 日本語なのでエンコード
					$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
					// sendメソッドでメールを送信
					$mailObject->send($recipients, $headers, $body);
				}
			}
			else{
				if($bef_shop == $shop_id){
					// メールヘッダ情報を連想配列としてセット
					$headers = array(
						"From" => $from,
						"Subject" => mb_encode_mimeheader(TITLE6)
					);
					if($mail_address){
						if(is_array($mail_address)){
							$recipients = "";
							foreach($mail_address as $key => $val){
								//$recipients = "tokai.mazda.test@gmail.com";
								//あとで↓を有効にする。↑は消す。
								if($recipients){
									$recipients .= ",".$val;
								}
								else{
									$recipients = $val;
								}
							}
						}
						$body = str_replace("@number@",$this->req->get_get('number'),BODY6);
						$body = str_replace("@shop_name1@",$shop_data['name'],$body);
						$body = str_replace("@shop_name2@",$shop_data['name'],$body);
						$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
						$body = str_replace("@reserve_date1@",$reserve_date1,$body);
						$body = str_replace("@reserve_date2@",$reserve_date2,$body);
						$body = str_replace("@reserve_date3@",$reserve_date3,$body);
						$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
						$body = str_replace("@customer_name@",$customer_name,$body);
						//$body = str_replace("@staff_name@",$val,$body);
						// 日本語なのでエンコード
						$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
						// sendメソッドでメールを送信
						$mailObject->send($recipients, $headers, $body);
					}
				}
				else{
					// メールヘッダ情報を連想配列としてセット
					$headers = array(
						"From" => $from,
						"Subject" => mb_encode_mimeheader(TITLE4)
					);
					if($mail_address){
						if(is_array($mail_address)){
							$recipients = "";
							foreach($mail_address as $key => $val){
								//$recipients = "tokai.mazda.test@gmail.com";
								//あとで↓を有効にする。↑は消す。
								if($recipients){
									$recipients .= ",".$val;
								}
								else{
									$recipients = $val;
								}
							}
						}
						$body = str_replace("@number@",$this->req->get_get('number'),BODY4);
						$body = str_replace("@shop_name1@",$shop_data['name'],$body);
						$body = str_replace("@shop_name2@",$shop_data['name'],$body);
						$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
						$body = str_replace("@reserve_date1@",$reserve_date1,$body);
						$body = str_replace("@reserve_date2@",$reserve_date2,$body);
						$body = str_replace("@reserve_date3@",$reserve_date3,$body);
						$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
						$body = str_replace("@customer_name@",$customer_name,$body);
						//$body = str_replace("@staff_name@",$val,$body);
						// 日本語なのでエンコード
						$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
						// sendメソッドでメールを送信
						$mailObject->send($recipients, $headers, $body);
					}
					//予約変更時 店舗も変更した場合、元の店舗にキャンセルメールを送信
					if($bef_shop){
						$shop_data = $this->util->shop_info_get($bef_shop,$this->DB);
					}
					// 本部・店舗スタッフメールアドレス取得
					$mail_address = $this->util->send_mail($this->DB3,$bef_shop);
					// メールヘッダ情報を連想配列としてセット
					$headers = array(
						"From" => $from,
						"Subject" => mb_encode_mimeheader(TITLE5)
					);
					if($mail_address){
						if(is_array($mail_address)){
							$recipients = "";
							foreach($mail_address as $key => $val){
								//$recipients = "tokai.mazda.test@gmail.com";
								//あとで↓を有効にする。↑は消す。
								if($recipients){
									$recipients .= ",".$val;
								}
								else{
									$recipients = $val;
								}
							}
						}
						$body = str_replace("@number@",$_SESSION['disp_number'],BODY5);
						$body = str_replace("@shop_name1@",$shop_data['name'],$body);
						$body = str_replace("@shop_name2@",$shop_data['name'],$body);
						$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
						$body = str_replace("@reserve_date1@",$bef_reserve_date1,$body);
						$body = str_replace("@reserve_date2@",$bef_reserve_date2,$body);
						$body = str_replace("@reserve_date3@",$bef_reserve_date3,$body);
						$body = str_replace("@reserve_car@",$bef_car_data['name']." ".$bef_car_data['name2'],$body);
						$body = str_replace("@customer_name@",$bef_customer_name,$body);
						//$body = str_replace("@offer_kind@",$offer_kind,$body);
						//$body = str_replace("@staff_name@",$val,$body);
						// 日本語なのでエンコード
						$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
						// sendメソッドでメールを送信
						$mailObject->send($recipients, $headers, $body);
					}
				}
			}
			//if($contact_kind == "2"){
				$record = NULL;
				$record['mail_send_flg'] = '1';
				$record['update_date'] = time();
				$where  = " disp_number='".$this->DB->getQStr($this->req->get_get('number'))."'";
				$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
			//}
		}
		$this->templ->smarty->assign("c",$this->req->get_get('c'));
//		$this->templ->smarty->assign("param",$_SESSION['param']);
		if($this->req->get_get('ybp')) {
            $this->templ->smarty->assign("param", $this->req->get_get('ybp'));
        }
        elseif($this->req->get_get('vcp76')){
            $this->templ->smarty->assign("param", $this->req->get_get('vcp76'));
        }
		$_SESSION['disp_number'] = "";
		$_SESSION['param'] = "";
        $_SESSION['param2'] = "";
        $_SESSION['param3'] = "";
		$_SESSION['sp_flg'] = "";
        $_SESSION['flyer'] = "";
        $_SESSION['step'] = "";
        unset($_SESSION['member']);
		$this->templ->smarty->display("sp/mail_end.html");
		exit;
	}
}
?>