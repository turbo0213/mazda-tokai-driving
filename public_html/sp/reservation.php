<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/calendar.php");
$form_class = new form_class();
$form_class->execute();
exit;
class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->cl = new calendar();
	}

	function execute(){
		switch($this->mode){
			// お客様入力画面からの戻り
			case 'entret':
				$this->entret_proc();
			break;
			// 仮押さえ登録
			case 'entry':
				$this->entry_proc();
			break;
			// 仮押さえ削除
			case 'delete':
				$this->delete_proc();
			break;
			// エラーチェック
			case 'check':
				$this->check_proc();
			break;
			// 予約日選択
			default:
				$this->default_proc();
			break;
		}
	}
	
	// 予約日選択
	function default_proc(){
		unset($_SESSION['reservation']);
		if($_REQUEST['year']){
			$year = $_REQUEST['year'];
		}else{
			$year = date('Y');
			$this->req->get_set('year',date("Y"));
		}
		if($_REQUEST['month']){
			$month = $_REQUEST['month'];
		}else{
			$month = date('m');
			$this->req->get_set('month',date("m"));
		}
		// カレンダーのパラメータ車種情報取得
		$disp_start_date = date("Y-m-d",mktime(0,0,0,$month,1,$year));
		$disp_end_date = date("Y-m-d",mktime(0,0,0,$month + 1,0,$year));
		$sql = "select ca.autono as car_detail_id,ca.shop_id as shop_id,ca.start_date as start_date,ca.end_date as end_date from car_detail as ca,car as c";
		$sql .= " where c.car_id = ca.car_id";
		$sql .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		$sql .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
		if($this->req->get_get('car3')){
			$sql .= " and ca.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
		}
		if($this->req->get_get('car4')){
			$sql .= " and ca.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
		}
		$sql .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
		$sql .= " and ca.shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."'";
		$sql .= " and c.del_flg = '0'";
		$sql .= " and ca.disp_flg='1'";
		$sql .= " and ca.del_flg='0'";
		$sql .= " order by ca.autono desc";
		$rs =& $this->DB->ASExecute($sql);
		$shop_id = "";
		$data_list = array();
		if($rs){
			while(!$rs->EOF){
				$shop_id = $rs->fields('shop_id');
				//$car_detail_id = $rs->fields('car_detail_id');
				//$start_date = strtotime($rs->fields('start_date'));
				//$end_date = strtotime($rs->fields('end_date'));
				$data_list[] = $rs->fields('car_detail_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		// カレンダー作成
		$this->cl->Year = $year;
		$this->cl->Month = $month;
		// 車種
		$car_name = "";
		$sql = "select name,name2 from car ";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		$sql .= " and car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$engine = "";
				if($this->req->get_get('car2') == "ge"){
					$engine = "ガソリン";
				}
				else if($this->req->get_get('car2') == "de"){
					$engine = "ディーゼル";
				}
				else if($this->req->get_get('car2') == "hev"){
					$engine = "ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($this->req->get_get('car2') == 'skyx'){
                    $engine = "SKYACTIV-X";
                }
				$mt = "";
				if($this->req->get_get('car4') == "mt"){
					$mt = "MT";
				}
				if($engine){
					$car_name = $rs->fields('name')." ".$engine;
				}
				else{
					$car_name = $rs->fields('name');
				}
				if($mt){
					$car_name .= " ".$mt;
				}
				$car_name .= " ".$this->req->get_get('car3');
			}
			$rs->Close();
		}
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//print_r($data_list);
//echo $shop_id."<br>";
}
		$this->cl->Data($data_list,$shop_id,$this->DB);
		// 店舗
		$shop_name = "";
		$sql = "select name from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($shop_id)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		$data_list3 = array();
		if($rs){
			if(!$rs->EOF){
				$shop_name = $rs->fields('name');
			}
			$rs->Close();
		}
		// datapickerで選択できる日付範囲指定
		$min_date = 0;
		$max_date = 0;
		if(date("H") < 12){
			$min_date = 2;
		}
		else{
			$min_date = 3;
		}
		$min_y = date("Y");
		$min_m = date("m");
		$min_d = date("d");
		$max_y = date("Y",mktime(0,0,0,date("m")+2,0,date("Y")));
		$max_m = date("m",mktime(0,0,0,date("m")+2,0,date("Y")));
		$max_d = date("d",mktime(0,0,0,date("m")+2,0,date("Y")));
		$max_date = (strtotime(date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y))) - strtotime(date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y))))/(3600*24);
		$this->templ->smarty->assign('min_date',$min_date."d");
		$this->templ->smarty->assign('max_date',$max_date."d");
		$list = $this->util->holiday_list($this->DB,$shop_id);
		$from_date = date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y));
		$to_date = date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y));
		$holiday_list = array();
		if($list){
			if(is_array($list)){
				foreach($list as $key => $val){
					if(strtotime($key) >= strtotime($from_date) and strtotime($key) <= strtotime($to_date)){
						$holiday_list[] = $key;
					}
//					else if(strtotime($key) > strtotime($to_date)){
//						break;
//					}
				}
			}
		}
		$this->templ->smarty->assign('holiday_list',$holiday_list);
		// 表示
		$this->templ->smarty->assign( 'Common_year',$this->cl->Year );
		$this->templ->smarty->assign( 'Common_month',$this->cl->Month );
		$this->templ->smarty->assign( 'Common_day',$this->cl->Day );
		$this->templ->smarty->assign( 'Common_Num',$this->cl->Num );
		// 翌月のカレンダー作成
		$this->cl->Num = array();
		$this->cl->Day = array();
		$this->cl->Year = date("Y",mktime(0,0,0,$month+1,1,$year));
		$this->cl->Month = date("m",mktime(0,0,0,$month+1,1,$year));
		$this->cl->Data($data_list,$shop_id,$this->DB);
		$this->templ->smarty->assign( 'Common_year2',$this->cl->Year );
		$this->templ->smarty->assign( 'Common_month2',$this->cl->Month );
		$this->templ->smarty->assign( 'Common_day2',$this->cl->Day );
		$this->templ->smarty->assign( 'Common_Num2',$this->cl->Num );
		$this->templ->smarty->assign("shop_id",$this->req->get_get('shop_id'));
		$this->templ->smarty->assign("car1",$this->req->get_get('car1'));
		$this->templ->smarty->assign("car2",$this->req->get_get('car2'));
		$this->templ->smarty->assign("car3",$this->req->get_get('car3'));
		$this->templ->smarty->assign("car4",$this->req->get_get('car4'));
		$this->templ->smarty->assign("c",$this->req->get_get('c'));
		$this->templ->smarty->assign("s",$this->req->get_get('s'));
		$this->templ->smarty->assign("car_name",$car_name);
		if($_SESSION['disp_number']){
			$sql = "select * from reservation ";
			$sql .= " where disp_number='".$this->DB->getQStr($_SESSION['disp_number'])."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("disp_number",$_SESSION['disp_number']);
					if($this->mode != 'entret'){
						$this->templ->smarty->assign("date_first",str_replace("-","/",$rs->fields('date')));
						$this->templ->smarty->assign("ampm_first",$rs->fields('ampm'));
						$this->templ->smarty->assign("date_second",str_replace("-","/",$rs->fields('date2')));
						$this->templ->smarty->assign("ampm_second",$rs->fields('ampm2'));
						$this->templ->smarty->assign("date_third",str_replace("-","/",$rs->fields('date3')));
						$this->templ->smarty->assign("ampm_third",$rs->fields('ampm3'));
						if($rs->fields('date')){
							$d = explode("-",$rs->fields('date'));
							$this->templ->smarty->assign("date1_id",$d[1].$d[2].'1');
							$this->templ->smarty->assign("m1",$d[1]);
						}
						if($rs->fields('date2')){
							$d = explode("-",$rs->fields('date2'));
							$this->templ->smarty->assign("date2_id",$d[1].$d[2].'2');
							$this->templ->smarty->assign("m2",$d[1]);
						}
						if($rs->fields('date3')){
							$d = explode("-",$rs->fields('date3'));
							$this->templ->smarty->assign("date3_id",$d[1].$d[2].'3');
							$this->templ->smarty->assign("m3",$d[1]);
						}
					}
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign("shop_name",$shop_name);
		$this->templ->smarty->display("sp/calendar.html");
		exit;
	}
	
	// エラーチェック
	function check_proc(){
		$error = $this->data_check();
		if($error){
			if($_REQUEST['year']){
				$year = $_REQUEST['year'];
			}else{
				$year = date('Y');
				$this->req->get_set('year',date("Y"));
			}
			if($_REQUEST['month']){
				$month = $_REQUEST['month'];
			}else{
				$month = date('m');
				$this->req->get_set('month',date("m"));
			}
			// カレンダーのパラメータ車種情報取得
			$disp_start_date = date("Y-m-d",mktime(0,0,0,$month,1,$year));
			$disp_end_date = date("Y-m-d",mktime(0,0,0,$month + 1,0,$year));
			$sql = "select ca.autono as car_detail_id,ca.shop_id as shop_id,ca.start_date as start_date,ca.end_date as end_date from car_detail as ca,car as c";
			$sql .= " where c.car_id = ca.car_id";
			$sql .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
			$sql .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
			if($this->req->get_get('car3')){
				$sql .= " and ca.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
			}
			if($this->req->get_get('car4')){
				$sql .= " and ca.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
			}
			$sql .= " and ca.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
			$sql .= " and ca.shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."'";
			$sql .= " and c.del_flg = '0'";
			$sql .= " and ca.disp_flg='1'";
			$sql .= " and ca.del_flg='0'";
			$sql .= " order by ca.autono desc";
			$rs =& $this->DB->ASExecute($sql);
			$shop_id = "";
			$data_list = array();
			if($rs){
				while(!$rs->EOF){
					$shop_id = $rs->fields('shop_id');
					$data_list[] = $rs->fields('car_detail_id');
					$rs->MoveNext();
				}
				$rs->Close();
			}
			// カレンダー作成
			$this->cl->Year = $year;
			$this->cl->Month = $month;
			// 車種
			$car_name = "";
			$sql = "select name,name2 from car ";
			$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
			$sql .= " and car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$engine = "";
					if($this->req->get_get('car2') == "ge"){
						$engine = "ガソリンエンジン";
					}
					else if($this->req->get_get('car2') == "de"){
						$engine = "ディーゼルエンジン";
					}
					else if($this->req->get_get('car2') == "hev"){
						$engine = "ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($this->req->get_get('car2') == 'skyx'){
                        $engine = "SKYACTIV-X";
                    }
					$mt = "";
					if($this->req->get_get('car4') == "mt"){
						$mt = "MT";
					}
					if($engine){
						$car_name = $rs->fields('name')." ".$engine;
					}
					else{
						$car_name = $rs->fields('name');
					}
					if($mt){
						$car_name .= " ".$mt;
					}
					$car_name .= " ".$this->req->get_get('car3');
				}
				$rs->Close();
			}
			$this->cl->Data($data_list,$shop_id,$this->DB);
			// 店舗
			$shop_name = "";
			$sql = "select name from shop ";
			$sql .= " where shop_id='".$this->DB->getQStr($shop_id)."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			$data_list3 = array();
			if($rs){
				if(!$rs->EOF){
					$shop_name = $rs->fields('name');
				}
				$rs->Close();
			}
			// datapickerで選択できる日付範囲指定
			$min_date = 0;
			$max_date = 0;
			if(date("H") < 12){
				$min_date = 2;
			}
			else{
				$min_date = 3;
			}
			$min_y = date("Y");
			$min_m = date("m");
			$min_d = date("d");
			$max_y = date("Y",mktime(0,0,0,date("m")+2,0,date("Y")));
			$max_m = date("m",mktime(0,0,0,date("m")+2,0,date("Y")));
			$max_d = date("d",mktime(0,0,0,date("m")+2,0,date("Y")));
			$max_date = (strtotime(date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y))) - strtotime(date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y))))/(3600*24);
			$this->templ->smarty->assign('min_date',$min_date."d");
			$this->templ->smarty->assign('max_date',$max_date."d");
			$list = $this->util->holiday_list($this->DB,$shop_id);
			$from_date = date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y));
			$to_date = date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y));
			$holiday_list = array();
			if($list){
				if(is_array($list)){
					foreach($list as $key => $val){
						if(strtotime($key) >= strtotime($from_date) and strtotime($key) <= strtotime($to_date)){
							$holiday_list[] = $key;
						}
//						else if(strtotime($key) > strtotime($to_date)){
//							break;
//						}
					}
				}
			}
			$this->templ->smarty->assign("date_first",$this->req->get('date_first'));
			$this->templ->smarty->assign("ampm_first",$this->req->get('ampm_first'));
			$this->templ->smarty->assign("date_second",$this->req->get('date_second'));
			$this->templ->smarty->assign("ampm_second",$this->req->get('ampm_second'));
			$this->templ->smarty->assign("date_third",$this->req->get('date_third'));
			$this->templ->smarty->assign("ampm_third",$this->req->get('ampm_third'));
			if($this->req->get('date_first')){
				$d = explode("/",$this->req->get('date_first'));
				$this->templ->smarty->assign("date1_id",$d[1].$d[2].'1');
				$this->templ->smarty->assign("m1",$d[1]);
			}
			if($this->req->get('date_second')){
				$d = explode("/",$this->req->get('date_second'));
				$this->templ->smarty->assign("date2_id",$d[1].$d[2].'2');
				$this->templ->smarty->assign("m2",$d[1]);
			}
			if($this->req->get('date_third')){
				$d = explode("/",$this->req->get('date_third'));
				$this->templ->smarty->assign("date3_id",$d[1].$d[2].'3');
				$this->templ->smarty->assign("m3",$d[1]);
			}

			$this->templ->smarty->assign('holiday_list',$holiday_list);
			$this->templ->smarty->assign( 'Common_year',$this->cl->Year );
			$this->templ->smarty->assign( 'Common_month',$this->cl->Month );
			$this->templ->smarty->assign( 'Common_day',$this->cl->Day );
			$this->templ->smarty->assign( 'Common_Num',$this->cl->Num );
			// 翌月のカレンダー作成
			$this->cl->Num = array();
			$this->cl->Day = array();
			$this->cl->Year = date("Y",mktime(0,0,0,$month+1,1,$year));
			$this->cl->Month = date("m",mktime(0,0,0,$month+1,1,$year));
			$this->cl->Data($data_list,$shop_id,$this->DB);
			$this->templ->smarty->assign( 'Common_year2',$this->cl->Year );
			$this->templ->smarty->assign( 'Common_month2',$this->cl->Month );
			$this->templ->smarty->assign( 'Common_day2',$this->cl->Day );
			$this->templ->smarty->assign( 'Common_Num2',$this->cl->Num );
			$this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
			$this->templ->smarty->assign("car1",$this->req->get('car1'));
			$this->templ->smarty->assign("car2",$this->req->get('car2'));
			$this->templ->smarty->assign("car3",$this->req->get('car3'));
			$this->templ->smarty->assign("car4",$this->req->get('car4'));
			$this->templ->smarty->assign("c",$this->req->get('c'));
			$this->templ->smarty->assign("s",$this->req->get('s'));
			$this->templ->smarty->assign("car_name",$car_name);
			$this->templ->smarty->assign("shop_name",$shop_name);
			$this->templ->smarty->assign("error",$error);
			$this->templ->smarty->display("sp/calendar.html");
		}
		else{
			$_SESSION['reservation']['shop_id'] = $this->req->get('shop_id');
			$_SESSION['reservation']['car1'] = $this->req->get('car1');
			$_SESSION['reservation']['car2'] = $this->req->get('car2');
			$_SESSION['reservation']['car3'] = $this->req->get('car3');
			$_SESSION['reservation']['car4'] = $this->req->get('car4');
			$_SESSION['reservation']['c'] = $this->req->get('c');
			$_SESSION['reservation']['s'] = $this->req->get('s');
			$_SESSION['reservation']['date_first'] = $this->req->get('date_first');
			$_SESSION['reservation']['ampm_first'] = $this->req->get('ampm_first');
			$_SESSION['reservation']['date_second'] = $this->req->get('date_second');
			$_SESSION['reservation']['ampm_second'] = $this->req->get('ampm_second');
			$_SESSION['reservation']['date_third'] = $this->req->get('date_third');
			$_SESSION['reservation']['ampm_third'] = $this->req->get('ampm_third');
			if($_SESSION['flyer']) {
                header("Location:./entry.php?flyer=".$_SESSION['flyer']);
            }
            else {
                header("Location:./entry.php");
            }
		}
		exit;
	}

	function entret_proc(){
		$this->templ->smarty->assign("date_first",$_SESSION['reservation']['date_first']);
		$this->templ->smarty->assign("ampm_first",$_SESSION['reservation']['ampm_first']);
		$this->templ->smarty->assign("date_second",$_SESSION['reservation']['date_second']);
		$this->templ->smarty->assign("ampm_second",$_SESSION['reservation']['ampm_second']);
		$this->templ->smarty->assign("date_third",$_SESSION['reservation']['date_third']);
		$this->templ->smarty->assign("ampm_third",$_SESSION['reservation']['ampm_third']);
		if($_SESSION['reservation']['date_first']){
			$d = explode("/",$_SESSION['reservation']['date_first']);
			$this->templ->smarty->assign("date1_id",$d[1].$d[2].'1');
			$this->templ->smarty->assign("m1",$d[1]);
		}
		if($_SESSION['reservation']['date_second']){
			$d = explode("/",$_SESSION['reservation']['date_second']);
			$this->templ->smarty->assign("date2_id",$d[1].$d[2].'2');
			$this->templ->smarty->assign("m2",$d[1]);
		}
		if($_SESSION['reservation']['date_third']){
			$d = explode("/",$_SESSION['reservation']['date_third']);
			$this->templ->smarty->assign("date3_id",$d[1].$d[2].'3');
			$this->templ->smarty->assign("m3",$d[1]);
		}
		$this->default_proc();
	}

	// 日付チェック処理
	function data_check(){
		$error = "";
		$first_error = false;
		$second_error = false;
		$third_error = false;
		$second_input = false;
		$third_input = false;
		if(!$this->req->get('date_first')){
			$error .= "第1希望の日付を選択してください\r\n";
			$first_error = true;
		}
		else{
			$year1 = "";
			$month1 = "";
			$day1 = "";
			list($year1,$month1,$day1) = explode("/",$this->req->get('date_first'));
			if(!checkdate($month1,$day1,$year1)){
				$error .= "第1希望の日付を正しく選択してください\r\n";
				$first_error = true;
			}
		}
		if(!$this->req->get('ampm_first')){
			$error .= "第1希望の時間帯を選択してください\r\n";
			$first_error = true;
		}
		else if($this->req->get('ampm_first') != '1' and $this->req->get('ampm_first') != '2'){
			$error .= "第1希望の時間帯を正しく選択してください\r\n";
			$first_error = true;
		}
		if($this->req->get('date_second')){
			$second_input = true;
			$year2 = "";
			$month2 = "";
			$day2 = "";
			list($year2,$month2,$day2) = explode("/",$this->req->get('date_second'));
			if(!checkdate($month2,$day2,$year2)){
				$error .= "第2希望の日付を正しく選択してください\r\n";
				$second_error = true;
			}
		}
		if($this->req->get('ampm_second')){
			if($this->req->get('ampm_second') != '1' and $this->req->get('ampm_second') != '2'){
				$error .= "第2希望の時間帯を正しく選択してください\r\n";
				$second_error = true;
			}
		}
		else if($second_input){
			$error .= "第2希望の時間帯を選択してください\r\n";
		}
		if($this->req->get('date_third')){
			$third_input = true;
			$year3 = "";
			$month3 = "";
			$day3 = "";
			list($year3,$month3,$day3) = explode("/",$this->req->get('date_third'));
			if(!checkdate($month3,$day3,$year3)){
				$error .= "第3希望の日付を正しく選択してください\r\n";
				$third_error = true;
			}
		}
		if($this->req->get('ampm_third')){
			if($this->req->get('ampm_third') != '1' and $this->req->get('ampm_third') != '2'){
				$error .= "第3希望の時間帯を正しく選択してください\r\n";
				$third_error = true;
			}
		}
		else if($third_input){
			$error .= "第3希望の時間帯を選択してください\r\n";
		}
		if(!$first_error and !$second_error and !$third_error){
			if($second_input){
				if(mktime(0,0,0,$month1,$day1,$year1) == mktime(0,0,0,$month2,$day2,$year2)){
					if($this->req->get('ampm_first') == $this->req->get('ampm_second')){
						$error .= "第1希望と第2希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
					}
					else if($third_input){
						if(mktime(0,0,0,$month2,$day2,$year2) == mktime(0,0,0,$month3,$day3,$year3)){
							if($this->req->get('ampm_second') == $this->req->get('ampm_third')){
								$error .= "第2希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
							}
						}
					}
				}
				else if($third_input){
					if(mktime(0,0,0,$month2,$day2,$year2) == mktime(0,0,0,$month3,$day3,$year3)){
						if($this->req->get('ampm_second') == $this->req->get('ampm_third')){
							$error .= "第2希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
						}
					}
				}
			}
			if($third_input){
				if(mktime(0,0,0,$month1,$day1,$year1) == mktime(0,0,0,$month3,$day3,$year3)){
					if($this->req->get('ampm_first') == $this->req->get('ampm_third')){
						$error .= "第1希望と第3希望の日付と時間帯は異なる日付・時間帯を選択してください\r\n";
					}
				}
			}
		}
		return $error;
	}

	// 仮押さえ登録
	function entry_proc(){
		// 入力画面へ
		header("Location:./entry.php?year=".$this->req->get_get('year')."&month=".$this->req->get_get('month')."&day=".$this->req->get_get('day')."&car_detail_id=".$this->req->get_get('car_detail_id')."&shop_id=".$this->req->get_get('shop_id')."&pref_id=".$this->req->get_get('pref_id')."&area_id=".$this->req->get_get('area_id')."");
	}

	//  仮押さえ削除
	function delete_proc(){
		header("Location:./reservation.php?year=".$this->req->get_get('year')."&month=".$this->req->get_get('month')."&car_detail_id=".$this->req->get_get('car_detail_id')."&shop_id=".$this->req->get_get('shop_id')."&pref_id=".$this->req->get_get('pref_id')."&area_id=".$this->req->get_get('area_id')."");
	}
}
?>