<?php

include_once("../../../mc_apl/top_test.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		//$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){

		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		if($this->req->get_get("D")){
			//$_SESSION['D'] = $this->req->get_get("D");
		}
		else{
			$this->req->setError("error1","アクセス元が特定できません");
		}
		if($this->req->get_get("M") != "1"){
			$this->req->setError("error2","パラメータが不正です");
		}
		if($this->req->get_get("T") != "S"){
			$this->req->setError("error2","パラメータが不正です");
		}
		if($this->req->get_get("C") != "AEyFAOKe7HUG"){
			$this->req->setError("error2","パラメータが不正です");
		}
		
		if($this->req->hasErrors()){
			exit;
		}
		
		if($this->util->ref_list(2,$this->req->get_get("D"))){
			//30日間
			setcookie("APP_REF", $this->req->get_get("D"), time()+60*60*24*30,"/");
		}
		$url = $this->util->ref_url_list(1,$this->req->get_get("D"));
		if($url){
			header("Location:".$url."?D=".$this->req->get_get("D")."&M=".$this->req->get_get("M")."&T=".$this->req->get_get("T")."&C=".$this->req->get_get("C"));
		}
		else{
			header("Location:index.html?D=".$this->req->get_get("D")."&M=".$this->req->get_get("M")."&T=".$this->req->get_get("T")."&C=".$this->req->get_get("C"));
		}
	}

}

?>