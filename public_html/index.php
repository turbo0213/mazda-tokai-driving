<?php
include_once("../mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		unset($_SESSION['step']);
		unset($_SESSION['reservation']);
		unset($_SESSION['disp_number']);
        unset($_SESSION['param']);
        unset($_SESSION['param2']);
        unset($_SESSION['param3']);
        unset($_SESSION['flyer']);
        if($this->req->get_get('uid')){
			$_SESSION['member']['uid'] = $this->req->get_get('uid');
		}
        if($this->req->get_get('ybp')){
            $_SESSION['param'] = $this->req->get_get('ybp');
            $this->templ->smarty->assign("ybp",$this->req->get_get('ybp'));
        }
        if($this->req->get_get('flyer')){
            $_SESSION['flyer'] = $this->req->get_get('flyer');
            $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
        }
        if($this->req->get_get('p')){
			$_SESSION['param2'] = $this->req->get_get('p');
			$this->templ->smarty->assign("p",$this->req->get_get('p'));
        }
        if($this->req->get_get('vcp76')){
            $_SESSION['param3'] = $this->req->get_get('vcp76');
            $this->templ->smarty->assign("vcp76",$this->req->get_get('vcp76'));
        }
		if($this->req->get_get('sp_flg')){
			$this->templ->smarty->assign("sp_flg",$this->req->get_get('sp_flg'));
		}
		$this->templ->smarty->display("index.html");
		exit;
	}
}

?>
