<?php
ini_set("memory_limit", "512M");
ini_set("max_execution_time", "600");

define("TEMPLATE", "oneday_admin/reserve_list.html");

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{

	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;

	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}
	
	function form_make(){
		if($_SESSION['oneday']['access_kb'] == 1){
			$_SESSION['search_r']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 2){
			$_SESSION['search_r']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 3){
			$_SESSION['search_r']['station'] = $_SESSION['oneday']['station_code'];
		}
		else if($_SESSION['oneday']['access_kb'] == '4' or $_SESSION['oneday']['access_kb'] == '9'){
			$station_list = $this->util->station_list_get(0,0,$this->DB);
			$this->templ->smarty->assign('station',$_SESSION['search_r']['station']);
			$this->templ->smarty->assign('station_list',$station_list);
		}
		if($_SESSION['oneday']['access_kb'] != 1 and $_SESSION['oneday']['access_kb'] != 2){
			$shop_list = $this->util->shop_list_get($_SESSION['search_r']['station'],0,0,$this->DB);
			$this->templ->smarty->assign('shop',$_SESSION['search_r']['shop']);
			$this->templ->smarty->assign('shop_list',$shop_list);
		}
		$car_list = array();
		// 車種
		if($_SESSION['search_r']['shop']){
			$sql = "select car_id,week_flg from car_detail ";
			$sql .= " where shop_id='".$this->DB->getQStr($_SESSION['search_r']['shop'])."'";
			$sql .= " and del_flg='0' and disp_flg='1'";
			$sql .= " group by car_id,week_flg,autono";
			$sql .= " order by car_id,autono";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$week_name = "（休日）";
					if($rs->fields('week_flg') == 1){
						$week_name = "（平日）";
					}
					$sql = "select * from car";
					$sql .= " where car_id='".$this->DB->getQStr($rs->fields('car_id'))."'";
					$sql .= " and del_flg='0' and disp_flg='1'";
					$rs2 =& $this->DB->ASExecute($sql);
					if($rs2){
						if(!$rs2->EOF){
							$car_list[$rs2->fields('car_id')."-".$rs->fields('week_flg')] = $rs2->fields('name')." ".$rs2->fields('name2')." ".$rs2->fields('name3').$week_name;
						}
						$rs2->Close();
					}
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$this->templ->smarty->assign("car", $_SESSION['search_r']['car']);
			$this->templ->smarty->assign("car_list", $car_list);
		}
		
		//予約日
		$year_list =$this->util->year_list();
		$month_list = $this->util->month_list2();
		$day_list = $this->util->day_list();
		$this->templ->smarty->assign("access_kb", $_SESSION['oneday']['access_kb']);
		$this->templ->smarty->assign("year_list", $year_list);
		$this->templ->smarty->assign("month_list", $month_list);
		$this->templ->smarty->assign("day_list", $day_list);
	}
	//一覧
	function default_proc(){
		$_SESSION['search_r']['car'] = "";
//		$_SESSION['search_r']['year'] = "";
//		$_SESSION['search_r']['month'] = "";
//		$_SESSION['search_r']['day'] = "";
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search_r']['station'] = $this->req->get('station');
		}
		if($this->req->get('type') == 'shop_change'){
			$_SESSION['search_r']['shop'] = $this->req->get('shop');
		}
		if($this->req->get('type') == 'search'){
			if($_SESSION['oneday']['access_kb'] == "4"){
				$_SESSION['search_r']['station'] = $this->req->get('station');
				$_SESSION['search_r']['shop'] = $this->req->get('shop');
			}
			$_SESSION['search_r']['car'] = $this->req->get('car');
			$_SESSION['search_r']['year'] = $this->req->get('year');
			$_SESSION['search_r']['month'] = $this->req->get('month');
			$_SESSION['search_r']['day'] = $this->req->get('day');
		}
		if(!$_SESSION['search_r']['year']){
			$_SESSION['search_r']['year'] = date("Y");
		}
		if(!$_SESSION['search_r']['month']){
			$_SESSION['search_r']['month'] = date("m");
		}
		if(!$_SESSION['search_r']['day']){
			// 18時以降は翌日の予約を表示
			if(date("His") >= "180000"){
				$holiday_flg = true;
				$i = 1;
				while($holiday_flg == true){
					$holiday_flg = $this->util->holiday_list($this->DB,$_SESSION['oneday']['shop_id'],1,date("Y-m-d",mktime(0,0,0,date("m"),date("d")+$i,date("Y"))));
					if($holiday_flg == "1"){
						$holiday_flg = true;
						$i++;
					}
					else{
						$holiday_flg = false;
					}
				}
				$_SESSION['search_r']['day'] = date("d",mktime(0,0,0,date("m"),date("d")+$i,date("Y")));
			}
			else{
				$_SESSION['search_r']['day'] = date("d");
			}
		}
		$this->templ->smarty->assign("year", $_SESSION['search_r']['year']);
		$this->templ->smarty->assign("month", $_SESSION['search_r']['month']);
		$this->templ->smarty->assign("day", $_SESSION['search_r']['day']);
		if($_SESSION['search_r']['day']){
			if(!checkdate($_SESSION['search_r']['month'],$_SESSION['search_r']['day'],$_SESSION['search_r']['year'])){
				$this->req->setError('error1','予約日が不正です');
				$this->form_make();
				$this->templ->error_assign($this->req);
				$this->templ->smarty->display(TEMPLATE);
				exit;
			}
		}
		$this->form_make();
		$this->list_proc();
		$this->templ->smarty->display(TEMPLATE);
		exit;
	}

	//一覧データ取得
	function list_proc(){
		$week = array("日", "月", "火", "水", "木", "金", "土");
		$data_list = array();
		$sijo_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search_r']['month'],$_SESSION['search_r']['day'],$_SESSION['search_r']['year']));
		$list_sql = "select * from reservation";
		$where_sql = " where date = '".$this->DB->getQStr($sijo_date)."'";
		$where_sql .= " and disp_flg = '1'";
		$where_sql .= " and del_flg = '0'";
		//本部権限以外の場合(sql文)
		if($_SESSION['oneday']['access_kb'] != "4"){
			$where_sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		//本部権限の場合(事業部のみ指定)
		else if($_SESSION['search_r']['station'] and !$_SESSION['search_r']['shop_id']){
			$where_sql .= " and (";
			$first_flg = true;
			$sql = "select shop_id from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['search_r']['station'])."'";
			$sql .= " and disp_flg = '1'";
			$sql .= " and del_flg = '0'";
			$sql .= " order by shop_id asc";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					if($first_flg){
						$where_sql .= "shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
						$first_flg = false;
					}
					else{
						$where_sql .= " or shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
					}
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$where_sql .= ")";
		}
		//本部権限の場合(事業部・店舗指定)
		else if($_SESSION['search_r']['station'] and $_SESSION['search_r']['shop_id']){
			$where_sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search_r']['shop_id'])."'";
		}
		if($_SESSION['search_r']['car']){
			$where_sql .= " and (";
			$first_flg = true;
			$sql = "select autono from car_detail";
			$sql .= " where car_id = '".$this->DB->getQStr(substr($_SESSION['search_r']['car'],0,8))."'";
			$sql .= " and week_flg = '".$this->DB->getQStr(substr($_SESSION['search_r']['car'],9,1))."'";
			$sql .= " and disp_flg = '1' and del_flg = '0'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					if($first_flg){
						$where_sql .= "car_detail_id = ".$this->DB->getQStr($rs->fields('autono'));
						$first_flg = false;
					}
					else{
						$where_sql .= " or car_detail_id = ".$this->DB->getQStr($rs->fields('autono'));
					}
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$where_sql .= ")";
		}
		//本部権限以外の場合(order句)
		if($_SESSION['oneday']['access_kb'] != "4"){
			$order_sql = " order by car_detail_id asc,hour_from asc";
		}
		else{
			$order_sql = " order by shop_id asc,car_detail_id asc,hour_from asc";
		}
		$sql = $list_sql.$where_sql.$order_sql;
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$data = array();
				//仮予約表示
				if($rs->fields('temporary_flg') == '1'){
					if($rs->fields('validity_date') >= date("Y-m-d H:i:s")){
						$data['disp_number'] = "仮予約中";
					}
					else{
						$rs->MoveNext();
						continue;
					}
				}
				else if($rs->fields('temporary_flg') == '2'){
					$data['disp_number'] = $rs->fields('disp_number');
				}
				//車種名・デモカーモニターカー
				$sql2 = "select c.name,c.name2,c.name3,d.week_flg,d.no_plate from car c,car_detail d";
				$sql2 .= " where d.autono = ".$this->DB->getQStr($rs->fields('car_detail_id'));
				$sql2 .= " and c.car_id = d.car_id";
				$sql2 .= " and c.disp_flg = '1' and c.del_flg = '0'";
				$sql2 .= " and d.disp_flg = '1' and d.del_flg = '0'";
//あとで必ずONにする！
//				$sql2 .= " and d.start_date <= '".$this->DB->getQStr($sijo_date)."'";
//				$sql2 .= " and d.end_date >= '".$this->DB->getQStr($sijo_date)."'";;
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					if(!$rs2->EOF){
						/*
						$week_name = "（休日）";
						if($rs2->fields('week_flg') == 1){
							$week_name = "（平日）";
						}
						*/
						$data['carname'] = $rs2->fields('name')." ".$rs2->fields('name2')." ".$rs2->fields('name3').$week_name;
						$data['no_plate'] = $rs2->fields('no_plate');
					}
					$rs2->Close();
				}
				if($rs->fields('sp_flg') == "1"){
					$data['sppc_value'] = "(SP)";
				}
				else{
					$data['sppc_value'] = "(PC)";
				}
				if($rs->fields('hour_from') or $rs->fields('hour_to')){
					if($rs->fields('hour_from')){
						$data['hour_from'] = $rs->fields('hour_from');
					}
					if($rs->fields('hour_to')){
						$data['hour_to'] = $rs->fields('hour_to');
					}
					$data['hour_flg'] = 1;
				}
				else{
					$data['hour_value'] = "終日";
					$data['hour_flg'] = 0;
				}
				$data['entry_date'] = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('update_date'),5,2),substr($rs->fields('update_date'),8,2),substr($rs->fields('update_date'),0,4)));
				$data['name'] = $rs->fields('sei')." ".$rs->fields('mei');
				$data['tel'] = $rs->fields('tel');
				$data['mail'] = $rs->fields('mail');
				$data['temporary_flg'] = $rs->fields('temporary_flg');
				$data['memo'] = $rs->fields('memo');
				$data['comment'] = "無";
				if($rs->fields('comment') or $rs->fields('memo')){
					$data['comment'] = "有";
				}
				if($rs->fields('staff_name')){
					$data['shop_input'] = $rs->fields('staff_name');
				}
				else if($rs->fields('staff_autono')){
					$data['shop_input'] = "○";
				}
				else{
					$data['shop_input'] = "";
				}
				$today_date = date("Y-m-d");
				$data_date = substr($rs->fields('update_date'),0,10);
				if($today_date == $data_date){
					if($rs->fields('temporary_flg') == 2){
						$data['back_class'] = "sizyoB";
					}
				}
				$data_list[] = $data;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($_SESSION['search_r']['year'] and $_SESSION['search_r']['month'] and $_SESSION['search_r']['day']){
			$title_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search_r']['month'],$_SESSION['search_r']['day'],$_SESSION['search_r']['year']));
			$time = strtotime($title_date);
			$w = date("w", $time);
			$title = date("Y年n月j日", $time)." (".$week[$w].")";
			$this->templ->smarty->assign("title", $title);
		}
		$this->templ->smarty->assign("zaiko_list", $data_list);
	}
}

?>