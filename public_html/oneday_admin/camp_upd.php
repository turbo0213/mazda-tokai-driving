<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
//require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
            case 'conf':
                $this->conf_proc();
                break;
			case 'regist':
				$this->regist_proc();
			break;
			case 'delete':
				$this->delete_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
        $car_list = $this->form_make();
        $this->assign_proc($car_list);
        if(!$this->req->get('car_detail_id')){
            if($car_list){
                if(is_array($car_list)){
                    foreach($car_list as $key => $val){
                        if($val == $this->req->get('car_name')){
                            $this->templ->smarty->assign("car_detail_id",$key);
                            $this->templ->smarty->assign("car_id",$key);
                            $this->templ->smarty->assign("car_name",$val);
                        }
                    }
                }
            }
        }
        // upd 20190117 メールからの詳細表示はベタ表示でそれ以外はポップアップ表示へ変更
		if($this->req->get('m') == "login"){
            $this->templ->smarty->display("oneday_admin/camp_upd.html");
            exit;
        }
		//if($this->req->get('ret') == "calendar"){
			$this->templ->smarty->display("oneday_admin/camp_cal_upd.html");
		//}
		//else{
		//	$this->templ->smarty->display("oneday_admin/camp_upd.html");
		//}
		exit;
	}

    function conf_proc(){
        $car_list = $this->form_make();
        $this->assign_proc($car_list);
        $this->check_proc();
        if($this->req->hasErrors()){
            $this->templ->error_assign($this->req);
            if($this->req->get('m') == "login"){
                $this->templ->smarty->display("oneday_admin/camp_upd.html");
                exit;
            }
            $this->templ->smarty->display("oneday_admin/camp_cal_detail.html");
            exit;
        }
        $this->chgCheck($car_list);
        if($this->req->get('m') == "login"){
            $this->templ->smarty->display("oneday_admin/camp_conf.html");
            exit;
        }
        $this->templ->smarty->display("oneday_admin/camp_cal_conf.html");
        exit;
    }

	function assign_proc($car_list){
		$this->templ->smarty->assign("car_detail_id",$this->req->get('car_detail_id'));
		$this->templ->smarty->assign("car_id",$this->req->get('car_detail_id'));
		$this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
		$this->templ->smarty->assign("shop_name",$this->req->get('shop_name'));
		$this->templ->smarty->assign("staff_name",$this->req->get('staff_name'));
//		$this->templ->smarty->assign("car_name",$this->req->get('car_name'));
        $this->templ->smarty->assign("car_name",$car_list[$this->req->get('car_detail_id')]);
		$this->templ->smarty->assign("month",$this->req->get('month'));
		$this->templ->smarty->assign("day",$this->req->get('day'));
		$this->templ->smarty->assign("date",$this->req->get('date'));
		$this->templ->smarty->assign("ampm",$this->req->get('ampm'));
		$this->templ->smarty->assign("month2",$this->req->get('month2'));
		$this->templ->smarty->assign("day2",$this->req->get('day2'));
		$this->templ->smarty->assign("date2",$this->req->get('date2'));
		$this->templ->smarty->assign("ampm2",$this->req->get('ampm2'));
		$this->templ->smarty->assign("month3",$this->req->get('month3'));
		$this->templ->smarty->assign("day3",$this->req->get('day3'));
		$this->templ->smarty->assign("date3",$this->req->get('date3'));
		$this->templ->smarty->assign("ampm3",$this->req->get('ampm3'));
		$this->templ->smarty->assign("disp_number",$this->req->get('disp_number'));
		$this->templ->smarty->assign("sei",$this->req->get('sei'));
		$this->templ->smarty->assign("mei",$this->req->get('mei'));
		$this->templ->smarty->assign("sei_kana",$this->req->get('sei_kana'));
		$this->templ->smarty->assign("mei_kana",$this->req->get('mei_kana'));
		$this->templ->smarty->assign("postcd1",$this->req->get('postcd1'));
		$this->templ->smarty->assign("postcd2",$this->req->get('postcd2'));
		$this->templ->smarty->assign("address",$this->req->get('address'));
		$this->templ->smarty->assign("contact_kind",$this->req->get('contact_kind'));
		$this->templ->smarty->assign("tel1",$this->req->get('tel1'));
		$this->templ->smarty->assign("v_schedule_hour",$this->req->get('v_schedule_hour'));
		$this->templ->smarty->assign("v_schedule_min",$this->req->get('v_schedule_min'));
		$this->templ->smarty->assign("mail",$this->req->get('mail'));
		$this->templ->smarty->assign("comment",$this->req->get('comment'));
		$this->templ->smarty->assign("ret",$this->req->get('ret'));
		$this->templ->smarty->assign("sel_date",$this->req->get('sel_date'));
        $this->templ->smarty->assign("m",$this->req->get('m'));
		if($this->req->get('drive_year') and $this->req->get('drive_month') and $this->req->get('drive_day')){
			$this->templ->smarty->assign("drive_ampm",$this->req->get('drive_ampm'));
			$this->templ->smarty->assign("drive_year",$this->req->get('drive_year'));
			$this->templ->smarty->assign("drive_month",$this->req->get('drive_month'));
			$this->templ->smarty->assign("drive_day",$this->req->get('drive_day'));
			$this->templ->smarty->assign("conf_date",$this->req->get('conf_date'));
			$date = explode('-',$this->req->get('conf_date'));
			$this->templ->smarty->assign("conf_month",$date[1]);
			$this->templ->smarty->assign("conf_day",$date[2]);
			$this->templ->smarty->assign("conf_ampm",$this->req->get('conf_ampm'));
		}
		else if($this->req->get('conf_date')){
			$conf_flg = false;
			$sql = "select conf_flg,conf_ampm,conf_date from reservation ";
			$sql .= "where disp_flg = '1' ";
			$sql .= "and del_flg = '0' ";
			$sql .= "and temporary_flg = '2' ";
			$sql .= "and disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('conf_flg')){
						$this->templ->smarty->assign("drive_ampm",$rs->fields('conf_ampm'));
						$date = explode('-',$rs->fields('conf_date'));
						$this->templ->smarty->assign("drive_year",$date[0]);
						$this->templ->smarty->assign("drive_month",$date[1]);
						$this->templ->smarty->assign("drive_day",$date[2]);
						$conf_flg = true;
						$this->templ->smarty->assign("conf_date",$rs->fields('conf_date'));
						$date = explode('-',$rs->fields('conf_date'));
						$this->templ->smarty->assign("conf_month",$date[1]);
						$this->templ->smarty->assign("conf_day",$date[2]);
						$this->templ->smarty->assign("conf_ampm",$rs->fields('conf_ampm'));
					}
				}
				$rs->Close();
			}
			if(!$conf_flg){
				if($this->req->get('sel_date')){
					list($select_date,$select_ampm) = explode(',',$this->req->get('sel_date'));
					$this->templ->smarty->assign("drive_ampm",$select_ampm);
					$date = explode('-',$select_date);
					$this->templ->smarty->assign("drive_year",$date[0]);
					$this->templ->smarty->assign("drive_month",$date[1]);
					$this->templ->smarty->assign("drive_day",$date[2]);
				}
				$this->templ->smarty->assign("conf_date","");
				$this->templ->smarty->assign("conf_month","");
				$this->templ->smarty->assign("conf_day","");
				$this->templ->smarty->assign("conf_ampm","");
			}
		}
	}

	function chgCheck($car_list){
        $selCar = null;
        $dbCar = null;
        $sql = "select car1,car2,car3,car4,car5 from car_detail ";
        $sql .= "where autono = ".$this->DB->getQStr($this->req->get('car_id'));
        $rs =& $this->DB->ASExecute($sql);
        if($rs){
            if(!$rs->EOF){
                $selCar = trim($rs->fields('car1'));
                $selCar .= trim($rs->fields('car2'));
                $selCar .= trim($rs->fields('car3'));
                $selCar .= trim($rs->fields('car4'));
                $selCar .= trim($rs->fields('car5'));
            }
            $rs->Close();
        }
        $sql = "select car1,car2,car3,car4,car5 from reservation ";
        $sql .= "where disp_flg = '1' ";
        $sql .= "and del_flg = '0' ";
        $sql .= "and temporary_flg = '2' ";
        $sql .= "and disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."' ";
        $rs =& $this->DB->ASExecute($sql);
        if($rs){
            if(!$rs->EOF){
                $dbCar = trim($rs->fields('car1'));
                $dbCar .= trim($rs->fields('car2'));
                $dbCar .= trim($rs->fields('car3'));
                $dbCar .= trim($rs->fields('car4'));
                $dbCar .= trim($rs->fields('car5'));
            }
            $rs->Close();
        }
        if($selCar != $dbCar){
            $this->templ->smarty->assign("carChgFlg","1");
        }
        $this->templ->smarty->assign("car_id",$this->req->get('car_id'));
        $this->templ->smarty->assign("car_detail_id",$this->req->get('car_id'));
        $this->templ->smarty->assign("car_name",$car_list[$this->req->get('car_id')]);
    }

	function form_make(){
		$car_list = array();
		// 店舗IDから車種取得
		if($this->req->get('shop_id')){
		    // upd 20190130 turbo対応
			$sql = "select max(cd.autono) as car_detail_id,";
			$sql .= "cd.car1 as car1,cd.car2 as car2,cd.car3 as car3,cd.car4 as car4,cd.car5 as car5,max(c.name) as name ";
			$sql .= "from car_detail as cd,car as c ";
			$sql .= "where cd.disp_flg = '1' ";
			$sql .= "and cd.del_flg = '0' ";
			$sql .= "and c.disp_flg = '1' ";
			$sql .= "and cd.del_flg = '0' ";
			$sql .= "and cd.car_id = c.car_id ";
			$sql .= "and cd.shop_id='".$this->DB->getQStr($this->req->get('shop_id'))."'";
			$sql .= "group by cd.car1,cd.car2,cd.car3,cd.car4,cd.car5,c.order_no ";
			$sql .= "order by cd.car1,cd.car2,cd.car3,cd.car4,cd.car5,c.order_no";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$car_name = $rs->fields('name');
					if($rs->fields('car2') == 'de'){
						$car_name .= " ディーゼル ";
					}
					else if($rs->fields('car2') == 'ge'){
						$car_name .= " ガソリン ";
					}
					else if($rs->fields('car2') == 'hev'){
						$car_name .= " ハイブリッド ";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($rs->fields('car2') == 'skyx'){
                        $car_name .= "　SKYACTIV-X";
                    }
					$car_name .= $rs->fields('car3');
					if($rs->fields('car4') == 'mt'){
						$car_name .= " MT";
					}
					// add 20190130 turbo対応
                    if($rs->fields('car5') == 'turbo'){
                        $car_name .= " ターボ";
                    }
					$car_list[$rs->fields('car_detail_id')] = $car_name;
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		if($car_list){
			$this->templ->smarty->assign("car_list",$car_list);
		}
		//$this->templ->smarty->assign("year_list",$this->util->year_list());
        $this->templ->smarty->assign("year_list",$this->util->year3_list());
		$this->templ->smarty->assign("month_list",$this->util->month_list());
		$this->templ->smarty->assign("day_list",$this->util->day_list());
        return $car_list;
	}

	function regist_proc(){
        if($this->req->get('back_flg')){
            $car_list = $this->form_make();
            $this->assign_proc($car_list);
            if($this->req->get('m') == "login"){
                $this->templ->smarty->display("oneday_admin/camp_upd.html");
                exit;
            }
            $this->templ->smarty->display("oneday_admin/camp_cal_upd.html");
            exit;
        }
		$this->db_proc();
        if($this->req->get('m') == "login"){
            header("Location:./camp_cal.php");
            exit;
        }
		//if($this->req->get('ret') == "campaign"){
		//	header("Location:./campaign_list.php?mode=list");
		//}
		//else{
			header("Location:./campaign_detail.php?close_flg=1");
		//}
	}

	function check_proc(){
	    $check1 = false;
        $check2 = false;
        $check3 = false;
		if(!$this->req->get('drive_year')){
            $this->req->setError('error1',"試乗日：年を選択して下さい。");
            $check1 = true;
        }
        if(!$this->req->get('drive_month')){
            $this->req->setError('error2',"試乗日：月を選択して下さい。");
            $check2 = true;
        }
        if(!$this->req->get('drive_day')){
            $this->req->setError('error3',"試乗日：日を選択して下さい。");
            $check3 = true;
        }
        if(!$check1 and !$check2 and !$check3){
            if(!checkdate($this->req->get('drive_month'),$this->req->get('drive_day'),$this->req->get('drive_year'))){
                $this->req->setError('error4',"試乗日を正しく選択して下さい。");
            }
        }
        if(!$this->req->get('drive_ampm')){
            $this->req->setError('error5',"試乗日：午前・午後を選択して下さい。");
        }
	}

	function db_proc(){
		$record = NULL;
		$record['car_detail_id'] = $this->req->get('car_id');
		// upd 20190130 turbo対応
		$sql = "select car1,car2,car3,car4,car5 from car_detail ";
		$sql .= "where autono = ".$this->DB->getQStr($this->req->get('car_id'));
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$record['car1'] = $rs->fields('car1');
				$record['car2'] = $rs->fields('car2');
				$record['car3'] = $rs->fields('car3');
				$record['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $record['car5'] = $rs->fields('car5');
			}
			$rs->Close();
		}
		$record['conf_date'] = date("Y-m-d",mktime(0,0,0,$this->req->get('drive_month'),$this->req->get('drive_day'),$this->req->get('drive_year')));
		$record['conf_ampm'] = $this->req->get('drive_ampm');
		$record['conf_flg'] = "1";
		$record['staff_autono'] = $_SESSION['oneday']['staff_autono'];
		$record['update_date'] = time();
//var_dump($record);
//var_dump($this->req->get('disp_number'));
//exit;
		$where = " disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."'";
		$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
	}
}
?>