<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
//require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			case 'regist':
				$this->regist_proc();
			break;
			case 'delete':
				$this->delete_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}
	
	function default_proc(){
		if($this->req->get_get('close_flg') == '1'){
			$this->templ->smarty->assign("close_flg",$this->req->get_get('close_flg'));
			$this->templ->smarty->display("oneday_admin/camp_cal_detail.html");
			exit;
		}
		$dat = array();
		if($this->req->get_get('ret')){
			$this->templ->smarty->assign("ret",$this->req->get_get('ret'));
		}
		$number = $this->req->get_get('number');
		
		$_SESSION['admin']['disp_number'] = $number;
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($number)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$dat['shop_id'] = $rs->fields('shop_id');
				$engine = "";
				if($rs->fields('car2') == "ge"){
					$engine = "ガソリン";
				}
				else if($rs->fields('car2') == "de"){
					$engine = "ディーゼル";
				}
				else if($rs->fields('car2') == "hev"){
					$engine = "ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == 'skyx'){
                    $engine = "SKYACTIV-X";
                }
				$car3 = $rs->fields('car3');
				$mt = "";
				if($rs->fields('car4') == "mt"){
					$mt = "MT";
				}
				// add 20190130 turbo対応
                $turbo = "";
                if($rs->fields('car5') == "turbo"){
                    $turbo = " ターボ";
                }
				// 店舗
				$sql2 = "select name from shop ";
				$sql2 .= " where shop_id='".$this->DB->getQStr($dat['shop_id'])."' ";
				$sql2 .= " and disp_flg='1' ";
				$sql2 .= " and del_flg='0' ";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					if(!$rs2->EOF){
						$dat['shop_name'] = $rs2->fields('name');
					}
					$rs2->Close();
				}
				$dat['car_id'] = $rs->fields('car_detail_id');
				if($this->req->get_get('cid')){
					$dat['car_id'] = $this->req->get_get('cid');
				}
				// 車種
				if($dat['car_id']){
					$sql2 = "select car_id from car_detail";
					$sql2 .= " where autono=".$this->DB->getQStr($dat['car_id']);
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$sql3 = "select name,name2,name3 from car ";
							$sql3 .= " where car_id='".$this->DB->getQStr($rs2->fields('car_id'))."' ";
							$sql3 .= " and disp_flg='1' ";
							$sql3 .= " and del_flg='0' ";
							$rs3 =& $this->DB->ASExecute($sql3);
							if($rs3){
								if(!$rs3->EOF){
									//$dat['car_name'] = $rs3->fields('name');
									// upd 20190130 turbo対応
                                    if($engine){
										if($mt){
											$dat['car_name'] = $rs3->fields('name')." ".$engine." ".$mt." ".$car3.$turbo;
										}
										else{
											$dat['car_name'] = $rs3->fields('name')." ".$engine." ".$car3.$turbo;
										}
									}
									else{
										if($mt){
											$dat['car_name'] = $rs3->fields('name')." ".$mt." ".$car3.$turbo;
										}
										else{
											$dat['car_name'] = $rs3->fields('name')." ".$car3.$turbo;
										}
									}
								}
								$rs3->Close();
							}
						}
						$rs2->Close();
					}
				}
				else{
					$sql3 = "select name from car ";
					$sql3 .= " where car1 = '".$this->DB->getQStr($rs->fields('car1'))."' ";
					$sql3 .= " and disp_flg='1' ";
					$sql3 .= " and del_flg='0' ";
					$rs3 =& $this->DB->ASExecute($sql3);
					if($rs3){
						if(!$rs3->EOF){
                            // upd 20190130 turbo対応
							if($engine){
								if($mt){
									$dat['car_name'] = $rs3->fields('name')." ".$engine." ".$mt." ".$car3.$turbo;
								}
								else{
									$dat['car_name'] = $rs3->fields('name')." ".$engine." ".$car3.$turbo;
								}
							}
							else{
								if($mt){
									$dat['car_name'] = $rs3->fields('name')." ".$mt." ".$car3.$turbo;
								}
								else{
									$dat['car_name'] = $rs3->fields('name')." ".$car3.$turbo;
								}
							}
						}
						$rs3->Close();
					}
				}
				$dat['number'] = $rs->fields('disp_number');
				$dat['conf_flg'] = $rs->fields('conf_flg');
				$dat['conf_date'] = $rs->fields('conf_date');
				$dat['date'] = $rs->fields('date');
				$dat['date2'] = $rs->fields('date2');
				$dat['date3'] = $rs->fields('date3');
				list($dat['year'],$dat['month'],$dat['day']) = explode("-",$rs->fields('date'));
				list($dat['year2'],$dat['month2'],$dat['day2']) = explode("-",$rs->fields('date2'));
				list($dat['year3'],$dat['month3'],$dat['day3']) = explode("-",$rs->fields('date3'));
				$dat['conf_ampm'] = $rs->fields('conf_ampm');
				$dat['ampm'] = $rs->fields('ampm');
				$dat['ampm2'] = $rs->fields('ampm2');
				$dat['ampm3'] = $rs->fields('ampm3');
				if($dat['conf_flg'] == "1"){
					if(($dat['conf_date'] != $dat['date'] or $dat['conf_ampm'] != $dat['ampm']) and ($dat['conf_date'] != $dat['date2'] or $dat['conf_ampm'] != $dat['ampm2']) and ($dat['conf_date'] != $dat['date3'] or $dat['conf_ampm'] != $dat['ampm3'])){
						$date = explode('-',$rs->fields('conf_date'));
						$dat['driver_month'] = $date[1];
						$dat['driver_day'] = $date[2];
						$dat['driver_ampm'] = $rs->fields('conf_ampm');
						$dat['driver_flg'] = 1;
					}
				}
				$dat['sei'] = $rs->fields('sei');
				$dat['mei'] = $rs->fields('mei');
				$dat['sei_kana'] = $rs->fields('sei_kana');
				$dat['mei_kana'] = $rs->fields('mei_kana');
				$postcd = explode("-", $rs->fields('postcd'));
				$dat['postcd1'] = $postcd[0];
				$dat['postcd2'] = $postcd[1];
				$dat['address'] = $rs->fields('address');
				$dat['contact_kind'] = $rs->fields('contact_kind');
				$dat['tel'] = $rs->fields('tel');
//				list($dat['tel1'],$dat['tel2'],$dat['tel3']) = explode("-",$rs->fields('tel'));
				if($rs->fields('v_schedule_time')){
					$dat['v_schedule_hour'] = date("H",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y")));
					$dat['v_schedule_min'] = date("i",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y")));
				}
				$dat['mail'] = $rs->fields('mail');
				$dat['comment'] = $rs->fields('comment');
			}
			$rs->Close();
		}
		if($dat['conf_flg']){
			$this->templ->smarty->assign("conf_date",$dat['conf_date']);
			$this->templ->smarty->assign("conf_ampm",$dat['conf_ampm']);
			$this->templ->smarty->assign("sel_date",$dat['conf_date'].",".$dat['conf_ampm']);
		}
		else{
			list($conf_date,$conf_ampm) = explode(",",$this->req->get_get('sel_date'));
			$this->templ->smarty->assign("conf_date",$conf_date);
			$this->templ->smarty->assign("conf_ampm",$conf_ampm);
			$this->templ->smarty->assign("sel_date",$this->req->get_get('sel_date'));
		}
		if($dat['driver_flg'] == 1){
			$this->templ->smarty->assign("driver_month",$dat['driver_month']);
			$this->templ->smarty->assign("driver_day",$dat['driver_day']);
			$this->templ->smarty->assign("driver_ampm",$dat['driver_ampm']);
			$this->templ->smarty->assign("driver_flg",$dat['driver_flg']);
		}
		$this->templ->smarty->assign("conf_flg",$dat['conf_flg']);
		$this->templ->smarty->assign("car_detail_id",$dat['car_id']);
		$this->templ->smarty->assign("shop_id",$dat['shop_id']);
		$this->templ->smarty->assign("shop_name",$dat['shop_name']);
		$this->templ->smarty->assign("car_id",$dat['car_id']);
		$this->templ->smarty->assign("car_name",$dat['car_name']);
		$this->templ->smarty->assign("number",$dat['number']);
		$this->templ->smarty->assign("date",$dat['date']);
		$this->templ->smarty->assign("date2",$dat['date2']);
		$this->templ->smarty->assign("date3",$dat['date3']);
		$this->templ->smarty->assign("car_id",$dat['car_id']);
		$this->templ->smarty->assign("month",$dat['month']);
		$this->templ->smarty->assign("day",$dat['day']);
		$this->templ->smarty->assign("month2",$dat['month2']);
		$this->templ->smarty->assign("day2",$dat['day2']);
		$this->templ->smarty->assign("month3",$dat['month3']);
		$this->templ->smarty->assign("day3",$dat['day3']);
		$this->templ->smarty->assign("ampm",$dat['ampm']);
		$this->templ->smarty->assign("ampm2",$dat['ampm2']);
		$this->templ->smarty->assign("ampm3",$dat['ampm3']);
		$this->templ->smarty->assign("sei",$dat['sei']);
		$this->templ->smarty->assign("mei",$dat['mei']);
		$this->templ->smarty->assign("sei_kana",$dat['sei_kana']);
		$this->templ->smarty->assign("mei_kana",$dat['mei_kana']);
		$this->templ->smarty->assign("postcd1",$dat['postcd1']);
		$this->templ->smarty->assign("postcd2",$dat['postcd2']);
		$this->templ->smarty->assign("address",$dat['address']);
		$this->templ->smarty->assign("contact_kind",$dat['contact_kind']);
		$this->templ->smarty->assign("tel1",$dat['tel']);
//		$this->templ->smarty->assign("tel1",$dat['tel1']);
//		$this->templ->smarty->assign("tel2",$dat['tel2']);
//		$this->templ->smarty->assign("tel3",$dat['tel3']);
		$this->templ->smarty->assign("v_schedule_hour",$dat['v_schedule_hour']);
		$this->templ->smarty->assign("v_schedule_min",$dat['v_schedule_min']);
		$this->templ->smarty->assign("mail",$dat['mail']);
		$this->templ->smarty->assign("comment",$dat['comment']);
		// upd 20190117 メールからの詳細表示はベタ表示でそれ以外はポップアップ表示へ変更
        $this->templ->smarty->assign("m",$this->req->get_get('m'));
		if($this->req->get_get('m') == "login"){
            $this->templ->smarty->display("oneday_admin/campaign_detail.html");
            exit;
        }
		//if($this->req->get_get('ret') == "calendar"){
			$this->templ->smarty->display("oneday_admin/camp_cal_detail.html");
		//}
		//else{
		//	$this->templ->smarty->display("oneday_admin/campaign_detail.html");
		//}
		exit;
	}

	function regist_proc(){
		$this->db_proc();
		if($this->req->get("m") == "login"){
            header("Location:./camp_cal.php");
            exit;
        }
        header("Location:./campaign_detail.php?close_flg=1");
		/*
        if($this->req->get('ret') == "campaign"){
			header("Location:./campaign_list.php?mode=list");
		}
		else{
			header("Location:./camp_cal.php");
		}*/
	}

	function db_proc(){
		$record = NULL;
		list($record['conf_date'],$record['conf_ampm']) = explode(",",$this->req->get('confirm'));
		$record['conf_flg'] = "1";
		$record['staff_autono'] = $_SESSION['oneday']['staff_autono'];
		if($this->req->get('car_detail_id')){
			$record['car_detail_id'] = $this->req->get('car_detail_id');
		}
		$record['update_date'] = time();
		$where = " disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."'";
		$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
	}

	function delete_proc(){
		$number = $this->req->get_get('number');
		$sql = "select * from reservation ";
		$sql .= " where disp_number='".$this->DB->getQStr($number)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$sql .= " order by date";
		$rs =& $this->DB->ASExecute($sql);
//		$dat = array();
		if($rs){
			if(!$rs->EOF){
				$record = null;
				$record['staff_autono'] = $_SESSION['oneday']['staff_autono'];
				$record['temporary_flg'] = "0";
				$record['del_flg'] = "1";
				$record['disp_flg'] = "0";
				$where  = " disp_number='".$this->DB->getQStr($number)."'";
				$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
			}
			$rs->Close();
		}
		if($this->req->get_get('m') == "login"){
            header("Location:./camp_cal.php");
            exit;
        }
		//if($this->req->get_get('ret') == "campaign"){
		//	header("Location:./campaign_list.php?mode=list");
		//}
		//else{
			header("Location:./campaign_detail.php?close_flg=1");
		//}
	}
}
?>