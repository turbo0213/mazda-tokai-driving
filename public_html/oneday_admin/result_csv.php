<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		$stationsOrder = $this->util->station_list();
		$stations = $this->getStations();
		$summaries = $this->list_data_get();
		$this->download_proc($stationsOrder,$stations,$summaries);
		exit;
	}

	function getStations(){
		$station_list = array();
		$num = 1;
		$sql = "select * from shop";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " order by station_code,order_no";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				if(!$station_list[$rs->fields('station_code')]['name']){
					$station_list[$rs->fields('station_code')]['name'] = $rs->fields('station_name');
					$num = 1;
				}
				$station_list[$rs->fields('station_code')]['shops'][$rs->fields('shop_id')] = $rs->fields('name');
				$station_list[$rs->fields('station_code')]['num'] = $num;
				$num++;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $station_list;
	}

	function list_data_get(){
		$summary = array();
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN
                if($rs->fields('car1') == "mz6s"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
                // MAZDA6 WAGON
                if($rs->fields('car1') == "mz6w"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
                // MAZDA3 SEDAN
                if($rs->fields('car1') == "mzs"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // MAZDA3 FASTBACK
                if($rs->fields('car1') == "mzf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
                // NAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
                // デミオ
                if($rs->fields('car1') == "dmo"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                }
                // ロードスター
                if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
                // ロードスターRF
                if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MX-30 add 20201009
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // CX-3
                if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
				}
                // CX-5
                if($rs->fields('car1') == "cx5"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // CX-8
                if($rs->fields('car1') == "cx8"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
                // アテンザ
                if($rs->fields('car1') == "atz"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
                // アクセラ
                if($rs->fields('car1') == "axl"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date2 between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN
                if($rs->fields('car1') == "mz6s"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
                // MAZDA6 WAGON
                if($rs->fields('car1') == "mz6w"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
                // MAZDA3 SEDAN
                if($rs->fields('car1') == "mzs"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // MAZDA3 FASTBACK
                if($rs->fields('car1') == "mzf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
                // MAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
                // デミオ
                if($rs->fields('car1') == "dmo"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                }
                // ロードスター
                if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
                // ロードスターRF
                if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MX-30 add 20201009
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // CX-3
                if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
				}
                // CX-5
                if($rs->fields('car1') == "cx5"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // CX-8
                if($rs->fields('car1') == "cx8"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
                // アテンザ
                if($rs->fields('car1') == "atz"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
                // アクセラ
                if($rs->fields('car1') == "axl"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date3 between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN
                if($rs->fields('car1') == "mz6s"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
                // MAZDA6 WAGON
                if($rs->fields('car1') == "mz6w"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
                // MAZDA3 SEDAN
                if($rs->fields('car1') == "mzs"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // MAZDA3 FASTBACK
                if($rs->fields('car1') == "mzf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
                // NAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
                // デミオ
                if($rs->fields('car1') == "dmo"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num6']++;
                    }
                }
                // ロードスター
                if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
                // ロードスターRF
                if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MX-30 add 20201009
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // CX-3
                if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					}
				}
                // CX-5
                if($rs->fields('car1') == "cx5"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // CX-8
                if($rs->fields('car1') == "cx8"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
                // アテンザ
                if($rs->fields('car1') == "atz"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
                // アクセラ
                if($rs->fields('car1') == "axl"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg = '1'";
		$sql .= " and conf_date between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				// MAZDA6 SEDAN
				if($rs->fields('car1') == "mz6s"){
					(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num1']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num1']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num1']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num1']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num1']++;
					}
				}
				// MAZDA6 WAGON
				if($rs->fields('car1') == "mz6w"){
					(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num2']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num2']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num2']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num2']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num2']++;
					}
				}
				// MAZDA3 SEDAN
				if($rs->fields('car1') == "mzs"){
					(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num3']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num3']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num3']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num3']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num3']++;
					}
				}
				// MAZDA3 FASTBACK
				if($rs->fields('car1') == "mzf"){
					(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num4']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num4']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num4']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num4']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num4']++;
					}
				}
                // MAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
                    (int)$summary[$rs->fields('shop_id')]['res_num5']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num5']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num5']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num5']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num5']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num5']++;
                    }
                }
                // デミオ
                if($rs->fields('car1') == "dmo"){
					(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num6']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num6']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num6']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num6']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num6']++;
					}
				}
                // ロードスター
                if($rs->fields('car1') == "rst"){
					(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num7']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num7']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num7']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num7']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num7']++;
					}
				}
                // ロードスターRF
                if($rs->fields('car1') == "rsrf"){
					(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num8']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num8']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num8']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num8']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num8']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num9']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num9']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num9']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num9']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num9']++;
                    }
                }
                // MX-30 add 20201009
                if($rs->fields('car1') == "mx30"){
                    (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num10']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num10']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num10']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num10']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num10']++;
                    }
                }
                // CX-3
                if($rs->fields('car1') == "cx3"){
					(int)$summary[$rs->fields('shop_id')]['res_num11']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num11']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num11']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num11']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num11']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num11']++;
					}
				}
                // CX-5
                if($rs->fields('car1') == "cx5"){
                    (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num12']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num12']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num12']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num12']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num12']++;
                    }
                }
                // CX-8
                if($rs->fields('car1') == "cx8"){
                    (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num13']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num13']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num13']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num13']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num13']++;
                    }
                }
                // アテンザ
                if($rs->fields('car1') == "atz"){
                    (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num14']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num14']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num14']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num14']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num14']++;
                    }
                }
                // アクセラ
                if($rs->fields('car1') == "axl"){
                    (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num15']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num15']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num15']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num15']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $summary;
	}

	function download_proc($stationsOrder,$stations,$summaries){
		$date_txt = $this->week_make();
		$file_name = "campaign_result".date("Ymd");
		header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/x-csv");
		header("Content-Disposition: attachment; filename=".$file_name.".csv"); 
		$csv = '"予約実績"'."\r\n";
		$csv .= $date_txt."\r\n";
		$csv .= '"","","MAZDA6 SEDAN","","","","","","MAZDA6 WAGON","","","","","","MAZDA3 SEDAN","","","","","","MAZDA3 FASTBACK","","","","","","MAZDA2","","","","","","デミオ","","","","","","ロードスター","","","","","","ロードスターRF","","","","","","CX-30","","","","","","MX-30","","","","","","CX-3","","","","","","CX-5","","","","","","CX-8","","","","","","アテンザ","","","","","","アクセラ","","","","",""'."\r\n";
		$csv .= '"","","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止","予約数","予約確定","来店数","成約数","フォロー継続","フォロー中止"'."\r\n";
		echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
		foreach($stationsOrder as $key => $val){
			foreach($stations[$val] as $key2 => $val2){
				// 事業部名
				$csv = '"'.$stations[$val]["name"].'"';
				foreach($stations[$val]["shops"] as $key3 => $val3){
					// 店舗名
					if($csv){
						$csv .= ',"'.$val3.'"';
					}
					else{
						$csv = '"","'.$val3.'"';
					}
					// 集計データ
					if($summaries[$key3]){
						foreach($summaries[$key3] as $key4 => $val4){
							for($i=1;$i<=15;$i++){
								if($summaries[$key3]['res_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['res_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
								if($summaries[$key3]['conf_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['conf_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
								if($summaries[$key3]['visit_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['visit_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
								if($summaries[$key3]['ab_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['ab_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
								if($summaries[$key3]['next_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['next_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
								if($summaries[$key3]['ng_num'.$i]){
									$csv .= ',"'.$summaries[$key3]['ng_num'.$i].'"';
								}
								else{
									$csv .= ',""';
								}
							}
							$csv .= "\r\n";
							echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
							$csv = "";
							break;
						}
					}
					else{
						for($i=0;$i<=48;$i++){
							$csv .= ',""';
						}
						$csv .= "\r\n";
						echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
						$csv = "";
					}
				}
				break;
			}
		}
	}

	function week_make(){
		$date_txt = "";
		$year = $this->req->get('year');
		$month = $this->req->get('month');
		$day = $this->req->get('day');
		$year_to = $this->req->get('year_to');
		$month_to = $this->req->get('month_to');
		$day_to = $this->req->get('day_to');
		if(!$year){
			$year = date("Y",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$month){
			$month = date("m",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$day){
			$day = date("d",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$year_to){
			$year_to = date("Y");
		}
		if(!$month_to){
			$month_to = date("m");
		}
		if(!$day_to){
			$day_to = date("d");
		}
		if(mktime(0,0,0,$month,$day,$year) > mktime(0,0,0,$month_to,$day_to,$year_to)){
			$year_to = date("Y",mktime(0,0,0,$month,$day + 6,$year));
			$month_to = date("m",mktime(0,0,0,$month,$day + 6,$year));
			$day_to = date("d",mktime(0,0,0,$month,$day + 6,$year));
		}
		$date_txt = "予約日：";
		$date_txt .= date("Y年m月d日",mktime(0,0,0,$month,$day,$year));
		$date_txt .= " ～ ";
		$date_txt .= date("Y年m月d日",mktime(0,0,0,$month_to,$day_to,$year_to));
		return $date_txt;
	}
}
?>