<?php
define("TEMPLATE1", "oneday_admin/staff_list.html");
define("TEMPLATE2", "oneday_admin/staff_conf.html");
define("TEMPLATE3", "oneday_admin/staff_end.html");
define("LIST_MAX", "20");
define("PAGE_ID_NAME", "qpage");

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
require_once("../../mc_apl/Sliding.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB2;
    var $DB3;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB2 = new ASDB_MAIN();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
		if(!$_SESSION['oneday']['staff_mng']){
			header("Location:camp_cal.php");
			exit;
		}
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			case 'list':
				$this->list_proc();
			break;
			case 'search':
				$this->search_proc();
			break;
			case 'station':
				$this->station_proc();
			break;
			case 'shop':
				$this->shop_proc();
			break;
			case 'station2':
				$this->station2_proc();
			break;
			case 'input':
				$this->input_proc();
			break;
			case 'check':
				$this->check_proc();
			break;
			case 'regist':
				$this->regist_proc();
			break;
			case 'del':
				$this->del_proc();
			break;
			case 'end':
				$this->end_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function station_proc(){
		$_SESSION['station'] = $this->req->get('station');
		$_SESSION['s_search']['station'] = $_SESSION['station'];
		$_SESSION['s_search']['shop'] = "";
		$shop_list = $this->shop_list();
		$this->templ->smarty->assign('shop_list',$shop_list);
		$staff_list = $this->staff_list();
		$this->templ->smarty->assign('staff_list',$staff_list);
		if($staff_list){
			if(is_array($staff_list)){
				foreach($staff_list as $key => $val){
					$staff_autono = $key;
					break;
				}
			}
		}
		$_SESSION['station'] = "";
		$_SESSION['shop'] = "";
		$_SESSION['s_search']['staff'] = "";
		$this->form_make2();
		$this->list_data_get();
		$this->templ->smarty->assign('station',$this->req->get('station'));
		$this->templ->smarty->assign('shop', $_SESSION['s_search']['shop']);
		$this->templ->smarty->assign('staff_id',$_SESSION['s_search']['staff_id']);
		$this->templ->smarty->assign('s_access_kb',$_SESSION['s_search']['access_kb']);
		$this->templ->smarty->assign('open_flg',1);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}

	function shop_proc(){
		$_SESSION['station'] = $this->req->get('station');
		$this->templ->smarty->assign('shop_list',$this->shop_list());
		if($this->req->get('shop')){
			$_SESSION['shop'] = $this->req->get('shop');
			$_SESSION['s_search']['shop'] = $_SESSION['shop'];
		}
		$staff_list = $this->staff_list();
		$this->templ->smarty->assign('staff_list',$staff_list);
		if($staff_list){
			if(is_array($staff_list)){
				foreach($staff_list as $key => $val){
					$staff_autono = $key;
					break;
				}
			}
		}
		$_SESSION['station'] = "";
		$_SESSION['shop'] = "";
		$_SESSION['s_search']['staff'] = "";
		$this->form_make2();
		$this->list_data_get();
		if($this->req->get('station')){
			$this->templ->smarty->assign('station',$this->req->get('station'));
		}
		else if($this->req->get('shop')){
			$sql = "select station_code from shop";
			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('shop'))."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign('station',$rs->fields('station_code'));
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign('shop',$this->req->get('shop'));
		$this->templ->smarty->assign('staff_id',$_SESSION['s_search']['staff_id']);
		$this->templ->smarty->assign('s_access_kb',$_SESSION['s_search']['access_kb']);
		$this->templ->smarty->assign('open_flg',1);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}

	function station2_proc(){
		$_SESSION['station'] = $this->req->get('station');
		$shop_list = $this->shop_list(1);
		$this->templ->smarty->assign('shop_list',$shop_list);
		$_SESSION['station'] = "";
		$_SESSION['shop'] = "";
		$this->detail_form_make2();
		if($this->req->get('at')){
			$this->detail_data_get();
		}
		else{
			$this->templ->smarty->assign('station',$this->req->get('station'));
			$this->templ->smarty->assign('shop',$this->req->get('shop'));
			$this->templ->smarty->assign('staff_id',$this->req->get('staff_id'));
			$this->templ->smarty->assign('name',$this->req->get('name'));
			$this->templ->smarty->assign('mail',$this->req->get('mail'));
			$this->templ->smarty->assign('access_kb',$this->req->get('access_kb'));
			$this->templ->smarty->assign('at',$this->req->get('at'));
            $this->templ->smarty->assign('driving_mail',$this->req->get('driving_mail'));
            $this->templ->smarty->assign('es_kind',$this->req->get('es_kind'));
            $this->templ->smarty->assign('web_user_flg',$this->req->get('web_user_flg'));
            $this->templ->smarty->assign('mainte_user_flg',$this->req->get('mainte_user_flg'));
            $this->templ->smarty->assign('driving_user_flg',$this->req->get('driving_user_flg'));
		}
		$this->templ->smarty->assign('station',$this->req->get('station'));
		$this->templ->smarty->assign('check_flg',0);
		$this->templ->smarty->display(TEMPLATE2);
		exit;
	}

	function default_proc(){
		$this->form_make();
		$station_list = $this->station_list();
		if($_SESSION['s_search']['station']){
			$_SESSION['station'] = $_SESSION['s_search']['station'];
		}
		else{
			$_SESSION['station'] = $this->req->get('station');
		}
		$this->templ->smarty->assign('station_list',$station_list);
		$shop_list = $this->shop_list();
		if($_SESSION['s_search']['shop']){
			$_SESSION['shop'] = $_SESSION['s_search']['shop'];
		}
		else{
			$_SESSION['shop'] = $this->req->get('shop');
		}
		$_SESSION['s_search']['shop'] = $_SESSION['shop'];
		$this->templ->smarty->assign('shop',$_SESSION['shop']);
		$this->templ->smarty->assign('shop_list',$shop_list);
		$staff_list = $this->staff_list();
		if($this->req->get('staff')){
			$staff = $this->req->get('staff');
		}
		$this->templ->smarty->assign('staff',$staff);
		$this->templ->smarty->assign('staff_list',$staff_list);
		$_SESSION['station'] = "";
		$_SESSION['shop'] = "";
		$this->list_data_get();
		$this->templ->smarty->assign('station',$_SESSION['s_search']['station']);
		$this->templ->smarty->assign('shop',$_SESSION['s_search']['shop']);
		$this->templ->smarty->assign('staff',$_SESSION['s_search']['staff']);
		$this->templ->smarty->assign('staff_id',$_SESSION['s_search']['staff_id']);
		$this->templ->smarty->assign('s_access_kb',$_SESSION['s_search']['access_kb']);
		$this->templ->smarty->assign('open_flg',0);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}
	
	function search_proc(){
		$this->form_make();
		//検索ボタン押下時
		if($this->req->get('search')){
			$_SESSION['s_search']['station'] = $this->req->get('station');
			$_SESSION['s_search']['shop'] = $this->req->get('shop');
			$_SESSION['s_search']['staff'] = $this->req->get('staff');
			$_SESSION['s_search']['staff_id'] = $this->req->get('staff_id');
			$_SESSION['s_search']['access_kb'] = $this->req->get('s_access_kb');
		}
		if($this->req->get('clear')){
			$_SESSION['s_search']['station'] = "";
			$_SESSION['s_search']['shop'] = "";
			$_SESSION['s_search']['staff'] = "";
			$_SESSION['s_search']['staff_id'] = "";
			$_SESSION['s_search']['access_kb'] = "";
			$this->req->set('station',"");
			$this->req->set('shop',"");
			$this->req->set('staff',"");
			$this->req->set('staff_id',"");
			$this->req->set('s_access_kb',"");
		}
		$station_list = $this->station_list();
		if(!$this->req->get('station') and !$_SESSION['s_search']['station']){
			foreach($station_list as $key => $val){
				$_SESSION['station'] = $key;
				break;
			}
		}
		else{
			if($_SESSION['s_search']['station']){
				$_SESSION['station'] = $_SESSION['s_search']['station'];
			}
			else{
				$_SESSION['station'] = $this->req->get('station');
			}
		}
		$_SESSION['s_search']['station'] = $_SESSION['station'];
		$this->templ->smarty->assign('station',$_SESSION['station']);
		$this->templ->smarty->assign('station_list',$station_list);
		$shop_list = $this->shop_list();
		if(!$this->req->get('shop') and !$_SESSION['s_search']['shop']){
			foreach($shop_list as $key => $val){
				$_SESSION['shop'] = $key;
				break;
			}
		}
		else{
			if($_SESSION['s_search']['shop']){
				$_SESSION['shop'] = $_SESSION['s_search']['shop'];
			}
			else{
				$_SESSION['shop'] = $this->req->get('shop');
			}
		}
		$_SESSION['s_search']['shop'] = $_SESSION['shop'];
		$this->templ->smarty->assign('shop',$_SESSION['shop']);
		$this->templ->smarty->assign('shop_list',$shop_list);
		$staff_list = $this->staff_list();
		if(!$this->req->get('staff')){
			foreach($staff_list as $key => $val){
				$staff = $key;
				break;
			}
		}
		else{
			$staff = $this->req->get('staff');
		}
		$this->templ->smarty->assign('staff',$staff);
		$this->templ->smarty->assign('staff_list',$staff_list);
		$_SESSION['station'] = "";
		$_SESSION['shop'] = "";
		$this->list_data_get();
		$this->templ->smarty->assign('station',$_SESSION['s_search']['station']);
		$this->templ->smarty->assign('shop',$_SESSION['s_search']['shop']);
		$this->templ->smarty->assign('staff',$_SESSION['s_search']['staff']);
		$this->templ->smarty->assign('staff_id',$_SESSION['s_search']['staff_id']);
		$this->templ->smarty->assign('s_access_kb',$_SESSION['s_search']['access_kb']);
		$this->templ->smarty->assign('open_flg',1);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}

	function input_proc(){
		$this->detail_form_make();
		if($this->req->get_get('at') or $this->req->get('at')){
			$this->detail_data_get();
		}
		$this->templ->smarty->assign('check_flg',0);
		$this->templ->smarty->display(TEMPLATE2);
		exit;
	}

	function check_proc(){
		$this->templ->smarty->assign('station',$this->req->get('station'));
		$this->templ->smarty->assign('shop',$this->req->get('shop'));
		$this->templ->smarty->assign('staff_id',$this->req->get('staff_id'));
		$this->templ->smarty->assign('name',$this->req->get('name'));
		$this->templ->smarty->assign('mail',$this->req->get('mail'));
		$this->templ->smarty->assign('access_kb',$this->req->get('access_kb'));
		$this->templ->smarty->assign('at',$this->req->get('at'));
        $this->templ->smarty->assign('driving_mail',$this->req->get('driving_mail'));
        $this->templ->smarty->assign('es_kind',$this->req->get('es_kind'));
        $this->templ->smarty->assign('web_user_flg',$this->req->get('web_user_flg'));
        $this->templ->smarty->assign('mainte_user_flg',$this->req->get('mainte_user_flg'));
        $this->templ->smarty->assign('driving_user_flg',$this->req->get('driving_user_flg'));
		if(!$this->req->get('back')){
			$this->data_check();
			if($this->req->hasErrors()){
				$this->detail_form_make();
				$this->templ->smarty->assign('check_flg',0);
				$this->templ->error_assign($this->req);
				$this->templ->smarty->display(TEMPLATE2);
				exit;
			}
			if($this->req->get('station')){
				$sql = "select * from shop ";
				$sql .= "where station_code = '".$this->DB->getQStr($this->req->get('station'))."' ";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$this->templ->smarty->assign('station_value',$rs->fields('station_name'));
					}
					$rs->Close();
				}
			}
			else{
				$this->templ->smarty->assign('station_value',"本部");
			}
			if($this->req->get('shop')){
				$sql = "select * from shop";
				$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('shop'))."' ";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$this->templ->smarty->assign('station_value',$rs->fields('station_name'));
						$this->templ->smarty->assign('station',$rs->fields('station_code'));
						$this->templ->smarty->assign('shop_value',$rs->fields('name'));
					}
					$rs->Close();
				}
			}
			else if(!$this->req->get('station')){
				$this->templ->smarty->assign('shop_value',"本社");
			}
			$this->templ->smarty->assign('access_kb',$this->req->get('access_kb'));
			$this->templ->smarty->assign('access_kb_val',$this->util->access_kb_list(1,$this->req->get('access_kb')));
            $this->templ->smarty->assign('es_kind_val',$this->util->es_kind_list(1,$this->req->get('es_kind')));
			$this->templ->smarty->assign('check_flg',1);
		}
		else{
			$this->templ->smarty->assign('check_flg',0);
		}
		$this->templ->smarty->display(TEMPLATE2);
		exit;
	}
	
	function regist_proc(){
		$this->templ->smarty->assign('station',$this->req->get('station'));
		$this->templ->smarty->assign('shop',$this->req->get('shop'));
		$this->templ->smarty->assign('staff_id',$this->req->get('staff_id'));
		$this->templ->smarty->assign('name',$this->req->get('name'));
		$this->templ->smarty->assign('access_kb',$this->req->get('access_kb'));
		$this->templ->smarty->assign('access_kb_val',$this->util->access_kb_list(1,$this->req->get('access_kb')));
		$this->templ->smarty->assign('mail',$this->req->get('mail'));
		$this->templ->smarty->assign('at',$this->req->get('at'));
        $this->templ->smarty->assign('driving_mail',$this->req->get('driving_mail'));
        $this->templ->smarty->assign('es_kind',$this->req->get('es_kind'));
        $this->templ->smarty->assign('web_user_flg',$this->req->get('web_user_flg'));
        $this->templ->smarty->assign('mainte_user_flg',$this->req->get('mainte_user_flg'));
        $this->templ->smarty->assign('driving_user_flg',$this->req->get('driving_user_flg'));
		if($this->req->get('station')){
			$sql = "select * from shop ";
			$sql .= "where station_code = '".$this->DB->getQStr($this->req->get('station'))."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign('station_value',$rs->fields('station_name'));
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign('access_kb',$this->req->get('access_kb'));
		$this->data_check();
		if($this->req->hasErrors()){
			$this->detail_form_make();
			$this->templ->smarty->assign('check_flg',0);
			$this->templ->error_assign($this->req);
			$this->templ->smarty->display(TEMPLATE2);
			exit;
		}
		if($this->req->get('back')){
			$this->detail_form_make();
			$this->templ->smarty->assign('check_flg',0);
			$this->templ->smarty->display(TEMPLATE2);
			exit;
		}
		$this->db_proc();
		header("Location: ./staff_mng.php?mode=end");
		exit;
	}
	
	function form_make(){
		$this->templ->smarty->assign('staff_list',$this->staff_list());
		$this->templ->smarty->assign('shop_list',$this->shop_list());
		$this->templ->smarty->assign('station_list',$this->station_list());
		$this->templ->smarty->assign('access_kb_list',$this->util->access_kb_list());
        $this->templ->smarty->assign('es_kind_list',$this->util->es_kind_list());
	}
	
	function form_make2(){
		$this->templ->smarty->assign('station_list',$this->station_list());
		$this->templ->smarty->assign('access_kb_list',$this->util->access_kb_list());
        $this->templ->smarty->assign('es_kind_list',$this->util->es_kind_list());
	}
	
	function detail_form_make(){
		$this->templ->smarty->assign('shop_list',$this->shop_list(1));
		$this->templ->smarty->assign('station_list',$this->station_list(1));
		$this->templ->smarty->assign('access_kb_list',$this->util->access_kb_list());
        $this->templ->smarty->assign('es_kind_list',$this->util->es_kind_list());
	}
	
	function detail_form_make2(){
		$this->templ->smarty->assign('station_list',$this->station_list(1));
		$this->templ->smarty->assign('access_kb_list',$this->util->access_kb_list());
        $this->templ->smarty->assign('es_kind_list',$this->util->es_kind_list());
	}
	
	function list_data_get(){
        $shop_sql = "";
		if($_SESSION['s_search']['station']){
			$shop_sql = " and station_code = '".$this->DB3->getQStr($_SESSION['s_search']['station'])."' ";
		}
		$list_sql = "select * from send_mail";
		$count_sql = "select count(autono) as staff_cnt from send_mail";
		$where_sql = " where del_flg = '0' and disp_flg = '1'";
		if($_SESSION['s_search']['station']){
			if($_SESSION['s_search']['station']){
				$where_sql .= $shop_sql;
			}
		}
		if($_SESSION['s_search']['shop']){
			if(!$where_sql){
				$where_sql = " where driving_shop_id = '".$this->DB3->getQStr($_SESSION['s_search']['shop'])."' ";
			}
			else{
				$where_sql .= " and driving_shop_id = '".$this->DB3->getQStr($_SESSION['s_search']['shop'])."' ";
			}
		}
		if($_SESSION['s_search']['staff']){
			if(!$where_sql){
				$where_sql = " where staff_id = '".$this->DB3->getQStr($_SESSION['s_search']['staff'])."' ";
			}
			else{
				$where_sql .= " and staff_id = '".$this->DB3->getQStr($_SESSION['s_search']['staff'])."' ";
			}
		}
		if($_SESSION['s_search']['access_kb']){
			if(!$where_sql){
				$where_sql .= " where access_kb = '".$this->DB3->getQStr($_SESSION['s_search']['access_kb'])."' ";
			}
			else{
				$where_sql .= " and access_kb = '".$this->DB3->getQStr($_SESSION['s_search']['access_kb'])."' ";
			}
		}
		if($_SESSION['s_search']['staff_id']){
			if(!$where_sql){
				$where_sql .= " where ";
			}
			else{
				$where_sql .= " and ";
			}
			$value2 = mb_convert_kana($_SESSION['s_search']['staff_id'], "c");
			$value3 = mb_convert_kana($_SESSION['s_search']['staff_id'], "C");
			$where_sql .= " (staff_id LIKE '%".$this->DB3->getQStr($_SESSION['s_search']['staff_id'])."%' ";
			$where_sql .= " or staff_id LIKE '%".$this->DB3->getQStr($value2)."%' ";
			$where_sql .= " or staff_id LIKE '%".$this->DB3->getQStr($value3)."%') ";
		}
        if(!$where_sql){
            $where_sql .= " where ";
        }
        else{
            $where_sql .= " and ";
        }
        $where_sql .= " shop_id <> staff_id";
        $where_sql .= " and staff_id <> '999'";
		$sql = $count_sql . $where_sql;
		$rs =& $this->DB3->ASExecute($sql);
		$count = 0;
		if($rs){
			if(!$rs->EOF){
				$count = $rs->fields('staff_cnt');
			}
			$rs->Close();
		}
		$limit_sql = "";
		if($count){
			//ページング処理
			$params = array(
			    'perPage' => LIST_MAX,
                'urlVar' => PAGE_ID_NAME,
                'totalItems' => $count,
                'path' => './',
                'fileName' => "staff_mng.php?".PAGE_ID_NAME."=%d",
                'curPageLinkClassName' => 'nowPage',
                'append' => false
            );
			$pager = new Pager_Sliding($params);
			$links = $pager->getLinks();
			$page = $pager->getCurrentPageID();
			$pagenum = $pager->numPages();
			$offset = ($page - 1) * LIST_MAX;
			$from = $offset + 1;
			if($offset + LIST_MAX > $count){
				$to = $count;
			}
			else{
				$to = $offset + LIST_MAX;
			}
			$page_array = array(
			    "count"=>$count,
               "from"=>$from,
               "to"=>$to,
               "page"=>$page,
               "pagenum"=>$pagenum
            );
			$link_text = str_replace("href=\".//","href=\"javascript:void(0)\" onclick=\"location.href='./",$links['all']);
			$link_text = str_replace("\" title=","'\" title=",$link_text);
			$this->templ->smarty->assign('link_text', $link_text );
			$this->templ->smarty->assign('page_array', $page_array);
			//LIMIT文生成
			$limit_sql = " LIMIT ".LIST_MAX." OFFSET ".$offset;
		}
		$sql = $list_sql . $where_sql;
		$sql .= " order by shop_id,driving_station_code,staff_id";
		$sql .= $limit_sql;
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$data = array();
				$data['at'] = $rs->fields('autono');
				//事業部名取得
				if($rs->fields('driving_shop_id')){
					$sql2 = "select station_name from shop";
					$sql2 .= " where shop_id = '".$this->DB->getQStr($rs->fields('driving_shop_id'))."'";
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$data['station_name'] = $rs2->fields('station_name');
						}
						$rs2->Close();
					}
				}
				else if($rs->fields('driving_station_code') != "000"){
					$sql2 = "select station_name from shop";
					$sql2 .= " where station_code = '".$this->DB->getQStr($rs->fields('driving_station_code'))."'";
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$data['station_name'] = $rs2->fields('station_name');
						}
						$rs2->Close();
					}
				}
				if(!$data['station_name']){
					$data['station_name'] = "本部";
				}
                if($rs->fields('shop_id') != "000") {
                    $data['shop_name'] = $rs->fields('shop_name');
                }
				$data['staff_id'] = $rs->fields('staff_id');
				$data['staff_name'] = $rs->fields('staff_name');
				$data['access_kb_val']  = $this->util->access_kb_list(1,$rs->fields('access_kb'));
				$data['access_kb']  = $rs->fields('access_kb');
                $data['web_user_flg'] = $rs->fields('web_user_flg');
                $data['mainte_user_flg'] = $rs->fields('mainte_user_flg');
                $data['driving_user_flg'] = $rs->fields('driving_user_flg');
                $data['es_kind_val'] = $this->util->es_kind_list(1,$rs->fields('es_kind'));
				$data_list[] = $data;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign('data_list',$data_list);
	}
	
	function detail_data_get(){
		$sql = "select * from send_mail";
		if($this->req->get_get('at')){
			$sql .= " where autono = ".$this->DB3->getQStr($this->req->get_get('at'));
			$this->templ->smarty->assign('at',$this->req->get_get('at'));
		}
		else{
			$sql .= " where autono = ".$this->DB3->getQStr($this->req->get('at'));
			$this->templ->smarty->assign('at',$this->req->get('at'));
		}
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$this->templ->smarty->assign('staff_autono',$rs->fields('autono'));
				$this->templ->smarty->assign('station',$rs->fields('driving_station_code'));
				$_SESSION['station'] = $rs->fields('driving_station_code');
				if($this->req->get_get('at')){
					$shop_list = $this->shop_list(1);
					$this->templ->smarty->assign('shop_list',$shop_list);
					$this->templ->smarty->assign('shop_id',$rs->fields('shop_id'));
					//店舗名
					$this->templ->smarty->assign('shop',$rs->fields('shop_id'));
				}
				$this->templ->smarty->assign('staff_id',$rs->fields('staff_id'));
				$this->templ->smarty->assign('name',$rs->fields('staff_name'));
				$this->templ->smarty->assign('access_kb',$rs->fields('access_kb'));
				$this->templ->smarty->assign('mail',$rs->fields('mail'));
                $this->templ->smarty->assign('driving_mail',$rs->fields('driving_mail'));
                $this->templ->smarty->assign('es_kind',$rs->fields('es_kind'));
                $this->templ->smarty->assign('web_user_flg',$rs->fields('web_user_flg'));
                $this->templ->smarty->assign('mainte_user_flg',$rs->fields('mainte_user_flg'));
                $this->templ->smarty->assign('driving_user_flg',$rs->fields('driving_user_flg'));
			}
			$rs->Close();
		}
	}

	function data_check(){
		if($this->req->get('station')){
			if(!$this->req->get('shop')){
				//$this->req->setError('error7', '店舗を選択してください。');
			}
			else{
				$find_flg = false;
				$sql = "select * from shop";
				$sql .= " where station_code = '".$this->DB->getQStr($this->req->get('station'))."'";
				$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop'))."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if (!$rs->EOF) {
						$find_flg = true;
					}
					$rs->Close();
				}
				if(!$find_flg){
					$this->req->setError('error7', '正しい店舗を選択してください。');
				}
			}
			$find_flg = false;
			$sql = "select * from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($this->req->get('station'))."'";
			$sql .= " order by autono desc limit 1";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if (!$rs->EOF) {
					$find_flg = true;
				}
				$rs->Close();
			}
			if(!$find_flg){
				$this->req->setError('error8', '正しい事業部を選択してください。');
			}
		}
		if(!$this->req->get('at')){
			if(!$this->req->get('staff_id')){
				$this->req->setError('error1', 'スタッフIDを入力してください。');
			}
			else{
				$find_flg = false;
				$sql = "select * from send_mail ";
				$sql .= "where staff_id = '".$this->DB3->getQStr($this->req->get('staff_id'))."' ";
				$sql .= "and disp_flg = '1' and del_flg = '0'";
				$rs =& $this->DB3->ASExecute($sql);
				if($rs){
					if (!$rs->EOF) {
						$find_flg = true;
					}
					$rs->Close();
				}
				if($find_flg){
					$this->req->setError('error1', 'スタッフIDが重複しています。');
				}
			}
		}
		if(!$this->req->get('name')){
			$this->req->setError('error2', 'スタッフ名を入力してください。');
		}
		if(!$this->req->get('mail')){
			//$this->req->setError('error8', 'メールアドレスを入力してください。');
		}
		else{
			$ret = $this->util->mail_check("WEB会員・メンテ予約メールアドレス",$this->req->get('mail'));
			if($ret){
				$this->req->setError('error9', $ret);
			}
		}
        if(!$this->req->get('driving_mail')){
            //$this->req->setError('error8', 'メールアドレスを入力してください。');
        }
        else{
            $ret = $this->util->mail_check("試乗予約メールアドレス",$this->req->get('driving_mail'));
            if($ret){
                $this->req->setError('error10', $ret);
            }
        }
        if(!$this->req->get('web_user_flg') and !$this->req->get('mainte_user_flg') and !$this->req->get('driving_user_flg')){
            $this->req->setError('error11', 'ログイン権限を選択してください。');
        }
        if($this->req->get('web_user_flg') or $this->req->get('mainte_user_flg')) {
            if (!$this->req->get('es_kind')) {
                $this->req->setError('error5', 'WEB会員・メンテ予約権限を選択してください。');
            } else if (!$this->util->es_kind_list(1, $this->req->get('es_kind'))) {
                $this->req->setError('error5', 'WEB会員・メンテ予約権限を正しく選択してください。');
            } else if ($this->req->get('shop')) {
                if ($this->req->get('es_kind') == "S") {
                    $this->req->setError('error5', '事業部・店舗選択時は「本部権限」の選択はできません。');
                }
            } else if ($this->req->get('station') and !$this->req->get('shop')) {
                if ($this->req->get('es_kind') == "E") {
                    $this->req->setError('error5', '店舗を選択してください。');
                }
            } else if (!$this->req->get('station') and !$this->req->get('shop')) {
                if ($this->req->get('access_kb') == "E") {
                    $this->req->setError('error5', '事業部・店舗未選択時は「店舗権限」の選択はできません。');
                }
            }
        }
		if($this->req->get('driving_user_flg')) {
            if (!$this->req->get('access_kb')) {
                $this->req->setError('error6', '試乗予約権限を選択してください。');
            } else if (!$this->util->access_kb_list(1, $this->req->get('access_kb'))) {
                $this->req->setError('error6', '試乗予約権限を正しく選択してください。');
            } else if ($this->req->get('shop')) {
                if ($this->req->get('access_kb') != "1" and $this->req->get('access_kb') != "2") {
                    $this->req->setError('error6', '事業部・店舗選択時は「本部権限」「事業部権限」の選択はできません。');
                }
            } else if ($this->req->get('station') and !$this->req->get('shop')) {
                if ($this->req->get('access_kb') != "3") {
                    $this->req->setError('error6', '事業部選択時は「本部権限」「スタッフ権限」の選択はできません。');
                }
            } else if (!$this->req->get('station') and !$this->req->get('shop')) {
                if ($this->req->get('access_kb') != "4") {
                    $this->req->setError('error6', '事業部・店舗未選択時は「事業部権限」「スタッフ権限」の選択はできません。');
                }
            }
        }
	}

	function db_proc(){
		$record = null;
		$find_flg = false;
		$upd_flg = false;
        $sql = "select * from send_mail ";
        $sql .= "where staff_id = '".$this->DB3->getQStr($this->req->get('staff_id'))."' ";
        $rs =& $this->DB3->ASExecute($sql);
        if($rs){
            if(!$rs->EOF){
                $upd_flg = true;
            }
            $rs->Close();
        }
		if($this->req->get('station') and $this->req->get('shop')){
            $sql = "select * from shop";
            $sql .= " where station_code = '".$this->DB->getQStr($this->req->get('station'))."'";
            $sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop'))."'";
            $rs =& $this->DB->ASExecute($sql);
            if($rs){
                if(!$rs->EOF){
                    $find_flg = true;
                    $record['driving_shop_id'] = $rs->fields('shop_id');
                    $record['driving_station_code'] = $rs->fields('station_code');
                }
                $rs->Close();
            }
			$sql = "select * from shop";
			$sql .= " where driving_shop_id = '".$this->DB2->getQStr($this->req->get('shop'))."'";
			$sql .= " and del_flg = '0' and disp_flg = '1'";
			$rs =& $this->DB2->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$find_flg = true;
					$record['shop_name'] = $rs->fields('shop_name');
                    $record['mainte_shop_id'] = $rs->fields('mainte_shop_id');
                    $record['web_shop_id'] = $rs->fields('web_shop_id');
                    $record['shop_id'] = $rs->fields('shop_id');
				}
				$rs->Close();
			}
		}
		if(!$find_flg){
			if($this->req->get('station')){
				$find_flg = false;
				$sql = "select * from shop";
				$sql .= " where station_code = '".$this->DB->getQStr($this->req->get('station'))."'";
				$sql .= " order by autono desc limit 1";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$find_flg = true;
						$record['shop_name'] = "";
						$record['shop_id'] = "";
						$record['driving_station_code'] = $rs->fields('station_code');
					}
					$rs->Close();
				}
			}
			if(!$find_flg){
				$record['shop_name'] = "本社";
				$record['driving_shop_id'] = "000";
			}
		}
		$record['staff_id'] = $this->req->get('staff_id');
		$record['staff_name'] = $this->req->get('name');
		$record['access_kb'] = $this->req->get('access_kb');
		$record['driving_mail'] = $this->req->get('mail');
        $record['web_user_flg'] = "0";
        $record['mainte_user_flg'] = "0";
        $record['driving_user_flg'] = "0";
        $record['disp_flg'] = "1";
        $record['del_flg'] = "0";
        if($this->req->get('web_user_flg')) {
            $record['web_user_flg'] = "1";
        }
        if($this->req->get('mainte_user_flg')) {
            $record['mainte_user_flg'] = "1";
        }
        if($this->req->get('driving_user_flg')) {
            $record['driving_user_flg'] = "1";
        }
        $record['es_kind'] = $this->req->get('es_kind');
		$record['update_date'] = date("Y-m-d H:i:s");
		if($this->req->get('at')){
			$where = "autono = ".$this->DB3->getQStr($this->req->get('at'));
			$ret = $this->DB3->con->AutoExecute("send_mail", $record, 'UPDATE', $where);
		}
		else if($upd_flg){
            $where = "staff_id = '".$this->DB3->getQStr($this->req->get('staff_id'))."'";
            $ret = $this->DB3->con->AutoExecute("send_mail", $record, 'UPDATE', $where);
		}
		else{
            $record['password'] = DEFAULT_PW;
            $record['create_date'] = date("Y-m-d H:i:s");
            $ret = $this->DB3->con->AutoExecute("send_mail", $record, 'INSERT');
        }
	}
	
	function del_proc(){
		if($this->req->get_get('at')){
			$sql = "delete from send_mail";
			$sql .= " where autono = ".$this->DB3->getQStr($this->req->get_get('at'));
			$ret =& $this->DB3->ASExecute($sql);
		}
		header("Location: ./staff_mng.php");
	}

	function end_proc(){
		$this->templ->smarty->display(TEMPLATE3);
		exit;
	}

	function station_list($flg=0){
		$station_list = array();
		if($flg){
			$station_list[""] = "本部";
		}
		else{
			$station_list[""] = "全事業部";
		}
		$sql = "select distinct(station_code),station_name from shop";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " order by station_code";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$station_list[$rs->fields('station_code')] = $rs->fields('station_name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $station_list;
	}

	function shop_list($flg=0){
		$shop_list = array();
		if($flg){
			$shop_list[""] = "";
		}
		else{
			$shop_list[""] = "全店舗";
		}
		if($_SESSION['station']){
			$sql = "select shop_id,name from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['station'])."'";
			$sql .= " and disp_flg = '1'";
			$sql .= " and del_flg = '0'";
			$sql .= " order by order_no";
		}
		else{
			$sql = "select shop_id,name from shop";
			$sql .= " where disp_flg = '1'";
			$sql .= " and del_flg = '0'";
			$sql .= " order by station_code,order_no";
		}
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$shop_list[$rs->fields('shop_id')] = $rs->fields('name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $shop_list;
	}

	function staff_list(){
		$staff_list = array();
		$staff_list[""] = "";
		if($_SESSION['station']){
			if(!$_SESSION['shop']){
                $sql = "select staff_id,staff_name from send_mail";
                $sql .= " where driving_station_code = '".$this->DB3->getQStr($_SESSION['station'])."'";
                $sql .= " and disp_flg = '1'";
                $sql .= " and del_flg = '0'";
                $sql .= " order by driving_shop_id,staff_id";
			}
			else{
				$sql = "select staff_id,staff_name from send_mail";
				$sql .= " where driving_shop_id = '".$this->DB3->getQStr($_SESSION['shop'])."'";
				$sql .= " and disp_flg = '1'";
				$sql .= " and del_flg = '0'";
				$sql .= " order by staff_id";
			}
		}
		else{
			$sql = "select staff_id,staff_name from send_mail order by shop_id,staff_id";
		}
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$staff_list[$rs->fields('staff_id')] = $rs->fields('staff_name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $staff_list;
	}
}

?>