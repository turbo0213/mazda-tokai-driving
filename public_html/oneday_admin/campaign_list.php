<?php
ini_set("memory_limit", "512M");
ini_set("max_execution_time", "600");

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

//if($_SERVER['REMOTE_ADDR'] != "219.119.179.4"){
//exit;
//}
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{

	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $DB2;
	var $util;

	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

    /**
     * 20190610 予約日時に年追加出力
     * 20190612 日付、曜日、予約日・曜日・時間を分けて出力
     **/
	function execute(){
		switch($this->mode){
			case 'list':
				$this->list_proc();
			break;
			case 'result':
				$this->result_proc();
			break;
			case 'download':
				$this->download_proc();
			break;
			case 'station_dl':
				$this->station_dl_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}
	
	function form_make(){
		if($_SESSION['oneday']['access_kb'] == 1){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 2){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 3){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
		}
		else if($_SESSION['oneday']['access_kb'] == '4'){
			$station_list = $this->util->station_list_get("0","0",$this->DB);
            $_SESSION['search']['station'] = $this->req->get('station');
			$this->templ->smarty->assign('station',$_SESSION['search']['station']);
			$this->templ->smarty->assign('station_list',$station_list);
		}
		if($_SESSION['oneday']['access_kb'] != 1 and $_SESSION['oneday']['access_kb'] != 2){
			$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
            if(!$_SESSION['search']['station'] and $this->req->get('type') != 'station_change'){
                $_SESSION['search']['shop'] = $this->req->get('shop');
                if($_SESSION['search']['shop']){
                    $sql = "select * from shop";
                    $sql .= " where shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
                    $rs =& $this->DB->ASExecute($sql);
                    if($rs){
                        if(!$rs->EOF){
                            $_SESSION['search']['station'] = $rs->fields('station_code');
                            $this->templ->smarty->assign('station',$_SESSION['search']['station']);
                            $shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
                        }
                        $rs->Close();
                    }
                }
            }
			$this->templ->smarty->assign('shop',$_SESSION['search']['shop']);
			$this->templ->smarty->assign('shop_list',$shop_list);
		}
		$car_list = array();
		// 車種
		if(!$_SESSION['search']['shop']){
			if($_SESSION['search']['station']){
				$shop_sql = "";
				$sql = "select shop_id from shop";
				$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
				$sql .= " and del_flg = '0'";
				$sql .= " and disp_flg = '1'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					while(!$rs->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " where shop_id in ('".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						$rs->MoveNext();
					}
					$rs->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
				}
			}
		}
		$sql = "select car_id from car_detail ";
		if($_SESSION['search']['shop']){
			$sql .= " where shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		else if($_SESSION['search']['station']){
			$sql .= $shop_sql;
		}
		$sql .= " group by car_id";
		$sql .= " order by car_id";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql = "select * from car";
				$sql .= " where car_id='".$this->DB->getQStr($rs->fields('car_id'))."'";
				$sql .= " and disp_flg = '1'";
				$rs2 =& $this->DB->ASExecute($sql);
				if($rs2){
					if(!$rs2->EOF){
						$engine = "";
						$mt = "";
						// add 20190130 turbo対応
                        $turbo = "";
						if($rs2->fields('car2') == "ge"){
							$engine = "ガソリン";
						}
						else if($rs2->fields('car2') == "hev"){
							$engine = "ハイブリッド";
						}
                        // add 20200109 SKYACTIV-X対応
                        else if($rs2->fields('car2') == 'skyx'){
                            $engine = "SKYACTIV-X";
                        }
                        // add 20201009 e-SKYACTIV G対応
                        else if($rs2->fields('car2') == 'eskyg'){
                            $engine = "e-SKYACTIV G";
                        }
						else{
							$engine = "ディーゼル";
						}
						if($rs2->fields('car4') == "mt"){
							$mt = "MT";
						}
						// add 20190130 turbo対応
                        if($rs2->fields('car5') == "turbo"){
                            $turbo = " ターボ";
                        }
						// upd 20190130 turbo対応
                        if($mt){
							$car_list[$rs2->fields('car_id').",".$rs2->fields('car1').",".$rs2->fields('car2')] = $rs2->fields('name')." ".$engine." ".$mt." ".$rs2->fields('car3').$turbo;
						}
						else{
							$car_list[$rs2->fields('car_id').",".$rs2->fields('car1').",".$rs2->fields('car2')] = $rs2->fields('name')." ".$engine." ".$rs2->fields('car3').$turbo;
						}
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("car", $_SESSION['search']['car']);
		$this->templ->smarty->assign("car_list", $car_list);
		$this->templ->smarty->assign("year_list",$this->util->year_list());
		$this->templ->smarty->assign("month_list",$this->util->month_list());
		$this->templ->smarty->assign("day_list",$this->util->day_list());
		$this->templ->smarty->assign("year_to_list",$this->util->year_list());
		$this->templ->smarty->assign("month_to_list",$this->util->month_list());
		$this->templ->smarty->assign("day_to_list",$this->util->day_list());
	}

	//一覧
	function default_proc(){
		$_SESSION['search']['car'] = "";
		$_SESSION['search']['year'] = "";
		$_SESSION['search']['month'] = "";
		$_SESSION['search']['day'] = "";
		$_SESSION['search']['year_to'] = "";
		$_SESSION['search']['month_to'] = "";
		$_SESSION['search']['day_to'] = "";
		$_SESSION['search']['status_flg'] = "";
		$_SESSION['search']['station'] = "";
		$_SESSION['search']['shop'] = "";
		$_SESSION['search']['sort'] = "";
		// add 20191019 日付選択フラグ追加
        $_SESSION['search']['date_flg'] = "";
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = "";
		}
		if($this->req->get('type') == 'shop_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
		}
		if($this->req->get('type') == 'search'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
			$_SESSION['search']['car'] = $this->req->get('car');
			$_SESSION['search']['day'] = $this->req->get('day');
			$_SESSION['search']['day_to'] = $this->req->get('day_to');
			$_SESSION['search']['status_flg'] = $this->req->get('status_flg');
			$_SESSION['search']['sort'] = $this->req->get('sort');
			// add 20191019 日付フラグ追加
            $_SESSION['search']['date_flg'] = $this->req->get('date_flg');
		}
		if(!$this->req->get('status_flg')){
			$_SESSION['search']['status_flg'] = "";
		}
		// add 20191019 日付フラグ追加
        if(!$this->req->get('date_flg')){
            $_SESSION['search']['date_flg'] = "1";
        }
		if(!$_SESSION['search']['sort']){
			$_SESSION['search']['sort'] = "reserve";
		}
		$this->templ->smarty->assign("sort", $_SESSION['search']['sort']);
		$this->templ->smarty->assign("status_flg", $_SESSION['search']['status_flg']);
		// add 20191019 日付フラグ追加
        $this->templ->smarty->assign("date_flg", $_SESSION['search']['date_flg']);
		$this->date_assign();
		$this->form_make();
		$this->search_proc();
		$this->templ->smarty->display("oneday_admin/campaign_list.html");
		exit;
	}

	function list_proc(){
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = "";
		}
		if($this->req->get('type') == 'shop_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
		}
		if($this->req->get('type') == 'search'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
			$_SESSION['search']['car'] = $this->req->get('car');
			$_SESSION['search']['day'] = $this->req->get('day');
			$_SESSION['search']['day_to'] = $this->req->get('day_to');
			$_SESSION['search']['status_flg'] = $this->req->get('status_flg');
			if($this->req->get('sort')){
				$_SESSION['search']['sort'] = $this->req->get('sort');
			}
		}
		if(!$_SESSION['search']['sort']){
			$_SESSION['search']['sort'] = "reserve";
		}
		$this->templ->smarty->assign("sort", $_SESSION['search']['sort']);
		$_SESSION['search']['status_flg'] = $this->req->get('status_flg');
		$this->templ->smarty->assign("status_flg", $_SESSION['search']['status_flg']);
		// add 20191019 日付フラグ追加
        $_SESSION['search']['date_flg'] = $this->req->get('date_flg');
        $this->templ->smarty->assign("date_flg", $_SESSION['search']['date_flg']);
		$this->date_assign();
		$this->form_make();
		$this->search_proc();
		$this->templ->smarty->display("oneday_admin/campaign_list.html");
		exit;
	}

	function search_proc(){
		$week = array("日", "月", "火", "水", "木", "金", "土");
        $weekCsv = array("日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日");
		// 入力日付チェック
		$this->search_date_check();
		$from_date = "";
		$to_date = mktime(0,0,0,$_SESSION['search']['month_to'],$_SESSION['search']['day_to'],$_SESSION['search']['year_to']);
		$date_list = array();
		for($i=0;$from_date < $to_date;$i++){
			$from_date = mktime(0,0,0,$_SESSION['search']['month'],$_SESSION['search']['day']+$i,$_SESSION['search']['year']);
			$date_list[] = date("Y-m-d",$from_date);
		}
        $data_list = array();
        $csv_data = array();
		$where_sql = "";
		$conf_sql = "";
		$order_sql = "";
		$list_sql = "select * from reservation ";
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$where_sql = " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		else if($_SESSION['oneday']['access_kb'] == "3"){
			$shop_search_flg = true;
			if($this->req->get('type') == 'shop_change'){
				if($_SESSION['search']['shop']){
					$where_sql = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
					$shop_search_flg = false;
				}
			}
			if($shop_search_flg){
				$shop_sql = "";
				$sql = "select * from shop";
				$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
				$sql .= " and del_flg = '0'";
				$sql .= " and disp_flg = '1'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					while(!$rs->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						$rs->MoveNext();
					}
					$rs->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$where_sql = $shop_sql;
				}
			}
		}
		else if($this->req->get('type') == 'shop_change'){
			if($_SESSION['search']['shop']){
				$where_sql = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
			}
		}
		else if($this->req->get('type') == 'station_change'){
			if($_SESSION['search']['station']){
				$shop_sql = "";
				$sql = "select * from shop";
				$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
				$sql .= " and del_flg = '0'";
				$sql .= " and disp_flg = '1'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					while(!$rs->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs->fields('shop_id'))."'";
						}
						$rs->MoveNext();
					}
					$rs->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$where_sql = $shop_sql;
				}
			}
		}
		else if($_SESSION['search']['shop']){
			$where_sql = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		else if($_SESSION['search']['station']){
			$shop_sql = "";
			$sql = "select * from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
			$sql .= " and del_flg = '0'";
			$sql .= " and disp_flg = '1'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					if($shop_sql){
						$shop_sql .= ",'".$this->DB->getQStr($rs->fields('shop_id'))."'";
					}
					else{
						$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs->fields('shop_id'))."'";
					}
					$rs->MoveNext();
				}
				$rs->Close();
			}
			if($shop_sql){
				$shop_sql .= ")";
				$where_sql = $shop_sql;
			}
		}
		$where_sql .= " and disp_flg='1'";
		$where_sql .= " and del_flg='0'";
		foreach($date_list as $key => $date){
			for($i=0;$i <= 3;$i++){
				//予約情報取得
				$date_sql = "";
				// 確定データ
				if($i == 0){
					if($_SESSION['search']['status_flg'] == "1" or $_SESSION['search']['status_flg'] == "3" or $_SESSION['search']['status_flg'] == "4" or $_SESSION['search']['status_flg'] == "5" or !$_SESSION['search']['status_flg']){
						$conf_sql = " and conf_flg = '1'";
						if($_SESSION['search']['status_flg'] == "3"){
							$conf_sql .= " and visit_flg not in ('1','2') ";
						}
						if($_SESSION['search']['status_flg'] == "4"){
							$conf_sql .= " and visit_flg in ('1','2') ";
						}
                        if($_SESSION['search']['status_flg'] == "5"){
                            $conf_sql .= " and visit_flg = '1'";
                            $conf_sql .= " and result_close = '1'";
                        }
                        // upd 20191019 日付フラグ追加
                        if($_SESSION['search']['date_flg'] == "1") {
                            $date_sql = " where conf_date = '" . $this->DB->getQStr($date) . "'";
                            $order_sql = " order by conf_date,conf_ampm";
                        }
                        else{
                            $date_sql = " where create_date between '" . $this->DB->getQStr($date." 00:00:00") . "' and '" . $this->DB->getQStr($date." 23:59:59") . "'";
                            $date_sql .= " and conf_date is not null";
                            $order_sql = " order by create_date";
                        }
						$car_detail_sql = "";
						if($_SESSION['search']['car']){
							$car_detail_list = $this->car_detail_get();
							if($car_detail_list){
								if(is_array($car_detail_list)){
									foreach($car_detail_list as $key => $val){
										if($car_detail_sql){
											$car_detail_sql .= ",".$this->DB->getQStr($val);
										}
										else{
											$car_detail_sql = " and car_detail_id in (".$this->DB->getQStr($val);
										}
									}
									if($car_detail_sql){
										$car_detail_sql .= ")";
									}
								}
							}
						}
					}
					else if($_SESSION['search']['status_flg'] == "2"){
						continue;
					}
				}
				// 未確定データ(第1希望)
				else if($i == 1){
					if($_SESSION['search']['status_flg'] == "2" or !$_SESSION['search']['status_flg']){
						$conf_sql = " and conf_date is NULL";
						$conf_sql .= " and (conf_flg = '0' or conf_flg ='' or conf_flg is NULL)";
						// upd 20191019 日付フラグ追加
						//$date_sql = " where date = '".$this->DB->getQStr($date)."'";
                        if($_SESSION['search']['date_flg'] == "1") {
                            $date_sql = " where date = '" . $this->DB->getQStr($date) . "'";
                            $order_sql = " order by date,ampm";
                        }
                        else{
                            $date_sql = " where create_date between '" . $this->DB->getQStr($date." 00:00:00") . "' and '" . $this->DB->getQStr($date." 23:59:59") . "'";
                            $date_sql .= " and date is not null";
                            $order_sql = " order by create_date";
                        }
						$car_detail_sql = "";
						if($_SESSION['search']['car']){
							$car = explode(",",$_SESSION['search']['car']);
							if($car){
								if(is_array($car)){
									$car_detail_sql = " and car1 = '".$this->DB->getQStr($car[1])."'";
									$car_detail_sql .= " and car2 = '".$this->DB->getQStr($car[2])."'";
								}
							}
						}
					}
					else if($_SESSION['search']['status_flg'] == "1" or $_SESSION['search']['status_flg'] == "3" or $_SESSION['search']['status_flg'] == "4" or $_SESSION['search']['status_flg'] == "5"){
						continue;
					}
				}
				// 未確定データ(第2希望)
				else if($i == 2){
					if($_SESSION['search']['status_flg'] == "2" or !$_SESSION['search']['status_flg']){
						$conf_sql = " and conf_date is NULL";
						$conf_sql .= " and (conf_flg = '0' or conf_flg ='' or conf_flg is NULL)";
                        // upd 20191019 日付フラグ追加
						//$date_sql = " where date2 = '".$this->DB->getQStr($date)."'";
                        if($_SESSION['search']['date_flg'] == "1") {
                            $date_sql = " where date2 = '" . $this->DB->getQStr($date) . "'";
                            $order_sql = " order by date2,ampm2";
                        }
                        else{
                            $date_sql = " where create_date between '" . $this->DB->getQStr($date." 00:00:00") . "' and '" . $this->DB->getQStr($date." 23:59:59") . "'";
                            $date_sql .= " and date2 is not null";
                            $order_sql = " order by create_date";
                        }
						$car_detail_sql = "";
						if($_SESSION['search']['car']){
							$car = explode(",",$_SESSION['search']['car']);
							if($car){
								if(is_array($car)){
									$car_detail_sql = " and car1 = '".$this->DB->getQStr($car[1])."'";
									$car_detail_sql .= " and car2 = '".$this->DB->getQStr($car[2])."'";
								}
							}
						}
					}
					else if($_SESSION['search']['status_flg'] == "1" or $_SESSION['search']['status_flg'] == "3" or $_SESSION['search']['status_flg'] == "4" or $_SESSION['search']['status_flg'] == "5"){
						continue;
					}
				}
				// 未確定データ(第3希望)
				else{
					if($_SESSION['search']['status_flg'] == "2" or !$_SESSION['search']['status_flg']){
						$conf_sql = " and conf_date is NULL";
						$conf_sql .= " and (conf_flg = '0' or conf_flg ='' or conf_flg is NULL)";
                        // upd 20191019 日付フラグ追加
						//$date_sql = " where date3 = '".$this->DB->getQStr($date)."'";
                        if($_SESSION['search']['date_flg'] == "1") {
                            $date_sql = " where date3 = '" . $this->DB->getQStr($date) . "'";
                            $order_sql = " order by date3,ampm3";
                        }
                        else{
                            $date_sql = " where create_date between '" . $this->DB->getQStr($date." 00:00:00") . "' and '" . $this->DB->getQStr($date." 23:59:59") . "'";
                            $date_sql .= " and date3 is not null";
                            $order_sql = " order by create_date";
                        }
						$car_detail_sql = "";
						if($_SESSION['search']['car']){
							$car = explode(",",$_SESSION['search']['car']);
							if($car){
								if(is_array($car)){
									$car_detail_sql = " and car1 = '".$this->DB->getQStr($car[1])."'";
									$car_detail_sql .= " and car2 = '".$this->DB->getQStr($car[2])."'";
								}
							}
						}
					}
					else if($_SESSION['search']['status_flg'] == "1" or $_SESSION['search']['status_flg'] == "3" or $_SESSION['search']['status_flg'] == "4" or $_SESSION['search']['status_flg'] == "5"){
						continue;
					}
				}
				$sql = $list_sql.$date_sql.$where_sql.$conf_sql.$car_detail_sql.$order_sql;
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					while(!$rs->EOF){
						$data = array();
						$data_flg = false;
						// 確定データ
						if($i == 0){
							if($_SESSION['search']['conf_flg'] == "1" or !$_SESSION['search']['conf_flg']){
								$time = strtotime($rs->fields('conf_date'));
								$data['conf_date'] = $rs->fields('conf_date');
								if($rs->fields('conf_ampm') == '1'){
									$data['ampm_value'] = "午前";
								}
								else{
									$data['ampm_value'] = "午後";
								}
								$data['sel_date'] = $rs->fields('conf_date').",".$rs->fields('conf_ampm');
								$data_flg = true;
							}
						}
						// 未確定データ(第1希望)
						else if($i == 1){
							if($_SESSION['search']['conf_flg'] == "2" or !$_SESSION['search']['conf_flg']){
								$time = strtotime($rs->fields('date'));
								$data['conf_date'] = $rs->fields('date');
								if($rs->fields('ampm') == '1'){
									$data['ampm_value'] = "午前";
								}
								else{
									$data['ampm_value'] = "午後";
								}
								$data['sel_date'] = $rs->fields('date').",".$rs->fields('ampm');
								if($_SESSION['search']['car']){
									$car_data_flg = $this->car_judge($rs->fields('shop_id'));
								}
								else{
									$data_flg = true;
								}
								if($car_data_flg){
									$data_flg = true;
								}
							}
						}
						// 未確定データ(第2希望)
						else if($i == 2){
							if($_SESSION['search']['conf_flg'] == "2" or !$_SESSION['search']['conf_flg']){
								$time = strtotime($rs->fields('date2'));
								$data['conf_date'] = $rs->fields('date2');
								if($rs->fields('ampm2') == '1'){
									$data['ampm_value'] = "午前";
								}
								else{
									$data['ampm_value'] = "午後";
								}
								$data['sel_date'] = $rs->fields('date2').",".$rs->fields('ampm2');
								if($_SESSION['search']['car']){
									$car_data_flg = $this->car_judge($rs->fields('shop_id'));
								}
								else{
									$data_flg = true;
								}
								if($car_data_flg){
									$data_flg = true;
								}
							}
						}
						// 未確定データ(第3希望)
						else{
							if($_SESSION['search']['conf_flg'] == "2" or !$_SESSION['search']['conf_flg']){
								$time = strtotime($rs->fields('date3'));
								$data['conf_date'] = $rs->fields('date3');
								if($rs->fields('ampm3') == '1'){
									$data['ampm_value'] = "午前";
								}
								else{
									$data['ampm_value'] = "午後";
								}
								$data['sel_date'] = $rs->fields('date3').",".$rs->fields('ampm3');
								if($_SESSION['search']['car']){
									$car_data_flg = $this->car_judge($rs->fields('shop_id'));
								}
								else{
									$data_flg = true;
								}
								if($car_data_flg){
									$data_flg = true;
								}
							}
						}
						if($data_flg){
							if($rs->fields('conf_flg') == 1) {
                                $data['conf_date2'] = $rs->fields('conf_date');
                                $csv_w = date("w", strtotime($rs->fields('conf_date')));
                                $data['conf_date2'] .= " (".$week[$csv_w].")";
                                // add 20190612 csv出力内容変更
                                $data['confDateCsv'] = date("Y年m月d日", strtotime($rs->fields('conf_date')));
                                $data['confWeekCsv'] = $weekCsv[$csv_w];
                                if($rs->fields('conf_ampm') == '1'){
                                    $data['conf_ampm'] = "午前";
                                }
                                else{
                                    $data['conf_ampm'] = "午後";
                                }
                            }
                            if($rs->fields('date')) {
                                $data['date1'] = $rs->fields('date');
                                $csv_w = date("w", strtotime($rs->fields('date')));
                                $data['date1'] .= " (".$week[$csv_w].")";
                                // add 20190612 csv出力内容変更
                                $data['date1Csv'] = date("Y年m月d日", strtotime($rs->fields('date')));
                                $data['week1Csv'] = $weekCsv[$csv_w];
                                if($rs->fields('ampm') == '1'){
                                    $data['ampm1'] = "午前";
                                }
                                else{
                                    $data['ampm1'] = "午後";
                                }
                            }
                            if($rs->fields('date2')) {
                                $data['date2'] = $rs->fields('date2');
                                $csv_w = date("w", strtotime($rs->fields('date2')));
                                $data['date2'] .= " (".$week[$csv_w].")";
                                // add 20190612 csv出力内容変更
                                $data['date2Csv'] = date("Y年m月d日", strtotime($rs->fields('date2')));
                                $data['week2Csv'] = $weekCsv[$csv_w];
                                if($rs->fields('ampm2') == '1'){
                                    $data['ampm2'] = "午前";
                                }
                                else{
                                    $data['ampm2'] = "午後";
                                }
                            }
                            if($rs->fields('date3')) {
                                $data['date3'] = $rs->fields('date3');
                                $csv_w = date("w", strtotime($rs->fields('date3')));
                                $data['date3'] .= " (".$week[$csv_w].")";
                                // add 20190612 csv出力内容変更
                                $data['date3Csv'] = date("Y年m月d日", strtotime($rs->fields('date3')));
                                $data['week3Csv'] = $weekCsv[$csv_w];
                                if($rs->fields('ampm3') == '1'){
                                    $data['ampm3'] = "午前";
                                }
                                else{
                                    $data['ampm3'] = "午後";
                                }
                            }
                            $data['conf_flg'] = $rs->fields('conf_flg');
                            $data['visit_val'] = "";
                            $data['result_close_val'] = "";
                            $data['result_status_val'] = "";
                            if($rs->fields('visit_flg')){
                                $data['visit_val'] = $this->util->visit_flg(1,$rs->fields('visit_flg'));
                                if($rs->fields('result_close')){
                                    $data['result_close_val'] = $this->util->result_close(1,$rs->fields('result_close'));
                                    if($rs->fields('result_close') == "2"){
                                        $data['result_status_val'] = $this->util->result_status(1,$rs->fields('result_status'));
                                    }
                                }
                            }
						    $data['create_date'] = $rs->fields('create_date');
							$data['regist_date'] = date("m月d日", strtotime($rs->fields('create_date')));
							$regist_w = date("w", strtotime($rs->fields('create_date')));
							$data['regist_week'] = $week[$regist_w];
							$data['regist_time'] = date("H:i", strtotime($rs->fields('create_date')));
							$w = date("w", $time);
							$data['weekday'] = $week[$w];
							$data['disp_date'] = date("m月d日", $time);
							$data['reg_csv_date'] = date("Y年m月d日", strtotime($rs->fields('create_date')));
                            // upd 20190612 csv出力内容変更
                            $data['reg_csv_week'] = $weekCsv[$regist_w];
                            $data['reg_csv_time'] = date("H:i", strtotime($rs->fields('create_date')));
                            $data['no_plate'] = $rs->fields('no_plate');
							$data['back_class'] = "";
							if($rs->fields('conf_flg') != "1"){
								$data['back_class'] = "st2";
							}
							else if(!$rs->fields('visit_flg') or !$rs->fields('result_close')){
								if($rs->fields('conf_date') >= date("Y-m-d")){
									$data['back_class'] = "st3";
								}
								else{
									$data['back_class'] = "st5";
								}
							}
							else{
								$data['back_class'] = "st6";
							}
							$data['visit_flg'] = $rs->fields('visit_flg');
							$data['result_close'] = $rs->fields('result_close');
							$data['result_status'] = $rs->fields('result_status');
							$data['disp_number'] = $rs->fields('disp_number');
                            $data['name'] = $rs->fields('sei')." ".$rs->fields('mei');
                            $data['sei'] = $rs->fields('sei');
                            $data['mei'] = $rs->fields('mei');
                            $data['sei_kana'] = $rs->fields('sei_kana');
                            $data['mei_kana'] = $rs->fields('mei_kana');
                            $data['tel'] = $rs->fields('tel');
                            $data['mail'] = $rs->fields('mail');
                            if($rs->fields('v_schedule_time')){
                                list($t1,$t2,$t3) = explode(":",$rs->fields('v_schedule_time'));
                                $data['v_schedule_time'] = date("H時i分頃",mktime($t1,$t2,$t3,date("m"),date("d"),date("Y")));
                            }
                            else{
                                $data['v_schedule_time'] = "";
                            }
							if($rs->fields('sp_flg') == "1"){
								$data['sppc_value'] = "(SP)";
							}
							else{
								$data['sppc_value'] = "(PC)";
							}
							$data['comment'] = "無";
							if($rs->fields('comment')){
								$data['comment'] = "有";
							}
                            $data['comment2'] = $rs->fields('comment');
							if($rs->fields('shop_id')){
								$sql2 = "select * from shop";
								$sql2 .= " where shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
								$rs2 =& $this->DB->ASExecute($sql2);
								if($rs2){
									if(!$rs2->EOF){
										$data['reserve_shop'] = $rs2->fields('name');
                                        $data['reserve_station'] = $rs2->fields('station_name');
									}
									$rs2->Close();
								}
							}
							if($rs->fields('car_detail_id')){
								$sql2 = "select * from car_detail";
								$sql2 .= " where autono = ".$this->DB->getQStr($rs->fields('car_detail_id'));
								$rs2 =& $this->DB->ASExecute($sql2);
								if($rs2){
									if(!$rs2->EOF){
										$data['no_plate'] = $rs2->fields('no_plate');
                                        $data['car_no'] = $rs2->fields('car_no');
                                        $price = "";
                                        if($rs2->fields('price')){
                                            $price = str_replace(",","",$rs2->fields('price'));
                                            if(is_numeric($price)){
                                                $price = number_format($price);
                                            }
                                        }
                                        $data['price'] = $price;
                                        $data['grade'] = $rs2->fields('grade');
                                        $data['model'] = $rs2->fields('model');
										$car_id = $rs2->fields('car_id');
										$car2 = $rs2->fields('car2');
										$car3 = $rs2->fields('car3');
										$car4 = $rs2->fields('car4');
										// add 20190130 turbo対応
                                        $car5 = $rs2->fields('car5');
									}
									$rs2->Close();
								}
								//車種名取得
								$sql2 = "select * from car ";
								$sql2 .= "where car_id='".$this->DB->getQStr($car_id)."'";
								$sql2 .= " and del_flg='0' and disp_flg='1'";
								$rs2 =& $this->DB->ASExecute($sql2);
								$data['shop_id'] = $rs->fields('shop_id');
								if($rs2){
									if(!$rs2->EOF){
										$data['carname'] = $rs2->fields('name');
										if($car2 == "ge"){
											$data['carname2'] = "ガソリン";
										}
										else if($car2 == "de"){
											$data['carname2'] = "ディーゼル";
										}
										else if($car2 == "hev"){
											$data['carname2'] = "ハイブリッド";
										}
                                        // add 20200109 SKYACTIV-X対応
                                        else if($car2 == 'skyx'){
                                            $data['carname2'] = "SKYACTIV-X";
                                        }
                                        // add 20201009 e-SKYACTIV G対応
                                        else if($car2 == 'eskyg'){
                                            $data['carname2'] = "e-SKYACTIV G";
                                        }
										$mt = "";
										if($car4 == "mt"){
											$mt = " MT";
										}
										// add 20190130 turbo対応
                                        $turbo = "";
                                        if($car5 == "turbo"){
                                            $turbo = " ターボ";
                                        }
                                        // upd 20190130 turbo対応
										if($car3){
											if($mt){
												$data['carname3'] = $mt." ".$car3.$turbo;
											}
											else{
												$data['carname3'] = $car3.$turbo;
											}
										}
										else{
											if($mt){
												$data['carname3'] = $mt.$turbo;
											}
										}
									}
									$rs2->Close();
								}
								$data_list[] = $data;
                                $csv_data[$data['disp_number']] = $data;
							}
							else{
								// 車種詳細ID取得
								$car_id = "";
								$sql2 = "select * from car_detail";
								$sql2 .= " where car1='".$this->DB->getQStr($rs->fields('car1'))."'";
								$sql2 .= " and car2='".$this->DB->getQStr($rs->fields('car2'))."'";
								if($rs->fields('car3')){
									$sql2 .= " and car3='".$this->DB->getQStr($rs->fields('car3'))."'";
								}
								if($rs->fields('car4')){
									$sql2 .= " and car4='".$this->DB->getQStr($rs->fields('car4'))."'";
								}
								// add 20190130 turbo対応
                                if($rs->fields('car5')){
                                    $sql2 .= " and car5='".$this->DB->getQStr($rs->fields('car5'))."'";
                                }
                                else{
                                    $sql2 .= " and (car5 is NULL or car5 = '')";
                                }
								$sql2 .= " and shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
								$sql2 .= " and del_flg = '0'";
								$sql2 .= " and start_date <= '".$this->DB->getQStr($date)."'";
								$sql2 .= " and end_date >= '".$this->DB->getQStr($date)."'";
								$rs2 =& $this->DB->ASExecute($sql2);
								if($rs2){
									while(!$rs2->EOF){
										$exception_flg = false;
									    $sql3 = "select * from exception";
										$sql3 .= " where shop_id = '".$this->DB->getQStr($rs->fields('shop_id'))."'";
										$sql3 .= " and date = '".$this->DB->getQStr($date)."'";
										$sql3 .= " and disp_flg = '1'";
										$sql3 .= " and del_flg = '0'";
										$sql3 .= " and car_detail_id = ".$this->DB->getQStr($rs2->fields('autono'));
                                        $rs3 =& $this->DB->ASExecute($sql3);
                                        if($rs3){
                                            if(!$rs3->EOF){
                                                $exception_flg = true;
                                            }
                                            $rs3->Close();
                                        }
                                        if($exception_flg){
                                            $rs2->MoveNext();
                                            continue;
                                        }
									    $data['car_detail_id'] = $rs2->fields('autono');
										$data['no_plate'] = $rs2->fields('no_plate');
                                        $data['car_no'] = $rs2->fields('car_no');
                                        $price = "";
                                        if($rs2->fields('price')){
                                            $price = str_replace(",","",$rs2->fields('price'));
                                            if(is_numeric($price)){
                                                $price = number_format($price);
                                            }
                                        }
                                        $data['price'] = $price;
                                        $data['grade'] = $rs2->fields('grade');
                                        $data['model'] = $rs2->fields('model');
										$car_id = $rs2->fields('car_id');
										$car2 = $rs2->fields('car2');
										$car3 = $rs2->fields('car3');
										$car4 = $rs2->fields('car4');
										// add 20190130 turbo対応
                                        $car5 = $rs2->fields('car5');
										//車種名取得
										$sql3 = "select * from car ";
										$sql3 .= "where car_id='".$this->DB->getQStr($car_id)."'";
										$sql3 .= " and del_flg='0' and disp_flg='1'";
										$rs3 =& $this->DB->ASExecute($sql3);
										$data['shop_id'] = $rs->fields('shop_id');
										if($rs3){
											if(!$rs3->EOF){
												$data['carname'] = $rs3->fields('name');
												if($car2 == "ge"){
													$data['carname2'] = "ガソリン";
												}
												else if($car2 == "de"){
													$data['carname2'] = "ディーゼル";
												}
												else if($car2 == "hev"){
													$data['carname2'] = "ハイブリッド";
												}
                                                // add 20200109 SKYACTIV-X対応
                                                else if($car2 == 'skyx'){
                                                    $data['carname2'] = "SKYACTIV-X";
                                                }
                                                // add 20201009 e-SKYACTIV G対応
                                                else if($car2 == 'eskyg'){
                                                    $data['carname2'] = "e-SKYACTIV G";
                                                }
												$mt = "";
												if($car4 == "mt"){
													$mt = " MT";
												}
												// add 20190130 turbo対応
                                                $turbo = "";
                                                if($car5 == "turbo"){
                                                    $turbo = " ターボ";
                                                }
                                                // upd 20190130 turbo対応
												if($car3){
													if($mt){
														$data['carname3'] = $mt." ".$car3.$turbo;
													}
													else{
														$data['carname3'] = $car3.$turbo;
													}
												}
												else{
													if($mt){
														$data['carname3'] = $mt.$turbo;
													}
												}
											}
											$rs3->Close();
										}
										$data_list[] = $data;
                                        $csv_data[$data['disp_number']] = $data;
										$rs2->MoveNext();
									}
									$rs2->Close();
								}
							}
						}
						$rs->MoveNext();
					}
					$rs->Close();
				}
			}
		}
		if($_SESSION['search']['sort'] == "regist"){
			if($data_list){
				if(is_array($data_list)){
					foreach ($data_list as $key => $value) {
						$sort[$key] = $value['create_date'];
					}
					array_multisort($sort, SORT_DESC, $data_list);
				}
			}
		}
		$this->templ->smarty->assign("data_list", $data_list);
		if($_SESSION['oneday']['access_kb'] == "3"){
			$sql = "select * from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
			$sql .= " order by shop_id limit 1";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("station_val", $rs->fields('station_name'));
				}
				$rs->Close();
			}
		}
		return $csv_data;
	}

	function result_proc(){
		$autono = 0;
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$autono = $rs->fields('autono');
			}
			$rs->Close();
		}
		if($autono){
			$record['visit_flg'] = $this->req->get('visit_flg');
			$record['result_status'] = $this->req->get('result_status');
			$record['result_close'] = $this->req->get('result_close');
			$record['update_date'] = time();
			$where = "autono = ".$this->DB->getQStr($autono);
			$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);

			$_SESSION['oneday']['no_result_flg'] = false;
			$limit_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-3,date("Y")));
			$sql = "select count(autono) as cnt from reservation ";
			$sql .= "where disp_flg = '1' ";
			$sql .= "and del_flg = '0' ";
			$sql .= "and temporary_flg = '2' ";
			$sql .= "and conf_flg = '1' ";
			$sql .= "and shop_id = '".$this->DB->getQStr( $_SESSION['oneday']['shop_id'] )."' ";
			$sql .= "and conf_date <= '".$this->DB->getQStr( $limit_date )."' ";
			$sql .= "and visit_flg not in ('1','2') ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('cnt') >= 0){
						$_SESSION['oneday']['no_result_flg'] = ture;
					}
				}
				$rs->Close();
			}
		}
		header("Location:./campaign_list.php?mode=list");
	}

	function car_detail_get(){
		if($_SESSION['search']['car']){
			$car_id = explode(",",$_SESSION['search']['car']);
		}
		$detail_list = array();
		$sql = "select autono from car_detail";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		if($_SESSION['search']['car']){
			$sql .= " and car_id = '".$this->DB->getQStr($car_id[0])."'";
		}
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		else if($_SESSION['oneday']['access_kb'] == "3"){
			$shop_search_flg = true;
			if($this->req->get('type') == 'shop_change'){
				if($_SESSION['search']['shop']){
					$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
					$shop_search_flg = false;
				}
			}
			if($shop_search_flg){
				$shop_sql = "";
				$sql2 = "select * from shop";
				$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
				$sql2 .= " and del_flg = '0'";
				$sql2 .= " and disp_flg = '1'";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$sql .= $shop_sql;
				}
			}
		}
		else if($this->req->get('type') == 'shop_change'){
			if($_SESSION['search']['shop']){
				$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
			}
		}
		else if($this->req->get('type') == 'station_change'){
			if($_SESSION['search']['station']){
				$shop_sql = "";
				$sql2 = "select * from shop";
				$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
				$sql2 .= " and del_flg = '0'";
				$sql2 .= " and disp_flg = '1'";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$sql .= $shop_sql;
				}
			}
		}
		else if($_SESSION['search']['shop']){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		else if($_SESSION['search']['station']){
			$shop_sql = "";
			$sql2 = "select * from shop";
			$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
			$sql2 .= " and del_flg = '0'";
			$sql2 .= " and disp_flg = '1'";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				while(!$rs2->EOF){
					if($shop_sql){
						$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					else{
						$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					$rs2->MoveNext();
				}
				$rs2->Close();
			}
			if($shop_sql){
				$shop_sql .= ")";
				$sql .= $shop_sql;
			}
		}
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$detail_list[] = $rs->fields('autono');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $detail_list;
	}
	
	function search_date_check(){
		if($_SESSION['search']['year'] and $_SESSION['search']['month'] and $_SESSION['search']['day']){
			$from_check = checkdate($_SESSION['search']['month'],$_SESSION['search']['day'],$_SESSION['search']['year']);
			if(!$from_check){
				$_SESSION['search']['year'] = date("Y");
				$_SESSION['search']['month'] = date("m");
				$_SESSION['search']['day'] = "";
			}
			else{
				$date_from = mktime(0,0,0,$_SESSION['search']['month'],$_SESSION['search']['day'],$_SESSION['search']['year']);
			}
		}
		else if($_SESSION['search']['year'] and $_SESSION['search']['month']){
			$from_check = checkdate($_SESSION['search']['month'],1,$_SESSION['search']['year']);
			if(!$from_check){
				$_SESSION['search']['year'] = date("Y");
				$_SESSION['search']['month'] = date("m");
				$_SESSION['search']['day'] = "";
			}
			else{
				$date_from = mktime(0,0,0,$_SESSION['search']['month'],1,$_SESSION['search']['year']);
			}
		}
		else if($_SESSION['search']['year']){
			$from_check = checkdate(1,1,$_SESSION['search']['year']);
			if(!$from_check){
				$_SESSION['search']['year'] = date("Y");
				$_SESSION['search']['month'] = date("m");
				$_SESSION['search']['day'] = "";
			}
			else{
				$date_from = mktime(0,0,0,1,1,$_SESSION['search']['year']);
			}
		}
		if($_SESSION['search']['year_to'] and $_SESSION['search']['month_to'] and $_SESSION['search']['day_to']){
			$to_check = checkdate($_SESSION['search']['month_to'],$_SESSION['search']['day_to'],$_SESSION['search']['year_to']);
			if(!$to_check){
				$_SESSION['search']['year_to'] = date("Y");
				$_SESSION['search']['month_to'] = date("m");
				$_SESSION['search']['day_to'] = "";
			}
			else{
				$date_to = mktime(0,0,0,$_SESSION['search']['month_to'],$_SESSION['search']['day_to'],$_SESSION['search']['year_to']);
			}
		}
		else if($_SESSION['search']['year_to'] and $_SESSION['search']['month_to']){
			$to_check = checkdate($_SESSION['search']['month_to'],1,$_SESSION['search']['year_to']);
			if(!$to_check){
				$_SESSION['search']['year_to'] = date("Y");
				$_SESSION['search']['month_to'] = date("m");
				$_SESSION['search']['day_to'] = "";
			}
			else{
				$date_to = mktime(0,0,0,$_SESSION['search']['month_to'],1,$_SESSION['search']['year_to']);
			}
		}
		else if($_SESSION['search']['year_to']){
			$to_check = checkdate(1,1,$_SESSION['search']['year_to']);
			if(!$to_check){
				$_SESSION['search']['year_to'] = date("Y");
				$_SESSION['search']['month_to'] = date("m");
				$_SESSION['search']['day_to'] = "";
			}
			else{
				$date_to = mktime(0,0,0,1,1,$_SESSION['search']['year_to']);
			}
		}
		if($date_from and $date_to){
			if($date_from > $date_to){
				$_SESSION['search']['year_to'] = "";
				$_SESSION['search']['month_to'] = "";
				$_SESSION['search']['day_to'] = "";
			}
		}
	}

	function date_assign(){
		if($this->req->get('year')){
			$_SESSION['search']['year'] = $this->req->get('year');
		}
		else if($this->req->get_get('year')){
			$_SESSION['search']['year'] = $this->req->get_get('year');
		}
		if($this->req->get('month')){
			$_SESSION['search']['month'] = $this->req->get('month');
		}
		else if($this->req->get_get('month')){
			$_SESSION['search']['month'] = $this->req->get_get('month');
		}
		if($this->req->get('day')){
			$_SESSION['search']['day'] = $this->req->get('day');
		}
		else if($this->req->get_get('day')){
			$_SESSION['search']['day'] = $this->req->get_get('day');
		}
		if(!$_SESSION['search']['year']){
			$_SESSION['search']['year'] = date("Y",mktime(0,0,0,date("m"),date("d")-15,date("Y")));
		}
		if(!$_SESSION['search']['month']){
			$_SESSION['search']['month'] = date("m",mktime(0,0,0,date("m"),date("d")-15,date("Y")));
		}
		if(!$_SESSION['search']['day']){
			$_SESSION['search']['day'] = date("d",mktime(0,0,0,date("m"),date("d")-15,date("Y")));
		}
		$this->templ->smarty->assign("day", $_SESSION['search']['day']);
		$this->templ->smarty->assign("year", $_SESSION['search']['year']);
		$this->templ->smarty->assign("month", $_SESSION['search']['month']);
		// TO追加
		if($this->req->get('year_to')){
			$_SESSION['search']['year_to'] = $this->req->get('year_to');
		}
		else if($this->req->get_get('year_to')){
			$_SESSION['search']['year_to'] = $this->req->get_get('year_to');
		}
		if($this->req->get('month_to')){
			$_SESSION['search']['month_to'] = $this->req->get('month_to');
		}
		else if($this->req->get_get('month_to')){
			$_SESSION['search']['month_to'] = $this->req->get_get('month_to');
		}
		if($this->req->get('day_to')){
			$_SESSION['search']['day_to'] = $this->req->get('day_to');
		}
		else if($this->req->get_get('day_to')){
			$_SESSION['search']['day_to'] = $this->req->get_get('day_to');
		}
		if(!$_SESSION['search']['year_to']){
			$_SESSION['search']['year_to'] = date("Y",mktime(0,0,0,date("m"),date("d")+15,date("Y")));
		}
		if(!$_SESSION['search']['month_to']){
			$_SESSION['search']['month_to'] = date("m",mktime(0,0,0,date("m"),date("d")+15,date("Y")));
		}
		if(!$_SESSION['search']['day_to']){
			$_SESSION['search']['day_to'] = date("d",mktime(0,0,0,date("m"),date("d")+15,date("Y")));
		}
		$this->templ->smarty->assign("day_to", $_SESSION['search']['day_to']);
		$this->templ->smarty->assign("year_to", $_SESSION['search']['year_to']);
		$this->templ->smarty->assign("month_to", $_SESSION['search']['month_to']);
        if($this->req->get('date_flg')){
            $_SESSION['search']['date_flg'] = $this->req->get('date_flg');
        }
        else if($this->req->get_get('date_flg')){
            $_SESSION['search']['date_flg'] = $this->req->get_get('date_flg');
        }
        if(!$_SESSION['search']['date_flg']){
            $_SESSION['search']['date_flg'] = "1";
        }
        $this->templ->smarty->assign("date_flg", $_SESSION['search']['date_flg']);
	}

	function download_proc(){
        $_SESSION['search']['car'] = $this->req->get('car');
        $_SESSION['search']['day'] = $this->req->get('day');
        $_SESSION['search']['day_to'] = $this->req->get('day_to');
        $_SESSION['search']['status_flg'] = $this->req->get('status_flg');
        // add 20191019 日付フラグ追加
        $_SESSION['search']['date_flg'] = $this->req->get('date_flg');
        if($this->req->get('sort')){
            $_SESSION['search']['sort'] = $this->req->get('sort');
        }
		// 日付アサイン
		$this->date_assign();
		// 入力日付チェック
		$this->search_date_check();
		$car = "";
		if($_SESSION['search']['car']){
			$car = substr($_SESSION['search']['car'],0,8)."_";
		}
		$day = "";
		if($_SESSION['search']['day']){
			$day = "_".$_SESSION['search']['day'];
		}
		$file_name = $_SESSION['search']['shop']."_".$car.$_SESSION['search']['year']."_".$_SESSION['search']['month'].$day;
		header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/x-csv");
		header("Content-Disposition: attachment; filename=".$file_name.".csv"); 
		$csv = '"事業部","店舗","予約確定日","確定曜日","確定時間","希望日1","希望曜日1","希望時間1","希望日2","希望曜日2","希望時間2","希望日3","希望曜日3","希望時間3","車種","","","姓","名","セイ","メイ","連絡先","連絡可能な時間帯","メールアドレス","意見・要望","予約番号","予約日","予約曜日","予約時間","登録番号","車体番号","グレード","型式","本体価格","来店状況","成約状況","ステータス"'."\r\n";
		echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
        $data_list = $this->search_proc();
        $comp_data = "";
        if($data_list){
            if(is_array($data_list)){
                foreach ($data_list as $key => $val){
                    $d = '"'.$val['reserve_station'].'","'.$val['reserve_shop'].'","'.$val['confDateCsv'].'","'.$val['confWeekCsv'].'","'.$val['conf_ampm'].'","'.$val['date1Csv'].'","'.$val['week1Csv'].'","'.$val['ampm1'].'","'.$val['date2Csv'].'","'.$val['week2Csv'].'","'.$val['ampm2'].'","'.
                        $val['date3Csv'].'","'.$val['week3Csv'].'","'.$val['ampm3'].'","'.$val['carname'].'","'.$val['carname2'].'","'.$val['carname3'].'","'.$val['sei'].'","'.$val['mei'].'","'.$val['sei_kana'].'","'.$val['mei_kana'].'","'.
                        $val['tel'].'","'.$val['v_schedule_time'].'","'.$val['mail'].'","'.$val['comment2'].'","'.$val['disp_number'].'","'.$val['reg_csv_date'].'","'.$val['reg_csv_week'].'","'.$val['reg_csv_time'].'","'.$val['no_plate'].'","'.$val['car_no'].'","'.
                        $val['grade'].'","'.$val['model'].'","'.$val['price'].'","'.$val['visit_val'].'","'.$val['result_close_val'].'","'.$val['result_status_val'].'"'."\r\n";
                    if($comp_data != $d){
                        echo mb_convert_encoding($d,"SJIS-win","UTF-8");
                        $comp_data = $d;
                    }
                    unset($d);
                }
            }
        }
	}
	
	function station_dl_proc(){
        $_SESSION['search']['station'] = $this->req->get('station');
        $_SESSION['search']['shop'] = $this->req->get('shop');
        $_SESSION['search']['car'] = $this->req->get('car');
        $_SESSION['search']['day'] = $this->req->get('day');
        $_SESSION['search']['day_to'] = $this->req->get('day_to');
        $_SESSION['search']['status_flg'] = $this->req->get('status_flg');
        // add 20191019 日付フラグ追加
        $_SESSION['search']['date_flg'] = $this->req->get('date_flg');
        if($this->req->get('sort')){
            $_SESSION['search']['sort'] = $this->req->get('sort');
        }
		// 日付アサイン
		$this->date_assign();
		// 入力日付チェック
		$this->search_date_check();
		$car = "";
		if($_SESSION['search']['car']){
			$car = substr($_SESSION['search']['car'],0,8)."_";
		}
		$day = "";
		if($_SESSION['search']['day']){
			$day = "_".$_SESSION['search']['day'];
		}
		$file_name = $car.$_SESSION['search']['year']."_".$_SESSION['search']['month'].$day;
		header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/x-csv");
		header("Content-Disposition: attachment; filename=".$file_name.".csv");
        $csv = '"事業部","店舗","予約確定日","確定曜日","確定時間","希望日1","希望曜日1","希望時間1","希望日2","希望曜日2","希望時間2","希望日3","希望曜日3","希望時間3","車種","","","姓","名","セイ","メイ","連絡先","連絡可能な時間帯","メールアドレス","意見・要望","予約番号","予約日","予約曜日","予約時間","登録番号","車体番号","グレード","型式","本体価格","来店状況","成約状況","ステータス"'."\r\n";
        echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
        $data_list = $this->search_proc();
        $comp_data = "";
        if($data_list){
            if(is_array($data_list)){
                foreach ($data_list as $key => $val){
                    $d = '"'.$val['reserve_station'].'","'.$val['reserve_shop'].'","'.$val['confDateCsv'].'","'.$val['confWeekCsv'].'","'.$val['conf_ampm'].'","'.$val['date1Csv'].'","'.$val['week1Csv'].'","'.$val['ampm1'].'","'.$val['date2Csv'].'","'.$val['week2Csv'].'","'.$val['ampm2'].'","'.
                        $val['date3Csv'].'","'.$val['week3Csv'].'","'.$val['ampm3'].'","'.$val['carname'].'","'.$val['carname2'].'","'.$val['carname3'].'","'.$val['sei'].'","'.$val['mei'].'","'.$val['sei_kana'].'","'.$val['mei_kana'].'","'.
                        $val['tel'].'","'.$val['v_schedule_time'].'","'.$val['mail'].'","'.$val['comment2'].'","'.$val['disp_number'].'","'.$val['reg_csv_date'].'","'.$val['reg_csv_week'].'","'.$val['reg_csv_time'].'","'.$val['no_plate'].'","'.$val['car_no'].'","'.
                        $val['grade'].'","'.$val['model'].'","'.$val['price'].'","'.$val['visit_val'].'","'.$val['result_close_val'].'","'.$val['result_status_val'].'"'."\r\n";
                    if($comp_data != $d){
                        echo mb_convert_encoding($d,"SJIS-win","UTF-8");
                        $comp_data = $d;
                    }
                    unset($d);
                }
            }
        }
	}
	
	function exception_proc($shop_id,$car_detail_id,$date){
		$sql = "select * from exception ";
		$sql .= " where shop_id = '".$this->DB->getQStr($shop_id)."' ";
		$sql .= " and car_detail_id = ".$this->DB->getQStr($car_detail_id);
		$sql .= " and date = '".$this->DB->getQStr($date)."' ";
		$sql .= " and del_flg = '0' and disp_flg = '1' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				return 1;
			}
			$rs->Close();
		}
		return 0;
	}

	// add 20140702 車種情報取得
	function get_car(){
		$data = array();
		//車種名取得
		$sql2 = "select * from car ";
		$sql2 .= "where del_flg='0' and disp_flg='1'";
		$rs2 =& $this->DB->ASExecute($sql2);
		if($rs2){
			while(!$rs2->EOF){
				$data[$rs2->fields('car_id')]['carname'] = $rs2->fields('name');
				$data[$rs2->fields('car_id')]['carname2'] = $rs2->fields('name2');
				$data[$rs2->fields('car_id')]['carname3'] = $rs2->fields('name3');
				$rs2->MoveNext();
			}
			$rs2->Close();
		}
		return $data;
	}

	// add 200140702 店舗情報取得
	function get_shop(){
		$data = array();
		//店舗情報取得
		$sql2 = "select * from shop ";
		$sql2 .= "where del_flg='0' and disp_flg='1'";
		$rs2 =& $this->DB->ASExecute($sql2);
		if($rs2){
			while(!$rs2->EOF){
				$data[$rs2->fields('shop_id')]['pref_id'] = $rs2->fields('pref_id');
				$data[$rs2->fields('shop_id')]['area_id'] = $rs2->fields('area_id');
				$data[$rs2->fields('shop_id')]['shop_name'] = $rs2->fields('name');
				$rs2->MoveNext();
			}
			$rs2->Close();
		}
		return $data;
	}

	function car_judge($shop_id){
		$car_data_flg = false;
		$car = explode(",",$_SESSION['search']['car']);
		if($car){
			if(is_array($car)){
				$sql = "select * from car_detail";
				$sql .= " where shop_id = '".$this->DB->getQStr($shop_id)."'";
				$sql .= " and car_id = '".$this->DB->getQStr($car[0])."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$car_data_flg = true;
					}
					$rs->Close();
				}
			}
		}
		return $car_data_flg;
	}
}
?>