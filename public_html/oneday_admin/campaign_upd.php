<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
require_once ( "Mail.php" ) ;

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			// 予約情報(確認)
			case 'check':
				$this->check_proc();
			break;
			// 予約情報(登録)
			case 'entry':
				$this->entry_proc();
			break;
			//郵便番号検索
			case 'postcd_search':
				$this->postcd_search_proc();
				break;
			// 予約情報(完了)
			case 'end':
				$this->end_proc();
			break;
			// 予約情報(入力)
			default:
				$this->default_proc();
			break;
		}
	}
	
	// ===============共通処理↓↓↓===============
	
	// データセット
	function post_proc(){
		$this->templ->smarty->assign("upd_flg",$this->req->get('upd_flg'));
		$this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
		if($this->req->get('staff_id')){
			$this->templ->smarty->assign("staff_id",$this->req->get('staff_id'));
			$staff_id = $this->req->get('staff_id');
		}
		else{
			$this->templ->smarty->assign("staff_id",$_SESSION['oneday']['staff_autono']);
			$staff_id = $_SESSION['oneday']['staff_autono'];
		}
		$sql = "select * from send_mail";
		$sql .= " where autono = ".$this->DB3->getQStr($staff_id);
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$this->templ->smarty->assign("staff_name",$rs->fields('staff_name'));
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("car_id",$this->req->get('car_id'));
		//$this->templ->smarty->assign("area_id",$this->req->get('area_id'));
		$this->templ->smarty->assign("age",$this->req->get('age'));
		$this->templ->smarty->assign("sei",$this->req->get('sei'));
		$this->templ->smarty->assign("mei",$this->req->get('mei'));
		$this->templ->smarty->assign("sei_kana",$this->req->get('sei_kana'));
		$this->templ->smarty->assign("mei_kana",$this->req->get('mei_kana'));
		$this->templ->smarty->assign("postcd1",$this->req->get('postcd1'));
		$this->templ->smarty->assign("postcd2",$this->req->get('postcd2'));
		$this->templ->smarty->assign("address",$this->req->get('address'));
		$this->templ->smarty->assign("tel1",$this->req->get('tel1'));
		$this->templ->smarty->assign("tel2",$this->req->get('tel2'));
		$this->templ->smarty->assign("tel3",$this->req->get('tel3'));
		$this->templ->smarty->assign("k_tel1",$this->req->get('k_tel1'));
		$this->templ->smarty->assign("k_tel2",$this->req->get('k_tel2'));
		$this->templ->smarty->assign("k_tel3",$this->req->get('k_tel3'));
		$this->templ->smarty->assign("mail",$this->req->get('mail'));
		$this->templ->smarty->assign("campaign",$this->req->get('campaign'));
		// add 20151104 キャンペーンを知った媒体をラジオボタン可対応
		$this->templ->smarty->assign("campaign_value",$this->util->campaign_list(1,$this->req->get('campaign')));
		if($this->req->get('campaign') == 1){
			$this->templ->smarty->assign("campaign1_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 2){
			$this->templ->smarty->assign("campaign2_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 3){
			$this->templ->smarty->assign("campaign3_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 4){
			$this->templ->smarty->assign("campaign4_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 5){
			$this->templ->smarty->assign("campaign5_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 6){
			$this->templ->smarty->assign("campaign6_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 7){
			$this->templ->smarty->assign("campaign7_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 8){
			$this->templ->smarty->assign("campaign8_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 9){
			$this->templ->smarty->assign("campaign9_checked",'checked="checked"');
		}
		// 出張展示会
		else if($this->req->get('campaign') == 10){
			$this->templ->smarty->assign("campaign10_checked",'checked="checked"');
		}
		// 営業スタッフの勧め
		else if($this->req->get('campaign') == 11){
			$this->templ->smarty->assign("campaign11_checked",'checked="checked"');
		}
		// TVCM
		else if($this->req->get('campaign') == 12){
			$this->templ->smarty->assign("campaign12_checked",'checked="checked"');
		}
		// 雑誌広告
		else if($this->req->get('campaign') == 13){
			$this->templ->smarty->assign("campaign13_checked",'checked="checked"');
		}
		else if($this->req->get('campaign') == 99){
			$this->templ->smarty->assign("campaign99_checked",'checked="checked"');
		}
		// add 20151030 キャンペーン申込の理由
		$this->templ->smarty->assign("reason",$this->req->get('reason'));
		$this->templ->smarty->assign("reason_value",$this->util->reason_list(1,$this->req->get('reason')));
		if($this->req->get('reason') == 1){
			$this->templ->smarty->assign("reason1_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 2){
			$this->templ->smarty->assign("reason2_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 3){
			$this->templ->smarty->assign("reason3_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 4){
			$this->templ->smarty->assign("reason4_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 5){
			$this->templ->smarty->assign("reason5_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 6){
			$this->templ->smarty->assign("reason6_checked",'checked="checked"');
		}
		else if($this->req->get('reason') == 99){
			$this->templ->smarty->assign("reason99_checked",'checked="checked"');
		}
		$this->templ->smarty->assign("reason99_text",$this->req->get('reason99_text'));
		// add 20160122 申込区分
		$this->templ->smarty->assign("offer_kind",$this->req->get('offer_kind'));
		$this->templ->smarty->assign("offer_kind_value",$this->util->offer_kind_list(1,$this->req->get('offer_kind')));
		if($this->req->get('offer_kind') == 1){
			$this->templ->smarty->assign("offer_kind1_checked",'checked="checked"');
		}
		else if($this->req->get('offer_kind') == 2){
			$this->templ->smarty->assign("offer_kind2_checked",'checked="checked"');
		}
		else if($this->req->get('offer_kind') == 3){
			$this->templ->smarty->assign("offer_kind3_checked",'checked="checked"');
		}
		if($this->req->get('use_shop')){
			$sql = "select * from shop";
			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('use_shop'))."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("use_shop_value",$rs->fields('name'));
				}
				$rs->Close();
			}
		}
		$this->templ->smarty->assign("use_shop",$this->req->get('use_shop'));
		$this->templ->smarty->assign("use_shop_staff",$this->req->get('use_shop_staff'));
		$this->templ->smarty->assign("kiyaku_check",$this->req->get('kiyaku_check'));
		// add 20160112 ご来店予定時間項目追加
		$this->templ->smarty->assign("v_schedule_hour",$this->req->get('v_schedule_hour'));
		$this->templ->smarty->assign("v_schedule_min",$this->req->get('v_schedule_min'));
		$this->templ->smarty->assign("month_from",$this->req->get('month_from'));
		$this->templ->smarty->assign("day_from",$this->req->get('day_from'));
		$this->templ->smarty->assign("month_to",$this->req->get('month_to'));
		$this->templ->smarty->assign("day_to",$this->req->get('day_to'));
		$this->templ->smarty->assign("comment",$this->req->get('comment'));
		$this->templ->smarty->assign("year",$this->req->get('year'));
		$this->templ->smarty->assign("month",$this->req->get('month'));
		$this->templ->smarty->assign("day",$this->req->get('day'));
		$this->templ->smarty->assign("memo",$this->req->get('memo'));
		$this->templ->smarty->assign("hour_from",$this->req->get('hour_from'));
		$this->templ->smarty->assign("hour_to",$this->req->get('hour_to'));
		$this->templ->smarty->assign("ca",$this->req->get('ca'));
		$this->templ->smarty->assign("at",$this->req->get('at'));
		$this->templ->smarty->assign("h_flg",$this->req->get('h_flg'));
		if($this->req->get_get('h_flg')){
			$this->templ->smarty->assign("h_flg",$this->req->get_get('h_flg'));
		}
		$this->templ->smarty->assign("date",$this->req->get('date'));
		$this->templ->smarty->assign("tab_no1",$this->req->get('tab_no1'));
		$this->templ->smarty->assign("tab_no2",$this->req->get('tab_no2'));
		$this->templ->smarty->assign("tab_no3",$this->req->get('tab_no3'));
		$this->templ->smarty->assign("selected1",$this->req->get('selected1'));
		$this->templ->smarty->assign("selected2",$this->req->get('selected2'));
		$this->templ->smarty->assign("selected3",$this->req->get('selected3'));

//		$this->templ->smarty->assign("month_from",$this->req->get('month_from'));
//		$this->templ->smarty->assign("day_from",$this->req->get('day_from'));
//		$this->templ->smarty->assign("month_to",$this->req->get('month_to'));
//		$this->templ->smarty->assign("day",$this->req->get('day_to'));

		if(!$this->req->get('selected1') and !$this->req->get('selected2') and !$this->req->get('selected3')){
			$this->templ->smarty->assign("selected1",1);
		}
/*
		if(!$this->req->get_get('h_flg')){
			if($this->req->get('hour_from') or $this->req->get('hour_to')){
				$this->templ->smarty->assign("h_flg",1);
			}
			else{
				$this->templ->smarty->assign("h_flg",0);
			}
		}
*/
		$this->templ->smarty->assign("ret",$this->req->get('ret'));

		// POSTの場合
		if( $this->req->get('number') ) $this->templ->smarty->assign("number",$this->req->get('number'));
		if( $this->req->get('year') ) $this->templ->smarty->assign("year",$this->req->get('year'));
		if( $this->req->get('month') ) $this->templ->smarty->assign("month",$this->req->get('month'));
		if( $this->req->get('day') ) $this->templ->smarty->assign("day",$this->req->get('day'));
		if( $this->req->get('car_id') ){ $this->templ->smarty->assign("car_id",$this->req->get('car_id')); $car_id = $this->req->get('car_id');}
		if( $this->req->get('shop_id') ){ $this->templ->smarty->assign("shop_id",$this->req->get('shop_id')); $shop_id = $this->req->get('shop_id');}
		if( $this->req->get('pref_id') ) $this->templ->smarty->assign("pref_id",$this->req->get('pref_id'));
		if( $this->req->get('area_id') ) $this->templ->smarty->assign("area_id",$this->req->get('area_id'));
		if( $this->req->get('car_name') ) $this->templ->smarty->assign("car_name",$this->req->get('car_name'));
		if( $this->req->get('shop_name') ) $this->templ->smarty->assign("shop_name",$this->req->get('shop_name'));
	
		// GETの場合
		if( $this->req->get_get('number') ) $this->templ->smarty->assign("number",$this->req->get_get('number'));
		if( $this->req->get_get('year') ) $this->templ->smarty->assign("year",$this->req->get_get('year'));
		if( $this->req->get_get('month') ) $this->templ->smarty->assign("month",$this->req->get_get('month'));
		if( $this->req->get_get('day') ) $this->templ->smarty->assign("day",$this->req->get_get('day'));
		if( $this->req->get_get('car_id') ){$this->templ->smarty->assign("car_id",$this->req->get_get('car_id'));$car_id = $this->req->get_get('car_id');}
		if( $this->req->get_get('shop_id') ){ $this->templ->smarty->assign("shop_id",$this->req->get_get('shop_id')); $shop_id = $this->req->get_get('shop_id');}
		if( $this->req->get_get('pref_id') ){ $this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));}
		if( $this->req->get_get('area_id') ){ $this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));}
	
		// 車種
		$sql = "select car_id from car_detail";
		$sql .= " where autono=".$this->DB->getQStr($car_id);
		//$sql .= " and disp_flg='1' ";
		//$sql .= " and del_flg='0' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$sql2 = "select name,name2,name3 from car ";
				$sql2 .= " where car_id='".$this->DB->getQStr($rs->fields('car_id'))."' ";
				$sql2 .= " and disp_flg='1' ";
				$sql2 .= " and del_flg='0' ";
				$rs2 =& $this->DB->ASExecute($sql2);
				$data_list = array();
				if($rs2){
					while(!$rs2->EOF){
						$dat = array();
						$dat['name'] = $rs2->fields('name')." ".$rs2->fields('name2')." ".$rs2->fields('name3');
						$data_list[] = $dat;
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
			}
			$rs->Close();
		}

		$this->templ->smarty->assign("car_name",$data_list[0][name]);
		
		// 店舗
		$sql = "select name from shop ";
		$sql .= " where shop_id='".$this->DB->getQStr($shop_id)."' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		
		$rs =& $this->DB->ASExecute($sql);
		
		$data_list = array();
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['name'] = $rs->fields('name');
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("shop_name",$data_list[0][name]);
		
	}
	
	// 入力データチェック
	function datacheck_proc(){
//if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
		// 日付from-toチェック
		$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year'));
		$date_to = mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year'));
		if($date_from > $date_to){
			$error .= "日付を正しく選択してください\r\n";
			return $error;
		}

//		if(!$this->req->get('upd_flg')){
//			$date = date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year')));
			$date = date("Y-m-d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year')));
			$sql = "select * from reservation where date = '".$this->DB->getQStr($date)."'";
			$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
			$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_id'));
			$sql .= " and temporary_flg='2' ";
			if($this->req->get('number')){
				$sql .= " and disp_number <> '".$this->DB->getQStr($this->req->get('number'))."'";
			}
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."<br>";
}
			if($rs){
				if(!$rs->EOF){
					if(!$rs->fields('hour_from') and !$rs->fields('hour_to')){
						$error .= "すでに予約されています\r\n";
						$rs->Close();
						return $error;
					}
					else if(!$this->req->get('hour_from') or !$this->req->get('hour_to')){
						$error .= "すでに予約されています\r\n";
						$rs->Close();
						return $error;
					}
					else if($rs->fields('hour_from') < $this->req->get('hour_to') and $this->req->get('hour_from') < $rs->fields('hour_to')){
//					else if($rs->fields('hour_from') <= $this->req->get('hour_from') and $rs->fields('hour_to') >= $this->req->get('hour_from')){
						$error .= "すでに予約されています\r\n";
						$rs->Close();
						return $error;
					}
//					else if($rs->fields('hour_from') <= $this->req->get('hour_to') and $rs->fields('hour_to') >= $this->req->get('hour_to')){
//						$error .= "すでに予約されています\r\n";
//						$rs->Close();
//						return $error;
//					}
				}
				$rs->Close();
			}
			$sql = "select * from reservation where date = '".$this->DB->getQStr($date)."'";
			$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
			$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_id'));
			$sql .= " and temporary_flg = '1' ";
			$sql .= " and validity_date >= '".$this->DB->getQStr(date("Y-m-d H:i:s"))."'";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."<br>";
}
			if($rs){
				if(!$rs->EOF){
					$error .= "すでに仮予約されています\r\n";
					$rs->Close();
					return $error;
				}
				$rs->Close();
			}
			if($date_from <> $date_to){
				for($i = 1;$date_from < $date_to;$i++){
					$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year'));
					$date = date("Y-m-d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year')));
					$sql = "select * from reservation where date = '".$this->DB->getQStr($date)."'";
					$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
					$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_id'));
					$sql .= " and temporary_flg='2' ";
					if($this->req->get('number')){
						$sql .= " and disp_number <> '".$this->DB->getQStr($this->req->get('number'))."'";
					}
					$sql .= " and disp_flg='1' ";
					$sql .= " and del_flg='0' ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							if(!$rs->fields('hour_from') and !$rs->fields('hour_to')){
								$error .= "すでに予約されています\r\n";
								$rs->Close();
								return $error;
							}
							/*
							else if(!$this->req->get('hour_from') or !$this->req->get('hour_to')){
								$error .= "すでに予約されています\r\n";
								$rs->Close();
								return $error;
							}
							*/
							else if($rs->fields('hour_from') < $this->req->get('hour_to') and $this->req->get('hour_from') < $rs->fields('hour_to')){
								$error .= "すでに予約されています\r\n";
								$rs->Close();
								return $error;
							}
							else if(!$this->req->get('hour_from') or !$this->req->get('hour_to')){
								$error .= "すでに予約されています\r\n";
								$rs->Close();
								return $error;
							}
						}
						$rs->Close();
					}
					$sql = "select * from reservation where date = '".$this->DB->getQStr($date)."'";
					$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
					$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_id'));
					$sql .= " and temporary_flg = '1' ";
					$sql .= " and validity_date >= '".$this->DB->getQStr(date("Y-m-d H:i:s"))."'";
					$sql .= " and disp_flg='1' ";
					$sql .= " and del_flg='0' ";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$error .= "すでに仮予約されています\r\n";
							$rs->Close();
							return $error;
						}
						$rs->Close();
					}
				}
			}
//		}
//}		
		mb_regex_encoding('UTF-8');
		mb_internal_encoding('UTF-8');
		// add 20140909 時間チェック
		if($this->req->get('hour_from')){
			if(!$this->req->get('hour_to')){
				$error .= "予約時間TOを選択してください\r\n";
			}
			else if($this->req->get('hour_from') < 0 or $this->req->get('hour_from') > 24){
				$error .= "予約時間FROMが正しくありません\r\n";
			}
		}
		if($this->req->get('hour_to')){
			if(!$this->req->get('hour_from')){
				$error .= "予約時間FROMを選択してください\r\n";
			}
			else if($this->req->get('hour_to') < 0 or $this->req->get('hour_to') > 24){
				$error .= "予約時間TOが正しくありません\r\n";
			}
		}
		if(!$this->req->get('hour_from') and !$this->req->get('hour_to')){
			if($this->req->get('hour_from') > $this->req->get('hour_to')){
				$error .= "予約時間FROM-TOが正しくありません\r\n";
			}
			else{
				$date = date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year')));
				$sql = "select * from reservation where date = '".$this->DB->getQStr($date)."'";
				$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
				$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_id'));
				$sql .= " and hour_from >= ".$this->DB->getQStr((int)$this->req->get('hour_from'));
				$sql .= " and hour_to <= ".$this->DB->getQStr((int)$this->req->get('hour_to'));
				$sql .= " and temporary_flg='2' ";
				$sql .= " and disp_flg='1' ";
				$sql .= " and del_flg='0' ";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$error .= "ご指定の時間はすでに予約されています\r\n";
						$rs->Close();
						return $error;
					}
					$rs->Close();
				}
			}
		}
		// 申込区分 add 20160122
		if($this->req->get('offer_kind') == null){
			$error .= "申込区分を選択してください\r\n";
		}
		else{
			$check1 = $this->util->number_check($this->req->get('offer_kind'));
			if(!empty($check1)){
				$error .= "申込区分を正しく選択してください\r\n";
			}
			else if(!$this->util->offer_kind_list(1,$this->req->get('offer_kind'))){
				$error .= "申込区分を正しく選択してください\r\n";
			}
		}
		// 名前
		if($this->req->get('sei') == null){
			//$error .= "名前・姓を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('sei'), "UTF-8" ) > 100 ){
			$error .= "名前・姓を100文字以内で入力してください\r\n";
		}
		if($this->req->get('mei') == null){
			//$error .= "名前・名を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('mei'), "UTF-8" ) > 100 ){
			$error .= "名前・名を100文字以内で入力してください\r\n";
		}
		if($this->req->get('sei_kana') == null){
			//$error .= "フリガナ・姓を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('sei_kana'), "UTF-8" ) > 100 ){
			$error .= "フリガナ・姓を100文字以内で入力してください\r\n";
		}
		else{
			$str = $this->req->get('sei_kana');
			if(!mb_ereg('^[ｦ-ﾟァ-ヶa-zA-Zー\-]+$', $str)) {
			    $error .= "フリガナ・姓をカタカナまたは英字で入力してください\r\n";
			}
		}
		if($this->req->get('mei_kana') == null){
			//$error .= "フリガナ・名を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('mei_kana'), "UTF-8" ) > 100 ){
			$error .= "フリガナ・名を100文字以内で入力してください\r\n";
		}
		else{
			$str = $this->req->get('mei_kana');
			if(!mb_ereg('^[ｦ-ﾟァ-ヶa-zA-Zー\-]+$', $str)) {
			    $error .= "フリガナ・名をカタカナまたは英字で入力してください\r\n";
			}
		}
		// 年齢
		if($this->req->get('age') != null){
			$check1 = null;
			$check1 = $this->util->number_check($this->req->get('age'));
			if(!empty($check1)) $error .= "年齢を数字で入力してください\r\n";
		}
		// 郵便番号
		if($this->req->get('postcd1') != null or $this->req->get('postcd2') != null){
			$check1 = null;
			$check2 = null;
			$check1 = $this->util->number_check($this->req->get('postcd1'));
			$check2 = $this->util->number_check($this->req->get('postcd2'));
			if(!empty($check1) or !empty($check2)) $error .= "郵便番号を数字で入力してください\r\n";
		}
		// 住所
		if($this->req->get('address') == null){
			//$error .= "住所を入力してください\r\n";
		}
		else if( mb_strlen($this->req->get('address'), "UTF-8" ) > 500 ){
			$error .= "住所を500文字以内で入力してください\r\n";
		}
		
		// 自宅tel
		if($this->req->get('tel1') == null or $this->req->get('tel2') == null or $this->req->get('tel3') == null){
			//$error .= "自宅telを入力してください\r\n";
		}
		else{
			$check1 = null;
			$check2 = null;
			$check3 = null;
			$check1 = $this->util->number_check($this->req->get('tel1'));
			$check2 = $this->util->number_check($this->req->get('tel2'));
			$check3 = $this->util->number_check($this->req->get('tel3'));
			if(!empty($check1) or !empty($check2) or !empty($check3)){
				$error .= "自宅telを数字で入力してください\r\n";
			}
			else{
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel1'))){
					$error .= "自宅tel1を正しく入力してください\r\n";
				}
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel2'))){
					$error .= "自宅tel2を正しく入力してください\r\n";
				}
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('tel3'))){
					$error .= "自宅tel3を正しく入力してください\r\n";
				}
			}
		}
		
		// 携帯tel
		if($this->req->get('k_tel1') == null or $this->req->get('k_tel2') == null or $this->req->get('k_tel3') == null){
			//$error .= "携帯telを入力してください\r\n";
		}else{
			$check1 = null;
			$check2 = null;
			$check3 = null;
			$check1 = $this->util->number_check($this->req->get('k_tel1'));
			$check2 = $this->util->number_check($this->req->get('k_tel2'));
			$check3 = $this->util->number_check($this->req->get('k_tel3'));
			if(!empty($check1) or !empty($check2) or !empty($check3)){
				$error .= "携帯telを数字で入力してください\r\n";
			}
			else{
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('k_tel1'))){
					$error .= "携帯tel1を正しく入力してください\r\n";
				}
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('k_tel2'))){
					$error .= "携帯tel2を正しく入力してください\r\n";
				}
				if(!mb_ereg("^[0-9]{1,5}$", $this->req->get('k_tel3'))){
					$error .= "携帯tel3を正しく入力してください\r\n";
				}
			}
		}
		
		// Mail
		if($this->req->get('mail') == null){
			//$error .= "Mailを入力してください\r\n";
		}
		else{
			$check1 = null;
			$check1 = $this->util->mail_check('Mail',$this->req->get('mail'));
			if(!empty($check1)){
				$error .= "Mailの形式が正しくありません\r\n";
			}
			else if(!preg_match("/^.{1,200}$/",$this->req->get('mail'))){
				$error .= "Mailを200文字以内で入力してください\r\n";
			}
		}
		
		// キャンペーンを知った媒体
		if($this->req->get('campaign') != null){
			$check1 = null;
			$check1 = $this->util->number_check($this->req->get('campaign'));
			if(!empty($check1)) $error .= "キャンペーンを知った媒体を正しく選択してください\r\n";
		}
		else{
			$error .= "キャンペーンを知った媒体を選択してください\r\n";
		}

		// キャンペーン申込の理由
		if($this->req->get('reason') != null){
			$check1 = $this->util->number_check($this->req->get('reason'));
			if(!empty($check1)){
				$error .= "キャンペーン申込の理由を正しく選択してください\r\n";
			}
			else if(!$this->util->reason_list(1,$this->req->get('reason'))){
				$error .= "キャンペーン申込の理由を正しく選択してください\r\n";
			}
		}

		// 普段ご利用されてる店舗・担当営業スタッフ
		if($this->req->get('use_shop') != null){
			$find_flg = false;
			$sql = "select * from shop";
			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('use_shop'))."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$find_flg = true;
				}
				$rs->Close();
			}
			if(!$find_flg){
				$error3 .= "普段ご利用されてる店舗を正しく選択してください\r\n";
			}
		}
		if($this->req->get('use_shop_staff') != null){
			if( mb_strlen($this->req->get('use_shop_staff'), "UTF-8" ) > 100 ){
				$error3 .= "普段ご利用されてる店舗の担当営業スタッフを100文字以内で入力してください\r\n";
			}
		}

		// ご来店予定時間
		if($this->req->get('v_schedule_hour') != null){
			$check1 = $this->util->number_check($this->req->get('v_schedule_hour'));
			if(!empty($check1)){
				$error .= "ご来店予定時間(時)を正しく選択してください\r\n";
			}
			else if(!$this->util->schedule_hour_list(1,$this->req->get('v_schedule_hour'))){
				$error .= "ご来店予定時間(時)を正しく選択してください\r\n";
			}
			else if($this->req->get('v_schedule_min') == null){
				$error .= "ご来店予定時間(分)を選択してください\r\n";
			}
		}
		if($this->req->get('v_schedule_min') != null){
			$check1 = $this->util->number_check($this->req->get('v_schedule_min'));
			if(!empty($check1)){
				$error .= "ご来店予定時間(分)を正しく選択してください\r\n";
			}
			else if(!$this->util->schedule_min_list(1,$this->req->get('v_schedule_min'))){
				$error .= "ご来店予定時間(分)を正しく選択してください\r\n";
			}
			else if($this->req->get('v_schedule_hour') == null){
				$error .= "ご来店予定時間(時)を選択してください\r\n";
			}
		}

		// その他ご意見・ご要望
		if($this->req->get('comment') != null){
			if( mb_strlen($this->req->get('comment'), "UTF-8" ) > 1000 ){
				$error .= "その他ご意見・ご要望を1000文字以内で入力してください\r\n";
			}
		}
/*		
		if($this->req->get('kiyaku_check') == null){
			$error .= "規約に同意されていません\r\n";
		}
*/
		return $error;
	}
	
	// ===============共通処理↑↑↑===============
	
	//郵便番号検索
	function postcd_search_proc(){
		list($prefecture,$address) = $this->util->searchAddr(POSTCD_FILE,$_REQUEST['p1'],$_REQUEST['p2']);
		echo $prefecture.",".$address;
	}
	
	//  仮押さえ削除
	function delete_proc(){
		
		// 仮押さえ登録
		$ret = $this->util->del_reservation($this->DB,$this->req->get_get('number'));
/*
		$record = null;
		$record['disp_flg'] = "0";
		$record['del_flg'] = "1";
		$record['temporary_flg'] = '0';
		$record['update_date'] = time();
		
		$where  = " autono=".$this->DB->getQStr($this->req->get_get('number'));
		$where .= " and shop_id=".$this->DB->getQStr($this->req->get_get('shop_id'));
		$where .= " and car_id='".$this->DB->getQStr($this->req->get_get('car_id'))."'";
		$where .= " and area_id=".$this->DB->getQStr($this->req->get_get('area_id'));
		$where .= " and date='".$this->DB->getQStr($this->req->get_get('year')."-".sprintf("%02d",$this->req->get_get('month'))."-".sprintf("%02d",$this->req->get_get('day')))."'";
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $where;
//exit;
}		
		$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
*/
		//echo $where;exit;
	}
	
	// 予約情報入力(入力)
	function default_proc(){
		if($this->req->get_get('ret')){
			$this->req->set('ret',$this->req->get_get('ret'));
		}
		if($this->req->get_get('shop_id') and $this->req->get_get('car_id') and $this->req->get_get('date') and $this->req->get_get('pref_id') and $this->req->get_get('area_id')){
			$date = explode("-",$this->req->get_get('date'));
			$this->req->set('year',$date[0]);
			$this->req->set('month',$date[1]);
			$this->req->set('day',$date[2]);
			if($this->req->get('month_from')){
				$this->req->set('month_from',$this->req->get('month_from'));
			}
			else{
				$this->req->set('month_from',$date[1]);
			}
			if($this->req->get('day_from')){
				$this->req->set('day_from',$this->req->get('day_from'));
			}
			else{
				$this->req->set('day_from',$date[2]);
			}
			if($this->req->get('month_to')){
				$this->req->set('month_to',$this->req->get('month_to'));
			}
			else{
				$this->req->set('month_to',$date[1]);
			}
			if($this->req->get('day_to')){
				$this->req->set('day_to',$this->req->get('day_to'));
			}
			else{
				$this->req->set('day_to',$date[2]);
			}

			// add 20140909
			$this->req->set('date',$this->req->get_get('date'));
			if($this->req->get_get('tab') == 1){
				$this->req->set('selected1',1);
			}
			else if($this->req->get_get('tab') == 2){
				$this->req->set('selected2',1);
			}
			else if($this->req->get_get('tab') == 3){
				$this->req->set('selected3',1);
			}
			if($this->req->get_get('h_flg')){
				$this->req->set('h_flg',$this->req->get_get('h_flg'));
			}
			if($this->req->get_get('ca')){
				$this->req->set('ca',$this->req->get_get('ca'));
				$this->req->set('at',$this->req->get_get('at'));
				if(!$this->req->get('age') and !$this->req->get('sei') and !$this->req->get('mei') and !$this->req->get('sei_kana') and !$this->req->get('mei_kana') and !$this->req->get('postcd1') and !$this->req->get('postcd2') and !$this->req->get('address') and !$this->req->get('tel1') and !$this->req->get('k_tel1') and !$this->req->get('k_tel2') and !$this->req->get('k_tel3') and !$this->req->get('mail')){
					$sql = "select * from m_customer where autono = ".$this->DB2->getQStr($this->req->get_get('ca'));
					$rs =& $this->DB2->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							if($rs->fields('age') > 0){
								$this->req->set('age',$rs->fields('age'));
							}
							$customer_name = explode(" ",$rs->fields('customer_name'));
							$this->req->set('sei',$customer_name[0]);
							$this->req->set('mei',$customer_name[1]);
							$customer_kana = explode(" ",$rs->fields('customer_kana'));
							$this->req->set('sei_kana',$customer_kana[0]);
							$this->req->set('mei_kana',$customer_kana[1]);
							$postcd = explode("-",$rs->fields('h_postcd'));
							$this->req->set('postcd1',$postcd[0]);
							$this->req->set('postcd2',$postcd[1]);
							$this->req->set('address',$rs->fields('h_address1').$rs->fields('h_address2').$rs->fields('h_address3'));
							$this->req->set('tel1',substr(trim($rs->fields('h_tel')),0,3));
							$this->req->set('tel2',substr(trim($rs->fields('h_tel')),3,4));
							$this->req->set('tel3',substr(trim($rs->fields('h_tel')),7));
							$this->req->set('k_tel1',substr($rs->fields('cell_phone'),0,3));
							$this->req->set('k_tel2',substr($rs->fields('cell_phone'),3,4));
							$this->req->set('k_tel3',substr($rs->fields('cell_phone'),7));
							$this->req->set('mail',$rs->fields('mail'));
						}
						$rs->Close();
					}
				}
			}
			$tab_array = array();
			$tab_no = 1;
			$sql2 = "select * from reservation ";
			$sql2 .= "where car_detail_id = ".$this->DB->getQStr($this->req->get_get('car_id'));
			$sql2 .= " and date='".$this->DB->getQStr($this->req->get_get('date'))."'";
			$sql2 .= " and shop_id='".$this->DB->getQStr($this->req->get_get('shop_id'))."'";
			$sql2 .= " and disp_flg='1' ";
			$sql2 .= " and del_flg='0' ";
			$sql2 .= " and temporary_flg = '2' ";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				while(!$rs2->EOF){
					$tab_array[$rs2->fields('hour_from')] = $rs2->fields('disp_number');
					$rs2->MoveNext();
				}
				$rs2->Close();
			}
			ksort($tab_array);
			$tab_no = 1;
			if($tab_array){
				if(is_array($tab_array)){
					foreach($tab_array as $hour => $disp_number){
						if($this->req->get_get('tab') == $tab_no){
							$tab_no++;
						}
						$this->req->set('tab_no'.$tab_no,$disp_number);
						$tab_no++;
					}
				}
			}
		}
		else if($_SESSION['admin']['disp_number']){
			$this->templ->smarty->assign("number",$_SESSION['admin']['disp_number']);
			$this->templ->smarty->assign("upd_flg",1);
			$sql = "select * from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($_SESSION['admin']['disp_number'])."'";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$sql .= " order by date ASC LIMIT 1";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->req->set('number',$_SESSION['admin']['disp_number']);
					$this->req->set('upd_flg',1);
					$this->req->set('age',$rs->fields('age'));
					$this->req->set('sei',$rs->fields('sei'));
					$this->req->set('mei',$rs->fields('mei'));
					$this->req->set('sei_kana',$rs->fields('sei_kana'));
					$this->req->set('mei_kana',$rs->fields('mei_kana'));
					$postcd = explode("-",$rs->fields('postcd'));
					$this->req->set('postcd1',$postcd[0]);
					$this->req->set('postcd2',$postcd[1]);
					$this->req->set('address',$rs->fields('address'));
					$tel = explode("-",$rs->fields('tel'));
					$this->req->set('tel1',$tel[0]);
					$this->req->set('tel2',$tel[1]);
					$this->req->set('tel3',$tel[2]);
					$k_tel = explode("-",$rs->fields('k_tel'));
					$this->req->set('k_tel1',$k_tel[0]);
					$this->req->set('k_tel2',$k_tel[1]);
					$this->req->set('k_tel3',$k_tel[2]);
					$this->req->set('mail',$rs->fields('mail'));
					$this->req->set('campaign',$rs->fields('campaign'));
					// add 20151030 キャンペーン申込の理由 項目追加
					$this->req->set('reason',$rs->fields('reason'));
					$this->req->set('reason99_text',$rs->fields('reason99_text'));
					// add 20160122 申込区分 項目追加
					$this->req->set('offer_kind',$rs->fields('offer_kind'));
					$this->req->set('use_shop',$rs->fields('use_shop'));
					$this->req->set('use_shop_staff',$rs->fields('use_shop_staff'));
					$this->req->set('comment',$rs->fields('comment'));
					// add 20160112 ご来店予定時間項目追加
					if($rs->fields('v_schedule_time')){
						$this->req->set('v_schedule_hour',date("H",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y"))));
						$this->req->set('v_schedule_min',date("i",mktime(substr($rs->fields('v_schedule_time'),0,2),substr($rs->fields('v_schedule_time'),3,2),0,1,1,date("Y"))));
					}
					$this->req->set('shop_id',$rs->fields('shop_id'));
					$this->req->set('area_id',$rs->fields('area_id'));
					$this->req->set('pref_id',$rs->fields('pref_id'));
					$this->req->set('car_id',$rs->fields('car_detail_id'));
					$this->req->set('staff_id',$rs->fields('staff_autono'));
					if($this->req->get('staff_id')){
						$sql2 = "select * from send_mail";
						$sql2 .= " where autono = ".$this->DB3->getQStr($this->req->get('staff_id'));
						$rs2 =& $this->DB3->ASExecute($sql2);
						if($rs2){
							if(!$rs2->EOF){
								$this->req->set('staff_name',$rs2->fields('staff_name'));
							}
							$rs2->Close();
						}
					}
					$this->req->set('date',$rs->fields('date'));
					$date = explode("-",$rs->fields('date'));
					$this->req->set('year',$date[0]);
					$this->req->set('month',$date[1]);
					$this->req->set('day',$date[2]);
					$this->req->set('month_from',$date[1]);
					$this->req->set('day_from',$date[2]);
					// add 20140909
					$this->req->set('date',$rs->fields('date'));
					$this->req->set('date_from',$rs->fields('date'));
					$this->req->set('memo',$rs->fields('memo'));
					if($rs->fields('hour_from') or $rs->fields('hour_to')){
						if(!$rs->fields('hour_to') and $rs->fields('hour_from') <= 10){
							$this->req->set('h_flg',0);
						}
						else{
							$this->req->set('h_flg',1);
						}
						$this->req->set('hour_from',$rs->fields('hour_from'));
						$this->req->set('hour_to',$rs->fields('hour_to'));
						$tab_array = array();
						$tab_array[$rs->fields('hour_from')] = $rs->fields('disp_number');
						$tab_no = 1;
						$sql2 = "select * from reservation ";
						$sql2 .= "where car_detail_id = ".$this->DB->getQStr($rs->fields('car_detail_id'));
						$sql2 .= " and date='".$this->DB->getQStr($rs->fields('date'))."'";
						$sql2 .= " and shop_id='".$this->DB->getQStr($rs->fields('shop_id'))."'";
						$sql2 .= " and disp_flg='1' ";
						$sql2 .= " and del_flg='0' ";
						$sql2 .= " and disp_number <> '".$this->DB->getQStr($rs->fields('disp_number'))."' ";
						$rs2 =& $this->DB->ASExecute($sql2);
						if($rs2){
							while(!$rs2->EOF){
								$tab_array[$rs2->fields('hour_from')] = $rs2->fields('disp_number');
								$rs2->MoveNext();
							}
							$rs2->Close();
						}
						ksort($tab_array);
						$tab_no = 1;
						if($tab_array){
							if(is_array($tab_array)){
								foreach($tab_array as $hour => $disp_number){
									$this->req->set('tab_no'.$tab_no,$disp_number);
									if($disp_number == $rs->fields('disp_number')){
										$this->req->set('selected'.$tab_no,1);
									}
									$tab_no++;
								}
							}
						}
					}
				}
				$rs->Close();
			}
			$f_flg = false;
			$sql = "select * from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($_SESSION['admin']['disp_number'])."'";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$sql .= " order by date DESC LIMIT 1";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$f_flg = true;
					$date = explode("-",$rs->fields('date'));
					$this->req->set('month_to',$date[1]);
					$this->req->set('day_to',$date[2]);
					$this->req->set('date_to',$rs->fields('date'));
					if($rs->fields('hour_to')){
						$this->req->set('hour_to',$rs->fields('hour_to'));
					}
				}
				$rs->Close();
			}
			if(!$f_flg){
				if($this->req->get('month_to')){
					$this->req->set('month_to',$this->req->get('month_to'));
				}
				else{
					$this->req->set('month_to',$this->req->get('month_from'));
				}
				if($this->req->get('day_to')){
					$this->req->set('day_to',$this->req->get('day_to'));
				}
				else{
					$this->req->set('day_to',$this->req->get('day_from'));
				}
				if($this->req->get('date_to')){
					$this->req->set('date_to',$this->req->get('date_to'));
				}
				else{
					$this->req->set('date_to',$this->req->get('date_from'));
				}
				$this->req->set('hour_to',$this->req->get('hour_to'));
			}
		}
		
		// データセット
		$this->post_proc();
		
		// フォーム作成
		$use_shop_list = array();
		$sql = "select * from shop";
		$sql .= " order by shop_id";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$use_shop_list[$rs->fields('shop_id')] = $rs->fields('name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("use_shop_list",$use_shop_list);
		$age_list = $this->util->age_list();
		$campaign_list = $this->util->campaign_list();
		$hour_from_list = $this->util->camp_hour_list();
		$hour_to_list = $this->util->camp_hour_list();
		$day_list = $this->util->day_list2();
		$month_list = $this->util->month_list2();
		if($this->req->get_get('shop_id')){
			$staff_list = $this->util->staff_list($this->DB,$this->req->get_get('shop_id'));
		}
		else if($this->req->get('shop_id')){
			$staff_list = $this->util->staff_list($this->DB,$this->req->get('shop_id'));
		}
		
		$this->templ->smarty->assign("age_list",$age_list);
		$this->templ->smarty->assign("campaign_list",$campaign_list);
		$this->templ->smarty->assign("staff_list",$staff_list);
		// add 20160112 ご来店予定時間項目追加
		$v_schedule_hour_list = $this->util->schedule_hour_list();
		$v_schedule_min_list = $this->util->schedule_min_list();
		$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
		$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);

		if(!$this->req->get('month_from')){
			$this->templ->smarty->assign("month_from",date("m"));
		}
		if(!$this->req->get('day_from')){
			$this->templ->smarty->assign("day_from",date("d"));
		}
		if(!$this->req->get('month_to')){
			$this->templ->smarty->assign("month_to",date("m"));
		}
		if(!$this->req->get('day_to')){
			$this->templ->smarty->assign("day_to",date("d"));
		}

		$this->templ->smarty->assign("day_list",$day_list);
		$this->templ->smarty->assign("month_list",$month_list);
		$this->templ->smarty->assign("hour_from_list",$hour_from_list);
		$this->templ->smarty->assign("hour_to_list",$hour_to_list);
		$this->templ->smarty->display("oneday_admin/campaign_upd_input.html");
		exit;
	}
	
	// 予約情報入力(確認)
	function check_proc(){
		
		// 入力データチェック
		$error = $this->datacheck_proc();
		
		if(!empty($error)){
//			$this->templ->smarty->assign("error",$error);
//			$this->default_proc();
//			return;
			$this->templ->smarty->assign("month_from",$this->req->get('month_from'));
			$this->templ->smarty->assign("day_from",$this->req->get('day_from'));
			$this->templ->smarty->assign("month_to",$this->req->get('month_to'));
			$this->templ->smarty->assign("day",$this->req->get('day_to'));
			$this->templ->smarty->assign("staff_id",$this->req->get('staff_id'));
			$this->post_proc();
			// フォーム作成
			$use_shop_list = array();
			$sql = "select * from shop";
			$sql .= " order by shop_id";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$use_shop_list[$rs->fields('shop_id')] = $rs->fields('name');
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$this->templ->smarty->assign("use_shop_list",$use_shop_list);
			$age_list = $this->util->age_list();
			$campaign_list = $this->util->campaign_list();
			$hour_from_list = $this->util->camp_hour_list();
			$hour_to_list = $this->util->camp_hour_list();
			$day_list = $this->util->day_list2();
			$month_list = $this->util->month_list2();
			if($this->req->get_get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get_get('shop_id'));
			}
			else if($this->req->get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get('shop_id'));
			}
			
			$this->templ->smarty->assign("age_list",$age_list);
			$this->templ->smarty->assign("campaign_list",$campaign_list);
			$this->templ->smarty->assign("staff_list",$staff_list);
			// add 20160112 ご来店予定時間項目追加
			$v_schedule_hour_list = $this->util->schedule_hour_list();
			$v_schedule_min_list = $this->util->schedule_min_list();
			$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
			$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);
			$this->templ->smarty->assign("day_list",$day_list);
			$this->templ->smarty->assign("month_list",$month_list);
			$this->templ->smarty->assign("hour_from_list",$hour_from_list);
			$this->templ->smarty->assign("hour_to_list",$hour_to_list);
			$this->templ->smarty->assign("error",$error);
			$this->templ->smarty->display("oneday_admin/campaign_upd_input.html");
			exit;
		}
		
		// データセット
		$this->post_proc();
		
		// フォーム変換
		//$age_list = $this->util->age_list(1,$this->req->get('age'));
		$use_shop_list = array();
		$sql = "select * from shop";
		$sql .= " order by shop_id";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$use_shop_list[$rs->fields('shop_id')] = $rs->fields('name');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("use_shop_list",$use_shop_list);
		$campaign_list = $this->util->campaign_list(1,$this->req->get('campaign'));
		$staff_list = $this->util->staff_list($this->DB,$this->req->get('shop_id'));
		$this->templ->smarty->assign("staff_list",$staff_list);
		//$this->templ->smarty->assign("age_list",$age_list);
		$this->templ->smarty->assign("campaign_list",$campaign_list);
		// add 20160112 ご来店予定時間項目追加
		$v_schedule_hour_list = $this->util->schedule_hour_list();
		$v_schedule_min_list = $this->util->schedule_min_list();
		$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
		$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);
		
		$this->templ->smarty->display("oneday_admin/campaign_upd_conf.html");
		exit;
	}
	
	// 予約情報入力(登録)
	function entry_proc(){
		if($this->req->get('type') == "return"){
//			$this->default_proc();
//			return;
			$this->templ->smarty->assign("month_from",$this->req->get('month_from'));
			$this->templ->smarty->assign("day_from",$this->req->get('day_from'));
			$this->templ->smarty->assign("month_to",$this->req->get('month_to'));
			$this->templ->smarty->assign("day",$this->req->get('day_to'));
			$this->templ->smarty->assign("staff_id",$this->req->get('staff_id'));
			$this->post_proc();
			// フォーム作成
			$use_shop_list = array();
			$sql = "select * from shop";
			$sql .= " order by shop_id";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$use_shop_list[$rs->fields('shop_id')] = $rs->fields('name');
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$this->templ->smarty->assign("use_shop_list",$use_shop_list);
			$age_list = $this->util->age_list();
			$campaign_list = $this->util->campaign_list();
			$hour_from_list = $this->util->camp_hour_list();
			$hour_to_list = $this->util->camp_hour_list();
			$day_list = $this->util->day_list2();
			$month_list = $this->util->month_list2();
			if($this->req->get_get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get_get('shop_id'));
			}
			else if($this->req->get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get('shop_id'));
			}
			
			$this->templ->smarty->assign("age_list",$age_list);
			$this->templ->smarty->assign("campaign_list",$campaign_list);
			$this->templ->smarty->assign("staff_list",$staff_list);
			// add 20160112 ご来店予定時間項目追加
			$v_schedule_hour_list = $this->util->schedule_hour_list();
			$v_schedule_min_list = $this->util->schedule_min_list();
			$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
			$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);
			$this->templ->smarty->assign("day_list",$day_list);
			$this->templ->smarty->assign("month_list",$month_list);
			$this->templ->smarty->assign("hour_from_list",$hour_from_list);
			$this->templ->smarty->assign("hour_to_list",$hour_to_list);
			$this->templ->smarty->assign("error",$error);
			$this->templ->smarty->display("oneday_admin/campaign_upd_input.html");
			exit;
		}
		// 入力データチェック
		$error = $this->datacheck_proc();
		if(!empty($error)){
			$this->templ->smarty->assign("month_from",$this->req->get('month_from'));
			$this->templ->smarty->assign("day_from",$this->req->get('day_from'));
			$this->templ->smarty->assign("month_to",$this->req->get('month_to'));
			$this->templ->smarty->assign("day",$this->req->get('day_to'));
			$this->templ->smarty->assign("staff_id",$this->req->get('staff_id'));
			$this->post_proc();
			// フォーム作成
			$use_shop_list = array();
			$sql = "select * from shop";
			$sql .= " order by shop_id";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				while(!$rs->EOF){
					$use_shop_list[$rs->fields('shop_id')] = $rs->fields('name');
					$rs->MoveNext();
				}
				$rs->Close();
			}
			$this->templ->smarty->assign("use_shop_list",$use_shop_list);
			$age_list = $this->util->age_list();
			$campaign_list = $this->util->campaign_list();
			$hour_from_list = $this->util->camp_hour_list();
			$hour_to_list = $this->util->camp_hour_list();
			$day_list = $this->util->day_list2();
			$month_list = $this->util->month_list2();
			if($this->req->get_get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get_get('shop_id'));
			}
			else if($this->req->get('shop_id')){
				$staff_list = $this->util->staff_list($this->DB,$this->req->get('shop_id'));
			}
			
			$this->templ->smarty->assign("age_list",$age_list);
			$this->templ->smarty->assign("campaign_list",$campaign_list);
			$this->templ->smarty->assign("staff_list",$staff_list);
			// add 20160112 ご来店予定時間項目追加
			$v_schedule_hour_list = $this->util->schedule_hour_list();
			$v_schedule_min_list = $this->util->schedule_min_list();
			$this->templ->smarty->assign("v_schedule_hour_list",$v_schedule_hour_list);
			$this->templ->smarty->assign("v_schedule_min_list",$v_schedule_min_list);
			$this->templ->smarty->assign("day_list",$day_list);
			$this->templ->smarty->assign("month_list",$month_list);
			$this->templ->smarty->assign("hour_from_list",$hour_from_list);
			$this->templ->smarty->assign("hour_to_list",$hour_to_list);
			$this->templ->smarty->assign("error",$error);
			$this->templ->smarty->display("oneday_admin/campaign_upd_input.html");
			exit;
		}
		if(!$this->req->get('upd_flg')){
			$record = null;
//			$record['staff_autono'] = $_SESSION['oneday']['staff_autono'];
//			$record['staff_name'] = $_SESSION['oneday']['staff_name'];
			$record['staff_autono'] = $this->req->get('staff_id');
			$sql = "select * from send_mail";
			$sql .= " where autono = ".$this->DB3->getQStr($this->req->get('staff_id'));
			$rs =& $this->DB3->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$record['staff_name'] = $rs->fields('staff_name');
				}
				$rs->Close();
			}
			$record['shop_id'] = $this->req->get('shop_id');
			$record['car_detail_id'] = $this->req->get('car_id');
			$record['area_id'] = $this->req->get('area_id');
			$record['date'] = $this->req->get('year')."-".$this->req->get('month')."-".$this->req->get('day');
			$record['pref_id'] = $this->req->get('pref_id');
			$record['age'] = $this->req->get('age');
			if(!$this->req->get('sei') and !$this->req->get('mei')){
				$sql = "select * from shop";
				$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$record['sei'] = $rs->fields('name');
					}
					$rs->Close();
				}
				$record['mei'] = $record['staff_name']."登録";
			}
			else{
				$record['sei'] = $this->req->get('sei');
				$record['mei'] = $this->req->get('mei');
			}
			$customer_name = $record['sei']." ".$record['mei']." 様";
			$record['sei_kana'] = $this->req->get('sei_kana');
			$record['mei_kana'] = $this->req->get('mei_kana');
			$postcd = "";
			if($this->req->get('postcd1') or $this->req->get('postcd2')){
				$postcd = $this->req->get('postcd1')."-".$this->req->get('postcd2');
			}
			$record['postcd'] = $postcd;
			$record['address'] = $this->req->get('address');
			$tel = "";
			if($this->req->get('tel1') or $this->req->get('tel2') or $this->req->get('tel3')){
				$tel = $this->req->get('tel1')."-".$this->req->get('tel2')."-".$this->req->get('tel3');
			}
			$record['tel'] = $tel;
			$k_tel = "";
			if($this->req->get('k_tel1') or $this->req->get('k_tel2') or $this->req->get('k_tel3')){
				$k_tel = $this->req->get('k_tel1')."-".$this->req->get('k_tel2')."-".$this->req->get('k_tel3');
			}
			$record['k_tel'] = $k_tel;
			$record['mail'] = $this->req->get('mail');
			$record['campaign'] = $this->req->get('campaign');
			// add 20151030 キャンペーン申込の理由 項目追加
			$record['reason'] = $this->req->get('reason');
			if($this->req->get('reason') == "99"){
				$record['reason99_text'] = $this->req->get('reason99_text');
			}
			// add 20160122 申込区分 項目追加
			$record['offer_kind'] = $this->req->get('offer_kind');
			$record['use_shop'] = $this->req->get('use_shop');
			$record['use_shop_staff'] = $this->req->get('use_shop_staff');
			// add 20160112 ご来店予定時間項目追加
			if($this->req->get('v_schedule_hour') and $this->req->get('v_schedule_min')){
				$record['v_schedule_time'] = date("H:i:s",mktime($this->req->get('v_schedule_hour'),$this->req->get('v_schedule_min'),0,1,1,date("Y")));
			}
			$record['comment'] = $this->req->get('comment');
			$record['temporary_flg'] = "2";
			$record['update_date'] = time();
			$record['create_date'] = time();
			// add 20140909
			$record['memo'] = $this->req->get('memo');
			if($this->req->get('hour_from')){
				$record['hour_from'] = (int)$this->req->get('hour_from');
			}
			if($this->req->get('hour_to')){
				$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year'));
				$date_to = mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year'));
				if($date_from == $date_to){
					$record['hour_to'] = (int)$this->req->get('hour_to');
				}
			}
			$con = $this->DB->getCon();
			$ret = $con->AutoExecute("reservation", $record, 'INSERT');
			$sql = "SELECT currval('reservation_autono_seq') as inid";
			$rs =& $this->DB->ASExecute($sql);
		
			$at = "";
			if($rs){
				if(!$rs->EOF){
					$at = $rs->fields('inid');
					$rs->Close();
				}
			}
//print_r($at);
			$record2 = null;
			$record2['disp_number'] = $this->number_proc($this->req->get('tel3'),$at);
			$number = $this->number_proc($this->req->get('tel3'),$at);
			$where  = " autono = ".$this->DB->getQStr($at);
//print_r($record);
//print_r($where);
			$con = $this->DB->getCon();
			$ret = $con->AutoExecute("reservation", $record2, 'UPDATE',$where);

			// 複数日予約
			$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year'));
			$date_to = mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year'));
			if($date_from <> $date_to){
				for($i = 1;$date_from < $date_to;$i++){
					$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year'));
					$record['disp_number'] = $record2['disp_number'];
					$record['date'] = date("Y-m-d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year')));
					$record['hour_from'] = NULL;
					if($date_from == $date_to){
						if($this->req->get('hour_to')){
							$record['hour_to'] = (int)$this->req->get('hour_to');
						}
					}
					$con = $this->DB->getCon();
					$ret = $con->AutoExecute("reservation", $record, 'INSERT');
				}
			}
			/******************/
			/* メール送信処理 */
			/******************/
			if($this->req->get('offer_kind') == 3){
				$min_year = date("Y",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year')));
				$min_month = date("m",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year')));
				$min_day = date("d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year')));
				$min_date = mktime(0,0,0,$min_month,$min_day,$min_year);
				$h_flg = "0";
				if($this->req->get('v_schedule_hour') and $this->req->get('v_schedule_min')){
					$v_schedule_time = date("Y年m月d日 H時i分",mktime($this->req->get('v_schedule_hour'),$this->req->get('v_schedule_min'),0,$min_month,$min_day,$min_year));
				}
				else{
					$v_schedule_time = date("Y年m月d日",mktime(0,0,0,$min_month,$min_day,$min_year));
				}
				$max_year = date("Y",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year')));
				$max_month = date("m",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year')));
				$max_day = date("d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year')));
				$max_date = mktime(0,0,0,$max_month,$max_day,$max_year);
				if($min_date != $max_date){
					if($this->req->get('hour_from')){
						if($this->req->get('hour_from') > 10){
							$h_flg = "1";
						}
						$reserve_date = date("Y年m月d日 H時～",mktime($this->req->get('hour_from'),0,0,$min_month,$min_day,$min_year));
						if($this->req->get('hour_to')){
							$reserve_date .= date("Y年m月d日 H時",mktime($this->req->get('hour_to'),0,0,$max_month,$max_day,$max_year));
						}
						else{
							$reserve_date .= date("Y年m月d日",mktime(0,0,0,$max_month,$max_day,$max_year));
						}
					}
					else{
						$reserve_date = date("Y年m月d日～",mktime(0,0,0,$min_month,$min_day,$min_year));
						if($this->req->get('hour_to')){
							$reserve_date .= date("Y年m月d日 H時",mktime($this->req->get('hour_to'),0,0,$max_month,$max_day,$max_year));
						}
						else{
							$reserve_date .= date("Y年m月d日",mktime(0,0,0,$max_month,$max_day,$max_year));
						}
					}
				}
				else{
					if($this->req->get('hour_from')){
						if($this->req->get('hour_from') > 10){
							$h_flg = "1";
						}
						$reserve_date = date("Y年m月d日 H時～",mktime($this->req->get('hour_from'),0,0,$min_month,$min_day,$min_year));
						if($this->req->get('hour_to')){
							$reserve_date .= date("H時",mktime($this->req->get('hour_to'),0,0,$min_month,$min_day,$min_year));
						}
					}
					else{
						$reserve_date = date("Y年m月d日",mktime(0,0,0,$min_month,$min_day,$min_year));
					}
				}
				$offer_kind = $this->util->offer_kind_list(1,$this->req->get('offer_kind'));
				//日本語メールを送る際に必要
				mb_language("Japanese");
				mb_internal_encoding("UTF-8");
				// SMTPサーバーの情報を連想配列にセット
				$params = array(
						"host" => HOST,
						"port" => PORT,
						"auth" => false,  // SMTP認証を使用する
						"username" => USERNAME,
						"password" => PASSWORD
				);
				// PEAR::Mailのオブジェクトを作成
				// ※バックエンドとしてSMTPを指定
				$mailObject = Mail::factory("sendmail", $params);

				// 送信先のメールアドレス
				//$recipients = "xxxxx_0213_xxxxx@yahoo.co.jp";
				$recipients = $this->req->get('mail');

				//$FromName = mb_encode_mimeheader(mb_convert_encoding(FROMMAILNAME,"JIS","UTF-8"));
				$FromName = mb_encode_mimeheader(FROMMAILNAME);
				
				$from= $FromName."<".FROMMAIL.">";

				// メールヘッダ情報を連想配列としてセット
				$headers = array(
					"From" => $from,
					"Subject" => mb_encode_mimeheader(TITLE2)
				);

//				$reserve_date = $this->req->get('year')."年".$this->req->get('month')."月".$this->req->get('day')."日";
				if($this->req->get('shop_id')){
					$shop_data = $this->util->shop_info_get($this->req->get('shop_id'),$this->DB);
				}
				if($this->req->get('car_id')){
					$sql = "select * from car_detail";
					$sql .= " where autono = ".$this->DB->getQStr($this->req->get('car_id'));
					$sql .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
					//$sql .= " and del_flg='0'";
					//$sql .= " and disp_flg='1'";
					$rs =& $this->DB->ASExecute($sql);
					if($rs){
						if(!$rs->EOF){
							$car_data = $this->util->car_info_get($rs->fields('autono'),$this->DB);
						}
						$rs->Close();
					}
				}
				// メール本文
				$body = str_replace("@number@",$number,BODY2);
				$body = str_replace("@shop_name1@",$shop_data['name'],$body);
				$body = str_replace("@shop_name2@",$shop_data['name'],$body);
				$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
				$body = str_replace("@reserve_date@",$reserve_date,$body);
				$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
				//$body .= $this->req->get('mail');

				// 日本語なのでエンコード
				$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");

				// sendメソッドでメールを送信
				$mailObject->send($recipients, $headers, $body);

				/*************************************/
				/* メール送信処理  本部・店舗スタッフ*/
				/*************************************/
				// 本部・店舗スタッフメールアドレス取得

				$mail_address = $this->util->send_mail($this->DB3,$this->req->get('shop_id'));
				// メールヘッダ情報を連想配列としてセット
				$headers = array(
						"From" => $from,
						"Subject" => mb_encode_mimeheader(TITLE4)
				);
				if($mail_address){
					if(is_array($mail_address)){
						$recipients = "";
						foreach($mail_address as $key => $val){
							//$recipients = "kyushu.mazda.test@gmail.com";
							//あとで↓を有効にする。↑は消す。
							if($recipients){
								$recipients .= ",".$val;
							}
							else{
								$recipients = $val;
							}
						}
					}
					$body = str_replace("@number@",$number,BODY4);
					$body = str_replace("@shop_name1@",$shop_data['name'],$body);
					$body = str_replace("@shop_name2@",$shop_data['name'],$body);
					$body = str_replace("@shop_tel@",$shop_data['tel'],$body);
					$body = str_replace("@reserve_date@",$reserve_date,$body);
					$body = str_replace("@reserve_car@",$car_data['name']." ".$car_data['name2']." ".$car_data['name3'],$body);
					$body = str_replace("@customer_name@",$customer_name,$body);
					$body = str_replace("@v_schedule_time@",$v_schedule_time,$body);
					$body = str_replace("@offer_kind@",$offer_kind,$body);
					$body = str_replace("@h_flg@",$h_flg,$body);
					//$body = str_replace("@staff_name@",$val,$body);
					//$body .= $val;
					// 日本語なのでエンコード
					$body = mb_convert_encoding($body, "ISO-2022-JP", "UTF-8");
					// sendメソッドでメールを送信
					$mailObject->send($recipients, $headers, $body);
				}
			}
		}
		else if($_SESSION['admin']['disp_number']){
			$record = null;
			//$record['staff_autono'] = $_SESSION['oneday']['staff_autono'];
			//$record['staff_name'] = $_SESSION['oneday']['staff_name'];
			$record['staff_autono'] = $this->req->get('staff_id');
			$sql = "select * from send_mail";
			$sql .= " where autono = ".$this->DB3->getQStr($this->req->get('staff_id'));
			$rs =& $this->DB3->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$record['staff_name'] = $rs->fields('staff_name');
				}
				$rs->Close();
			}
			$record['shop_id'] = $this->req->get('shop_id');
			$record['car_detail_id'] = $this->req->get('car_id');
			$record['area_id'] = $this->req->get('area_id');
			$record['area_id'] = $this->req->get('area_id');
			$record['pref_id'] = $this->req->get('pref_id');
			$record['disp_number'] = $_SESSION['admin']['disp_number'];
			$record['age'] = $this->req->get('age');
			if(!$this->req->get('sei') and !$this->req->get('mei')){
				$sql = "select * from shop";
				$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$record['sei'] = $rs->fields('name');
					}
					$rs->Close();
				}
				$record['mei'] = $record['staff_name']."登録";
			}
			else{
				$record['sei'] = $this->req->get('sei');
				$record['mei'] = $this->req->get('mei');
			}
			$record['sei_kana'] = $this->req->get('sei_kana');
			$record['mei_kana'] = $this->req->get('mei_kana');
			$postcd = $this->req->get('postcd1')."-".$this->req->get('postcd2');
			$record['postcd'] = $postcd;
			$record['address'] = $this->req->get('address');
			$tel = $this->req->get('tel1')."-".$this->req->get('tel2')."-".$this->req->get('tel3');
			$record['tel'] = $tel;
			$k_tel = $this->req->get('k_tel1')."-".$this->req->get('k_tel2')."-".$this->req->get('k_tel3');
			$record['k_tel'] = $k_tel;
			$record['mail'] = $this->req->get('mail');
			$record['campaign'] = $this->req->get('campaign');
			// add 20151030 キャンペーン申込の理由 項目追加
			$record['reason'] = $this->req->get('reason');
			if($this->req->get('reason') == "99"){
				$record['reason99_text'] = $this->req->get('reason99_text');
			}
			// add 20160122 申込区分 項目追加
			$record['offer_kind'] = $this->req->get('offer_kind');
			$record['use_shop'] = $this->req->get('use_shop');
			$record['use_shop_staff'] = $this->req->get('use_shop_staff');
			// add 20160112 ご来店予定時間項目追加
			if($this->req->get('v_schedule_hour') and $this->req->get('v_schedule_min')){
				$record['v_schedule_time'] = date("H:i:s",mktime($this->req->get('v_schedule_hour'),$this->req->get('v_schedule_min'),0,1,1,date("Y")));
			}
			$record['comment'] = $this->req->get('comment');
			$record['temporary_flg'] = "2";
			$record['update_date'] = time();
			$record['create_date'] = time();
			// add 20140909
			$record['memo'] = $this->req->get('memo');
//			$record['date'] = $this->req->get('year')."-".$this->req->get('month')."-".$this->req->get('day');
			$record['date'] = date("Y-m-d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year')));
			if($this->req->get('hour_from')){
				$record['hour_from'] = (int)$this->req->get('hour_from');
			}
			$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from'),$this->req->get('year'));
			$date_to = mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year'));
			if($date_from == $date_to){
				if($this->req->get('hour_to')){
					$record['hour_to'] = (int)$this->req->get('hour_to');
				}
			}
			$where  = " disp_number = '".$this->DB->getQStr($_SESSION['admin']['disp_number'])."' ";
			$con = $this->DB->getCon();
			$con->AutoExecute("reservation", array('disp_flg' => 0, 'del_flg' => 1, 'temporary_flg' => 0), 'UPDATE', $where);
			$ret = $con->AutoExecute("reservation", $record, 'INSERT');

			for($i = 1;$date_from < $date_to;$i++){
				$date_from = mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year'));
				$record['date'] = date("Y-m-d",mktime(0,0,0,$this->req->get('month_from'),$this->req->get('day_from')+$i,$this->req->get('year')));
				if ($i === 0) {

				} elseif ($date_from == $date_to){
					if($this->req->get('hour_to')){
						$record['hour_from'] = NULL;
						$record['hour_to'] = (int)$this->req->get('hour_to');
					}
				}
				else{
					$record['hour_from'] = NULL;
					$record['hour_to'] = NULL;
				}
				$con = $this->DB->getCon();
				$ret = $con->AutoExecute("reservation", $record, 'INSERT');
			}
		}
		// 完了画面へ
		header("Location:./campaign_upd.php?mode=end&ret=".$this->req->get('ret'));
	}
	
	//予約番号自動生成
	function number_proc($tel3,$number){
		$number = "od".$tel3.$number;
		return $number;
	}
	
	// 予約情報入力(完了)
	function end_proc(){
		if($this->req->get_get('ret') == "campaign"){
			header("Location:./campaign_list.php");
		}
		else{
			header("Location:./camp_cal.php");
		}
	}

}

?>