<?php
ini_set("memory_limit", "512M");
ini_set("max_execution_time", "600");

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

define(DISP_YM,'201912');

//if($_SERVER['REMOTE_ADDR'] != "219.119.179.4"){
//exit;
//}
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{

	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;

	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			case 'search':
				$this->search_proc();
			break;
			case 'download':
				$this->download_proc();
			break;
			case 'station_dl':
				$this->station_dl_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function form_make(){
		if($_SESSION['oneday']['access_kb'] == 1){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 2){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 3){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
		}
		else if($_SESSION['oneday']['access_kb'] == '4' or $_SESSION['oneday']['access_kb'] == '9'){
			$station_list = $this->util->station_list_get("0","0",$this->DB);
//			if($this->req->get('station')){
				$_SESSION['search']['station'] = $this->req->get('station');
//			}
/*
			if(!$_SESSION['search']['station']){
				foreach($station_list as $key => $val){
					$_SESSION['search']['station'] = $key;
					break;
				}
			}
*/
			$this->templ->smarty->assign('station',$_SESSION['search']['station']);
			$this->templ->smarty->assign('station_list',$station_list);
		}
		if($_SESSION['oneday']['access_kb'] != 1 and $_SESSION['oneday']['access_kb'] != 2){
			$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
//			if($this->req->get('shop')){
				$_SESSION['search']['shop'] = $this->req->get('shop');
//			}
/*
			if(!$_SESSION['search']['shop'] or $this->req->get('type') == 'station_change'){
				foreach($shop_list as $key => $val){
					$_SESSION['search']['shop'] = $key;
					break;
				}
			}
*/
			if($_SESSION['search']['shop']){
				$sql = "select * from shop";
				$sql .= " where shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$_SESSION['search']['station'] = $rs->fields('station_code');
						$this->templ->smarty->assign('station',$_SESSION['search']['station']);
						$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
					}
					$rs->Close();
				}
			}
			$this->templ->smarty->assign('shop',$_SESSION['search']['shop']);
			$this->templ->smarty->assign('shop_list',$shop_list);
		}
//		$car_list = array();
		// 車種
//		$sql = "select car_id from car_detail ";
//		$sql .= " where shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."'";
/*
		if(!$_SESSION['search']['car_disp_flg']){
			$sql .= " and del_flg='0' ";
			$sql .= " and disp_flg='1'";
		}
		$sql .= " group by car_id";
		$sql .= " order by car_id";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql = "select * from car";
				$sql .= " where car_id='".$this->DB->getQStr($rs->fields('car_id'))."'";
				$rs2 =& $this->DB->ASExecute($sql);
				if($rs2){
					if(!$rs2->EOF){
						$car_list[$rs2->fields('car_id')] = $rs2->fields('name')." ".$rs2->fields('name2')." ".$rs2->fields('name3');
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("car", $_SESSION['search']['car']);
		$this->templ->smarty->assign("car_list", $car_list);
*/
		$this->templ->smarty->assign("year_list",$this->util->year_list());
		$this->templ->smarty->assign("month_list",$this->util->month_list());
//		$this->templ->smarty->assign("day_list",$this->util->day_list());
	}

	//一覧
	function default_proc(){
		$_SESSION['search']['car'] = "";
		$_SESSION['search']['year'] = "";
		$_SESSION['search']['month'] = "";
		$_SESSION['search']['day'] = "";
		$_SESSION['search']['station'] = "";
		$_SESSION['search']['shop'] = "";
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = "";
/*
			$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
			if($shop_list){
				if(is_array($shop_list)){
					foreach($shop_list as $key => $val){
						$_SESSION['search']['shop'] = $key;
						break;
					}
				}
			}
*/
		}
		if($this->req->get('type') == 'shop_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
		}
		if($this->req->get('type') == 'search'){
			$_SESSION['search']['station'] = $this->req->get('station');
			$_SESSION['search']['shop'] = $this->req->get('shop');
//			$_SESSION['search']['car'] = $this->req->get('car');
			$_SESSION['search']['year'] = $this->req->get('year');
			$_SESSION['search']['month'] = $this->req->get('month');
		}
		// 20180720 0件車種対応
		if($this->req->get('type') == 'car_disp'){
			if(!$_SESSION['search']['car_disp_flg']){
				$_SESSION['search']['car_disp_flg'] = true;
			}
			else{
				$_SESSION['search']['car_disp_flg'] = false;
			}
		}
		$this->templ->smarty->assign("car_disp_flg", $_SESSION['search']['car_disp_flg']);
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == "3"){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
		}
		if($this->req->get('year')){
			$_SESSION['search']['year'] = $this->req->get('year');
		}
		else if($this->req->get_get('year')){
			$_SESSION['search']['year'] = $this->req->get_get('year');
		}
		if($this->req->get('month')){
			$_SESSION['search']['month'] = $this->req->get('month');
		}
		else if($this->req->get_get('month')){
			$_SESSION['search']['month'] = $this->req->get_get('month');
		}
		if(!$_SESSION['search']['year']){
			$_SESSION['search']['year'] = date("Y");
		}
		if(!$_SESSION['search']['month']){
			$_SESSION['search']['month'] = date("m");
		}
		// 仮
//		if(!$_SESSION['search']['shop']){
//			$_SESSION['search']['shop'] = "807";
//		}
		$this->templ->smarty->assign("year", $_SESSION['search']['year']);
		$this->templ->smarty->assign("month", $_SESSION['search']['month']);
		
		$before_year = date("Y",mktime(0,0,0,$_SESSION['search']['month'] - 1,1,$_SESSION['search']['year']));
		$before_month = date("m",mktime(0,0,0,$_SESSION['search']['month'] - 1,1,$_SESSION['search']['year']));
		$next_year = date("Y",mktime(0,0,0,$_SESSION['search']['month'] + 1,1,$_SESSION['search']['year']));
		$next_month = date("m",mktime(0,0,0,$_SESSION['search']['month'] + 1,1,$_SESSION['search']['year']));

		$before_date = date("Ym",mktime(0,0,0,$before_month,1,$before_year));
		if($before_date < "201806"){
			$this->templ->smarty->assign("not_before", 1);
		}
		$next_date = date("Ym",mktime(0,0,0,$next_month,1,$next_year));

		if (date("d") >= 25) {
			$max_time = date("Ym", mktime(0, 0, 0, date("m") + 2, date("d"), date("Y")));
		} else {
			$max_time = date("Ym", mktime(0, 0, 0, date("m") + 1, date("d"), date("Y")));
		}

		if ($next_date > $max_time) {
			$this->templ->smarty->assign("not_after", 1);
			$this->templ->smarty->assign("bef_link_del_flg","1");
		}

		$day_array = array();
		$j = date("N",mktime(0, 0, 0, $_SESSION['search']['month'],1, $_SESSION['search']['year']));
		$end_day = date("d",mktime(0, 0, 0, $_SESSION['search']['month']+1,0, $_SESSION['search']['year']));
		$j = $j-1;
		for($i=1;$i<=$end_day;$i++){
			$d = date("d",mktime(0, 0, 0, $_SESSION['search']['month'],$i, $_SESSION['search']['year']));
			$day_array[$j] = (int)$d;
			$j++;
		}
		$day_array2 = array();
		$j = date("N",mktime(0, 0, 0, $_SESSION['search']['month']+1,1, $_SESSION['search']['year']));
		$end_day2 = date("d",mktime(0, 0, 0, $_SESSION['search']['month']+2,0, $_SESSION['search']['year']));
		$j = $j-1;
		for($i=1;$i<=$end_day2;$i++){
			$d = date("d",mktime(0, 0, 0, $_SESSION['search']['month']+1,$i, $_SESSION['search']['year']));
			$day_array2[$j] = (int)$d;
			$j++;
		}
		$this->templ->smarty->assign( 'Common_year',$_SESSION['search']['year']);
		$this->templ->smarty->assign( 'Common_month',$_SESSION['search']['month'] );
		$this->templ->smarty->assign( 'Common_day',$day_array );
		$this->templ->smarty->assign( 'Common_next_year',$next_year );
		$this->templ->smarty->assign( 'Common_next_month',$before_month );
		$this->templ->smarty->assign( 'Common_next_day',$day_array2 );
		$this->templ->smarty->assign( 'Common_back_year',$before_year );
		$this->templ->smarty->assign( 'Common_back_month',$before_month );

		$this->templ->smarty->assign("before_year", $before_year);
		$this->templ->smarty->assign("before_month", $before_month);
		$this->templ->smarty->assign("next_year", $next_year);
		$this->templ->smarty->assign("next_month", $next_month);
		$this->search_proc($day_array,false);
		$this->search_proc($day_array2,true);
		$this->form_make();
		$this->templ->smarty->display("oneday_admin/camp_cal.html");
		exit;
	}

	//絞り込み
	function search_proc($day_array,$next_flg=false){
		$week = array("日", "月", "火", "水", "木", "金", "土");
		$zaiko_list = array();
		$car_detail_list = array();
		$car_list = array();
		$sql = "select distinct(car1) as car1,min(start_date) as start_date,max(end_date) as end_date from car_detail";
		if($next_flg){
			$from_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month']+1,1,$_SESSION['search']['year']));
			$to_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month']+2,0,$_SESSION['search']['year']));
			$sql .= " where ((start_date <= '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and end_date >= '".$this->DB->getQStr($to_date)."') ";
			$sql .= " or (start_date between '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and '".$this->DB->getQStr($to_date)."') ";
			$sql .= " or (end_date between '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and '".$this->DB->getQStr($to_date)."')) ";
		}
		else{
			$from_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'],1,$_SESSION['search']['year']));
			$to_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month']+1,0,$_SESSION['search']['year']));
			$sql .= " where ((start_date <= '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and end_date >= '".$this->DB->getQStr($to_date)."') ";
			$sql .= " or (start_date between '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and '".$this->DB->getQStr($to_date)."') ";
			$sql .= " or (end_date between '".$this->DB->getQStr($from_date)."' ";
			$sql .= " and '".$this->DB->getQStr($to_date)."')) ";
		}
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		else if($_SESSION['oneday']['access_kb'] == "3"){
			$shop_search_flg = true;
			if($_SESSION['search']['shop']){
				$sql = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
				$shop_search_flg = false;
			}
			if($shop_search_flg){
				$shop_sql = "";
				$sql2 = "select * from shop";
				$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
				$sql2 .= " and del_flg = '0'";
				$sql2 .= " and disp_flg = '1'";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$sql .= $shop_sql;
				}
			}
		}
		else if($_SESSION['search']['shop']){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		else if($_SESSION['search']['station']){
			$shop_sql = "";
			$sql2 = "select * from shop";
			$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
			$sql2 .= " and del_flg = '0'";
			$sql2 .= " and disp_flg = '1'";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				while(!$rs2->EOF){
					if($shop_sql){
						$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					else{
						$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					$rs2->MoveNext();
				}
				$rs2->Close();
			}
			if($shop_sql){
				$shop_sql .= ")";
				$sql .= $shop_sql;
			}
		}
		$sql .= " and del_flg = '0'";
		$sql .= " group by car1";
		$sql .= " order by car1,start_date,end_date";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql2 = "select name from car";
				$sql2 .= " where car1 = '".$rs->fields('car1')."'";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					if(!$rs2->EOF){
						$car_list[$rs->fields('car1')]['start_date'] = $rs->fields('start_date');
						$car_list[$rs->fields('car1')]['end_date'] = $rs->fields('end_date');
						$car_list[$rs->fields('car1')]['car1'] = $rs->fields('car1');
						$car_list[$rs->fields('car1')]['carname'] = $rs2->fields('name');
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if($day_array){
			if(is_array($day_array)){
				foreach($day_array as $key => $val){
					foreach($car_list as $key2 => $val2){
						if($next_flg){
							$year = date("Y",mktime(0,0,0,$_SESSION['search']['month']+1,$val,$_SESSION['search']['year']));
							$month = date("m",mktime(0,0,0,$_SESSION['search']['month']+1,$val,$_SESSION['search']['year']));
							$comp_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month']+1,$val,$_SESSION['search']['year']));
						}
						else{
							$year = $_SESSION['search']['year'];
							$month = $_SESSION['search']['month'];
							$comp_date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'],$val,$_SESSION['search']['year']));
						}
						$day = $val;
						if($this->util->holiday_list($this->DB,"807",1,date("Y-m-d",mktime(0,0,0,$month,$day,$year)))){
							$data = array();
							$data['day'] = $val;
							$data['pday'] = "定休日";
							$zaiko_list[$key]['pday'] = $data;
						}
						else if($comp_date >= $car_list[$key2]['start_date'] and $comp_date <= $car_list[$key2]['end_date']){
							$zaiko_list[$key][$key2]['day'] = $val;
							$zaiko_list[$key][$key2]['carname'] = $car_list[$key2]['carname'];
							$zaiko_list[$key][$key2]['car1'] = $car_list[$key2]['car1'];
						}
					}
				}
			}
		}
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//print_r($zaiko_list);
}
//print_r($car_detail_list);
		// ステータス取得
		if($zaiko_list){
			if(is_array($zaiko_list)){
				foreach($zaiko_list as $key => $val){
					$status_array = array();
					foreach($zaiko_list[$key] as $key2 => $val2){
						if($key2 == "pday"){
							break;
						}
						if($next_flg){
							$date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month']+1,$zaiko_list[$key][$key2]['day'],$_SESSION['search']['year']));
						}
						else{
							$date = date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'],$zaiko_list[$key][$key2]['day'],$_SESSION['search']['year']));
						}
						$status = "1";
//						if($_SESSION['search']['car_disp_flg']){
							if($zaiko_list[$key][$key2]['status'] == '4'){
								$status = "4";
							}
//						}
						$conf_flg = false;
						$result_data_flg = true;
						$reserve_num = 0;
						$sql = "select * from reservation";
						$sql .= " where car1 = '".$this->DB->getQStr($zaiko_list[$key][$key2]['car1'])."'";
						$sql .= " and ((conf_date = '".$this->DB->getQStr($date)."' and conf_ampm = '1')";
						$sql .= " or (date = '".$this->DB->getQStr($date)."' and ampm = '1')";
						$sql .= " or (date2 = '".$this->DB->getQStr($date)."' and ampm2 = '1')";
						$sql .= " or (date3 = '".$this->DB->getQStr($date)."' and ampm3 = '1')";
						$sql .= " or (conf_date = '".$this->DB->getQStr($date)."' and conf_ampm = '2')";
						$sql .= " or (date = '".$this->DB->getQStr($date)."' and ampm = '2')";
						$sql .= " or (date2 = '".$this->DB->getQStr($date)."' and ampm2 = '2')";
						$sql .= " or (date3 = '".$this->DB->getQStr($date)."' and ampm3 = '2'))";
						if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
							$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
						}
						else if($_SESSION['oneday']['access_kb'] == "3"){
							$shop_search_flg = true;
							if($_SESSION['search']['shop']){
								$sql = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
								$shop_search_flg = false;
							}
							if($shop_search_flg){
								$shop_sql = "";
								$sql2 = "select * from shop";
								$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
								$sql2 .= " and del_flg = '0'";
								$sql2 .= " and disp_flg = '1'";
								$rs2 =& $this->DB->ASExecute($sql2);
								if($rs2){
									while(!$rs2->EOF){
										if($shop_sql){
											$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
										}
										else{
											$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
										}
										$rs2->MoveNext();
									}
									$rs2->Close();
								}
								if($shop_sql){
									$shop_sql .= ")";
									$sql .= $shop_sql;
								}
							}
						}
						else if($_SESSION['search']['shop']){
							$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
						}
						else if($_SESSION['search']['station']){
							$shop_sql = "";
							$sql2 = "select * from shop";
							$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
							$sql2 .= " and del_flg = '0'";
							$sql2 .= " and disp_flg = '1'";
							$rs2 =& $this->DB->ASExecute($sql2);
							if($rs2){
								while(!$rs2->EOF){
									if($shop_sql){
										$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
									}
									else{
										$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
									}
									$rs2->MoveNext();
								}
								$rs2->Close();
							}
							if($shop_sql){
								$shop_sql .= ")";
								$sql .= $shop_sql;
							}
						}
						$sql .= " and del_flg = '0'";
						$sql .= " and disp_flg = '1'";
						$rs =& $this->DB->ASExecute($sql);
						$find_flg = false;
						if($rs){
							while(!$rs->EOF){
								if($rs->fields('conf_flg') == '1'){
									if($date == $rs->fields('conf_date')){
										$reserve_num++;
										$conf_flg = true;
									}
									if(!$rs->fields('visit_flg') or !$rs->fields('result_close')){
										$result_data_flg = false;
									}
									$cnt = 1;
								}
								else{
									$status = "2";
									$result_data_flg = false;
									$conf_flg = false;
									$cnt = count($car_detail_list[$zaiko_list[$key][$key2]['car1'].$rs->fields('car2')]);
									if($date == $rs->fields('date') and $rs->fields('ampm') == '1'){
										$reserve_num++;
									}
									if($date == $rs->fields('date2') and $rs->fields('ampm2') == '1'){
										$reserve_num++;
									}
									if($date == $rs->fields('date3') and $rs->fields('ampm3') == '1'){
										$reserve_num++;
									}
									if($date == $rs->fields('date') and $rs->fields('ampm') == '2'){
										$reserve_num++;
									}
									if($date == $rs->fields('date2') and $rs->fields('ampm2') == '2'){
										$reserve_num++;
									}
									if($date == $rs->fields('date3') and $rs->fields('ampm3') == '2'){
										$reserve_num++;
									}
								}
								$rs->MoveNext();
							}
							$rs->Close();
						}
						if($status == '1' and $conf_flg){
							if($result_data_flg){
								$status = "5";
							}
							else{
								$status = "3";
							}
						}
						$car_cnt = count($car_detail_list[$zaiko_list[$key][$key2]['car1']]);
						if($_SESSION['search']['car_disp_flg']){
							$zaiko_list[$key][$key2]['reserve_num'] = $reserve_num;
							$zaiko_list[$key][$key2]['status'] = $status;
						}
						else{
							if($reserve_num > 0){
								$zaiko_list[$key][$key2]['reserve_num'] = $reserve_num;
								$zaiko_list[$key][$key2]['status'] = $status;
							}
							else{
								unset($zaiko_list[$key][$key2]);
							}
						}
					}
				}
			}
		}
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//print_r($zaiko_list);
}
		if($next_flg){
			$this->templ->smarty->assign("data_list2", $zaiko_list);
		}
		else{
			$this->templ->smarty->assign("data_list", $zaiko_list);
		}
		if($_SESSION['oneday']['access_kb'] == "3"){
			$sql = "select * from shop";
			$sql .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
			$sql .= " order by shop_id limit 1";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$this->templ->smarty->assign("station_val", $rs->fields('station_name'));
				}
				$rs->Close();
			}
		}
	}

	function download_proc(){
		$csv_date = "";
		
		$week = array("日", "月", "火", "水", "木", "金", "土");
		// add 20140702 車種・店舗情報取得
		$car_data = $this->get_car();
		$shop_data = $this->get_shop();

		//エラー処理が必要
		//店舗だけが選択されていた場合、店舗車種が選択されていた場合などを後で考慮して作る
		
		//検索
		$zaiko_list = array();
		//在庫リスト
		$sql = "select a.autono as autono,a.shop_id as shop_id,a.car_id as car_id,b.date,a.week_flg as week_flg,a.no_plate as no_plate,a.car_no as car_no,a.price as price,a.grade as grade,a.model as model";
		$sql .= ",c.temporary_flg as temporary_flg,c.disp_number as disp_number,c.sei as sei,c.mei as mei,c.tel as tel,c.mail as mail,c.comment as comment,c.update_date as update_date";
		$sql .= ",c.area_id as area_id,c.sei_kana as sei_kana,c.mei_kana as mei_kana,c.mei as mei,c.age as age,c.address as address,c.k_tel as k_tel,c.campaign as campaign,c.pref_id as pref_id";
		$sql .= ",c.hour_from as hour_from,c.hour_to as hour_to,c.ref as ref,c.reason as reason,c.reason99_text as reason99_text";
		$sql .= ",c.campaign1 as campaign1,c.campaign2 as campaign2,c.campaign3 as campaign3,c.campaign4 as campaign4,c.campaign5 as campaign5,c.campaign6 as campaign6,c.campaign7 as campaign7,c.campaign8 as campaign8,c.campaign9 as campaign9,c.campaign10 as campaign10";
		$sql .= " from (zaiko b LEFT JOIN car_detail a ON a.autono = b.car_detail_id) LEFT JOIN reservation c ON c.car_detail_id = a.autono and c.date = b.date and c.shop_id = a.shop_id and c.temporary_flg ='2' and c.disp_flg='1' and c.del_flg='0' ";
		//$sql .= " from car_detail a,zaiko b ";
		$sql .= " where a.autono = b.car_detail_id";
		$sql .= " and a.del_flg='0'";
		//$sql .= " and a.del_flg='0' and a.disp_flg='1'";
		$sql .= " and b.del_flg='0' and b.disp_flg='1'";
		//$sql .= " and a.propriety = '○'";
		$sql .= " and a.shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		if($_SESSION['search']['car']){
			$sql .= " and a.car_id='".$this->DB->getQStr(substr($_SESSION['search']['car'],0,8))."'";
			$sql .= " and a.week_flg='".$this->DB->getQStr(substr($_SESSION['search']['car'],9,1))."'";
		}
		
		if($_SESSION['search']['day']){
			$sql .= " and b.date = '".$this->DB->getQStr($_SESSION['search']['year'])."-".$this->DB->getQStr($_SESSION['search']['month'])."-".$this->DB->getQStr($_SESSION['search']['day'])."' ";
		}
		else{
			$sql .= " and b.date >= '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'],1,$_SESSION['search']['year'])))."' ";
			$sql .= " and b.date < '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'] + 1,1,$_SESSION['search']['year'])))."' ";
		}
		$sql .= " order by b.date,a.car_id";
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."\r\n";
}
		$rs =& $this->DB->ASExecute($sql);
		
		$prev_date = "";

		if($rs){
			while(!$rs->EOF){
				$data = array();
				if($rs->fields('hour_from') and $rs->fields('hour_to')){
					$data['hour'] = $rs->fields('hour_from')."-".$rs->fields('hour_to');
				}
				else{
					$data['hour'] = "";
				}
				$data['no_plate'] = $rs->fields('no_plate');
				$data['car_no'] = $rs->fields('car_no');
				$price = "";
				if($rs->fields('price')){
					$price = str_replace(",","",$rs->fields('price'));
					if(is_numeric($price)){
						$price = number_format($price);
					}
				}
				$data['price'] = $price;
				$data['grade'] = $rs->fields('grade');
				$data['model'] = $rs->fields('model');
				$data['date'] = $rs->fields('date');
				$data['date2'] = $rs->fields('date');

				if( $prev_date == $rs->fields('date') ){
					$data['date'] = "";
				}
				if( $data['date'] ){
					$prev_date = $data['date'];
				}
//echo $zaiko_list['date']."<BR>";
				$time = strtotime($rs->fields('date'));
				$w = date("w", $time);
				$data['weekday'] = $week[$w];
				/*
				$week_name = "（休日）";
				if($rs->fields('week_flg') == 1){
					$week_name = "（平日）";
				}
				*/
				$week_flg = $rs->fields('week_flg');
				
				$data['car_id'] = $rs->fields('autono');
				$data['shop_id'] = $rs->fields('shop_id');
				
				// add 20140702 車種名取得
				$data['carname'] = $car_data[$rs->fields('car_id')]['carname'];
				$data['carname2'] = $car_data[$rs->fields('car_id')]['carname2'];
				$data['carname3'] = $car_data[$rs->fields('car_id')]['carname3'];
				// add 20140702 店舗情報取得
				$data['pref_id'] = $shop_data[$rs->fields('shop_id')]['pref_id'];
				$data['area_id'] = $shop_data[$rs->fields('shop_id')]['area_id'];
				$data['shop_name'] = $shop_data[$rs->fields('shop_id')]['shop_name'];

				if($week_flg == 1){
					if($this->util->pday_list(1,date("Y-m-d",mktime(0,0,0, date("m", $time),date("d", $time),date("Y", $time))))){
		        		$rs->MoveNext();
						continue;
		        	}
					if($w == 0 or $w == 6){
		        		$rs->MoveNext();
						continue;
					}
				}
				if($this->util->holiday_list($this->DB,$rs->fields('shop_id'),1,date("Y-m-d",mktime(0,0,0,  date("m", $time),date("d", $time),date("Y", $time))))){
					$rs->MoveNext();
					continue;
				}
				if($this->exception_proc($rs->fields('shop_id'),$rs->fields('autono'),date("Y-m-d",mktime(0,0,0,  date("m", $time),date("d", $time),date("Y", $time))))){
					$rs->MoveNext();
					continue;
				}
				// upd 20140702
				$data['disp_number'] = "";
				$data['name'] = "";
				$data['tel'] = "";
				$data['mail'] = "";
				$data['temporary_flg'] = "";
				$data['comment'] = "";
				$data['temporary_flg'] = $rs->fields('temporary_flg');
				if($rs->fields('temporary_flg') == '1'){
					$data['disp_number'] = "仮予約中";
				}
				if($rs->fields('temporary_flg') == '2'){
					$data['disp_number'] = $rs->fields('disp_number');
					$data['name'] = $rs->fields('sei')."　".$rs->fields('mei');
					$data['tel'] = $rs->fields('tel');
					$data['mail'] = $rs->fields('mail');
					$data['comment'] = "なし";
					if( $rs->fields('comment') ){
						$data['comment'] = "あり";
					}
					$data['entry_date'] = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('update_date'),5,2),substr($rs->fields('update_date'),8,2),substr($rs->fields('update_date'),0,4)));
				}
				
				$data['shop_id'] = $rs->fields('shop_id');
				$data['area_id'] = $rs->fields('area_id');
				$data['sei'] = $rs->fields('sei');
				$data['mei'] = $rs->fields('mei');
				$data['sei_kana'] = $rs->fields('sei_kana');
				$data['mei_kana'] = $rs->fields('mei_kana');
				$data['age'] = $rs->fields('age');
				$data['address'] = $rs->fields('address');
				$data['tel'] = $rs->fields('tel');
				$data['k_tel'] = $rs->fields('k_tel');
				$data['mail'] = $rs->fields('mail');
				$data['comment'] = $rs->fields('comment');
				$data['campaign'] = $this->util->campaign_list(1,$rs->fields('campaign'));
				$data['disp_number'] = $rs->fields('disp_number');
				$data['pref_id'] = $rs->fields('pref_id');
				$data['campaign1'] = $rs->fields('campaign1');
				$data['campaign2'] = $rs->fields('campaign2');
				$data['campaign3'] = $rs->fields('campaign3');
				$data['campaign4'] = $rs->fields('campaign4');
				$data['campaign5'] = $rs->fields('campaign5');
				$data['campaign6'] = $rs->fields('campaign6');
				$data['campaign7'] = $rs->fields('campaign7');
				$data['campaign8'] = $rs->fields('campaign8');
				$data['campaign9'] = $rs->fields('campaign9');
				$data['campaign10'] = $rs->fields('campaign10');
				$data['ref'] = $rs->fields('ref');
				$data['reason'] = $this->util->reason_list(1,$rs->fields('reason'));
				$data['reason99_text'] = "";
				if($rs->fields('reason') == 99){
					$data['reason99_text'] = $rs->fields('reason99_text');
				}
				$d = '"'.$data['shop_name'].'","'.$data['date2'].'","'.$data['weekday'].'","'.$data['carname'].'","'.$data['carname2'].'","'.$data['carname3'].'","'.
				$data['hour'].'","'.$data['sei'].'","'.$data['mei'].'","'.$data['sei_kana'].'","'.$data['mei_kana'].'","'.$data['age'].'","'.$data['address'].'","'.$data['tel'].'","'.
				$data['k_tel'].'","'.$data['mail'].'","'.$data['comment'].'","'.$data['campaign'].'","'.$data['reason'].'","'.$data['reason99_text'].'","'.
				$data['disp_number'].'","'.$data['entry_date'].'","'.$data['no_plate'].'","'.$data['car_no'].'","'.$data['grade'].'","'.$data['model'].'","'.$data['price'].'","'.$data['ref'].'"'."\r\n";
				$csv_date .= $d;
				$zaiko_list[] = $data;
				$rs->MoveNext();
			}
			$rs->Close();
		}

		$car = "";
		if($_SESSION['search']['car']){
			$car = substr($_SESSION['search']['car'],0,8)."_";
		}
		$day = "";
		if($_SESSION['search']['day']){
			$day = "_".$_SESSION['search']['day'];
		}
		
		$file_name = $_SESSION['search']['shop']."_".$car.$_SESSION['search']['year']."_".$_SESSION['search']['month'].$day;
		
		header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/x-csv");
		header("Content-Disposition: attachment; filename=".$file_name.".csv"); 

		$csv = '"店舗","日付","曜日","車種","","","時間","姓","名","セイ","メイ","年齢","住所","電話番号","携帯電話番号","メールアドレス","意見・要望","キャンペーン","キャンペーン申込の理由","キャンペーン申込の理由-その他","予約番号","予約日","登録番号","車体番号","グレード","型式","本体価格","キャンペーンID"'."\r\n";
		$csv .= $csv_date;
		echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
	}
	
	function station_dl_proc(){
		$csv_date = "";
		
		$week = array("日", "月", "火", "水", "木", "金", "土");
		// add 20140702 車種・店舗情報取得
		$car_data = $this->get_car();
		$shop_data = $this->get_shop();

		//エラー処理が必要
		//店舗だけが選択されていた場合、店舗車種が選択されていた場合などを後で考慮して作る
		
		//検索
		$zaiko_list = array();
		//在庫リスト
		$sql = "select a.autono as autono,a.shop_id as shop_id,a.car_id as car_id,b.date,a.week_flg as week_flg,a.no_plate as no_plate,a.car_no as car_no,a.price as price,a.grade as grade,a.model as model";
		$sql .= ",c.temporary_flg as temporary_flg,c.disp_number as disp_number,c.sei as sei,c.mei as mei,c.tel as tel,c.mail as mail,c.comment as comment,c.update_date as update_date";
		$sql .= ",c.area_id as area_id,c.sei_kana as sei_kana,c.mei_kana as mei_kana,c.mei as mei,c.age as age,c.address as address,c.k_tel as k_tel,c.campaign as campaign,c.pref_id as pref_id";
		$sql .= ",c.hour_from as hour_from,c.hour_to as hour_to,c.ref as ref,c.reason as reason,c.reason99_text as reason99_text";
		$sql .= ",c.campaign1 as campaign1,c.campaign2 as campaign2,c.campaign3 as campaign3,c.campaign4 as campaign4,c.campaign5 as campaign5,c.campaign6 as campaign6,c.campaign7 as campaign7,c.campaign8 as campaign8,c.campaign9 as campaign9,c.campaign10 as campaign10";
		$sql .= " from (zaiko b LEFT JOIN car_detail a ON a.autono = b.car_detail_id) LEFT JOIN reservation c ON c.car_detail_id = a.autono and c.date = b.date and c.shop_id = a.shop_id and c.temporary_flg ='2' and c.disp_flg='1' and c.del_flg='0' ";
		//$sql .= " from car_detail a,zaiko b ";
		$sql .= " where a.autono = b.car_detail_id";
		$sql .= " and a.del_flg='0'";
		//$sql .= " and a.del_flg='0' and a.disp_flg='1'";
		$sql .= " and b.del_flg='0' and b.disp_flg='1'";
		if($_SESSION['search']['car']){
			$sql .= " and a.car_id='".$this->DB->getQStr(substr($_SESSION['search']['car'],0,8))."'";
			$sql .= " and a.week_flg='".$this->DB->getQStr(substr($_SESSION['search']['car'],9,1))."'";
		}
		
		if($_SESSION['search']['day']){
			$sql .= " and b.date = '".$this->DB->getQStr($_SESSION['search']['year'])."-".$this->DB->getQStr($_SESSION['search']['month'])."-".$this->DB->getQStr($_SESSION['search']['day'])."' ";
		}
		else{
			$sql .= " and b.date >= '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'],1,$_SESSION['search']['year'])))."' ";
			$sql .= " and b.date < '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$_SESSION['search']['month'] + 1,1,$_SESSION['search']['year'])))."' ";
		}

		$sql .= " order by a.shop_id,b.date,a.car_id";
//echo $sql;
		$rs =& $this->DB->ASExecute($sql);
if($_SERVER['REMOTE_ADDR'] == "219.119.179.4"){
//echo $sql."\r\n";
}		
		$prev_date = "";

		if($rs){
			while(!$rs->EOF){
				$data = array();
				if($rs->fields('hour_from') and $rs->fields('hour_to')){
					$data['hour'] = $rs->fields('hour_from')."-".$rs->fields('hour_to');
				}
				else{
					$data['hour'] = "";
				}
				$data['no_plate'] = $rs->fields('no_plate');
				$data['car_no'] = $rs->fields('car_no');
				$price = "";
				if($rs->fields('price')){
					$price = str_replace(",","",$rs->fields('price'));
					if(is_numeric($price)){
						$price = number_format($price);
					}
				}
				$data['price'] = $price;
				$data['grade'] = $rs->fields('grade');
				$data['model'] = $rs->fields('model');
				$data['date'] = $rs->fields('date');
				$data['date2'] = $rs->fields('date');

				if( $prev_date == $rs->fields('date') ){
					$data['date'] = "";
				}
				if( $data['date'] ){
					$prev_date = $data['date'];
				}
//echo $zaiko_list['date']."<BR>";
				$time = strtotime($rs->fields('date'));
				$w = date("w", $time);
				$data['weekday'] = $week[$w];
				/*
				$week_name = "（休日）";
				if($rs->fields('week_flg') == 1){
					$week_name = "（平日）";
				}
				*/
				$week_flg = $rs->fields('week_flg');
				
				$data['car_id'] = $rs->fields('autono');
				$data['shop_id'] = $rs->fields('shop_id');

				// upd 20140702 車種名取得
				$data['carname'] = $car_data[$rs->fields('car_id')]['carname'];
				$data['carname2'] = $car_data[$rs->fields('car_id')]['carname2'];
				$data['carname3'] = $car_data[$rs->fields('car_id')]['carname3'];
				// upd 20140702 店舗情報取得
				$data['pref_id'] = $shop_data[$rs->fields('shop_id')]['pref_id'];
				$data['area_id'] = $shop_data[$rs->fields('shop_id')]['area_id'];
				$data['shop_name'] = $shop_data[$rs->fields('shop_id')]['shop_name'];

				if($week_flg == 1){
					if($this->util->pday_list(1,date("Y-m-d",mktime(0,0,0, date("m", $time),date("d", $time),date("Y", $time))))){
		        		$rs->MoveNext();
						continue;
		        	}
					if($w == 0 or $w == 6){
		        		$rs->MoveNext();
						continue;
					}
				}
				if($this->util->holiday_list($this->DB,$rs->fields('shop_id'),1,date("Y-m-d",mktime(0,0,0,  date("m", $time),date("d", $time),date("Y", $time))))){
					$rs->MoveNext();
					continue;
				}
				if($this->exception_proc($rs->fields('shop_id'),$rs->fields('autono'),date("Y-m-d",mktime(0,0,0,  date("m", $time),date("d", $time),date("Y", $time))))){
					$rs->MoveNext();
					continue;
				}

				// upd 20140702
				$data['disp_number'] = "";
				$data['name'] = "";
				$data['tel'] = "";
				$data['mail'] = "";
				$data['temporary_flg'] = "";
				$data['comment'] = "";
				$data['temporary_flg'] = $rs->fields('temporary_flg');
				if($rs->fields('temporary_flg') == '1'){
					$data['disp_number'] = "仮予約中";
				}
				if($rs->fields('temporary_flg') == '2'){
					$data['disp_number'] = $rs->fields('disp_number');
					$data['name'] = $rs->fields('sei')."　".$rs->fields('mei');
					$data['tel'] = $rs->fields('tel');
					$data['mail'] = $rs->fields('mail');
					$data['comment'] = "なし";
					if( $rs->fields('comment') ){
						$data['comment'] = "あり";
					}
					$data['entry_date'] = date("Y年m月d日",mktime(0,0,0,substr($rs->fields('update_date'),5,2),substr($rs->fields('update_date'),8,2),substr($rs->fields('update_date'),0,4)));
				}
				
				$data['shop_id'] = $rs->fields('shop_id');
				$data['area_id'] = $rs->fields('area_id');
				$data['sei'] = $rs->fields('sei');
				$data['mei'] = $rs->fields('mei');
				$data['sei_kana'] = $rs->fields('sei_kana');
				$data['mei_kana'] = $rs->fields('mei_kana');
				$data['age'] = $rs->fields('age');
				$data['address'] = $rs->fields('address');
				$data['tel'] = $rs->fields('tel');
				$data['k_tel'] = $rs->fields('k_tel');
				$data['mail'] = $rs->fields('mail');
				$data['comment'] = $rs->fields('comment');
				$data['campaign'] = $this->util->campaign_list(1,$rs->fields('campaign'));
				$data['disp_number'] = $rs->fields('disp_number');
				$data['pref_id'] = $rs->fields('pref_id');
				$data['campaign1'] = $rs->fields('campaign1');
				$data['campaign2'] = $rs->fields('campaign2');
				$data['campaign3'] = $rs->fields('campaign3');
				$data['campaign4'] = $rs->fields('campaign4');
				$data['campaign5'] = $rs->fields('campaign5');
				$data['campaign6'] = $rs->fields('campaign6');
				$data['campaign7'] = $rs->fields('campaign7');
				$data['campaign8'] = $rs->fields('campaign8');
				$data['campaign9'] = $rs->fields('campaign9');
				$data['campaign10'] = $rs->fields('campaign10');
				$data['ref'] = $rs->fields('ref');
				$data['reason'] = $this->util->reason_list(1,$rs->fields('reason'));
				$data['reason99_text'] = "";
				if($rs->fields('reason') == 99){
					$data['reason99_text'] = $rs->fields('reason99_text');
				}
				$d = '"'.$data['shop_name'].'","'.$data['date2'].'","'.$data['weekday'].'","'.$data['carname'].'","'.$data['carname2'].'","'.$data['carname3'].'","'.
				$data['hour'].'","'.$data['sei'].'","'.$data['mei'].'","'.$data['sei_kana'].'","'.$data['mei_kana'].'","'.$data['age'].'","'.$data['address'].'","'.$data['tel'].'","'.
				$data['k_tel'].'","'.$data['mail'].'","'.$data['comment'].'","'.$data['campaign'].'","'.$data['reason'].'","'.$data['reason99_text'].'","'.
				$data['disp_number'].'","'.$data['entry_date'].'","'.$data['no_plate'].'","'.$data['car_no'].'","'.$data['grade'].'","'.$data['model'].'","'.$data['price'].'","'.$data['ref'].'"'."\r\n";
				$csv_date .= $d;
				$zaiko_list[] = $data;
				$rs->MoveNext();
			}
			$rs->Close();
		}

		$car = "";
		if($_SESSION['search']['car']){
			$car = substr($_SESSION['search']['car'],0,8)."_";
		}
		$day = "";
		if($_SESSION['search']['day']){
			$day = "_".$_SESSION['search']['day'];
		}
		
		$file_name = $car.$_SESSION['search']['year']."_".$_SESSION['search']['month'].$day;
		header("Cache-Control: public");
		header("Pragma: public");
		header("Content-Type: application/x-csv");
		header("Content-Disposition: attachment; filename=".$file_name.".csv"); 

		$csv = '"店舗","日付","曜日","車種","","","時間","姓","名","セイ","メイ","年齢","住所","電話番号","携帯電話番号","メールアドレス","意見・要望","キャンペーン","キャンペーン申込の理由","キャンペーン申込の理由-その他","予約番号","予約日","登録番号","車体番号","グレード","型式","本体価格","キャンペーンID"'."\r\n";
		$csv .= $csv_date;
		echo mb_convert_encoding($csv,"SJIS-win","UTF-8");
	}
	
	function exception_proc($shop_id,$car_detail_id,$date){
		$sql = "select * from exception ";
		$sql .= " where shop_id = '".$this->DB->getQStr($shop_id)."' ";
		$sql .= " and car_detail_id = ".$this->DB->getQStr($car_detail_id);
		$sql .= " and date = '".$this->DB->getQStr($date)."' ";
		$sql .= " and del_flg = '0' and disp_flg = '1' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				return 1;
			}
			$rs->Close();
		}
		return 0;
	}

	// add 20140702 車種情報取得
	function get_car(){
		$data = array();
		//車種名取得
		$sql2 = "select * from car ";
		$sql2 .= "where del_flg='0' and disp_flg='1'";
		$rs2 =& $this->DB->ASExecute($sql2);
		if($rs2){
			while(!$rs2->EOF){
				$data[$rs2->fields('car_id')]['carname'] = $rs2->fields('name');
				$data[$rs2->fields('car_id')]['carname2'] = $rs2->fields('name2');
				$data[$rs2->fields('car_id')]['carname3'] = $rs2->fields('name3');
				$rs2->MoveNext();
			}
			$rs2->Close();
		}
		return $data;
	}

	// add 200140702 店舗情報取得
	function get_shop(){
		$data = array();
		//店舗情報取得
		$sql2 = "select * from shop ";
		$sql2 .= "where del_flg='0' and disp_flg='1'";
		$rs2 =& $this->DB->ASExecute($sql2);
		if($rs2){
			while(!$rs2->EOF){
				$data[$rs2->fields('shop_id')]['pref_id'] = $rs2->fields('pref_id');
				$data[$rs2->fields('shop_id')]['area_id'] = $rs2->fields('area_id');
				$data[$rs2->fields('shop_id')]['shop_name'] = $rs2->fields('name');
				$rs2->MoveNext();
			}
			$rs2->Close();
		}
		return $data;
	}
}