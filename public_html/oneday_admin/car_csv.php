<?php

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
	}

	function execute(){
		//覚書・access_kb='1'がスタッフ・access_kb='2'が店長・access_kb='4'が本部・access_kb='9'が確認ユーザー
		switch($this->mode){
			// 試乗車種CSV出力
			default:
				$this->default_proc();
			break;
		}
	}

    // 一覧
    function default_proc(){
        $file_name = "car_data".date("Ymd");
        header("Cache-Control: public");
        header("Pragma: public");
        header("Content-Type: application/x-csv");
        header("Content-Disposition: attachment; filename=" . $file_name . ".csv");
        echo mb_convert_encoding('"事業部","店舗名","車種","","","車体番号","登録番号","WEB公開",""', "SJIS-win", "UTF-8");
        echo "\r\n";
        // 検索条件作成
        $where = "";
        if($_SESSION['search']['shop']){
            $where .= " and sh.shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."'";
        }
        else if($_SESSION['search']['station']){
            $where .= " and sh.station_code='".$this->DB->getQStr($_SESSION['search']['station'])."'";
        }
        $sql = "SELECT dl.autono as car_detail_id,dl.car_no as car_no,dl.no_plate as no_plate,dl.start_date as start_date,dl.end_date as end_date,sh.name as shop_name,sh.station_name as station_name,ca.name as car_name1,";
        $sql .= "ca.name2 as car_name2,ca.name3 as car_name3,dl.disp_flg as disp_flg,dl.car1 as car1,dl.car2 as car2,dl.car3 as car3,dl.car4 as car4,dl.car5 as car5 FROM car_detail as dl ";
        $sql .= "INNER JOIN shop as sh ON dl.shop_id = sh.shop_id ";
        $sql .= "INNER JOIN car as ca ON dl.car_id = ca.car_id ";
        $sql .= "where ";
        $sql .= "dl.del_flg = '0' ";
        $sql .= "and sh.disp_flg = '1' and sh.del_flg = '0' ";
        $sql .= "and ca.disp_flg = '1' and ca.del_flg = '0' ";
        $sql .= $where;
        $sql .= " order by sh.station_code,sh.shop_id,dl.end_date desc,dl.car1,dl.car2,dl.car3,dl.car4,dl.car5";
        $rs =& $this->DB->ASExecute($sql);
        $data_list = array();
        if($rs){
            while(!$rs->EOF){
                $dat = array();
                $dat['car_detail_id'] = $rs->fields('car_detail_id');
                $dat['station_name'] = $rs->fields('station_name');
                $dat['shop_name'] = $rs->fields('shop_name');
                $dat['car_no'] = $rs->fields('car_no');
                $dat['no_plate'] = $rs->fields('no_plate');
                $dat['start_date'] = $rs->fields('start_date');
                $dat['end_date'] = $rs->fields('end_date');
                $dat['name'] = $rs->fields('car_name1');
                if($rs->fields('car2') == "ge"){
                    $dat['name2'] = "ガソリン";
                }
                else if($rs->fields('car2') == "de"){
                    $dat['name2'] = "ディーゼル";
                }
                else if($rs->fields('car2') == "hev"){
                    $dat['name2'] = "ハイブリッド";
                }
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == 'skyx'){
                    $data['carname2'] = "SKYACTIV-X";
                }
                $dat['name3'] = "";
                if($rs->fields('car4') == "mt"){
                    $dat['name3'] = "MT ";
                }
                $dat['name3'] .= $rs->fields('car3');
                if($rs->fields('car5') == "turbo"){
                    $dat['name3'] .= " ターボ";
                }
                $dat['disp_flg_value'] = $this->util->disp_flg2_list(1,$rs->fields('disp_flg'));
                $data_list[] = $dat;
                $rs->MoveNext();
            }
            $rs->Close();
        }
        if($data_list){
            if(is_array($data_list)){
                foreach($data_list as $key => $val){
                    $data = '"'.$val['station_name'].'","'.$val['shop_name'].'","'.$val['name'].'","'.$val['name2'].'","'.$val['name3'].'","'.$val['car_no'].'","'.$val['no_plate'].'","'.$val['start_date'].'～'.$val['end_date'].'","'.$val['disp_flg_value'].'"';
                    echo mb_convert_encoding($data, "SJIS-win", "UTF-8");
                    echo "\r\n";
                }
            }
        }
    }
}
?>