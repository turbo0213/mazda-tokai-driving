<?php
ini_set("memory_limit", "512M");
ini_set("max_execution_time", "600");

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

//if($_SERVER['REMOTE_ADDR'] != "219.119.179.4"){
//exit;
//}
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{

	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $DB2;
	var $util;

	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
//			case 'conf':
//				$this->conf_proc();
//			break;
			default:
				// 一覧画面
				$this->default_proc();
			break;
		}
	}
	
	function form_make(){
		if($_SESSION['oneday']['access_kb'] == 1){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 2){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 3){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
		}
		else if($_SESSION['oneday']['access_kb'] == '4'){
			$station_list = $this->util->station_list_get("0","0",$this->DB);
//			if($this->req->get('station')){
				$_SESSION['search']['station'] = $this->req->get('station');
//			}
/*
			if(!$_SESSION['search']['station']){
				foreach($station_list as $key => $val){
					$_SESSION['search']['station'] = $key;
					break;
				}
			}
*/
			$this->templ->smarty->assign('station',$_SESSION['search']['station']);
			$this->templ->smarty->assign('station_list',$station_list);
		}
		if($_SESSION['oneday']['access_kb'] != 1 and $_SESSION['oneday']['access_kb'] != 2){
			$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
//			if($this->req->get('shop')){
				$_SESSION['search']['shop'] = $this->req->get('shop');
//			}
/*
			if(!$_SESSION['search']['shop'] or $this->req->get('type') == 'station_change'){
				foreach($shop_list as $key => $val){
					$_SESSION['search']['shop'] = $key;
					break;
				}
			}
*/
			if($_SESSION['search']['shop']){
				$sql = "select * from shop";
				$sql .= " where shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
				$rs =& $this->DB->ASExecute($sql);
				if($rs){
					if(!$rs->EOF){
						$_SESSION['search']['station'] = $rs->fields('station_code');
						$this->templ->smarty->assign('station',$_SESSION['search']['station']);
						$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
					}
					$rs->Close();
				}
			}
			$this->templ->smarty->assign('shop',$_SESSION['search']['shop']);
			$this->templ->smarty->assign('shop_list',$shop_list);
		}
		$this->templ->smarty->assign("year_list",$this->util->year_list());
		$this->templ->smarty->assign("month_list",$this->util->month_list());
//		$this->templ->smarty->assign("day_list",$this->util->day_list());
	}

	//一覧
	function default_proc(){
		$data_list = array();
        if($_SESSION['search']['month'] > $this->req->get_get('m')){
            $date = date("Y-m-d",mktime(0,0,0,$this->req->get_get('m'),$this->req->get_get('d'),$_SESSION['search']['year']+1));
        }
        else{
            $date = date("Y-m-d",mktime(0,0,0,$this->req->get_get('m'),$this->req->get_get('d'),$_SESSION['search']['year']));
        }
		//$date = date("Y-m-d",mktime(0,0,0,$this->req->get_get('m'),$this->req->get_get('d'),$_SESSION['search']['year']));
		$carname = "";
		$data = array();
		// 車種情報取得
		$sql = "select * from car_detail";
		$sql .= " where car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		else if($_SESSION['oneday']['access_kb'] == "3"){
			$shop_search_flg = true;
			if($_SESSION['search']['shop']){
				$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
				$shop_search_flg = false;
			}
			if($shop_search_flg){
				$shop_sql = "";
				$sql2 = "select * from shop";
				$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['oneday']['station_code'])."'";
				$sql2 .= " and del_flg = '0'";
				$sql2 .= " and disp_flg = '1'";
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						if($shop_sql){
							$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						else{
							$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				if($shop_sql){
					$shop_sql .= ")";
					$sql .= $shop_sql;
				}
			}
		}
		else if($_SESSION['search']['shop']){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		else if($_SESSION['search']['station']){
			$shop_sql = "";
			$sql2 = "select * from shop";
			$sql2 .= " where station_code = '".$this->DB->getQStr($_SESSION['search']['station'])."'";
			$sql2 .= " and del_flg = '0'";
			$sql2 .= " and disp_flg = '1'";
			$rs2 =& $this->DB->ASExecute($sql2);
			if($rs2){
				while(!$rs2->EOF){
					if($shop_sql){
						$shop_sql .= ",'".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					else{
						$shop_sql = " and shop_id in ('".$this->DB->getQStr($rs2->fields('shop_id'))."'";
					}
					$rs2->MoveNext();
				}
				$rs2->Close();
			}
			if($shop_sql){
				$shop_sql .= ")";
				$sql .= $shop_sql;
			}
		}
//		$sql .= " group by car1,car2";
        // upd 20190130 turbo対応
		$sql .= " order by car1,car2,car3,car4,car5";
//if($_SERVER['REMOTE_ADDR'] == "118.21.112.109"){
//echo $sql."<br>";
//}
		$rs =& $this->DB->ASExecute($sql);
		$car_list = array();
		if($rs){
			while(!$rs->EOF){
			    // upd 20190130 turbo対応
				$car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car1'] = $rs->fields('car1');
				$car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car2'] = $rs->fields('car2');
				$car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car3'] = $rs->fields('car3');
				$car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car5'] = $rs->fields('car5');
				$car_list[$rs->fields('car2').$rs->fields('car3').$rs->fields('car4').$rs->fields('car5')]['car_id'] = $rs->fields('car_id');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$data_cnt = 0;
		if($car_list){
			if(is_array($car_list)){
				foreach($car_list as $key => $val){
					$engine = "";
					if($car_list[$key]['car2'] == "ge"){
						$engine = "ガソリン";
					}
					else if($car_list[$key]['car2'] == "de"){
						$engine = "ディーゼル";
					}
					else if($car_list[$key]['car2'] == "hev"){
						$engine = "ハイブリッド";
					}
                    // add 20200109 SKYACTIV-X対応
                    else if($car_list[$key]['car2'] == 'skyx'){
                        $engine = "SKYACTIV-X";
                    }
                    // add 20201009 e-SKYACTIV G対応
                    else if($car_list[$key]['car2'] == 'eskyg'){
                        $engine = "e-SKYACTIV G";
                    }
					$car3 = $car_list[$key]['car3'];
					$mt = "";
					if($car_list[$key]['car4'] == "mt"){
						$mt = "MT";
					}
					// add 20190130 turbo対応
                    $turbo = "";
                    if($car_list[$key]['car5'] == "turbo"){
                        $turbo = " ターボ";
                    }
					$sql2 = "select * from car";
					$sql2 .= " where car_id = '".$this->DB->getQStr($car_list[$key]['car_id'])."'";
					$sql2 .= " order by car2,car_id";
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						if(!$rs2->EOF){
							$carname = $rs2->fields('name');
							// upd 20190130 turbo対応
							if($engine){
								if($mt){
									$data['carname'] = $engine." ".$mt." ".$car3.$turbo;
								}
								else{
									$data['carname'] = $engine." ".$car3.$turbo;
								}
							}
							else{
								if($mt){
									$data['carname'] = $mt." ".$car3.$turbo;
								}
								else{
									$data['carname'] = $car3.$turbo;
								}
							}
						}
						$rs2->Close();
					}
					// 予約確認(午前)
					$am_list = array();
					$sql2 = "select * from reservation";
					$sql2 .= " where car1 = '".$this->DB->getQStr($car_list[$key]['car1'])."'";
					$sql2 .= " and car2 = '".$this->DB->getQStr($car_list[$key]['car2'])."'";
					if($car_list[$key]['car3']){
						$sql2 .= " and car3 = '".$this->DB->getQStr($car_list[$key]['car3'])."'";
					}
					if($car_list[$key]['car4']){
						$sql2 .= " and car4 = '".$this->DB->getQStr($car_list[$key]['car4'])."'";
					}
					// add 20190130 turbo対応
                    if($car_list[$key]['car5']){
                        $sql2 .= " and car5 = '".$this->DB->getQStr($car_list[$key]['car5'])."'";
                    }
                    else{
                        $sql2 .= " and (car5 is NULL or car5 = '')";
                    }
					$sql2 .= " and ((conf_date = '".$this->DB->getQStr($date)."' and conf_ampm = '1')";
					$sql2 .= " or (date = '".$this->DB->getQStr($date)."' and ampm = '1')";
					$sql2 .= " or (date2 = '".$this->DB->getQStr($date)."' and ampm2 = '1')";
					$sql2 .= " or (date3 = '".$this->DB->getQStr($date)."' and ampm3 = '1'))";
					if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
						$sql2 .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
					}
					else if($_SESSION['oneday']['access_kb'] == "3"){
						$shop_search_flg = true;
						if($_SESSION['search']['shop']){
							$sql2 = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
							$shop_search_flg = false;
						}
						if($shop_search_flg){
							$sql2 .= $shop_sql;
						}
					}
					else if($_SESSION['search']['shop']){
						$sql2 .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
					}
					else if($_SESSION['search']['station']){
						$sql2 .= $shop_sql;
					}
					$sql2 .= " and disp_flg='1'";
					$sql2 .= " and del_flg='0'";
					$sql2 .= " and temporary_flg = '2'";
					$sql2 .= " order by create_date";
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//echo $sql2."<br>";
}
					$rs2 =& $this->DB->ASExecute($sql2);
					if($rs2){
						while(!$rs2->EOF){
							$am_data = array();
							$am_data['disp_number'] = $rs2->fields('disp_number');
							$shop_name = "";
							$sql3 = "select * from shop";
							$sql3 .= " where shop_id = '".$rs2->fields('shop_id')."'";
							$rs3 =& $this->DB->ASExecute($sql3);
							if($rs3){
								if(!$rs3->EOF){
									$shop_name = $rs3->fields('name');
								}
								$rs3->Close();
							}
							if($shop_name){
								$am_data['name'] = $shop_name.":".$rs2->fields('sei')." ".$rs2->fields('mei');
							}
							else{
								$am_data['name'] = $rs2->fields('sei')." ".$rs2->fields('mei');
							}
							if($rs2->fields('conf_flg') =='1' and $rs2->fields('car_detail_id')){
								$am_flg = false;
								if($date == $rs2->fields('conf_date') and $rs2->fields('conf_ampm') == '1'){
									$sql3 = "select car_id from car_detail";
									$sql3 .= " where autono = ".$this->DB->getQStr($rs2->fields('car_detail_id'));
									$rs3 =& $this->DB->ASExecute($sql3);
									if($rs3){
										if(!$rs3->EOF){
											if($rs3->fields('car_id') == $car_list[$key]['car_id']){
												$am_flg = true;
											}
										}
										$rs3->Close();
									}
								}
							}
							else{
								$am_flg = false;
								$sql3 = "select * from car_detail";
								$sql3 .= " where car1 = '".$this->DB->getQStr($car_list[$key]['car1'])."'";
								$sql3 .= " and car2 = '".$this->DB->getQStr($car_list[$key]['car2'])."'";
								if($car_list[$key]['car3']){
									$sql3 .= " and car3 = '".$this->DB->getQStr($car_list[$key]['car3'])."'";
								}
								if($car_list[$key]['car4']){
									$sql3 .= " and car4 = '".$this->DB->getQStr($car_list[$key]['car4'])."'";
								}
								// add 20190130 turbo対応
                                if($car_list[$key]['car5']){
                                    $sql3 .= " and car5 = '".$this->DB->getQStr($car_list[$key]['car5'])."'";
                                }
                                else{
                                    $sql3 .= " and (car5 is NULL or car5 = '')";
                                }
								$sql3 .= " and shop_id = '".$this->DB->getQStr($rs2->fields('shop_id'))."'";
								$rs3 =& $this->DB->ASExecute($sql3);
								if($rs3){
									if(!$rs3->EOF){
										$am_flg = true;
									}
									$rs3->Close();
								}
							}
							if($am_flg){
                                $data_cnt++;
								$am_list[] = $am_data;
							}
							$rs2->MoveNext();
						}
						$rs2->Close();
					}
					$data['am'] = $am_list;
					// 予約確認(午後)
					$pm_list = array();
					$sql2 = "select * from reservation";
					$sql2 .= " where car1 = '".$this->DB->getQStr($car_list[$key]['car1'])."'";
					$sql2 .= " and car2 = '".$this->DB->getQStr($car_list[$key]['car2'])."'";
					if($car_list[$key]['car3']){
						$sql2 .= " and car3 = '".$this->DB->getQStr($car_list[$key]['car3'])."'";
					}
					if($car_list[$key]['car4']){
						$sql2 .= " and car4 = '".$this->DB->getQStr($car_list[$key]['car4'])."'";
					}
					// add 20190130 turbo対応
                    if($car_list[$key]['car5']){
                        $sql2 .= " and car5 = '".$this->DB->getQStr($car_list[$key]['car5'])."'";
                    }
                    else{
                        $sql2 .= " and (car5 is NULL or car5 = '')";
                    }
					$sql2 .= " and ((conf_date = '".$this->DB->getQStr($date)."' and conf_ampm = '2')";
					$sql2 .= " or (date = '".$this->DB->getQStr($date)."' and ampm = '2')";
					$sql2 .= " or (date2 = '".$this->DB->getQStr($date)."' and ampm2 = '2')";
					$sql2 .= " or (date3 = '".$this->DB->getQStr($date)."' and ampm3 = '2'))";
					if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
						$sql2 .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
					}
					else if($_SESSION['oneday']['access_kb'] == "3"){
						$shop_search_flg = true;
						if($_SESSION['search']['shop']){
							$sql2 = " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
							$shop_search_flg = false;
						}
						if($shop_search_flg){
							$sql2 .= $shop_sql;
						}
					}
					else if($_SESSION['search']['shop']){
						$sql2 .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
					}
					else if($_SESSION['search']['station']){
						$sql2 .= $shop_sql;
					}
					$sql2 .= " and disp_flg='1'";
					$sql2 .= " and del_flg='0'";
					$sql2 .= " and temporary_flg = '2'";
					$sql2 .= " order by create_date";
					$rs2 =& $this->DB->ASExecute($sql2);
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//echo $sql2."<br>";
}
					if($rs2){
						while(!$rs2->EOF){
							$pm_data = array();
							$pm_data['disp_number'] = $rs2->fields('disp_number');
							$shop_name = "";
							$sql3 = "select * from shop";
							$sql3 .= " where shop_id = '".$rs2->fields('shop_id')."'";
							$rs3 =& $this->DB->ASExecute($sql3);
							if($rs3){
								if(!$rs3->EOF){
									$shop_name = $rs3->fields('name');
								}
								$rs3->Close();
							}
							if($shop_name){
								$pm_data['name'] = $shop_name.":".$rs2->fields('sei')." ".$rs2->fields('mei');
							}
							else{
								$pm_data['name'] = $rs2->fields('sei')." ".$rs2->fields('mei');
							}
							if($rs2->fields('conf_flg') =='1' and $rs2->fields('car_detail_id')){
								$pm_flg = false;
								if($date == $rs2->fields('conf_date') and $rs2->fields('conf_ampm') == '2'){
									$sql3 = "select car_id from car_detail";
									$sql3 .= " where autono = ".$rs2->fields('car_detail_id');
									$rs3 =& $this->DB->ASExecute($sql3);
									if($rs3){
										if(!$rs3->EOF){
											if($rs3->fields('car_id') == $car_list[$key]['car_id']){
												$pm_flg = true;
											}
										}
										$rs3->Close();
									}
								}
							}
							else{
								$pm_flg = false;
								$sql3 = "select * from car_detail";
								$sql3 .= " where car1 = '".$this->DB->getQStr($car_list[$key]['car1'])."'";
								$sql3 .= " and car2 = '".$this->DB->getQStr($car_list[$key]['car2'])."'";
								if($car_list[$key]['car3']){
									$sql3 .= " and car3 = '".$this->DB->getQStr($car_list[$key]['car3'])."'";
								}
								if($car_list[$key]['car4']){
									$sql3 .= " and car4 = '".$this->DB->getQStr($car_list[$key]['car4'])."'";
								}
								// add 20190130 turbo対応
                                if($car_list[$key]['car5']){
                                    $sql3 .= " and car5 = '".$this->DB->getQStr($car_list[$key]['car5'])."'";
                                }
                                else{
                                    $sql3 .= " and (car5 is NULL or car5 = '')";
                                }
								$sql3 .= " and shop_id = '".$this->DB->getQStr($rs2->fields('shop_id'))."'";
								$rs3 =& $this->DB->ASExecute($sql3);
								if($rs3){
									if(!$rs3->EOF){
										$pm_flg = true;
									}
									$rs3->Close();
								}
							}
							if($pm_flg){
                                $data_cnt++;
								$pm_list[] = $pm_data;
							}
							$rs2->MoveNext();
						}
						$rs2->Close();
					}
					$data['pm'] = $pm_list;
					$data_list[] = $data;
				}
			}
		}
if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
//print_r($data_list);
}
		if($data_cnt <= 0){
			$this->templ->smarty->assign('nodata_flg',1);
		}
        $this->templ->smarty->assign('reload_flg',1);
		$this->templ->smarty->assign('shop_id',$_SESSION['search']['shop']);
		$this->templ->smarty->assign('date',$date);
		$this->templ->smarty->assign('carname',$carname);
		$this->templ->smarty->assign('data_list',$data_list);
		$this->templ->smarty->assign('car1',$this->req->get_get('car1'));
		$this->templ->smarty->assign('m',$this->req->get_get('m'));
		$this->templ->smarty->assign('d',$this->req->get_get('d'));
		$this->templ->smarty->assign('e',$this->req->get_get('e'));
		$this->templ->smarty->assign('c',$this->req->get_get('c'));
		$this->templ->smarty->display("oneday_admin/time_table.html");
		exit;
	}
/*
	function conf_proc(){
		if(!$this->req->get('disp_number')){
			header("Location:time_table.php?car1=".$this->req->get('car1')."&m=".$this->req->get('m')."&d=".$this->req->get('d'));
		}
		$sql = "select * from reservation";
		$sql .= " where disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$record = NULL;
				// 未確定→確定済
				if(!$this->req->get('conf_flg')){
					$car_detail_id = $this->req->get('car_detail_id');
					if($this->req->get('date') == $rs->fields('date') and $this->req->get('ampm') == $rs->fields('ampm')){
						$error = $this->conf_check($rs->fields('date'),$rs->fields('ampm'),$this->req->get('disp_number'),1);
						if(!$error){
							$conf_date = $rs->fields('date');
							$conf_ampm = $rs->fields('ampm');
							$conf_flg = "1";
						}
					}
					else if($this->req->get('date') == $rs->fields('date2') and $this->req->get('ampm') == $rs->fields('ampm2')){
						$error = $this->conf_check($rs->fields('date2'),$rs->fields('ampm2'),$this->req->get('disp_number'),1);
						if(!$error){
							$conf_date = $rs->fields('date2');
							$conf_ampm = $rs->fields('ampm2');
							$conf_flg = "1";
						}
					}
					else if($this->req->get('date') == $rs->fields('date3') and $this->req->get('ampm') == $rs->fields('ampm3')){
						$error = $this->conf_check($rs->fields('date3'),$rs->fields('ampm3'),$this->req->get('disp_number'),1);
						if(!$error){
							$conf_date = $rs->fields('date3');
							$conf_ampm = $rs->fields('ampm3');
							$conf_flg = "1";
						}
					}
					else{
						header("Location:time_table.php?car1=".$this->req->get('car1')."&m=".$this->req->get('m')."&d=".$this->req->get('d'));
					}
				}
				// 確定済→未確定
				else{
					$error = $this->conf_check($this->req->get('date'),$this->req->get('ampm'),$this->req->get('disp_number'),2);
					$car_detail_id = NULL;
					$conf_date = NULL;
					$conf_ampm = NULL;
					$conf_flg = "0";
				}
				if(!$error){
					$record['car_detail_id'] = $car_detail_id;
					$record['conf_date'] = $conf_date;
					$record['conf_ampm'] = $conf_ampm;
					$record['conf_flg'] = $conf_flg;
					$record['update_date'] = time();
					$where = "disp_number = '".$this->DB->getQStr($this->req->get('disp_number'))."'";
					$ret = $this->DB->getCon()->AutoExecute("reservation", $record, 'UPDATE',$where);
				}
			}
			$rs->Close();
		}
		if($error){
			header("Location:time_table.php?car1=".$this->req->get('car1')."&m=".$this->req->get('m')."&d=".$this->req->get('d')."&e=".$error);
		}
		else{
			header("Location:time_table.php?car1=".$this->req->get('car1')."&m=".$this->req->get('m')."&d=".$this->req->get('d')."&c=1");
		}
	}

	function conf_check($date,$ampm,$number,$check_type){
		$error = "";
		// 未確定→確定済のチェック
		if($check_type == "1"){
			$sql = "select * from reservation";
			$sql .= " where conf_date = '".$this->DB->getQStr($date)."'";
			$sql .= " and conf_ampm = '".$this->DB->getQStr($ampm)."'";
			$sql .= " and del_flg = '0'";
			$sql .= " and disp_flg = '1'";
			$sql .= " and conf_flg = '1'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$error = "1";
				}
				$rs->Close();
			}
		}
		// 確定済→未確定のチェック
		elseif($check_type== "2"){
			$sql = "select * from reservation";
			$sql .= " where disp_number = '".$this->DB->getQStr($number)."'";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('conf_date') != $date){
						$error = "2";
					}
					else if($rs->fields('conf_ampm') != $ampm){
						$error = "2";
					}
				}
				$rs->Close();
			}
		}
		return $error;
	}
*/
}
?>