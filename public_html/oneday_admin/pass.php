<?php
define("TEMPLATE1", "oneday_admin/pass_conf.html");
define("TEMPLATE2", "oneday_admin/pass_end.html");
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
    var $DB3;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			case 'check':
				$this->check_proc();
			break;
			case 'regist':
				$this->regist_proc();
			break;
			case 'end':
				$this->end_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
//		$sql = "select * from send_mail";
//		$sql .= " where driving_shop_id = '".$this->DB3->getQStr($_SESSION['oneday']['shop_id'])."'";
//		$sql .= " and autono = ". $this->DB3->getQStr($_SESSION['oneday']['staff_autono']);
//		$rs =& $this->DB3->ASExecute($sql);
//		if($rs){
//			if (!$rs->EOF) {
				//$this->templ->smarty->assign('old_login_pass',$rs->fields('login_pass'));
				//$this->templ->smarty->assign('old_auth_pass',$rs->fields('auth_pass'));
//			}
//			$rs->Close();
//		}
		$this->templ->smarty->assign('pass_chg_flg',$_SESSION['oneday']['pass_chg_flg']);
		$this->templ->smarty->assign('check_flg',0);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}
	
	function check_proc(){
		$this->templ->smarty->assign('old_login_pass',$this->req->get('old_login_pass'));
		$this->templ->smarty->assign('new_login_pass',$this->req->get('new_login_pass'));
		$this->templ->smarty->assign('new_login_pass_k',$this->req->get('new_login_pass_k'));
		$this->templ->smarty->assign('pass_chg_flg',$_SESSION['oneday']['pass_chg_flg']);
		$this->data_check();
		if($this->req->hasErrors()){
			$this->templ->error_assign($this->req);
			$this->templ->smarty->display(TEMPLATE1);
			exit;
		}
		$ast = "************";
		$len1 = strlen($this->req->get('old_login_pass')) - 3;
		$len2 = strlen($this->req->get('new_login_pass')) - 3;
		$this->templ->smarty->assign('disp_old_login_pass',substr($this->req->get('old_login_pass'),0,3).substr($ast,0,$len1));
		$this->templ->smarty->assign('disp_new_login_pass',substr($this->req->get('new_login_pass'),0,3).substr($ast,0,$len2));
		$this->templ->smarty->assign('check_flg',1);
		$this->templ->smarty->display(TEMPLATE1);
		exit;
	}
	
	function data_check(){
		if(!$this->req->get('old_login_pass')){
			$this->req->setError('error1', '現パスワードを入力して下さい。');
		}
		else{
			$find_flg = false;
			$sql = "select * from send_mail";
			$sql .= " where driving_shop_id = '".$this->DB3->getQStr($_SESSION['oneday']['shop_id'])."'";
			$sql .= " and autono = ". $this->DB3->getQStr($_SESSION['oneday']['staff_autono']);
			$rs =& $this->DB3->ASExecute($sql);
			if($rs){
				if (!$rs->EOF) {
					//$this->templ->smarty->assign('old_login_pass',$rs->fields('login_pass'));
					if($rs->fields('password') == $this->req->get('old_login_pass')){
						$find_flg = true;
					}
				}
				$rs->Close();
			}
			if(!$find_flg){
				$this->req->setError('error1', '現パスワードが一致しません。');
			}
		}
		if(!$this->req->get('new_login_pass')){
			$this->req->setError('error2', '新パスワードを入力して下さい。');
		}
		else{
			// 20140415 upd セキュリティ対策 新パスワードルール変更
			if(!ereg("^[0-9a-zA-Z]{8,20}$", $this->req->get('new_login_pass'))) {
				$this->req->setError('error2', '新パスワードを半角英字1文字以上、半角数字1文字以上使用して8～20文字で入力してください。');
			}
		}
		if(!$this->req->get('new_login_pass_k')){
			$this->req->setError('error3', 'パスワード確認を入力して下さい。');
		}
		if($this->req->get('new_login_pass') and $this->req->get('new_login_pass_k')){
			if($this->req->get('new_login_pass') != $this->req->get('new_login_pass_k')){
				$this->req->setError('error3', '新パスワードが一致しません。');
			}
		}

	}
	
	function regist_proc(){
		$this->templ->smarty->assign('old_login_pass',$this->req->get('old_login_pass'));
		$this->templ->smarty->assign('new_login_pass',$this->req->get('new_login_pass'));
		$this->templ->smarty->assign('new_login_pass_k',$this->req->get('new_login_pass_k'));
		if($this->req->get('back')){
			$this->templ->smarty->assign('check_flg',0);
			$this->templ->smarty->display(TEMPLATE1);
			exit;
		}
		$this->data_check();
		if($this->req->hasErrors()){
			$this->templ->error_assign($this->req);
			$this->templ->smarty->assign('check_flg',0);
			$this->templ->smarty->display(TEMPLATE1);
			exit;
		}
		$this->upd_proc();
		if($this->req->hasErrors()){
			$this->templ->error_assign($this->req);
			$this->templ->smarty->assign('check_flg',0);
			$this->templ->smarty->display(TEMPLATE1);
			exit;
		}
		header("Location: ./pass.php?mode=end");
		exit;
	}
	
	function upd_proc(){
		$sql = "select * from send_mail";
		$sql .= " where driving_shop_id = '". $this->DB3->getQStr($_SESSION['oneday']['shop_id'])."'";
		$sql .= " and autono = ". $this->DB3->getQStr($_SESSION['oneday']['staff_autono']);
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			if (!$rs->EOF) {
				$sql2 = "update send_mail set";
				if($this->req->get('new_login_pass')){
					$sql2 .= " password = '".$this->DB3->getQStr($this->req->get('new_login_pass'))."'";
				}
				$sql2 .= " where shop_id = '". $this->DB3->getQStr($_SESSION['oneday']['shop_id'])."'";
				$sql2 .= " and autono = ". $this->DB3->getQStr($_SESSION['oneday']['staff_autono']);
				$rs2 =& $this->DB3->ASExecute($sql2);
			}
			else{
				$this->req->setError('error', 'スタッフ情報が存在しません。');
			}
			$rs->Close();
		}
		//$_SESSION['oneday']['auth_pass'] = $this->req->get('new_auth_pass');
	}
	
	function end_proc(){
		// 20140415add セキュリティ対策 パスワード変更フラグ初期化
		$_SESSION['oneday']['pw_chg_flg'] = 0;
		$this->templ->smarty->display(TEMPLATE2);
		exit;
	}
}
?>