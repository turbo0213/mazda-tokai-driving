<?php
include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		switch($this->mode){
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		$this->form_make();
		$this->week_make();
		if($_SESSION['oneday']['access_kb'] == "3"){
	        $stationsOrder = $this->util->station_list(0,0,$_SESSION['oneday']['station_code']);
	    }
	    else{
	        $stationsOrder = $this->util->station_list();
	    }
        if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
        	$myStation = array();
        	foreach($stationsOrder as $key => $val){
        		if($_SESSION['oneday']['station_code'] == $val){
        			$myStation[1] = $val;
        			break;
        		}
        	}
        	$stationsOrder = $myStation;
        }
        $stations = $this->getStations();
		$summaries = $this->list_data_get();
		$this->templ->smarty->assign("stationsOrder",$stationsOrder);
		$this->templ->smarty->assign("stations",$stations);
		$this->templ->smarty->assign("summaries",$summaries);
		$this->templ->smarty->display("oneday_admin/campaign_result.html");
		exit;
	}
	
	function form_make(){
		$this->templ->smarty->assign("year_list",$this->util->year_list());
		$this->templ->smarty->assign("month_list",$this->util->month_list());
		$this->templ->smarty->assign("day_list",$this->util->day_list());
		$this->templ->smarty->assign("year_to_list",$this->util->year_list());
		$this->templ->smarty->assign("month_to_list",$this->util->month_list());
		$this->templ->smarty->assign("day_to_list",$this->util->day_list());
	}

	function getStations(){
		$station_list = array();
		$num = 1;
		$sql = "select * from shop";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['oneday']['shop_id'])."'";
		}
		$sql .= " order by station_code,order_no";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				if(!$station_list[$rs->fields('station_code')]['name']){
					$station_list[$rs->fields('station_code')]['name'] = $rs->fields('station_name');
					$num = 1;
				}
				$station_list[$rs->fields('station_code')]['shops'][$rs->fields('shop_id')] = $rs->fields('name');
				$station_list[$rs->fields('station_code')]['num'] = $num;
				$num++;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $station_list;
	}

	function week_make(){
		$year = $this->req->get('year');
		$month = $this->req->get('month');
		$day = $this->req->get('day');
		$year_to = $this->req->get('year_to');
		$month_to = $this->req->get('month_to');
		$day_to = $this->req->get('day_to');
		if(!$year){
			$year = date("Y",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$month){
			$month = date("m",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$day){
			$day = date("d",mktime(0,0,0,date("m"),date("d") - 6,date("Y")));
		}
		if(!$year_to){
			$year_to = date("Y");
		}
		if(!$month_to){
			$month_to = date("m");
		}
		if(!$day_to){
			$day_to = date("d");
		}
		if(mktime(0,0,0,$month,$day,$year) > mktime(0,0,0,$month_to,$day_to,$year_to)){
			$year_to = date("Y",mktime(0,0,0,$month,$day + 6,$year));
			$month_to = date("m",mktime(0,0,0,$month,$day + 6,$year));
			$day_to = date("d",mktime(0,0,0,$month,$day + 6,$year));
		}
		$this->req->set("year",$year);
		$this->req->set("month",$month);
		$this->req->set("day",$day);
		$this->req->set("year_to",$year_to);
		$this->req->set("month_to",$month_to);
		$this->req->set("day_to",$day_to);

		$this->templ->smarty->assign("year",$year);
		$this->templ->smarty->assign("month",$month);
		$this->templ->smarty->assign("day",$day);
		$this->templ->smarty->assign("year_to",$year_to);
		$this->templ->smarty->assign("month_to",$month_to);
		$this->templ->smarty->assign("day_to",$day_to);
		$result_title = date("Y年m月d日",mktime(0,0,0,$month,$day,$year));
		$result_title .= " ～ ";
		$result_title .= date("Y年m月d日",mktime(0,0,0,$month_to,$day_to,$year_to));
		$this->templ->smarty->assign("result_title",$result_title);
	}

	function list_data_get(){
		$summary = array();
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
			    // MAZDA6 SEDAN add 20190916
			    if($rs->fields('car1') == "mz6s"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                }
                // MAZDA6 WAGON add 20190916
                if($rs->fields('car1') == "mz6w"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // MAZDA3 add 20190515 SEDANのみに upd 20190520
                if($rs->fields('car1') == "mzs"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MAZDA3 FB add 20190520
                if($rs->fields('car1') == "mzf"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // MAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
				// デミオ
				if($rs->fields('car1') == "dmo"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
				// ロードスター
				if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
				// ロードスターRF
				if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
				// CX-3
				if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
				// CX-5
				if($rs->fields('car1') == "cx5"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
				// CX-8
				if($rs->fields('car1') == "cx8"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
				}
				// アテンザ
				if($rs->fields('car1') == "atz"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
				// アクセラ
				if($rs->fields('car1') == "axl"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // MX-30 add 20201005
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
        $sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date2 between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN add 20190916
                if($rs->fields('car1') == "mz6s"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                }
                // MAZDA6 WAGON add 20190916
                if($rs->fields('car1') == "mz6w"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // MAZDA3 SEDAN add 20190515
                if($rs->fields('car1') == "mzs"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MAZDA3 FB add 20190515
                if($rs->fields('car1') == "mzf"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // MAZDA2 add 20191212
                if($rs->fields('car1') == "mz2"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
				// デミオ
				if($rs->fields('car1') == "dmo"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
				// ロードスター
				if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
				// ロードスターRF
				if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
				// CX-3
				if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
				// CX-5
				if($rs->fields('car1') == "cx5"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
				// CX-8
				if($rs->fields('car1') == "cx8"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
				}
				// アテンザ
				if($rs->fields('car1') == "atz"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
				// アクセラ
				if($rs->fields('car1') == "axl"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // MX-30 add 20201005
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg <> '1'";
		$sql .= " and date3 between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN add 20190916
                if($rs->fields('car1') == "mz6s"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    }
                }
                // MAZDA6 WAGON add 20190916
                if($rs->fields('car1') == "mz6w"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    }
                }
                // MAZDA3 SEDAN add 20190515
                if($rs->fields('car1') == "mzs"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    }
                }
                // MAZDA3 FB add 20190515
                if($rs->fields('car1') == "mzf"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    }
                }
                // MAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    }
                }
				// デミオ
				if($rs->fields('car1') == "dmo"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					}
				}
				// ロードスター
				if($rs->fields('car1') == "rst"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					}
				}
				// ロードスターRF
				if($rs->fields('car1') == "rsrf"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    }
                }
				// CX-3
				if($rs->fields('car1') == "cx3"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					}
				}
				// CX-5
				if($rs->fields('car1') == "cx5"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					}
				}
				// CX-8
				if($rs->fields('car1') == "cx8"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					}
				}
				// アテンザ
				if($rs->fields('car1') == "atz"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					}
				}
				// アクセラ
				if($rs->fields('car1') == "axl"){
					if($rs->fields('ampm') == '1'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
					if($rs->fields('ampm') == '2'){
						(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					}
				}
                // MX-30 add 20201005
                if($rs->fields('car1') == "mx30"){
                    if($rs->fields('ampm') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                    if($rs->fields('ampm') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select * from reservation";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg = '0'";
		$sql .= " and temporary_flg = '2'";
		$sql .= " and conf_flg = '1'";
		$sql .= " and conf_date between '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month'),$this->req->get('day'),$this->req->get('year'))))."'";
		$sql .= " and '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get('month_to'),$this->req->get('day_to'),$this->req->get('year_to'))))."'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
                // MAZDA6 SEDAN add 20190916
                if($rs->fields('car1') == "mz6s"){
                    (int)$summary[$rs->fields('shop_id')]['res_num11']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num11']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num11']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num11']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num11']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num11']++;
                    }
                }
                // MAZDA6 WAGON add 20190916
                if($rs->fields('car1') == "mz6w"){
                    (int)$summary[$rs->fields('shop_id')]['res_num12']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num12']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num12']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num12']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num12']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num12']++;
                    }
                }
                // MAZDA3 add 20190515 SEDANのみに upd 20190520
                if($rs->fields('car1') == "mzs"){
                    (int)$summary[$rs->fields('shop_id')]['res_num9']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num9']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num9']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num9']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num9']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num9']++;
                    }
                }
                // MAZDA3 FB add 20190520
                if($rs->fields('car1') == "mzf"){
                    (int)$summary[$rs->fields('shop_id')]['res_num10']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num10']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num10']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num10']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num10']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num10']++;
                    }
                }
                // MAZDA2 add 20191213
                if($rs->fields('car1') == "mz2"){
                    (int)$summary[$rs->fields('shop_id')]['res_num14']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num14']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num14']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num14']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num14']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num14']++;
                    }
                }
				// デミオ
				if($rs->fields('car1') == "dmo"){
					(int)$summary[$rs->fields('shop_id')]['res_num1']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num1']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num1']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num1']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num1']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num1']++;
					}
				}
				// ロードスター
				if($rs->fields('car1') == "rst"){
					(int)$summary[$rs->fields('shop_id')]['res_num2']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num2']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num2']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num2']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num2']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num2']++;
					}
				}
				// ロードスターRF
				if($rs->fields('car1') == "rsrf"){
					(int)$summary[$rs->fields('shop_id')]['res_num3']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num3']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num3']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num3']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num3']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num3']++;
					}
				}
                // CX-30 add 20191003
                if($rs->fields('car1') == "cx30"){
                    (int)$summary[$rs->fields('shop_id')]['res_num13']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num13']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num13']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num13']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num13']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num13']++;
                    }
                }
				// CX-3
				if($rs->fields('car1') == "cx3"){
					(int)$summary[$rs->fields('shop_id')]['res_num4']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num4']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num4']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num4']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num4']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num4']++;
					}
				}
				// CX-5
				if($rs->fields('car1') == "cx5"){
					(int)$summary[$rs->fields('shop_id')]['res_num5']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num5']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num5']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num5']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num5']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num5']++;
					}
				}
				// CX-8
				if($rs->fields('car1') == "cx8"){
					(int)$summary[$rs->fields('shop_id')]['res_num6']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num6']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num6']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num6']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num6']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num6']++;
					}
				}
				// アテンザ
				if($rs->fields('car1') == "atz"){
					(int)$summary[$rs->fields('shop_id')]['res_num7']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num7']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num7']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num7']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num7']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num7']++;
					}
				}
				// アクセラ
				if($rs->fields('car1') == "axl"){
					(int)$summary[$rs->fields('shop_id')]['res_num8']++;
					(int)$summary[$rs->fields('shop_id')]['conf_num8']++;
					if($rs->fields('visit_flg') == '1'){
						(int)$summary[$rs->fields('shop_id')]['visit_num8']++;
					}
					// 成約数
					if($rs->fields('result_close') == '1'){
						(int)$summary[$rs->fields('shop_id')]['ab_num8']++;
					}
					// フォロー継続
					if($rs->fields('result_status') == '1'){
						(int)$summary[$rs->fields('shop_id')]['next_num8']++;
					}
					// フォロー中止
					else if($rs->fields('result_status') == '2'){
						(int)$summary[$rs->fields('shop_id')]['ng_num8']++;
					}
				}
                // MX-30 add 20201005
                if($rs->fields('car1') == "mx30"){
                    (int)$summary[$rs->fields('shop_id')]['res_num15']++;
                    (int)$summary[$rs->fields('shop_id')]['conf_num15']++;
                    if($rs->fields('visit_flg') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['visit_num15']++;
                    }
                    // 成約数
                    if($rs->fields('result_close') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['ab_num15']++;
                    }
                    // フォロー継続
                    if($rs->fields('result_status') == '1'){
                        (int)$summary[$rs->fields('shop_id')]['next_num15']++;
                    }
                    // フォロー中止
                    else if($rs->fields('result_status') == '2'){
                        (int)$summary[$rs->fields('shop_id')]['ng_num15']++;
                    }
                }
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $summary;
	}
}

?>