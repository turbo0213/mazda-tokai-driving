<?php
define("TEMPLATE", "oneday_admin/login.html");
define("TEMPLATE1", "oneday_admin/menu.html");
include_once("../../mc_apl/top.php");
$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB3 = new ASDB_SERVICE();
//		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			case 'login':
				$this->login_proc();
			break;
			case 'logout':
				$this->logout_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
		unset($_SESSION['login']);
		if($_GET['number']){
			$_SESSION['login']['login_number'] = $_GET['number'];
		}
		if($_GET['h_flg']){
			$_SESSION['login']['login_h_flg'] = $_GET['h_flg'];
		}
		if($_SESSION['oneday']['authenticate']){
			if($_SESSION['login']['login_number']){
				$login_number = $_SESSION['login']['login_number'];
				$login_h_flg = $_SESSION['login']['login_h_flg'];
				unset($_SESSION['login']);
				header("Location:campaign_detail.php?number=".$login_number."&h_flg=".$login_h_flg."&m=login&ret=calender");
				exit;
			}
			header("Location:camp_cal.php");
			exit;
		}
		$this->templ->smarty->display(TEMPLATE);
		exit;
	}

	function login_proc(){
		unset( $_SESSION['oneday'] );
		unset( $_SESSION['search'] );
		if(!$this->req->get('login_id')){
			$this->req->setError('error',"IDを入力して下さい。");
			$this->templ->error_assign($this->req);
			$this->templ->smarty->display(TEMPLATE);
			exit;
		}
		if(!$this->req->get('password')){
			$this->req->setError('error',"パスワードを入力して下さい。");
			$this->templ->error_assign($this->req);
			$this->templ->smarty->display(TEMPLATE);
			exit;
		}
		if(!$this->authenticate($this->req->get('login_id'), $this->req->get('password'))){
			$this->req->setError('error','IDまたはパスワードが間違っています。');
			$this->templ->error_assign($this->req);
			$this->templ->smarty->display(TEMPLATE);
			exit;
		}
		if($_SESSION['login']['login_number']){
			$login_number = $_SESSION['login']['login_number'];
			$login_h_flg = $_SESSION['login']['login_h_flg'];
			unset($_SESSION['login']);
			header("Location:campaign_detail.php?number=".$login_number."&h_flg=".$login_h_flg."&ret=calender");
			exit;
		}
		header("Location:camp_cal.php");
		exit;
	}

	function logout_proc(){
		unset( $_SESSION['oneday'] );
		unset( $_SESSION['search'] );
		$_SESSION = array();
		header("Location:login.php");
		exit;
	}

	//認証
	function authenticate($login_id,$password){
		if(!$login_id or !$password){
			return false;
		}
		$sql = "select * from send_mail";
		$sql .= " where staff_id = '".$this->DB3->getQStr( $login_id )."'";
		$sql .= " and driving_user_flg = '1'";
		$sql .= " and del_flg='0' and disp_flg = '1'";
		$rs =& $this->DB3->ASExecute($sql);
		if($rs){
			if (!$rs->EOF){
				if($login_id == $rs->fields('staff_id') && $password == $rs->fields('password') ) {
					//$_SESSION = array();
					$_SESSION['oneday']['authenticate'] = true;
					$_SESSION['oneday']['staff_autono'] = $rs->fields('autono');
					$_SESSION['oneday']['staff_id'] = $rs->fields('staff_id');
					$_SESSION['oneday']['shop_id'] = $rs->fields('driving_shop_id');
					$_SESSION['oneday']['staff_name'] = $rs->fields('staff_name');
					$_SESSION['oneday']['access_kb'] = $rs->fields('access_kb');
					$_SESSION['oneday']['station_code'] = $rs->fields('driving_station_code');
					if($rs->fields('access_kb') == "4"){
						$_SESSION['oneday']['staff_mng'] = true;
					}
					else{
						$_SESSION['oneday']['staff_mng'] = false;
					}
					$_SESSION['oneday']['no_result_flg'] = $this->result_check();
					if($password == DEFAULT_PW){
                        $_SESSION['oneday']['pw_chg_flg'] = 1;
                        header("Location:pass.php");
                        exit;
                    }
					return true;
				}
			}
			$rs->Close();
		}
		return false;
	}

	// 試乗結果未登録データ取得
	function result_check(){
		if($_SESSION['oneday']['access_kb'] == "1" or $_SESSION['oneday']['access_kb'] == "2"){
			$limit_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-3,date("Y")));
			$sql = "select count(autono) as cnt from reservation ";
			$sql .= "where disp_flg = '1' ";
			$sql .= "and del_flg = '0' ";
			$sql .= "and temporary_flg = '2' ";
			$sql .= "and conf_flg = '1' ";
			$sql .= "and shop_id = '".$this->DB->getQStr( $_SESSION['oneday']['shop_id'] )."' ";
			$sql .= "and conf_date <= '".$this->DB->getQStr( $limit_date )."' ";
			$sql .= "and visit_flg not in ('1','2') ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('cnt') > 0){
						$rs->Close();
						return ture;
					}
				}
				$rs->Close();
			}
		}
		return false;
	}
}

?>