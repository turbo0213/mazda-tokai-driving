<?php

include_once("../../mc_apl/top.php");
include_once("../../mc_apl/auth.php");
include_once("../../mc_apl/calendar_make.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
		$this->util = new util();
		$this->cl = new calendar();
		$this->templ->smarty->assign("login_staff_name",$_SESSION['oneday']['staff_name']);
		$this->templ->smarty->assign("access_kb",$_SESSION['oneday']['access_kb']);
	}

	function execute(){
		//覚書・access_kb='1'がスタッフ・access_kb='2'が店長・access_kb='4'が本部・access_kb='9'が確認ユーザー
		switch($this->mode){
            // 詳細確認・変更
			case 'edit':
				$this->edit_proc();
				break;
            // 新規追加
			case 'input':
				$this->input_proc();
				break;
            // 登録(新規)
			case 'input_entry':
				$this->input_entry_proc();
				break;
            // 登録(編集)
			case 'edit_entry':
				$this->edit_entry_proc();
				break;
            // 削除
			case 'delete':
				$this->delete_proc();
				break;
            // 終了画面
			case 'end':
				$this->end_proc();
				break;
            // 試乗車種一覧
			default:
				$this->default_proc();
				break;
		}
	}
	function form_make(){
		if($_SESSION['oneday']['access_kb'] == 1){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 2){
			$_SESSION['search']['shop'] = $_SESSION['oneday']['shop_id'];
		}
		else if($_SESSION['oneday']['access_kb'] == 3){
			$_SESSION['search']['station'] = $_SESSION['oneday']['station_code'];
		}
		else if($_SESSION['oneday']['access_kb'] == '4' or $_SESSION['oneday']['access_kb'] == '9'){
			$station_list = $this->util->station_list_get("0","0",$this->DB);
			if($this->req->get('station')){
				$_SESSION['search']['station'] = $this->req->get('station');
			}
//			if(!$_SESSION['search']['station']){
//				foreach($station_list as $key => $val){
//					$_SESSION['search']['station'] = $key;
//					break;
//				}
//			}
			$this->templ->smarty->assign('station',$_SESSION['search']['station']);
			$this->templ->smarty->assign('station_list',$station_list);
			$shop_list = $this->util->shop_list_get($_SESSION['search']['station'],"0","0",$this->DB);
			if($this->req->get('type') == 'station_change' or !$_SESSION['search']['shop']){
				//店舗初期値設定
//				if($shop_list){
//					if(is_array($shop_list)){
//						foreach($shop_list as $key => $val){
//							$_SESSION['search']['shop'] = $key;
//							break;
//						}
//					}
//				}
			}
			else if($this->req->get('shop')){
				$_SESSION['search']['shop'] = $this->req->get('shop');
			}
			$this->templ->smarty->assign('shop',$_SESSION['search']['shop']);
			$this->templ->smarty->assign('shop_list',$shop_list);
		}
		$car_list = array();
		// 車種 upd 20190130 turbo対応
		$sql = "select car1,car2,car3,car4,car5 from car_detail ";
		$sql .= " where shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		$sql .= " group by car1,car2,car3,car4,car5";
		$sql .= " order by car1,car2,car3,car4,car5";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$engine = "";
				if($rs->fields('car2') == "ge"){
					$engine = "ガソリン";
				}
				else if($rs->fields('car2') == "de"){
					$engine = "ディーゼル";
				}
				else if($rs->fields('car2') == "hev"){
					$engine = "ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == 'skyx'){
                    $engine = "SKYACTIV-X";
                }
                // add 20201009 e-SKYACTIV G対応
                else if($rs->fields('car2') == 'eskyg'){
                    $engine = "e-SKYACTIV G";
                }
				$car3 = $rs->fields('car3');
				$mt = "";
				if($rs->fields('car4') == "mt"){
					$mt = "MT";
				}
				// add 20190130 turbo対応
				$turbo = "";
				if($rs->fields('car5') == "turbo"){
					$turbo = " ターボ";
				}
//				$week_name = "（休日）";
//				if($rs->fields('week_flg') == 1){
//					$week_name = "（平日）";
//				}
				$sql = "select * from car";
//				$sql .= " where car_id='".$this->DB->getQStr($rs->fields('car_id'))."'";
				$sql .= " where car1 = '".$this->DB->getQStr($rs->fields('car1'))."'";
				$sql .= " and car2 = '".$this->DB->getQStr($rs->fields('car2'))."'";
				$rs2 =& $this->DB->ASExecute($sql);
				if($rs2){
					if(!$rs2->EOF){
						$carname = $rs2->fields('name');
						if($engine){
							$carname .= " ".$engine;
						}
						if($mt){
							$carname .= " ".$mt." ".$car3.$turbo;
						}
						else{
							$carname .= " ".$car3.$turbo;
						}
						$car_list[$rs2->fields('car_id')] = $carname;
//						$car_list[$rs2->fields('car_id')] = $rs2->fields('name')." ".$rs2->fields('name2')." ".$rs2->fields('name3').$week_name;
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("car", $_SESSION['search']['car']);
		$this->templ->smarty->assign("car_list", $car_list);
		$this->templ->smarty->assign("start_year_list",$this->util->year_list());
		$this->templ->smarty->assign("start_month_list",$this->util->month_list());
		$this->templ->smarty->assign("start_day_list",$this->util->day_list());
		$this->templ->smarty->assign("end_year_list",$this->util->year_list());
		$this->templ->smarty->assign("end_month_list",$this->util->month_list());
		$this->templ->smarty->assign("end_day_list",$this->util->day_list());
		$this->templ->smarty->assign("exception_year_list",$this->util->year_list());
		$this->templ->smarty->assign("exception_month_list",$this->util->month_list());
		$this->templ->smarty->assign("exception_day_list",$this->util->day_list());
		$this->templ->smarty->assign("regist_year_list",$this->util->year2_list());
		$this->templ->smarty->assign("regist_month_list",$this->util->month_list());
		$this->templ->smarty->assign("regist_day_list",$this->util->day_list());
	}

	// データチェック (新規登録、編集登録)
	function datacheck_proc($upd_flg="0"){
		$error = null;
		$date_error = null;
		// 事業部
		if($this->req->get('station') == null){
			$error .= "事業部を選択してください\r\n";
		}
		// 店舗
		if($this->req->get('shop') == null){
			$error .= "店舗を選択してください\r\n";
		}
		else if($upd_flg == '1'){
			$sql = "select * from car_detail";
			$sql .= " where autono = ".$this->DB->getQStr($this->req->get('car_detail_id'));
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('shop_id') != $this->req->get('shop')){
						$error .= "店舗変更できません\r\n";
						$this->templ->smarty->assign("car_detail_id",$this->req->get('car_detail_id'));
					}
				}
				$rs->Close();
			}
		}
		// 車種
		if($this->req->get('car_id') == null){
			$error .= "車種を選択してください\r\n";
		}
		// 試乗期間
		if($this->req->get('start_year') == null or $this->req->get('start_month') == null or $this->req->get('start_day') == null
			    or $this->req->get('end_year') == null or $this->req->get('end_month') == null or $this->req->get('end_day') == null){
			$date_error .= "試乗期間を選択してください\r\n";
		}else{
			// 日付チェック
			if(checkdate($this->req->get('start_month'),$this->req->get('start_day'),$this->req->get('start_year')) == false ){
				$date_error .= "試乗期間(開始)の日付が存在しません\r\n";
			}
			if(checkdate($this->req->get('end_month'),$this->req->get('end_day'),$this->req->get('end_year')) == false ){
				$date_error .= "試乗期間(終了)の日付が存在しません\r\n";
			}

			$sUnix = mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day'),$this->req->get('start_year'));
			$eUnix = mktime(0,0,0,$this->req->get('end_month'),$this->req->get('end_day'),$this->req->get('end_year'));

			if($sUnix > $eUnix){
				$date_error .= "試乗期間(開始)の日付が試乗期間(終了)より後になっています\r\n";
			}
		}
		if(empty($date_error) and $this->req->get('shop') != null and empty($error)){
			if($this->req->get('car_detail_id')){
				// 予約データチェック
				$error .= $this->datacheck_reservation_proc(
					$upd_flg,
					$this->req->get('start_year') . "-" . $this->req->get('start_month') . "-" . $this->req->get('start_day'),
					$this->req->get('end_year') . "-" . $this->req->get('end_month') . "-" . $this->req->get('end_day'),
					$this->req->get('shop'),
					$this->req->get('car_detail_id')
				);
			}
		}else{
			$error .= $date_error;
		}
		// 区分
		/*
		if($this->req->get('week_flg') == null){
			$error .= "区分を選択してください\r\n";
		}elseif(is_numeric($this->req->get('week_flg')) == false){
			$error .= "不正な区分データです\r\n";
		}
		*/
		// 掲載可否(WEB表示)
		if($this->req->get('disp_flg') == null){
			$error .= "WEB表示を選択してください\r\n";
		}elseif(is_numeric($this->req->get('disp_flg')) == false){
			$error .= "WEB表示の値が不正です\r\n";
		}
		if($this->req->get('price') == null) { } else{
			$price = str_replace(",","",$this->req->get('price'));
			if(is_numeric($price) == false){
				$error .= "本体価格を半角数字で入力してください\r\n";
			}
		}
		$exception_date = $_SESSION["exception_date_list"];//$this->req->params['exception_date'];
		if($exception_date){
			if(is_array($exception_date)){
				foreach($exception_date as $key => $val){
					$year = substr($val,0,4);
					$month = substr($val,5,2);
					$day = substr($val,8,2);
					//相関チェック(試乗期間内)
//					if($this->req->get('start_year') and $this->req->get('start_month') and $this->req->get('start_day') and $this->req->get('end_year') and $this->req->get('end_month') and $this->req->get('end_day')){
//						$from = mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day'),$this->req->get('start_year'));
//						$to = mktime(0,0,0,$this->req->get('end_month'),$this->req->get('end_day'),$this->req->get('end_year'));
//						$check_ymd = mktime(0,0,0,$month,$day,$year);
//						if($check_ymd < $from or $check_ymd > $to ){
                            //$error .= "貸出不可日の日付は試乗期間内に設定してください\r\n";
//						}
//					}
					if($this->req->get('car_detail_id')){
						$cnt1 = 0;
						$cnt2 = 0;
						//予約チェック
						$sql = "select count(autono) as cnt from reservation ";
						$sql .= " where disp_flg = '1' and del_flg = '0' and temporary_flg = '2' ";
						$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_detail_id'));
						$sql .= " and date = '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$month,$day,$year)))."' ";
						$rs =& $this->DB->ASExecute($sql);
						$data = array();
						if($rs){
							if(!$rs->EOF){
								$cnt1 = $rs->fields('cnt');
								//$dat = array();
								//$dat[$rs->fields('cnt')] = $rs->fields('cnt');
								//$data[] = $dat[$rs->fields('cnt')];
							}
							$rs->Close();
						}
						//すでに登録されている設定不可日のチェックはスルー
						$sql = "select * from exception";
						$sql .= " where disp_flg = '1' and del_flg = '0'";
						$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_detail_id'));
						$sql .= " and date = '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$month,$day,$year)))."' ";
						$rs =& $this->DB->ASExecute($sql);
						if($rs){
							if(!$rs->EOF){
								$cnt1 = 0;
							}
							$rs->Close();
						}
						$sql = "select count(autono) as cnt from reservation ";
						$sql .= " where disp_flg = '1' and del_flg = '0' and temporary_flg = '1' ";
						$sql .= " and car_detail_id = ".$this->DB->getQStr($this->req->get('car_detail_id'));
						$sql .= " and validity_date > '".$this->DB->getQStr(date("Y-m-d H:i:s"))."' ";
						$rs =& $this->DB->ASExecute($sql);
						if($rs){
							if(!$rs->EOF){
								$cnt2 = $rs->fields('cnt');
							}
							$rs->Close();
						}
						if($cnt1 > 0){
							$error .= date("Y-m-d",mktime(0,0,0,$month,$day,$year))."は、すでに予約があるため貸出不可日に設定することはできません\r\n";
						}
						else if($cnt2 > 0){
							$error .= date("Y-m-d",mktime(0,0,0,$month,$day,$year))."は、仮予約があるため貸出不可日に設定することはできません\r\n";
						}
					}
				}
			}
		}
		return $error;
	}

	// 貸出不可日データチェック (新規登録、編集登録)
	function datacheck_exception_proc(){
		$error = null;
		// 日付チェック(選択)
		if($this->req->get('exception_year') == null or $this->req->get('exception_month') == null or $this->req->get('exception_day') == null){
			$error .= "貸出不可日の日付を選択して下さい\r\n";
			return $error;
		}
		// 日付チェック(数字)
		if(is_numeric($this->req->get('exception_year')) == false or is_numeric($this->req->get('exception_month')) == false or is_numeric($this->req->get('exception_day')) == false){
			$error .= "貸出不可日の日付を数字で選択して下さい\r\n";
		}
		// 日付チェック(日付)
		if(checkdate($this->req->get('exception_month'),$this->req->get('exception_day'),$this->req->get('exception_year')) == false ){
			$error .= "貸出不可日の日付がカレンダーに存在しません\r\n";
		}
		// 日付チェック(重複)
		if(!empty($this->req->params['exception_date'])){
			if(array_search($this->req->get('exception_year')."-".$this->req->get('exception_month')."-".$this->req->get('exception_day'),$this->req->params['exception_date']) == true){
				$error .= "貸出不可日の日付が既に選択されています\r\n";
			}
		}
		return $error;
	}

	// 予約データチェック (新規登録、編集登録、削除登録)
	function datacheck_reservation_proc($upd_flg,$start_date="",$end_date="",$shop_id,$car_detail_id){
		$error = null;
		if($upd_flg == 1){
			//店舗変更の場合は、削除処理と同じチェックを行う
			$sql = "select autono,shop_id from car_detail ";
			$sql .= "where autono = ".$this->DB->getQStr($car_detail_id);
			$sql .= " and del_flg = '0' and disp_flg = '1' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					if($rs->fields('shop_id') != $shop_id){
						$upd_flg = 3;
						$shop_id = $rs->fields('shop_id');
					}
				}
				$rs->Close();
			}
			if($upd_flg == 1){
				$cnt = 0;
				$sql = "select count(autono) as cnt from reservation ";
				$sql .= " where disp_flg = '1' and del_flg = '0' and temporary_flg = '2' ";
				$sql .= "and shop_id = '".$this->DB->getQStr($shop_id)."' ";
				$sql .= "and (date < '".$this->DB->getQStr($start_date)."' ";
				$sql .= "or date > '".$this->DB->getQStr($end_date)."') ";
				$sql .= "and car_detail_id = ".$this->DB->getQStr($car_detail_id);
				$rs =& $this->DB->ASExecute($sql);
				$data = array();
				if($rs){
					if(!$rs->EOF){
						$cnt = $rs->fields('cnt');
					}
					$rs->Close();
				}
				if($cnt > 0){
					$error = "試乗期間外に予約データがあるため変更できません\r\n";
				}
			}
		}
		if($upd_flg == 2 or $upd_flg == 3){
			$cnt1 = 0;
			$cnt2 = 0;
			$sql = "select count(autono) as cnt from reservation ";
			$sql .= " where disp_flg = '1' and del_flg = '0' and temporary_flg = '2' ";
			$sql .= " and shop_id = '".$this->DB->getQStr($shop_id)."' ";
			$sql .= " and car_detail_id = ".$this->DB->getQStr($car_detail_id);
			$rs =& $this->DB->ASExecute($sql);
			$data = array();
			if($rs){
				if(!$rs->EOF){
					$cnt1 = $rs->fields('cnt');
					//$dat = array();
					//$dat[$rs->fields('cnt')] = $rs->fields('cnt');
					//$data[] = $dat[$rs->fields('cnt')];
				}
				$rs->Close();
			}
			$sql = "select count(autono) as cnt from reservation ";
			$sql .= " where disp_flg = '1' and del_flg = '0' and temporary_flg = '1' ";
			$sql .= " and shop_id = '".$this->DB->getQStr($shop_id)."' ";
			$sql .= " and car_detail_id = ".$this->DB->getQStr($car_detail_id);
			$sql .= " and validity_date > '".$this->DB->getQStr(date("Y-m-d H:i:s"))."' ";
			$rs =& $this->DB->ASExecute($sql);
			if($rs){
				if(!$rs->EOF){
					$cnt2 = $rs->fields('cnt');
				}
				$rs->Close();
			}
			if($cnt1 > 0){
				if($upd_flg == 2){
					$error = "すでに予約データがあるため削除できません\r\n";
				}
				else{
					$error = "すでに予約データがあるため変更できません\r\n";
				}
			}
			if($cnt2 > 0){
				if($upd_flg == 2){
					$error = "仮予約データがあるため削除できません\r\n";
				}
				else{
					$error = "仮予約データがあるため変更できません\r\n";
				}
			}
		}
		return $error;
	}

	// 貸出不可日 新規登録 (新規登録、編集登録)
	function data_input_exception_proc($mysql_insert_id){
		$exception_date = $_SESSION["exception_date_list"];// $this->req->get('exception_date');
		if(!empty($exception_date)){
			foreach($exception_date as $key => $exception_dates){
				$record = null;
				$record['shop_id'] = $this->req->get('shop');
				$record['car_id'] = $this->req->get('car_id');
				$record['date'] = $exception_dates;
				$record['car_detail_id'] = $mysql_insert_id;
				$record['create_date'] = time();
				$record['update_date'] = time();
				$ret = $this->DB->getCon()->AutoExecute("exception", $record, 'INSERT');
			}
		}
	}

	// 貸出不可日 新規登録 (新規登録時のみ土日祝日貸出不可)
	function data_input_exception2_proc($mysql_insert_id){
		$sql = "select * from car_detail";
		$sql .= " where autono = ".$this->DB->getQStr($mysql_insert_id);
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$start_date = $rs->fields('start_date');
				$end_date = $rs->fields('end_date');
				$car_id = $rs->fields('car_id');
				$shop_id = $rs->fields('shop_id');
			}
			$rs->Close();
		}
		$start_y = substr($start_date,0,4);
		$start_m = substr($start_date,5,2);
		$start_d = substr($start_date,8,2);
		$start = mktime(0,0,0,$start_m,$start_d,$start_y);
		$end = mktime(0,0,0,substr($end_date,5,2),substr($end_date,8,2),substr($end_date,0,4));
		for($i=0;$start < $end;$i++){
			$start = mktime(0,0,0,$start_m,$start_d+$i,$start_y);
			$date = date("Y-m-d",mktime(0,0,0,$start_m,$start_d+$i,$start_y));
			$start_w = date("w",mktime(0,0,0,$start_m,$start_d+$i,$start_y));
			if($this->util->pday_list(1,$date) or $start_w == 0 or $start_w == 6 or $this->util->holiday_list($this->DB,$shop_id,1,$date)){
				$record = null;
				$record['shop_id'] = $shop_id;
				$record['car_id'] = $car_id;
				$record['date'] = $date;
				$record['car_detail_id'] = $mysql_insert_id;
				$record['create_date'] = time();
				$record['update_date'] = time();
				$ret = $this->DB->getCon()->AutoExecute("exception", $record, 'INSERT');
			}
		}
	}

	// 在庫新規登録 (新規登録、編集登録)
	function data_input_zaiko_proc($mysql_insert_id){
		// 日付取得
//		$start = mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day'),$this->req->get('start_year'));
//		$end = mktime(0,0,0,$this->req->get('end_month'),$this->req->get('end_day'),$this->req->get('end_year'));
//		for($i = 0;$start < $end;$i++){
//			$start = mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day')+$i,$this->req->get('start_year'));
//			$date = date("Y-m-d",mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day')+$i,$this->req->get('start_year')));
		// 登録(在庫)
//			$record = null;
//			$record['shop_id'] = $this->req->get('shop');
//			$record['car_detail_id'] = $mysql_insert_id;
//			$record['date'] = $date;
//			$record['num'] = "1";
		//$record['week_flg'] = $this->req->get('week_flg');
//			$record['car_detail_id'] = $mysql_insert_id;
//			$record['create_date'] = time();
//			$record['update_date'] = time();
//			$ret = $this->DB->getCon()->AutoExecute("zaiko", $record, 'INSERT');
//			if($ret == false){
//				$sql = "rollback;";
//				$rs =& $this->DB->ASExecute($sql);
//				echo "ERROR:driving_list_new(3)";exit;
//			}
//		}
	}

	// データ編集登録 (編集登録)
	function data_edit_entry_proc(){
		$sql = "START TRANSACTION;";
		$rs =& $this->DB->ASExecute($sql);
		$start_date = $this->req->get('start_year')."-".$this->req->get('start_month')."-".$this->req->get('start_day');
		$end_date = $this->req->get('end_year')."-".$this->req->get('end_month')."-".$this->req->get('end_day');
		// 貸出不可日 削除登録
		$this->data_delete_exception_proc($this->req->get('car_detail_id'));
		// 在庫削除登録
//		$this->data_delete_zaiko_proc($this->req->get('car_detail_id'),$this->req->get('shop'),$start_date,$end_date);
		// 貸出不可日 新規登録
		$this->data_input_exception_proc($this->req->get('car_detail_id'));
		// 在庫新規登録
//		$this->data_input_zaiko_proc($this->req->get('car_detail_id'));
		// 編集登録(試乗車種)
		$record = null;
		$record['shop_id'] = $this->req->get('shop');
		$record['car_id'] = $this->req->get('car_id');
		//$record['week_flg'] = $this->req->get('week_flg');
		$record['car_model'] = $this->req->get('car_model');
		$record['grade'] = $this->req->get('grade');
		$record['color'] = $this->req->get('color');
		$record['model_code'] = $this->req->get('model_code');
		$record['model'] = $this->req->get('model');
		$record['car_no'] = $this->req->get('car_no');
		$record['no_plate'] = $this->req->get('no_plate');
		//$record['regist_date'] = $this->req->get('regist_date');
		$record['regist_year'] = $this->req->get('regist_year');
		$record['regist_month'] = $this->req->get('regist_month');
		$record['regist_day'] = $this->req->get('regist_day');
		$record['regist_ym'] = $this->req->get('regist_ym');
		$record['price'] = str_replace(",","",$this->req->get('price'));
		$record['note1'] = $this->req->get('note1');
		$record['note2'] = $this->req->get('note2');
		$record['option'] = $this->req->get('option');
		$record['propriety'] = $this->util->disp_flg_list(1,$this->req->get('disp_flg'));
		$record['disp_flg'] = $this->req->get('disp_flg');
		$record['start_date'] = $start_date;
		$record['end_date'] = $end_date;
		$record['update_date'] = time();
		$where  = " autono=".$this->DB->getQStr($this->req->get('car_detail_id'));
		$where .= " and del_flg = '0'";
		$ret = $this->DB->getCon()->AutoExecute("car_detail", $record, 'UPDATE',$where);
		$sql = "COMMIT;";
		$rs =& $this->DB->ASExecute($sql);
	}

	// データ新規登録 (新規登録)
	function data_input_entry_proc(){
		$sql = "BEGIN;";
		$rs = &$this->DB->ASExecute($sql);
		// 登録(試乗車種)
		$record = null;
		$record['shop_id'] = $this->req->get('shop');
		$car_id = explode(",",$this->req->get('car_id'));
		$record['car1'] = $car_id[0];
		$record['car2'] = $car_id[1];
		$record['car3'] = $car_id[2];
		$record['car4'] = $car_id[3];
		// add 20190130 turbo対応
        $record['car5'] = $car_id[4];
		$sql = "select car_id from car";
		$sql .= " where car1 ='".$this->DB->getQStr($car_id[0])."'";
		$sql .= " and car2 ='".$this->DB->getQStr($car_id[1])."'";
		$sql .= " and car4 ='".$this->DB->getQStr($car_id[3])."'";
		// add 20190130 turbo対応
		if($car_id[4]){
            $sql .= " and car5 ='".$this->DB->getQStr($car_id[4])."'";
        }
		else{
            $sql .= " and car5 is NULL";
        }
		$sql .= " order by car_id";
		$sql .= " limit 1";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$record['car_id'] = $rs->fields('car_id');
			}
			$rs->Close();
		}
		$record['week_flg'] = "0";
		$record['car_model'] = $this->req->get('car_model');
		$record['grade'] = $this->req->get('grade');
		$record['color'] = $this->req->get('color');
		$record['model_code'] = $this->req->get('model_code');
		$record['model'] = $this->req->get('model');
		$record['car_no'] = $this->req->get('car_no');
		$record['no_plate'] = $this->req->get('no_plate');
		//$record['regist_date'] = $this->req->get('regist_date');
		$record['regist_year'] = $this->req->get('regist_year');
		$record['regist_month'] = $this->req->get('regist_month');
		$record['regist_day'] = $this->req->get('regist_day');
		$record['regist_ym'] = $this->req->get('regist_ym');
		$record['price'] = str_replace(",","",$this->req->get('price'));
		$record['note1'] = $this->req->get('note1');
		$record['note2'] = $this->req->get('note2');
		$record['option'] = $this->req->get('option');
		$record['propriety'] = $this->util->disp_flg_list(1,$this->req->get('disp_flg'));
		$record['disp_flg'] = $this->req->get('disp_flg');
		$record['start_date'] = date("Y-m-d");
		$add_day = 2;
		if(date("d") >= 25){
			$add_day = 3;
		}
		$record['end_date'] = date("Y-m-d",mktime(0,0,0,date("m")+$add_day,0,date("Y")));
		$record['start_date'] = $this->req->get('start_year')."-".$this->req->get('start_month')."-".$this->req->get('start_day');
		$record['end_date'] = $this->req->get('end_year')."-".$this->req->get('end_month')."-".$this->req->get('end_day');
		$record['create_date'] = time();
		$record['update_date'] = time();
		$ret = $this->DB->getCon()->AutoExecute("car_detail", $record, 'INSERT');
		if($ret == false){
			echo "ERROR:driving_list_new(1)";exit;
		}
		$sql = "SELECT currval('car_detail_autono_seq') as inid";
		$rs =& $this->DB->ASExecute($sql);
		$at = "";
		if($rs){
			if(!$rs->EOF){
				$at = $rs->fields('inid');
				$rs->Close();
			}
		}
		$mysql_insert_id = $at;
		// 貸出不可日 新規登録
		$this->data_input_exception_proc($mysql_insert_id);
		// 新規登録の場合のみ土日・祝日貸出不可に設定
		//$this->data_input_exception2_proc($mysql_insert_id);
		// 在庫新規登録
		//$this->data_input_zaiko_proc($mysql_insert_id);
		$sql = "COMMIT;";
		$rs =& $this->DB->ASExecute($sql);
	}

	// 貸出不可日 削除登録(編集登録、削除登録)
	function data_delete_exception_proc($autono){
		$sql = "select * from exception ";
		$sql .= "where car_detail_id = ".$this->DB->getQStr($autono);
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				// 削除登録(貸出不可日)
				$record = null;
				$record['del_flg'] = "1";
				$record['disp_flg'] = "0";
				$record['update_date'] = time();
				$where  = " car_detail_id = ".$this->DB->getQStr($autono);
				$where .= " and del_flg = '0' and disp_flg = '1' ";
				$ret = $this->DB->getCon()->AutoExecute("exception", $record, 'UPDATE',$where);
			}
			$rs->Close();
		}
	}

	// 在庫削除登録(編集登録、削除登録)
	function data_delete_zaiko_proc($autono,$shop_id,$start_date="",$end_date=""){
		$record = null;
		$record['del_flg'] = "1";
		$record['disp_flg'] = "0";
		$record['update_date'] = time();
		$where  = " car_detail_id=".$this->DB->getQStr($autono);
		$where .= " and del_flg = '0' and disp_flg = '1' ";
		$ret = $this->DB->getCon()->AutoExecute("zaiko", $record, 'UPDATE',$where);
		if($ret == false){
			echo "ERROR:driving_list_delete(3)";exit;
		}
	}

	// データ削除登録 (削除登録)
	function data_delete_entry_proc($autono,$shop_id,$start_date,$end_date){
		$sql = "START TRANSACTION;";
		$rs =& $this->DB->ASExecute($sql);
		// 削除登録(試乗車種)
		$record = null;
		$record['del_flg'] = "1";
		$record['disp_flg'] = "0";
		$record['create_date'] = time();
		$record['update_date'] = time();
		$where  = " autono=".$this->DB->getQStr($autono);
		$ret = $this->DB->getCon()->AutoExecute("car_detail", $record, 'UPDATE',$where);
		// 貸出不可日 削除登録
		$this->data_delete_exception_proc($autono);
		// 在庫削除登録 日付がいるかどうか後で確認
		//$this->data_delete_zaiko_proc($autono,$shop_id,$start_date,$end_date);
		$sql = "COMMIT;";
		$rs =& $this->DB->ASExecute($sql);
	}

	// 事業部リスト (新規登録、編集登録、一覧)
	function station_name_proc($station_name="",$mode=0){
		$sql = "select autono,station_name from shop ";
		$sql .= " where disp_flg = '1' and del_flg = '0' ";
		$sql .= " group by station_name ";
		$sql .= " order by station_name ";
		$rs =& $this->DB->ASExecute($sql);
		$data = array();
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['autono'] = $rs->fields('autono');
				$dat['station_name'] = $rs->fields('station_name');
				$data[$dat['autono']] = $dat['station_name'];
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$list = $this->util->shop_id_list($data);
		switch($mode){
			case '0':
				$list = $this->util->shop_id_list($data);
				break;
			case '1':
				$list = $this->util->shop_id_list($data,1,$station_name);
				break;
		}
		return $list;
	}

	// 店舗リスト (新規登録、編集登録、一覧)
	function shop_id_proc($station_name="",$mode=0){
		$where = null;
		$name = $this->station_name_proc($station_name,1);
		if(!empty($name)) $where = "and station_name = '".$this->DB->getQStr($name)."' ";
		$sql = "select shop_id,name from shop ";
		$sql .= " where disp_flg = '1' and del_flg = '0' ";
		$sql .= $where;
		$rs =& $this->DB->ASExecute($sql);
		$data = array();
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['shop_id'] = $rs->fields('shop_id');
				$dat['name'] = $rs->fields('name');
				$data[$dat['shop_id']] = $dat['name'];
				$rs->MoveNext();
			}
			$rs->Close();
		}
		switch($mode){
			case '0':
				$list = $this->util->shop_id_list($data);
				break;
			case '1':
				$list = $this->util->shop_id_list($data,1,$station_name);
				break;
		}
		return $list;
	}

	// 車種リスト (新規登録、編集登録)
	function car_proc($shop_id){
/*
		$shop_car = array();
		$sql = "select car1,car2,car3,car4 from car_detail";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg ='0'";
		if($shop_id){
			$sql .= " and shop_id = '".$this->DB->getQStr($shop_id)."'";
		}
		else if($_SESSION['search']['shop']){
			$sql .= " and shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."'";
		}
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$shop_car[] = $rs->fields('car1').$rs->fields('car2').$rs->fields('car3').$rs->fields('car4');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$sql = "select car1,car2,car3,car4 from car_detail";
		$sql .= " where disp_flg = '1'";
		$sql .= " and del_flg ='0'";
		$sql .= " group by car1,car2,car3,car4";
		$sql .= " order by car1,car2,car3,car4";
		$rs =& $this->DB->ASExecute($sql);
		$data = array();
		if($rs){
			while(!$rs->EOF){
				$search_txt = $rs->fields('car1').$rs->fields('car2').$rs->fields('car3').$rs->fields('car4');
				if(in_array($search_txt,$shop_car,true)){
					$rs->MoveNext();
					continue;
				}
				else{
*/
		$sql2 = "select * from car ";
		$sql2 .= " where disp_flg = '1'";
		$sql2 .= " and del_flg = '0'";
		$sql2 .= " order by car1,car2,car3,car4";
		/*
					$sql2 .= " and car1 = '".$this->DB->getQStr($rs->fields('car1'))."'";
					$sql2 .= " and car2 = '".$this->DB->getQStr($rs->fields('car2'))."'";
					if($rs->fields('car4')){
						$sql2 .= " and car4 = '".$this->DB->getQStr($rs->fields('car4'))."'";
					}
					$sql2 .= " order by car_id";
					$sql2 .= " limit 1";
*/
		$rs2 = &$this->DB->ASExecute($sql2);
		if ($rs2) {
			while (!$rs2->EOF) {
				$dat = array();
				$dat['name'] = $rs2->fields('name');
				if ($rs2->fields('car2') == "ge") {
					$dat['name'] .= " ガソリン";
				} else if ($rs2->fields('car2') == "de") {
					$dat['name'] .= " ディーゼル";
				} else if ($rs2->fields('car2') == "hev") {
					$dat['name'] .= " ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($rs2->fields('car2') == 'skyx'){
                    $dat['name'] .= "　SKYACTIV-X";
                }
                // add 20201009 e-SKYACTIV G対応
                else if($rs2->fields('car2') == 'eskyg'){
                    $dat['name'] .= "e-SKYACTIV G";
                }
				if ($rs2->fields('car4') == "mt") {
					$dat['name'] .= " MT";
				}
				$dat['name'] .= " " . $rs2->fields('car3');
				// add 20190130 turbo対応
				if ($rs2->fields('car5') == "turbo") {
					$dat['name'] .= " ターボ";
				}
				// upd 20190130 turbo対応
				$data[$rs2->fields('car1') . "," . $rs2->fields('car2') . "," . $rs2->fields('car3') . "," . $rs2->fields('car4') . "," . $rs2->fields('car5')] = $dat['name'];
				$rs2->MoveNext();
			}
			$rs2->Close();
		}
/*
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
*/
//		$list = $this->util->shop_id_list($data);
		return $data;
	}

	// 試乗車種データ取得 (編集登録、削除登録)
	function driving_list_proc($autono){
	    // upd 20190130 turbo対応
		$sql  = "SELECT ";
		$sql .= "dl.autono,dl.shop_id,dl.car_id,dl.start_date,dl.end_date,dl.week_flg,dl.car_model,dl.grade,dl.color,";
		//$sql .= "dl.model_code,dl.model,dl.car_no,dl.no_plate,dl.regist_date,dl.regist_ym,dl.price,dl.note1,dl.note2,";
		$sql .= "dl.model_code,dl.model,dl.car_no,dl.no_plate,dl.regist_ym,dl.price,dl.note1,dl.note2,";
		$sql .= "dl.option,dl.moni_type,dl.navi,dl.propriety,dl.disp_flg,dl.regist_year,dl.regist_month,dl.regist_day,";
		$sql .= "sh.station_name,sh.station_code,dl.car1 as car1,dl.car2 as car2,dl.car3 as car3,dl.car4 as car4,dl.car5 as car5 ";
		$sql .= "FROM car_detail as dl ";
		$sql .= "INNER JOIN shop as sh ON dl.shop_id = sh.shop_id ";
		$sql .= "where ";
		$sql .= "dl.del_flg = '0' ";
		$sql .= "and sh.disp_flg = '1' and sh.del_flg = '0' ";
		$sql .= "and dl.autono = ".$this->DB->getQStr($autono);
		$rs =& $this->DB->ASExecute($sql);
		$data_list = array();
		if($rs){
			if(!$rs->EOF){
				$dat = array();
				$dat['car_detail_id'] = $rs->fields('autono');
				$dat['shop'] = $rs->fields('shop_id');
				$dat['station'] = $rs->fields('station_code');
				$dat['car_id'] = $rs->fields('car_id');
				$dat['car1'] = $rs->fields('car1');
				$dat['car2'] = $rs->fields('car2');
				$dat['car3'] = $rs->fields('car3');
				$dat['car4'] = $rs->fields('car4');
				// add 20190130 turbo対応
                $dat['car5'] = $rs->fields('car5');
				$dat['start_date'] = $rs->fields('start_date');
				$dat['end_date'] = $rs->fields('end_date');
				$dat['week_flg'] = $rs->fields('week_flg');
				$dat['car_model'] = $rs->fields('car_model');
				$dat['grade'] = $rs->fields('grade');
				$dat['color'] = $rs->fields('color');
				$dat['model_code'] = $rs->fields('model_code');
				$dat['model'] = $rs->fields('model');
				$dat['car_no'] = $rs->fields('car_no');
				$dat['no_plate'] = $rs->fields('no_plate');
				//$dat['regist_date'] = $rs->fields('regist_date');
				$dat['regist_year'] = $rs->fields('regist_year');
				$dat['regist_month'] = $rs->fields('regist_month');
				$dat['regist_day'] = $rs->fields('regist_day');
				$dat['regist_ym'] = $rs->fields('regist_ym');
				$dat['price'] = str_replace(",","",$rs->fields('price'));
				$dat['pricef'] = $this->price_proc($rs->fields('price'));
				$dat['note1'] = $rs->fields('note1');
				$dat['note2'] = $rs->fields('note2');
				$dat['option'] = $rs->fields('option');
				$dat['disp_flg'] = $rs->fields('disp_flg');
			}
			$rs->Close();
		}
		return $dat;
	}

	// 貸出不可日データ取得 (編集登録)
	function exception_proc($autono){
		$sql  = "SELECT autono,shop_id,car_detail_id,date from exception ";
		$sql .= "where disp_flg = '1' and del_flg = '0' ";
		$sql .= "and car_detail_id = ".$this->DB->getQStr($autono);
		$rs =& $this->DB->ASExecute($sql);
		$data_list = array();
		if($rs){
			while(!$rs->EOF){
				$dat = array();
				$dat['autono'] = $rs->fields('autono');
				$dat['shop'] = $rs->fields('shop_id');
				$dat['car_detail_id'] = $rs->fields('car_detail_id');
				$dat['date'] = $rs->fields('date');
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		return $data_list;
	}

	// 新規追加
	function input_proc(){
		//事業部変更
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
		}
		if($this->req->get('shop_id')){
			$_SESSION['search']['shop'] = $this->req->get('shop_id');
		}
		else if($_SESSION['search']['shop']){
			$this->req->set('shop_id',$_SESSION['search']['shop']);
		}
		$this->form_make();
		// フォーム作成(車種)
		$car_id_list = $this->car_proc($this->req->get('shop_id'));
		// 区分
		$week_flg_list = $this->util->week_flg_list();
		$week_flg_list_value[0] = 1;
		$week_flg_list_value[1] = 0;
		$disp_flg_list = $this->util->disp_flg2_list();
		$this->templ->smarty->assign("car_model",$this->req->get('car_model'));
		$this->templ->smarty->assign("grade",$this->req->get('grade'));
		$this->templ->smarty->assign("color",$this->req->get('color'));
		$this->templ->smarty->assign("model_code",$this->req->get('model_code'));
		$this->templ->smarty->assign("model",$this->req->get('model'));
		$this->templ->smarty->assign("car_no",$this->req->get('car_no'));
		$this->templ->smarty->assign("no_plate",$this->req->get('no_plate'));
		//$this->templ->smarty->assign("regist_date",$this->req->get('regist_date'));
		$this->templ->smarty->assign("regist_year",$this->req->get('regist_year'));
		$this->templ->smarty->assign("regist_month",$this->req->get('regist_month'));
		$this->templ->smarty->assign("regist_day",$this->req->get('regist_day'));
		$this->templ->smarty->assign("regist_ym",$this->req->get('regist_ym'));
		$this->templ->smarty->assign("price",str_replace(",","",$this->req->get('price')));
		$this->templ->smarty->assign("pricef",$this->price_proc($this->req->get('price')));
		$this->templ->smarty->assign("note1",$this->req->get('note1'));
		$this->templ->smarty->assign("note2",$this->req->get('note2'));
		$this->templ->smarty->assign("option",$this->req->get('option'));
		$this->templ->smarty->assign("exception_date",$this->req->get('exception_date'));
		if($this->req->get('exception_year')){
			$this->templ->smarty->assign("exception_year",$this->req->get('exception_year'));
		}
		else{
			$this->templ->smarty->assign("exception_year",date("Y"));
		}
		if($this->req->get('exception_month')){
			$this->templ->smarty->assign("exception_month",$this->req->get('exception_month'));
		}
		else{
			$this->templ->smarty->assign("exception_month",date("m"));
		}
		if($this->req->get('exception_day')){
			$this->templ->smarty->assign("exception_day",$this->req->get('exception_day'));
		}
		else{
			$this->templ->smarty->assign("exception_day",date("d"));
		}
		$this->templ->smarty->assign("upd_flg",$this->req->get('upd_flg'));
		$this->templ->smarty->assign("shop",$_SESSION['search']['shop']);
//		$this->templ->smarty->assign("shop",$this->req->get('shop'));
//		$this->templ->smarty->assign("station",$this->req->get('station'));
		$this->templ->smarty->assign("station",$_SESSION['search']['station']);
		$this->templ->smarty->assign("station_name",$this->req->get('station_name'));
		$this->templ->smarty->assign("shop_name",$this->req->get('shop_name'));
		$this->templ->smarty->assign("car_id",$this->req->get('car_id'));
		$this->templ->smarty->assign("car_id_list",$car_id_list);
		//車種名称取得(詳細確認用)
		$sql = "select name,name2,name3 from car ";
		$sql .= "where car_id = '".$this->DB->getQStr($this->req->get('car_id'))."' ";
		$sql .= "and del_flg = '0' and disp_flg = '1' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$this->templ->smarty->assign("car_name",$rs->fields('name')." ".$rs->fields('name2')." ".$rs->fields('name3')) ;
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("disp_flg_list",$disp_flg_list ) ;
		$this->templ->smarty->assign("disp_flg",$this->req->get('disp_flg'));
		$this->templ->smarty->assign("week_flg_list",$week_flg_list ) ;
		$this->templ->smarty->assign("week_flg_list_value",$week_flg_list_value);
		$this->templ->smarty->assign("start_date",$this->req->get('start_date'));
		$this->templ->smarty->assign("end_date",$this->req->get('end_date'));
		if($this->start_end_date_check()){
			if($this->req->get('start_year')){
				$start_year = $this->req->get('start_year');
			}
			else{
				$start_year = date("Y");
			}
			if($this->req->get('start_month')){
				$start_month = sprintf("%02d",$this->req->get('start_month'));
			}
			else{
				$start_month = date("m");
			}
			if($this->req->get('start_day')){
				$start_day = sprintf("%02d",$this->req->get('start_day'));
			}
			else{
				$start_day = date("d");
			}
			if($this->req->get('end_year')){
				$end_year = $this->req->get('end_year');
			}
			else{
				$end_year = date("Y");
			}
			if($this->req->get('end_month')){
				$end_month = sprintf("%02d",$this->req->get('end_month'));
			}
			else{
				$end_month = date("m");
			}
			if($this->req->get('end_day')){
				$end_day = sprintf("%02d",$this->req->get('end_day'));
			}
			else{
				$end_day = date("d");
			}
		}
		$this->templ->smarty->assign("start_year",$start_year);
		$this->templ->smarty->assign("start_month",$start_month);
		$this->templ->smarty->assign("start_day",$start_day);
		$this->templ->smarty->assign("end_year",$end_year);
		$this->templ->smarty->assign("end_month",$end_month);
		$this->templ->smarty->assign("end_day",$end_day);
		/*
		if($this->req->get('week_flg')){
			$this->templ->smarty->assign("week_flg",$this->req->get('week_flg'));
		}else{
			$this->templ->smarty->assign("week_flg",0);
		}
		$common_year = date("Y");
		if($this->req->get('year')){
			$common_year = $this->req->get('year');
		}
		*/
		if($this->req->get_get('year')){
			$common_year = $this->req->get_get('year');
		}
		$common_month = date("m");
		if($this->req->get('month')){
			$common_month = $this->req->get('month');
		}
		if($this->req->get_get('month')){
			$common_month = $this->req->get_get('month');
		}
		$this->templ->smarty->assign( 'Common_year',$common_year );
		$this->templ->smarty->assign( 'Common_month',$common_month );
		if($this->req->get_get('mode') == 'input'){//新規追加ボタンを押すとGETでmodeがわたってくる
			$_SESSION["exception_date_list"] = array();
		}
		/*
		if($this->req->get('week_flg')){
			$this->templ->smarty->assign("week_flg",$this->req->get('week_flg'));
		}else{
			$this->templ->smarty->assign("week_flg",0);
		}
		*/
		$common_year = date("Y");
		if($this->req->get('year')){
			$common_year = $this->req->get('year');
		}
		if($this->req->get_get('year')){
			$common_year = $this->req->get_get('year');
		}
		$common_month = date("m");
		if($this->req->get('month')){
			$common_month = $this->req->get('month');
		}
		if($this->req->get_get('month')){
			$common_month = $this->req->get_get('month');
		}
		$this->templ->smarty->assign( 'Common_year',$common_year );
		$this->templ->smarty->assign( 'Common_month',$common_month );
		if($this->req->get('type') == "date_delete"){
			$key = array_search($this->req->get('delete_date'),$_SESSION["exception_date_list"]);
			if($key !== FALSE){
				unset($_SESSION["exception_date_list"][$key]);
			}
		}
		if($this->req->get('type') == "date_add"){
			list($year,$month,$day) = explode('-',$this->req->get('add_date'));
			$add_date = date("Y-m-d",mktime(0,0,0, $month,$day,$year));

			$start_date = $start_year."-".$start_month."-".$start_day;
			$end_date = $end_year."-".$end_month."-".$end_day;
			//試乗開始日終了日内ならば
			if( $add_date >= $start_date && $add_date <= $end_date){
	        	//定休日でない、土日でない、祝日でない
				//if($this->check_disp_ok_date($add_date,$this->req->get('week_flg'),$_SESSION['search']['shop'])){
				if($this->check_disp_ok_date($add_date,0,$_SESSION['search']['shop'])){
					//掲載不可でなければ
					if(!$_SESSION["exception_date_list"] || !in_array($add_date, $_SESSION["exception_date_list"])){
						$_SESSION["exception_date_list"][] = $this->req->get('add_date');
					}else{
						$this->templ->smarty->assign("error_add_date","貸出不可日以外を選択してください。");
					}
				}else{
	        		$this->templ->smarty->assign("error_add_date","休日以外を選択してください。");
	        	}
        	}else{
        		$this->templ->smarty->assign("error_add_date","試乗期間内を選択してください。");
        	}
		}
		if($_SESSION["exception_date_list"]){
			asort($_SESSION["exception_date_list"]);
			$this->templ->smarty->assign("exception_date_list",$_SESSION["exception_date_list"]);
			$this->templ->smarty->assign("exception_date",$_SESSION["exception_date_list"]);
		}else{
			$this->templ->smarty->assign("exception_date_list",array());
			$this->templ->smarty->assign("exception_date",array());
		}
		$this->calendar_proc();
		$this->templ->smarty->assign("mode","input") ;
		$this->templ->smarty->display("oneday_admin/car_input.html");
		exit;
	}

	// 登録(新規)
	function input_entry_proc(){
		//事業部変更
		if($this->req->get('type') == "station_change"){
			$_SESSION['search']['shop'] = "";
			$_SESSION['search']['car'] = "";
			$this->req->set('shop',"");
			$this->req->set('car',"");
			$this->form_make();
			$this->input_proc();
			return;
		}
		//店舗変更
		if($this->req->get('type') == "shop_change"){
			$_SESSION['search']['car'] = "";
			$this->req->set('car',"");
			$this->form_make();
			$this->input_proc();
			return;
		}
		// データチェック
		$error = $this->datacheck_proc($this->req->get('upd_flg'));
		if(!empty($error)){
			// データセット
			$this->templ->smarty->assign("error",$error);
			$this->input_proc();
			return;
		}
		// データ新規登録
		if($this->req->get('upd_flg') == "1"){
			$this->data_edit_entry_proc();
		}
		else{
			$this->data_input_entry_proc();
		}

		header("Location:car_list.php");
	}

	function start_end_date_check(){
//		$sUnix = mktime(0,0,0,$this->req->get('start_month'),$this->req->get('start_day'),$this->req->get('start_year'));
//		$eUnix = mktime(0,0,0,$this->req->get('end_month'),$this->req->get('end_day'),$this->req->get('end_year'));
//		if($sUnix > $eUnix){
//			$error = "試乗期間(開始)の日付が試乗期間(終了)より後になっています\r\n";
//		}
//		if(!empty($error)){
//			// データセット
//			$this->templ->smarty->assign("error",$error);
//			return false;
//		}
		return true;
	}

	// 詳細確認・変更
	function edit_proc(){
		// 試乗車種データ取得
		if($this->req->get_get('car_detail_id')){
			$data_list = $this->driving_list_proc($this->req->get_get('car_detail_id'));
		}else if($this->req->get('car_detail_id')){
			$data_list = $this->driving_list_proc($this->req->get('car_detail_id'));
		}
		if($this->req->get('type') == 'station_change'){
			$_SESSION['search']['station'] = $this->req->get('station');
		}
		$this->form_make();
		// 区分
		$week_flg_list = $this->util->week_flg_list();
		$week_flg_list_value[0] = 1;
		$week_flg_list_value[1] = 0;
		$disp_flg_list = $this->util->disp_flg2_list();
		// 貸出不可日データ取得
		if($this->req->get_get('car_detail_id')){
			$exception_data_list = $this->exception_proc($this->req->get_get('car_detail_id'));
		}else if($this->req->get('car_detail_id')){
			$exception_data_list = $this->exception_proc($this->req->get('car_detail_id'));
		}
		if(!empty($exception_data_list)){
			foreach($exception_data_list as $key => $exception_data_lists){
				if($this->check_disp_ok_date($exception_data_lists['date'],$data_list['week_flg'],$_SESSION['search']['shop'])){
					$exception_date[] = $exception_data_lists['date'];
				}
			}
		}
		// データセット
		$sql = "select * from shop ";
		$sql .= "where shop_id = '".$this->DB->getQStr($_SESSION['search']['shop'])."' ";
		$sql .= "and disp_flg='1' and del_flg='0'";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$station_name = $rs->fields('station_name');
				$shop_name = $rs->fields('name');
			}
			$rs->Close();
		}
		list($start_year,$start_month,$start_day) = explode('-',$data_list['start_date']);
		list($end_year,$end_month,$end_day) = explode('-',$data_list['end_date']);
		if($this->start_end_date_check()){
			if($this->req->get('start_year')){
				$start_year = $this->req->get('start_year');
			}
			if($this->req->get('start_month')){
				$start_month = sprintf("%02d",$this->req->get('start_month'));
			}
			if($this->req->get('start_day')){
				$start_day = sprintf("%02d",$this->req->get('start_day'));
			}
			if($this->req->get('end_year')){
				$end_year = $this->req->get('end_year');
			}
			if($this->req->get('end_month')){
				$end_month = sprintf("%02d",$this->req->get('end_month'));
			}
			if($this->req->get('end_day')){
				$end_day = sprintf("%02d",$this->req->get('end_day'));
			}
		}
		$start_date = $start_year."-".$start_month."-".$start_day;
		$end_date = $end_year."-".$end_month."-".$end_day;
		if($this->req->get_get('upd_flg')){
			$this->templ->smarty->assign("upd_flg",$this->req->get_get('upd_flg'));
		}else if($this->req->get('upd_flg')){
			$this->templ->smarty->assign("upd_flg",$this->req->get('upd_flg'));
		}
		$this->templ->smarty->assign("car_detail_id",$data_list['car_detail_id']);
		$shop = $data_list['shop'];
		if($this->req->get('shop')){
			$shop = $this->req->get('shop');
		}
		$this->templ->smarty->assign("shop",$shop);
		$this->templ->smarty->assign("station",$data_list['station']);
		$this->templ->smarty->assign("station_name",$station_name);
		$this->templ->smarty->assign("shop_name",$shop_name);
		$this->templ->smarty->assign("start_year",$start_year);
		$this->templ->smarty->assign("start_month",$start_month);
		$this->templ->smarty->assign("start_day",$start_day);
		$this->templ->smarty->assign("end_year",$end_year);
		$this->templ->smarty->assign("end_month",$end_month);
		$this->templ->smarty->assign("end_day",$end_day);
		$this->templ->smarty->assign("car_detail_id",$data_list['car_detail_id']);
		$this->templ->smarty->assign("car_id",$data_list['car_id']);
//		$this->templ->smarty->assign("car_id_list",$car_id_list ) ;
		//車種名称取得(詳細確認用)
		$sql = "select name,name2,name3 from car ";
//		$sql .= "where car_id = '".$this->DB->getQStr($data_list['car_id'])."' ";
		$sql .= "where car1 = '".$this->DB->getQStr($data_list['car1'])."' ";
		$sql .= "and car2 = '".$this->DB->getQStr($data_list['car2'])."' ";
		$sql .= "and del_flg = '0' and disp_flg = '1' ";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			if(!$rs->EOF){
				$car_name = "";
				$car_name = $rs->fields('name');
				if($data_list['car2'] == "ge"){
					$car_name .= " ガソリン";
				}
				else if($data_list['car2'] == "de"){
					$car_name .= " ディーゼル";
				}
				else if($data_list['car2'] == "hev"){
					$car_name .= " ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($data_list['car2'] == 'skyx'){
                    $car_name .= " SKYACTIV-X";
                }
                // add 20201009 e-SKYACTIV G対応
                else if($data_list['car2'] == 'eskyg'){
                    $car_name .= "e-SKYACTIV G";
                }
				if($data_list['car4'] == "mt"){
					$car_name .= " MT";
				}
				$car_name .= " ".$data_list['car3'];
				// add 20190130 turbo対応
                if($data_list['car5'] == "turbo"){
                    $car_name .= " ターボ";
                }
				$this->templ->smarty->assign("car_name",$car_name) ;
//				$this->templ->smarty->assign("car_name",$rs->fields('name')." ".$rs->fields('name2')." ".$rs->fields('name3')) ;
			}
			$rs->Close();
		}
		$this->templ->smarty->assign("disp_flg_list",$disp_flg_list ) ;
		$disp_flg = $data_list['disp_flg'];
		if($this->req->get('disp_flg') == "0" or $this->req->get('disp_flg') == "1"){
			$disp_flg = $this->req->get('disp_flg');
		}
		$this->templ->smarty->assign("disp_flg",$disp_flg);
		//掲載可否取得(詳細確認用)
		$this->templ->smarty->assign("disp_flg_value",$this->util->disp_flg2_list(1,$data_list['disp_flg']));
		$week_flg = 0;
		/*
		if($this->req->get_get('mode') == 'edit' && $this->req->get_get('car_detail_id') && $this->req->get_get('upd_flg')){//変更ボタンを押すとGETでmode,car_detail_id,upd_flgがわたってくる
			$week_flg = $data_list['week_flg'];
			$this->templ->smarty->assign("week_flg",$week_flg);
			
		}else{
			if($this->req->get('week_flg')){//次の月へなどが押された
				$week_flg = $this->req->get('week_flg');
				$this->templ->smarty->assign("week_flg",$week_flg);
			}else{
				$this->templ->smarty->assign("week_flg",0);
			}
		}
		*/
		$this->templ->smarty->assign("week_flg_list",$week_flg_list );
		$this->templ->smarty->assign("week_flg_list_value",$week_flg_list_value);
		//区分取得(詳細確認用)
		$this->templ->smarty->assign("week_flg_value",$this->util->week_flg_list(1,$data_list['week_flg']));
		$this->templ->smarty->assign("start_date",$data_list['start_date']);
		$this->templ->smarty->assign("end_date",$data_list['end_date']);
		if($this->req->get('exception_year')){
			$this->templ->smarty->assign("exception_year",$this->req->get('exception_year'));
		}
		else{
			$this->templ->smarty->assign("exception_year",date("Y"));
		}
		if($this->req->get('exception_month')){
			$this->templ->smarty->assign("exception_month",$this->req->get('exception_month'));
		}
		else{
			$this->templ->smarty->assign("exception_month",date("m"));
		}
		if($this->req->get('exception_day')){
			$this->templ->smarty->assign("exception_day",$this->req->get('exception_day'));
		}
		else{
			$this->templ->smarty->assign("exception_day",date("d"));
		}
		if($this->req->get_get('car_detail_id')){
			$car_model = $data_list['car_model'];
			$grade = $data_list['grade'];
			$color = $data_list['color'];
			$model_code = $data_list['model_code'];
			$model = $data_list['model'];
			$car_no = $data_list['car_no'];
			$no_plate = $data_list['no_plate'];
			//$regist_date = $data_list['regist_date'];
			$regist_year = $data_list['regist_year'];
			$regist_month = $data_list['regist_month'];
			$regist_day = $data_list['regist_day'];
			$regist_ym = $data_list['regist_ym'];
			$price = $data_list['price'];
			$note1 = $data_list['note1'];
			$note2 = $data_list['note2'];
			$option = $data_list['option'];
		}
		else{
			$car_model = $this->req->get('car_model');
			$grade = $this->req->get('grade');
			$color = $this->req->get('color');
			$model_code = $this->req->get('model_code');
			$model = $this->req->get('model');
			$car_no = $this->req->get('car_no');
			$no_plate = $this->req->get('no_plate');
			//$regist_date = $this->req->get('regist_date');
			$regist_year = $this->req->get('regist_year');
			$regist_month = $this->req->get('regist_month');
			$regist_day = $this->req->get('regist_day');
			$regist_ym = $this->req->get('regist_ym');
			$price = $this->req->get('price');
			$note1 = $this->req->get('note1');
			$note2 = $this->req->get('note2');
			$option = $this->req->get('option');
		}
		$this->templ->smarty->assign("car_model",$car_model);
		$this->templ->smarty->assign("grade",$grade);
		$this->templ->smarty->assign("color",$color);
		$this->templ->smarty->assign("model_code",$model_code);
		$this->templ->smarty->assign("model",$model);
		$this->templ->smarty->assign("car_no",$car_no);
		$this->templ->smarty->assign("no_plate",$no_plate);
		//$this->templ->smarty->assign("regist_date",$regist_date);
		$this->templ->smarty->assign("regist_year",$regist_year);
		$this->templ->smarty->assign("regist_month",$regist_month);
		$this->templ->smarty->assign("regist_day",$regist_day);
		$this->templ->smarty->assign("regist_ym",$regist_ym);
		$this->templ->smarty->assign("price",str_replace(",","",$price));
		$this->templ->smarty->assign("pricef",$this->price_proc($price));
		$this->templ->smarty->assign("note1",$note1);
		$this->templ->smarty->assign("note2",$note2);
		$this->templ->smarty->assign("option",$option);
		if($this->req->get_get('mode') == 'edit' && $this->req->get_get('car_detail_id') && $this->req->get_get('upd_flg')){//変更ボタンを押すとGETでmode,car_detail_id,upd_flgがわたってくる
			$_SESSION["exception_date_list"] = array();
			if(!$_SESSION["exception_date_list"]){
				$_SESSION["exception_date_list"] = $exception_date;
			}
		}
		else{
			if(!$this->req->get_get('upd_flg') && !$this->req->get('upd_flg')){
				$_SESSION["exception_date_list"] = array();
			}
		}
		//貸出不可日の設定(定休日以外)
		if($this->req->get('type') == "date_delete"){
			$key = array_search($this->req->get('delete_date'),$_SESSION["exception_date_list"]);
			if($key !== FALSE){
				unset($_SESSION["exception_date_list"][$key]);
			}
		}
		if($this->req->get('type') == "date_add"){
			list($year,$month,$day) = explode('-',$this->req->get('add_date'));
			$add_date = date("Y-m-d",mktime(0,0,0, $month,$day,$year));
			$start_date = $start_year."-".$start_month."-".$start_day;
			$end_date = $end_year."-".$end_month."-".$end_day;
			//試乗開始日終了日内ならば
			if( $add_date >= $start_date && $add_date <= $end_date){
				//定休日でない、土日でない、祝日でない
				if($this->check_disp_ok_date($add_date,$week_flg,$_SESSION['search']['shop'])){
					//予約がない
					if(!$this->reservation_flg_proc($data_list['car_detail_id'],$add_date)){
						//掲載不可でなければ
						if(!$_SESSION["exception_date_list"] || !in_array($add_date, $_SESSION["exception_date_list"])){
							$_SESSION["exception_date_list"][] = $this->req->get('add_date');
						}else{
							$this->templ->smarty->assign("error_add_date","貸出不可日以外を選択してください。");
						}
					}else{
						$this->templ->smarty->assign("error_add_date","予約日以外を選択してください。");
					}
				}else{
	        		$this->templ->smarty->assign("error_add_date","休日以外を選択してください。");
	        	}
	        }else{
        		$this->templ->smarty->assign("error_add_date","試乗期間内を選択してください。");
        	}
		}
		if($_SESSION["exception_date_list"]){
			asort($_SESSION["exception_date_list"]);
			$this->templ->smarty->assign("exception_date_list",$_SESSION["exception_date_list"]);
			$this->templ->smarty->assign("exception_date",$_SESSION["exception_date_list"]);
		}else{
			if(!$this->req->get_get('upd_flg') && !$this->req->get('upd_flg')){
				$this->templ->smarty->assign("exception_date_list",$exception_date);
				$this->templ->smarty->assign("exception_date",$exception_date);
			}
			else{
				$this->templ->smarty->assign("exception_date_list",array());
				$this->templ->smarty->assign("exception_date",array());

			}
		}
		$this->calendar_proc();
		$this->templ->smarty->assign("mode", "edit");

		if (date("d") >= 25) {
			$max_year = date("Y", mktime(0, 0, 0, date("m") + 2, date("d"), date("Y")));
			$max_month = date("m", mktime(0, 0, 0, date("m") + 2, date("d"), date("Y")));
		} else {
			$max_year = date("Y", mktime(0, 0, 0, date("m") + 1, date("d"), date("Y")));
			$max_month = date("m", mktime(0, 0, 0, date("m") + 1, date("d"), date("Y")));
		}

		$this->templ->smarty->assign("max_year", $max_year);
		$this->templ->smarty->assign("max_month", $max_month);

		// 変更
		if ($this->req->get_get('upd_flg') || $this->req->get('upd_flg')) {
			$this->templ->smarty->display("oneday_admin/car_input.html");
		} else {
			$this->templ->smarty->display("oneday_admin/car_conf.html");
		}
		exit;
	}

	function check_disp_ok_date($date, $week_flg, $shop_id)
	{
		list($year, $month, $day) = explode('-', $date);
		$now_w = date("w", mktime(0, 0, 0, $month, $day, $year));
		//平日のみ車種の場合祝祭日を取得
		if ($week_flg == "1") {
			if ($this->util->pday_list(1, $date)) {
				return false;
			}
			if ($now_w == 0 or $now_w == 6) {
				return false;
			}
		}
		if ($this->util->holiday_list($this->DB, $shop_id, 1, $date)) {
			return false;
		}
		return true;
	}

	function reservation_flg_proc($car_detail_id, $date)
	{
		list($year, $month, $day) = explode('-', $date);
		// 日付
		$num = 0;
		// 仮押さえ取得
		$sql = "select COUNT(autono) as num from reservation ";
		$sql .= " where car_detail_id=" . $this->DB->getQStr($car_detail_id);
		$sql .= " and shop_id='" . $this->DB->getQStr($_SESSION['search']['shop']) . "' ";
		$sql .= " and date = '" . $this->DB->getQStr($year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $day)) . "' ";
		$sql .= " and validity_date >= '" . date('Y-m-d H:i:s') . "' ";
		$sql .= " and temporary_flg='1' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs = &$this->DB->ASExecute($sql);
		if ($rs) {
			while (!$rs->EOF) {
				$num = $rs->fields('num');
				$rs->MoveNext();
			}
			$rs->Close();
		}
		// 本登録データ取得
		$sql = "select COUNT(autono) as num from reservation ";
		$sql .= " where car_detail_id=" . $this->DB->getQStr($car_detail_id);
		$sql .= " and shop_id='" . $this->DB->getQStr($_SESSION['search']['shop']) . "' ";
		$sql .= " and date = '" . $this->DB->getQStr($year . '-' . sprintf("%02d", $month) . '-' . sprintf("%02d", $day)) . "' ";
		$sql .= " and temporary_flg='2' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		if ($_SESSION['disp_number']) {
			$sql .= " and disp_number <> '" . $this->DB->getQStr($_SESSION['disp_number']) . "' ";
		}
		$rs = &$this->DB->ASExecute($sql);
		$reservation_flg = false;
		if ($rs) {
			while (!$rs->EOF) {
				$dat = array();
				$dat['num'] = $num + $rs->fields('num');
				if ($dat['num'] >= 1) {
					$reservation_flg = true;
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
		if ($reservation_flg) {
			return true;
		}
		return false;
	}

	function price_proc($price)
	{
		if ($price) {
			$price = str_replace(",", "", $price);
			if (is_numeric($price)) {
				$price = number_format($price);
			}
			return $price;
		}
	}

	// 戻る(編集)
	function edit_back_proc()
	{
		//事業部変更
		if ($this->req->get('type') == "station_change") {
			$_SESSION['search']['shop'] = "";
			$_SESSION['search']['car'] = "";
			$this->req->set('shop', "");
			$this->req->set('car', "");
		}
		//店舗変更
		if ($this->req->get('type') == "shop_change") {
			$_SESSION['search']['car'] = "";
			$this->req->set('car', "");
		}
		$this->form_make();
		// フォーム作成(車種)
		$car_id_list = $this->car_proc($this->req->get('shop_id'));
		// 年月日
		$year_list = $this->util->year_list();
		$month_list = $this->util->month_list();
		$day_list = $this->util->day_list();
		$year_list2 = $this->util->year_list2();
		$month_list2 = $this->util->month_list2();
		$day_list2 = $this->util->day_list2();
		// 区分
		$week_flg_list = $this->util->week_flg_list();
		$week_flg_list_value[0] = 1;
		$week_flg_list_value[1] = 0;
		//print_r($week_flg_list);
		$disp_flg_list = $this->util->disp_flg2_list();
		// データセット
		$this->templ->smarty->assign("upd_flg", $this->req->get('upd_flg'));
		$this->templ->smarty->assign("car_detail_id", $this->req->get('car_detail_id'));
		$this->templ->smarty->assign("autono", $this->req->get('autono'));
		$this->templ->smarty->assign("shop", $this->req->get('shop'));
		$this->templ->smarty->assign("station_name", $this->req->get('station_name'));
		//		$this->templ->smarty->assign("station_name_list",$station_name_list);
		//		$this->templ->smarty->assign("shop_id_list",$shop_id_list);
		$this->templ->smarty->assign("start_year", $this->req->get('start_year'));
		$this->templ->smarty->assign("start_year_list", $year_list);
		$this->templ->smarty->assign("start_month", $this->req->get('start_month'));
		$this->templ->smarty->assign("start_month_list", $month_list);
		$this->templ->smarty->assign("start_day", $this->req->get('start_day'));
		$this->templ->smarty->assign("start_day_list", $day_list);
		$this->templ->smarty->assign("end_year", $this->req->get('end_year'));
		$this->templ->smarty->assign("end_year_list", $year_list);
		$this->templ->smarty->assign("end_month", $this->req->get('end_month'));
		$this->templ->smarty->assign("end_month_list", $month_list);
		$this->templ->smarty->assign("end_day", $this->req->get('end_day'));
		$this->templ->smarty->assign("end_day_list", $day_list);
		$this->templ->smarty->assign("car_id", $this->req->get('car_id'));
		$this->templ->smarty->assign("car_id_list", $car_id_list);
		$this->templ->smarty->assign("start_date", $this->req->get('start_date'));
		$this->templ->smarty->assign("end_date", $this->req->get('end_date'));
		$this->templ->smarty->assign("week_flg", $this->req->get('week_flg'));
		$this->templ->smarty->assign("week_flg_list", $week_flg_list);
		$this->templ->smarty->assign("week_flg_list_value", $week_flg_list_value);
		$this->templ->smarty->assign("body_number", $this->req->get('body_number'));
		$this->templ->smarty->assign("registration_number", $this->req->get('registration_number'));
		$this->templ->smarty->assign("car_information", $this->req->get('car_information'));
		$this->templ->smarty->assign("exception_year_list", $year_list2);
		$this->templ->smarty->assign("exception_month_list", $month_list2);
		$this->templ->smarty->assign("exception_day_list", $day_list2);
		$this->templ->smarty->assign("exception_date", $this->req->get('exception_date'));
		$this->templ->smarty->display("oneday_admin/car_input.html");
		exit;
	}

	// 登録(編集)
	function edit_entry_proc()
	{
		//事業部変更
		if ($this->req->get('type') == "station_change") {
			$_SESSION['search']['shop'] = "";
			$_SESSION['search']['car'] = "";
			$this->req->set('shop', "");
			$this->req->set('car', "");
		}
		//店舗変更
		if ($this->req->get('type') == "shop_change") {
			$_SESSION['search']['car'] = "";
			$this->req->set('car', "");
		}
		$this->form_make();
		// 貸出不可日追加
		if ($this->req->get('type') == "date_add") {
			// 貸出不可日データチェック
			$error = $this->datacheck_exception_proc();
			if (!empty($error)) {
				// データセット
				$this->templ->smarty->assign("error", $error);
				$this->templ->smarty->assign("exception_year", $this->req->get('exception_year'));
				$this->templ->smarty->assign("exception_month", $this->req->get('exception_month'));
				$this->templ->smarty->assign("exception_day", $this->req->get('exception_day'));
				$this->edit_back_proc();
				return;
			}
			$this->req->params['exception_date'][] = $this->req->get('exception_year') . "-" . $this->req->get('exception_month') . "-" . $this->req->get('exception_day');
			$this->edit_back_proc();
			return;
		}
		// 貸出不可日削除
		if ($this->req->get('type') == "date_delete") {
			$exception_date = $this->req->params['exception_date'];
			$list = null;
			foreach ($exception_date as $key => $exception_dates) {
				if ($this->req->get('delete_date') == $exception_dates) {
					continue;
				}
				$list[] = $exception_dates;
			}
			$this->req->params['exception_date'] = $list;
			$this->edit_back_proc();
			return;
		}
		// データチェック
		$error = $this->datacheck_proc($this->req->get('upd_flg'));
		if (!empty($error)) {
			// データセット
			$this->templ->smarty->assign("error", $error);
			$this->edit_back_proc();
			return;
		}
		// データ編集登録
		$this->data_edit_entry_proc();
		header("Location:car_list.php");
	}

	// 一覧
	function default_proc()
	{
		if ($this->req->get('type') == 'station_change') {
			$_SESSION['search']['station'] = $this->req->get('station');
			unset($_SESSION['search']['shop']);
		} else if ($this->req->get('type') == 'shop_change') {
			$_SESSION['search']['shop'] = $this->req->get('shop');
		} else {
			unset($_SESSION['search']);
		}
		$this->form_make();
		// 検索条件作成
		$where = "";
		if ($_SESSION['search']['shop']) {
			$where .= " and sh.shop_id='" . $this->DB->getQStr($_SESSION['search']['shop']) . "'";
		} else if ($_SESSION['search']['station']) {
			$where .= " and sh.station_code='" . $this->DB->getQStr($_SESSION['search']['station']) . "'";
		}
		// 件数取得 upd 20190130 turbo対応
		$sql = "SELECT dl.autono as car_detail_id,dl.car_no as car_no,dl.no_plate as no_plate,dl.start_date as start_date,dl.end_date as end_date,sh.name as shop_name,sh.station_name as station_name,ca.name as car_name1,";
		$sql .= "ca.name2 as car_name2,ca.name3 as car_name3,dl.disp_flg as disp_flg,dl.car1 as car1,dl.car2 as car2,dl.car3 as car3,dl.car4 as car4,dl.car5 as car5 FROM car_detail as dl ";
		$sql .= "INNER JOIN shop as sh ON dl.shop_id = sh.shop_id ";
		$sql .= "INNER JOIN car as ca ON dl.car_id = ca.car_id ";
		$sql .= "where ";
		$sql .= "dl.del_flg = '0' ";
		$sql .= "and sh.disp_flg = '1' and sh.del_flg = '0' ";
		$sql .= "and ca.disp_flg = '1' and ca.del_flg = '0' ";
		$sql .= $where;
		$sql .= " order by sh.station_code,sh.shop_id,dl.end_date desc,dl.car1,dl.car2,dl.car3,dl.car4,dl.car5";
		$rs = &$this->DB->ASExecute($sql);
		$shop_name = "";
		$station_name = "";
		$data_list = array();
		if ($rs) {
			while (!$rs->EOF) {
				$dat = array();
				if ($rs->fields('end_date') <= date("Y-m-d")) {
					$dat['end_class'] = "sizyoA";
				} else {
					$dat['end_class'] = "";
				}
				$dat['car_detail_id'] = $rs->fields('car_detail_id');
				$dat['station_name'] = $rs->fields('station_name');
				$dat['shop_name'] = $rs->fields('shop_name');
				if ($_SESSION['search']['shop']) {
					$shop_name = $rs->fields('shop_name');
				}
				if ($_SESSION['search']['station']) {
					$station_name = $rs->fields('station_name');
				}
				$dat['car_no'] = $rs->fields('car_no');
				$dat['no_plate'] = $rs->fields('no_plate');
				$dat['start_date'] = $rs->fields('start_date');
				$dat['end_date'] = $rs->fields('end_date');
				$dat['name'] = $rs->fields('car_name1');
				if ($rs->fields('car2') == "ge") {
					$dat['name2'] = "ガソリン";
				} else if ($rs->fields('car2') == "de") {
					$dat['name2'] = "ディーゼル";
				} else if ($rs->fields('car2') == "hev") {
					$dat['name2'] = "ハイブリッド";
				}
                // add 20200109 SKYACTIV-X対応
                else if($rs->fields('car2') == 'skyx'){
                    $dat['name2'] = "SKYACTIV-X";
                }
                // add 20201009 e-SKYACTIV G対応
                else if($rs->fields('car2') == 'eskyg'){
                    $dat['name2'] = "e-SKYACTIV G";
                }
//				$dat['name2'] = $rs->fields('car_name2');
				$dat['name3'] = "";
				if ($rs->fields('car4') == "mt") {
					$dat['name3'] = "MT ";
				}
				$dat['name3'] .= $rs->fields('car3');
				// add 20190130 turbo対応
				if ($rs->fields('car5') == "turbo") {
					$dat['name3'] .= " ターボ";
				}
				$dat['disp_flg_value'] = $this->util->disp_flg2_list(1, $rs->fields('disp_flg'));
				$data_list[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		$this->templ->smarty->assign('shop_name', $shop_name);
		$this->templ->smarty->assign('station_name', $station_name);
		//		$this->templ->smarty->assign("station_name_list",$station_name_list ) ;
		//		$this->templ->smarty->assign("shop_id_list",$shop_id_list ) ;
		$this->templ->smarty->assign("data_list", $data_list);
		//		$this->templ->smarty->assign("search_station_name",$search_station_name ) ;
		//		$this->templ->smarty->assign("search_shop_name",$search_shop_name ) ;
		$this->templ->smarty->display("oneday_admin/car_list.html");
		exit;
	}

	// 削除
	function delete_proc()
	{
		if ($this->req->get_get('car_detail_id') == null) {
			$this->templ->smarty->assign("error", "不正な画面移動です");
			$this->default_proc();
			return;
		}
		// 試乗車種データ取得
		$data_list = $this->driving_list_proc($this->req->get_get('car_detail_id'));
		if (empty($data_list['car_detail_id']) or empty($data_list['start_date']) or empty($data_list['end_date']) or empty($data_list['shop'])) {
			$this->templ->smarty->assign("error", "不正な画面移動です");
			$this->default_proc();
			return;
		}
		// 予約データチェック
		$error = $this->datacheck_reservation_proc(2, $data_list['start_date'], $data_list['end_date'], $data_list['shop'], $data_list['car_detail_id']);
		if (!empty($error)) {
			$this->templ->smarty->assign("error", $error);
			$this->default_proc();
			return;
		}
		// 削除登録
		$this->data_delete_entry_proc($data_list['car_detail_id'], $data_list['shop'], $data_list['start_date'], $data_list['end_date']);
		header("Location:car_list.php");
	}

	// カレンダー画面作成　貸出不可日の設定(定休日以外)
	function calendar_proc()
	{
		if ($_SESSION['autono']) {
			$ret = $this->util->del_reservation($this->DB, $_SESSION['autono']);
		}
		if ($this->req->get('year')) {
			$this->req->get_set('year', $this->req->get('year'));
		}
		if ($this->req->get('month')) {
			$this->req->get_set('month', $this->req->get('month'));
		}
		if ($this->req->get('car_detail_id')) {
			$this->req->get_set('car_detail_id', $this->req->get('car_detail_id'));
		}
		if ($this->req->get('shop_id')) {
			$this->req->get_set('shop_id', $this->req->get('shop_id'));
		}
		if ($this->req->get('area_id')) {
			$this->req->get_set('area_id', $this->req->get('area_id'));
		}
		if ($this->req->get('pref_id')) {
			$this->req->get_set('pref_id', $this->req->get('pref_id'));
		}
		$_SESSION['autono'] = "";
		if (!$this->req->get_get('year')) {
			$this->req->get_set('year', date("Y"));
		}
		if (!$this->req->get_get('month')) {
			$this->req->get_set('month', date("m"));
		}
		if ($this->req->get_get('car_detail_id')) {
			/*
			// 在庫取得
			$sql = "select * from zaiko ";
			$sql .= " where car_detail_id=".$this->DB->getQStr($this->req->get_get('car_detail_id'));
			$sql .= " and shop_id='".$this->DB->getQStr($_SESSION['search']['shop'])."' ";
			$sql .= " and date >= '".$this->DB->getQStr($this->req->get_get('year').'-'.$this->req->get_get('month').'-01')."' ";
			$sql .= " and date < '".$this->DB->getQStr(date("Y-m-d",mktime(0,0,0,$this->req->get_get('month') + 1,1,$this->req->get_get('year'))))."' ";
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs =& $this->DB->ASExecute($sql);
			$data_list = array();
			if($rs){
				while(!$rs->EOF){
					$dat = array();
					$dat['shop_id'] = $rs->fields('shop_id');
					$dat['car_detail_id'] = $rs->fields('car_detail_id');
					$dat['date'] = $rs->fields('date');
					$dat['num'] = $rs->fields('num');
					$dat['area_id'] = $this->req->get_get('area_id');
					$dat['pref_id'] = $this->req->get_get('pref_id');
					$data_list[] = $dat;
					$rs->MoveNext();
				}
				$rs->Close();
			}
*/
			// カレンダー作成
			if ($this->req->get_get('year')) {
				$year = $this->req->get_get('year');
			} else {
				$year = date('Y');
			}
			if ($this->req->get_get('month')) {
				$month = $this->req->get_get('month');
			} else {
				$month = date('m');
			}
			$this->cl->Year = $year;
			$this->cl->Month = $month;
			// 車種
			$sql = "select car_id,week_flg from car_detail ";
			$sql .= " where autono=" . $this->DB->getQStr($this->req->get_get('car_detail_id'));
			$sql .= " and disp_flg='1' ";
			$sql .= " and del_flg='0' ";
			$rs2 = &$this->DB->ASExecute($sql);
			$week_flg = 0;
			$data_list2 = array();
			if ($rs2) {
				if (!$rs2->EOF) {
					$sql = "select name,name2,name3 from car ";
					$sql .= " where car_id='" . $this->DB->getQStr($rs2->fields('car_id')) . "' ";
					$sql .= " and disp_flg='1' ";
					$sql .= " and del_flg='0' ";
					$rs = &$this->DB->ASExecute($sql);
					if ($rs) {
						while (!$rs->EOF) {
							$dat = array();
							$dat['name'] = $rs->fields('name') . " " . $rs->fields('name2') . " " . $rs->fields('name3');
							$dat['week_flg'] = $rs2->fields('week_flg');
							$week_flg =  $rs2->fields('week_flg');
							$data_list2[] = $dat;
							$rs->MoveNext();
						}
						$rs->Close();
					}
				}
				$rs2->Close();
			}
			// 試乗車種データ取得
			$car_data_list = $this->driving_list_proc($this->req->get_get('car_detail_id'));
			//print_r($car_data_list);
			//			list($start_year,$start_month,$start_day) = explode('-',$car_data_list['start_date']);
			//			list($end_year,$end_month,$end_day) = explode('-',$car_data_list['end_date']);
			//echo $car_data_list[0]['start_date'].":".$car_data_list[0]['end_date']."\r\n";
			//echo $start_year."/".$start_month."/".$start_day."\r\n";
			//echo $end_year."/".$end_month."/".$end_day."\r\n";
			//			if($this->start_end_date_check()){
			//				if($this->req->get('start_year')){
			//					$start_year = $this->req->get('start_year');
			//				}
			//				if($this->req->get('start_month')){
			//					$start_month = sprintf("%02d",$this->req->get('start_month'));
			//				}
			//				if($this->req->get('start_day')){
			//					$start_day = sprintf("%02d",$this->req->get('start_day'));
			//				}

			//				if($this->req->get('end_year')){
			//					$end_year = $this->req->get('end_year');
			//				}
			//				if($this->req->get('end_month')){
			//					$end_month = sprintf("%02d",$this->req->get('end_month'));
			//				}
			//				if($this->req->get('end_day')){
			//					$end_day = sprintf("%02d",$this->req->get('end_day'));
			//				}
			//			}
			//			$start_date = $start_year."-".$start_month."-".$start_day;
			//			$end_date = $end_year."-".$end_month."-".$end_day;
			//			$start_end_day = array($start_date,$end_date);
			//print_r($start_end_day);
			//			$this->cl->Data($this->req->get_get('car_detail_id'),$_SESSION['search']['shop'],$data_list,$this->DB,$week_flg,$start_end_day);
			$id[] = $this->req->get_get('car_detail_id');
			//if($_SERVER['REMOTE_ADDR'] == "121.84.137.51"){
			//print_r($id);
			//echo $_SESSION['search']['shop']."<br>";
			//}
			$this->cl->Data($id, $_SESSION['search']['shop'], $this->DB);
		} else {
			//			$start_year = '2013';
			//			$start_month = '12';
			//			$start_day = '04';
			//			$end_year = '2014';
			//			$end_month = '05';
			//			$end_day = '31';
			//			if($this->start_end_date_check()){
			//				if($this->req->get('start_year')){
			//					$start_year = $this->req->get('start_year');
			//				}
			//				if($this->req->get('start_month')){
			//					$start_month = sprintf("%02d",$this->req->get('start_month'));
			//				}
			//				if($this->req->get('start_day')){
			//					$start_day = sprintf("%02d",$this->req->get('start_day'));
			//				}
			//				if($this->req->get('end_year')){
			//					$end_year = $this->req->get('end_year');
			//				}
			//				if($this->req->get('end_month')){
			//					$end_month = sprintf("%02d",$this->req->get('end_month'));
			//				}
			//				if($this->req->get('end_day')){
			//					$end_day = sprintf("%02d",$this->req->get('end_day'));
			//				}
			//			}
			//			$start_date = $start_year."-".$start_month."-".$start_day;
			//			$end_date = $end_year."-".$end_month."-".$end_day;
			// カレンダー作成
			if ($this->req->get_get('year')) {
				$year = $this->req->get_get('year');
			} else {
				$year = date('Y');
			}
			if ($this->req->get_get('month')) {
				$month = $this->req->get_get('month');
			} else {
				$month = date('m');
			}
			$this->cl->Year = $year;
			$this->cl->Month = $month;
			$data_list = array();
			/*
			$date = $start_date;
			while($date <= $end_date){
				$dat = array();
				$dat['shop_id'] = $_SESSION['search']['shop'];
				$dat['car_detail_id'] = "";
				$dat['date'] = $date;
				$dat['num'] = 1;
				$dat['area_id'] = $this->req->get_get('area_id');
				$dat['pref_id'] = $this->req->get_get('pref_id');
				$data_list[] = $dat;
				$date = date("Y-m-d", strtotime($date." +1 day"));
			}
			$start_end_day = array($start_date,$end_date);
			$week_flg = 0;
			if($this->req->get('week_flg')){
				$week_flg = $this->req->get('week_flg');
			}
			if($this->req->get_get('week_flg')){
				$week_flg = $this->req->get_get('week_flg');
			}
*/
			//			$this->cl->Data("",$_SESSION['search']['shop'],$data_list,$this->DB,$week_flg,$start_end_day);
			$this->cl->Data("", $_SESSION['search']['shop'], $this->DB);
		}
		// 店舗
		$sql = "select name from shop ";
		$sql .= " where shop_id='" . $this->DB->getQStr($_SESSION['search']['shop']) . "' ";
		$sql .= " and disp_flg='1' ";
		$sql .= " and del_flg='0' ";
		$rs = &$this->DB->ASExecute($sql);
		$data_list3 = array();
		if ($rs) {
			while (!$rs->EOF) {
				$dat = array();
				$dat['name'] = $rs->fields('name');
				$data_list3[] = $dat;
				$rs->MoveNext();
			}
			$rs->Close();
		}
		// 表示
		$this->templ->smarty->assign('Common_year', $this->cl->Year);
		$this->templ->smarty->assign('Common_month', $this->cl->Month);
		$this->templ->smarty->assign('Common_day', $this->cl->Day);
		$this->templ->smarty->assign('Common_day_format', $this->cl->DayFormat);
		$this->templ->smarty->assign('Common_link', $this->cl->Link);
		$this->templ->smarty->assign('Common_Num', $this->cl->Num);
		//print_r($this->cl->Num);
		$this->templ->smarty->assign('Common_next_year', $this->cl->Next_Year);
		$this->templ->smarty->assign('Common_next_month', $this->cl->Next_Month);
		$this->templ->smarty->assign('Common_back_year', $this->cl->Back_Year);
		$this->templ->smarty->assign('Common_back_month', $this->cl->Back_Month);
		$cal_date = date("Y-m-d", mktime(0, 0, 0, $this->req->get_get('month'), 1, $this->req->get_get('year')));
		$cal_date_month = date("n", mktime(0, 0, 0, $this->req->get_get('month'), 1, $this->req->get_get('year')));
		$start_day = date("n月j日", mktime(0, 0, 0, $this->req->get_get('month') - 1, 1, $this->req->get_get('year')));
		$ok_date = date("Y-m-d", mktime(0, 0, 0, date("m") + 2, 1, date("Y")));
		$ok_date_month = date("n", mktime(0, 0, 0, date("m") + 2, 1, date("Y")));
	}
}