<?php
include_once("../mc_apl/top.php");

$form_class = new form_class();
$form_class->execute();
exit;

class form_class{
	var $req;
	var $mode;
	var $templ;
	var $DB;
	var $util;
	function form_class(){
		$this->templ = new smTemplate();
		$this->req = new reqData();
		$this->mode = $_REQUEST['mode'];
		$this->DB = new ASDB();
        $this->DB2 = new ASDB_MAIN();
		$this->util = new util();
	}

	function execute(){
		switch($this->mode){
			case 'check':
				$this->check_proc();
			break;
			default:
				$this->default_proc();
			break;
		}
	}

	function default_proc(){
        if($this->req->get_get('flyer')){
            if(!$_SESSION['flyer']) {
                $_SESSION['flyer'] = $this->req->get_get('flyer');
                $this->templ->smarty->assign("flyer",$this->req->get_get('flyer'));
            }
        }
        if($this->req->get_get('p')){
            if(!$_SESSION['param2']) {
                $_SESSION['param2'] = $this->req->get_get('p');
                $this->templ->smarty->assign("p",$this->req->get_get('p'));
            }
        }
        $this->form_make();
        $this->assign_holiday_list();
		$this->data_get();
//		$this->templ->smarty->assign("c",$this->req->get_get('c'));
//		$this->templ->smarty->assign("s",$this->req->get_get('s'));
//		$this->templ->smarty->assign("pref_id",$this->req->get_get('pref_id'));
//		$this->templ->smarty->assign("area_id",$this->req->get_get('area_id'));
//		$this->templ->smarty->assign("shop_id",$this->req->get_get('shop_id'));
		$this->templ->smarty->display("step1.html");
		exit;
	}

    function check_proc(){
	    $this->assign_proc();
        $this->form_make();
        $this->assign_holiday_list();
        $this->data_get();
        $error = $this->error_proc();
        if($error){
            $this->templ->smarty->assign("error",$error);
            $this->templ->smarty->display("step1.html");
            exit;
        }
        $this->templ->smarty->assign("next_flg",1);
        $this->templ->smarty->display("step1.html");
        exit;
    }

    function assign_proc(){
		$this->templ->smarty->assign("car_name",$this->req->get('car_name'));
        $this->templ->smarty->assign("car1",$this->req->get('car1'));
        $this->templ->smarty->assign("car2",$this->req->get('car2'));
        $this->templ->smarty->assign("car3",$this->req->get('car3'));
        $this->templ->smarty->assign("car4",$this->req->get('car4'));
        $this->templ->smarty->assign("car5",$this->req->get('car5'));
        $this->templ->smarty->assign("car_detail_id",$this->req->get('car_detail_id'));
        $this->templ->smarty->assign("shop_id",$this->req->get('shop_id'));
        $this->templ->smarty->assign("date_first",$this->req->get('date_first'));
        $this->templ->smarty->assign("first_time",$this->req->get('first_time'));
    }

    function error_proc(){
	    $error = NULL;
	    if(!$this->req->get('car1') and !$this->req->get('car2')){
	        $error = "試乗いただく車種を選択してください。\r\n";
        }
	    if(!$this->req->get('shop_id')){
	        $error .= "ご来店いただく店舗を選択してください。\r\n";
        }
	    else{
	        $sql = "select * from shop ";
	        $sql .= "where driving_shop_id = '".$this->DB2->getQStr($this->req->get('shop_id'))."' ";
	        $sql .= "and disp_flg = '1' and del_flg = '0'";
            $rs =& $this->DB2->ASExecute($sql);
            $find_flg = false;
            if($rs){
                if(!$rs->EOF){
                    $find_flg = true;
                }
                $rs->Close();
            }
            if(!$find_flg){
                $error .= "ご来店いただく店舗を正しく選択してください。\r\n";
            }
        }
	    if(!$this->req->get('date_first')){
            $error .= "ご来店いただく日付を選択してください。\r\n";
        }
	    else{
	        $y = date("Y",strtotime($this->req->get('date_first')));
            $m = date("m",strtotime($this->req->get('date_first')));
            $d = date("d",strtotime($this->req->get('date_first')));
            if(!checkdate($m,$d,$y)){
                $error .= "ご来店いただく日付を正しく選択してください。\r\n";
            }
        }
	    if(!$this->req->get('first_time')){
            $error .= "ご来店いただく時間を選択してください。\r\n";
        }
	    elseif($this->req->get('first_time') < "10:00" or $this->req->get('first_time') > "19:30"){
            $error .= "ご来店いただく時間を正しく選択してください。\r\n";
        }
	    return $error;
    }

	function data_get(){
		$data_list = array();
		// add 20190304 HPのMY店舗からの予約
		if($this->req->get_get('uid')){
            $_SESSION['member']['uid'] = $this->req->get_get('uid');
		    $sql = "select fav_shop_id from car_model_info ";
            $sql .= "where driving_uid = '".$this->DB2->getQStr($this->req->get_get('uid'))."' ";
            $rs =& $this->DB2->ASExecute($sql);
            if($rs){
                if(!$rs->EOF){
                    $driving_shop_id = NULL;
                    if($rs->fields('fav_shop_id')){
                        $sql2 = "select driving_shop_id from shop ";
                        $sql2 .= "where shop_id = '".$this->DB2->getQStr($rs->fields('fav_shop_id'))."' ";
                        $rs2 =& $this->DB2->ASExecute($sql2);
                        if($rs2){
                            if(!$rs2->EOF){
                                $driving_shop_id = $rs2->fields('driving_shop_id');
                            }
                            $rs2->Close();
                        }
                    }
                    $this->req->get_set('shop_id',$driving_shop_id);
                }
                $rs->Close();
            }
        }
		// 店舗から選択の場合
//		if($this->req->get_get('shop_id')){
//			$this->templ->smarty->assign("shop_flg",1);
//			$sql = "select * from shop ";
//			$sql .= " where shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."' ";
//			$rs =& $this->DB->ASExecute($sql);
//			if($rs){
//				if(!$rs->EOF){
//					$this->templ->smarty->assign("name",$rs->fields('name'));
//				}
//				$rs->Close();
//			}
//		}
//		else{
//			$this->templ->smarty->assign("car_flg",1);
//		}
		$car3_list = array();
		$i = 0;
		$car3 = "";
		$car12 = "";
		$sql = "select * from car";
		$sql .= " where disp_flg='1'";
		$sql .= " and del_flg='0'";
        $sql .= " order by order_no";
//		$sql .= " order by car1,car2,car4,car3";
		$rs =& $this->DB->ASExecute($sql);
		if($rs){
			while(!$rs->EOF){
				$sql2 = "select * from car_detail";
				$sql2 .= " where end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
				$sql2 .= " and car_id = '".$this->DB->getQStr($rs->fields('car_id'))."'";
				if($this->req->get_get('shop_id')){
					$sql2 .= " and shop_id = '".$this->DB->getQStr($this->req->get_get('shop_id'))."'";
				}
				elseif($this->req->get('shop_id')){
                    $sql2 .= " and shop_id = '".$this->DB->getQStr($this->req->get('shop_id'))."'";
                }
				$sql2 .= " and disp_flg='1'";
				$sql2 .= " and del_flg='0'";
				$sql2 .= " order by car1,car2,car3,car4";
//				$sql2 .= " group by car3,car4";
//if($this->req->get_get('shop_id') == '921' and $rs->fields('car_id') == "car00008"){
//if($rs->fields('car_id') == "car00029"){
//echo $sql2."\r\n";
//}
				$rs2 =& $this->DB->ASExecute($sql2);
				if($rs2){
					while(!$rs2->EOF){
						$dat = array();
						$comp_car12 = $rs2->fields('car1').$rs2->fields('car2');
						// upd 20190130 turbo追加対応
						$comp_car = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
//if($rs2->fields('shop_id') == '921' and $rs->fields('car_id') == "car00008"){
//	echo $car12.":".$comp_car12."\r\n";
//	echo $car3.":".$comp_car."\r\n";
//}
						if($car12 != $comp_car12){
							$car12 = $rs2->fields('car1').$rs2->fields('car2');
							// upd 20190130 turbo追加対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo追加対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						else if($car3 != $comp_car){
							// upd 20190130 turbo追加対応
							$car3 = $rs2->fields('car3').$rs2->fields('car4').$rs2->fields('car5');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car3'] = $rs2->fields('car3');
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car4'] = $rs2->fields('car4');
							// add 20190130 turbo追加対応
							$car3_list[$rs->fields('car1').$rs->fields('car2')][$i]['car5'] = $rs2->fields('car5');
							$i++;
						}
						if($data_list[$rs->fields('car1')]){
							$data_list[$rs->fields('car1')][$rs->fields('car2')] = $rs->fields('car2');
//							$data_list[$rs->fields('car1')][$rs->fields('car2')][$rs2->fields('car3')] = $rs2->fields('car3');
						}
						else{
							$dat['car1'] = $rs->fields('car1');
							$dat[$rs->fields('car2')] = $rs->fields('car2');
//							$dat[$rs->fields('car2')][$rs2->fields('car3')] = $rs2->fields('car3');
							$dat['name'] = $rs->fields('name');
							$dat['car_image'] = $this->util->car_image(0,1,$rs->fields('car_id'));
							$data_list[$rs->fields('car1')] = $dat;
						}
						$rs2->MoveNext();
					}
					$rs2->Close();
				}
				$rs->MoveNext();
			}
			$rs->Close();
		}
//print_r($data_list);
//print_r($car3_list);
		$this->templ->smarty->assign("data_list",$data_list);
		$this->templ->smarty->assign("car3_list",$car3_list);
		$car_name = NULL;
		if($this->req->get_get('car1')) {
            $this->templ->smarty->assign("car1", $this->req->get_get('car1'));
        }
        if($this->req->get_get('car2')) {
            $this->templ->smarty->assign("car2", $this->req->get_get('car2'));
            $sql = "select * from car";
            $sql .= " where disp_flg='1'";
            $sql .= " and del_flg='0'";
            $sql .= " and car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
            $rs =& $this->DB->ASExecute($sql);
            if($rs){
                if(!$rs->EOF){
                    $car_name = $rs->fields('name');
                    if($this->req->get_get('car2') == "de"){
                        $car_name .= " ディーゼル";
                    }
                    elseif($this->req->get_get('car2') == "ge"){
                        $car_name .= " ガソリン";
                    }
                    elseif($this->req->get_get('car2') == "hev"){
                        $car_name .= " ハイブリッド";
                    }
                    // add 20200109 SKYACTIV-X対応
                    else if($this->req->get_get('car2') == 'skyx'){
                        $car_name .= " SKYACTIV-X";
                    }
                    // add 20201009 e-SKYACTIV G対応
                    else if($this->req->get_get('car2') == 'eskyg'){
                        $car_name .= " e-SKYACTIV G";
                    }
                    if($this->req->get_get('car3')){
                        $car_name .= " ".$this->req->get_get('car3');
                    }
                    if($this->req->get_get('car4') == "MT"){
                        $car_name .= " MT";
                    }
                    if($this->req->get_get('car5') == "turbo"){
                        $car_name .= " ターボ";
                    }
                }
                $rs->Close();
            }
        }
        if($car_name){
            $this->templ->smarty->assign("car_name", $car_name);
        }
        if($this->req->get_get('car3')) {
            $this->templ->smarty->assign("car3", $this->req->get_get('car3'));
        }
        if($this->req->get_get('car4')) {
            $this->templ->smarty->assign("car4", $this->req->get_get('car4'));
        }
        if($this->req->get_get('car5')) {
            $this->templ->smarty->assign("car5", $this->req->get_get('car5'));
        }
        if($this->req->get('shop_id')) {
            $this->templ->smarty->assign("shop_id", $this->req->get('shop_id'));
        }
        elseif($this->req->get_get('shop_id')) {
            $this->templ->smarty->assign("shop_id", $this->req->get_get('shop_id'));
        }
        if($this->req->get('date_first')){
            $this->templ->smarty->assign("date_first", $this->req->get('date_first'));
        }
        if($this->req->get('first_time')){
            $this->templ->smarty->assign("first_time", $this->req->get('first_time'));
        }
	}

	function form_make(){
        if($this->req->get_get('car1') and $this->req->get_get('car2')) {
            $shop_list = array();
            $sql = "select s.shop_id as shop_id,s.name as shop_name from car_detail as c,shop as s";
            $sql .= " where s.shop_id = c.shop_id";
            $sql .= " and c.end_date >= '".$this->DB->getQStr(date("Y-m-d"))."'";
            $sql .= " and c.car1 = '".$this->DB->getQStr($this->req->get_get('car1'))."'";
            $sql .= " and c.car2 = '".$this->DB->getQStr($this->req->get_get('car2'))."'";
            if($this->req->get_get('car3')){
                $sql .= " and c.car3 = '".$this->DB->getQStr($this->req->get_get('car3'))."'";
            }
            else{
                $sql .= " and (c.car3 is NULL or c.car3 = '')";
            }
            if($this->req->get_get('car4')){
                $sql .= " and c.car4 = '".$this->DB->getQStr($this->req->get_get('car4'))."'";
            }
            else{
                $sql .= " and (c.car4 is NULL or c.car4 = '')";
            }
            if($this->req->get_get('car5')){
                $sql .= " and c.car5 = '".$this->DB->getQStr($this->req->get_get('car5'))."'";
            }
            else{
                $sql .= " and (c.car5 is NULL or c.car5 = '')";
            }
            $sql .= " and c.disp_flg='1'";
            $sql .= " and c.del_flg='0'";
            $sql .= " and s.disp_flg='1'";
            $sql .= " and s.del_flg='0'";
            $sql .= " order by s.station_code,s.order_no";
            $rs =& $this->DB->ASExecute($sql);
            if($rs) {
                while (!$rs->EOF) {
                    $shop_list[$rs->fields('shop_id')] = $rs->fields('shop_name');
                    $rs->MoveNext();
                }
                $rs->Close();
            }
            $this->templ->smarty->assign("shop_list", $shop_list);
        }
        else {
            $this->templ->smarty->assign("shop_list", $this->util->shop_list_get(0, 0, 0, $this->DB));
        }
//var_dump($shop_list);
    }

    function assign_holiday_list(){
        // datapickerで選択できる日付範囲指定
        if(date("H") < 12){
            $min_date = 2;
        }
        else{
            $min_date = 3;
        }
        $min_y = date("Y");
        $min_m = date("m");
        $min_d = date("d");
        $max_y = date("Y",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_m = date("m",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_d = date("d",mktime(0,0,0,date("m")+2,0,date("Y")));
        $max_date = (strtotime(date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y))) - strtotime(date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y))))/(3600*24);
        $this->templ->smarty->assign('min_date',$min_date."d");
        $this->templ->smarty->assign('max_date',$max_date."d");
        $list = $this->util->holiday_list($this->DB);
        $from_date = date("Y-m-d",mktime(0,0,0,$min_m,$min_d,$min_y));
        $to_date = date("Y-m-d",mktime(0,0,0,$max_m,$max_d,$max_y));
        $holiday_list = array();
        if($list){
            if(is_array($list)){
                foreach($list as $key => $val){
                    if(strtotime($key) >= strtotime($from_date) and strtotime($key) <= strtotime($to_date)){
                        $holiday_list[] = $key;
                    }
                }
            }
        }
        $this->templ->smarty->assign('holiday_list',$holiday_list);
    }
}
?>